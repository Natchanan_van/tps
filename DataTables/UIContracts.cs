﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Collections.Specialized;

namespace Ecobiz.WebUI.DataTables
{
    #region Retrieving Parameters

    public enum SortDirection
    {
        Ascending = 0,
        Descending = 1
    }

    public class TableSort
    {
        public int ColumnNo { get; set; }
        public SortDirection Direction { get; set; }
    }

    public class ServerTableParams
    {
        private readonly int echo;
       

        public int Echo { get { return echo; } }

        /// <summary>
        /// Starting Index of data. Set to -1 If you want to retrieve all data
        /// </summary>
        public int Start { get; set; }

        /// <summary>
        /// Set How many records you want to retireve, -1 for all
        /// </summary>
        public int Count { get; set; }
        public List<TableSort> SortCols { get; set; }
        public string SearchTerm { get; set; }
        public Dictionary<string, object> AdditionalParams { get; set; }

        private ServerTableParams(int echo)
        {
            this.echo = echo;
        }

        public static bool IsParamsFromDataTable(string paramNames)
        {
            if (paramNames == "iDisplayStart") return true;
            if (paramNames == "iDisplayLength") return true;
            if (paramNames == "iColumns") return true;
            if (paramNames == "sSearch") return true;
            if (paramNames == "bRegex") return true;
            if (paramNames == "sEcho") return true;
            if (paramNames == "iSortingCols") return true;

            System.Text.RegularExpressions.Regex myRegEx;

            myRegEx = new System.Text.RegularExpressions.Regex(@"^bSearchable_\d+$");
            if (myRegEx.IsMatch(paramNames)) return true;
            myRegEx = new System.Text.RegularExpressions.Regex(@"^sSearch_\d+$");
            if (myRegEx.IsMatch(paramNames)) return true;
            myRegEx = new System.Text.RegularExpressions.Regex(@"^bRegex_\d+$");
            if (myRegEx.IsMatch(paramNames)) return true;
            myRegEx = new System.Text.RegularExpressions.Regex(@"^bSortable_\d+$");
            if (myRegEx.IsMatch(paramNames)) return true;
            myRegEx = new System.Text.RegularExpressions.Regex(@"^iSortCol_\d+$");
            if (myRegEx.IsMatch(paramNames)) return true;
            myRegEx = new System.Text.RegularExpressions.Regex(@"^sSortDir_\d+$");
            if (myRegEx.IsMatch(paramNames)) return true;
            myRegEx = new System.Text.RegularExpressions.Regex(@"^mDataProp_\d+$");
            if (myRegEx.IsMatch(paramNames)) return true;

            return false;
        }

        public static ServerTableParams RetrieveFromRequest(NameValueCollection reqParams, bool customSearch = false, string customSearchParamsName = null)
        {
            try
            {
                var result = new ServerTableParams(Convert.ToInt32(reqParams["sEcho"]));
                var startString = reqParams["iDisplayStart"];
                var lengthString = reqParams["iDisplayLength"];
                var sortColNo = reqParams["iSortingCols"];

                int start = -1, count = -1;
                int.TryParse(startString, out start);
                int.TryParse(lengthString, out count);
                result.Count = count;
                if (!customSearch || !reqParams.AllKeys.Any(c => c == customSearchParamsName))
                {
                    result.SearchTerm = reqParams["sSearch"];
                }
                else
                {
                    result.SearchTerm = reqParams[customSearchParamsName];
                }

                result.SortCols = new List<TableSort>();

                var sortRegEx=new System.Text.RegularExpressions.Regex(@"^iSortCol_\d+$");
                var sortDirRegEx = new System.Text.RegularExpressions.Regex(@"^sSortDir_\d+$");

                foreach (var existsCols in reqParams.AllKeys.Where(c => sortRegEx.IsMatch(c)))
                {
                    int index = 0;
                    if (int.TryParse(existsCols.Split('_')[1], out index))
                    {
                        result.SortCols.Add(new TableSort()
                        {
                            ColumnNo = Convert.ToInt32(reqParams[existsCols]),
                            Direction = reqParams["sSortDir_" + index.ToString()] == "asc" ? SortDirection.Ascending : SortDirection.Descending
                        });
                    }
                }

                result.Start = start;

                result.AdditionalParams = new Dictionary<string, object>();
                foreach (var anotherParams in reqParams.AllKeys.Where(c => !IsParamsFromDataTable(c)))
                {
                    result.AdditionalParams.Add(anotherParams, reqParams[anotherParams]);
                }

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }

    #endregion

    #region Returning Parameters

    public class TableData<T>
    {
        private int echo;

        public int sEcho { get { return echo; } }
        public int iTotalRecords { get; set; }
        public int iTotalDisplayRecords { get; set; }
        public List<T> aaData { get; set; }

        public static TableData<T> CreateData(int echo, int allRecords, int filterRecords, List<T> Data)
        {
            var result = new TableData<T>();
            result.echo = echo;
            result.iTotalRecords = allRecords;
            result.iTotalDisplayRecords = filterRecords;
            result.aaData = Data;
            return result;
        }
    }

    #endregion
}
