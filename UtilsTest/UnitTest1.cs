﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Text.RegularExpressions;
using Ecobz.Utilities;

namespace UtilsTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestReplace()
        {
            var testString = "Chakrit asdf1234iiiii";
            var capture = Regex.Match(testString, @"[^0-9]*(?<chris>\d+)");
            var result = capture.ReplaceByGroupName("chris", testString, "4567");
            Assert.AreEqual(result, "Chakrit asdf4567iiiii");
        }
    }
}
