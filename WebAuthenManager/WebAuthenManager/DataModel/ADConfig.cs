﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace WebAuthenManager
{
    public enum ADContextType
    {
        // Summary:
        //     The computer store. This represents the SAM store.
        Machine = 0,
        //
        // Summary:
        //     The domain store. This represents the AD DS store.
        Domain = 1,
        //
        // Summary:
        //     The application directory store. This represents the AD LDS store.
        ApplicationDirectory = 2,
    }
    [DataContract]
    public class ADConfig
    {
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public ADContextType ContextType { get; set; }
        [DataMember]
        public string Username { get; set; }
        [DataMember]
        public string Password { get; set; }
    }
}
