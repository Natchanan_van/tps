﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebAuthenManager
{
    [Serializable]
    [System.Runtime.Serialization.DataContract]
    public class ADUser
    {
        public string UserName { get; set; }
        public string Name { get; set; }
        public string SID { get; set; }
    }
}
