﻿using System;
using System.Collections.Generic;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using WebAuthenManager;

namespace WebAuthenManager
{
    public class ADAuthentication
    {
        #region Static Methods
        /// <summary>
        /// Return Current Windows User ID Which can be use as authentication
        /// </summary>
        /// <returns>User SSID, null if error</returns>
        public static WindowsIdentity GetCurrentUserId()
        {
            if (System.Web.HttpContext.Current.User.Identity is WindowsIdentity)
            {
                var identity = (WindowsIdentity)HttpContext.Current.User.Identity;
                return identity;
            }
            else
                return null;
        }
        #endregion

        #region Fields
        private string adAddress = string.Empty;
        private string userName = string.Empty;
        private string password = string.Empty;
        #endregion

        #region Private Methods
        private List<ADUser> LoadUserList()
        {
            PrincipalContext ctx = new PrincipalContext(ContextType.Domain, adAddress);
            return null;
        }
        #endregion
    }

    public class ADConnector
    {
        private List<ADConfig> adConfig;
        private List<ADConfig> failedConfig;

        public ADConnector(List<ADConfig> configs)
        {
            this.adConfig = configs;
            this.failedConfig = new List<ADConfig>();
        }

        public List<ADConfig> FailedDomain
        {
            get
            {
                return failedConfig;
            }
        }

        public List<UserPrincipal> GetAllUserPrincipals()
        {
            var groupname = "Domain Users";
            //dsFindOUs.Filter = "(objectClass=organizationalUnit)";
            var res = new List<UserPrincipal>();
            foreach (var thisAd in adConfig)
            {
                try
                {
                    var ctx = new PrincipalContext((ContextType)thisAd.ContextType, thisAd.Name, thisAd.Username, thisAd.Password);
                    GroupPrincipal grp = GroupPrincipal.FindByIdentity(ctx, IdentityType.SamAccountName, groupname);
                    //var searcher = new PrincipalSearcher(new UserPrincipal(ctx));
                    var searcher = grp.GetMembers();
                    foreach (var searchRes in searcher)
                    {
                        if (searchRes is UserPrincipal && searchRes.DistinguishedName.ToUpper().Contains("OU="))
                        {
                            var user = (UserPrincipal)searchRes;
                            res.Add(user);       
                        }
                    }
                }
                catch (Exception)
                {
                    if (!FailedDomain.Exists(c => c.Name == thisAd.Name)) FailedDomain.Add(thisAd);
                }
            }
            return res;
        }

        public static UserPrincipal GetPrincipalFromGuid(string guid, ADConfig ad)
        {
            
                var ctx = new PrincipalContext((ContextType)ad.ContextType, ad.Name, ad.Username, ad.Password);
                var usr = UserPrincipal.FindByIdentity(ctx, IdentityType.Guid, guid);
                return usr;
            
        }

        public static bool authenviaactivedomain(ADConfig directory, string username, string password)
        {
            var pcontext = new PrincipalContext((ContextType)directory.ContextType, directory.Name);
            return pcontext.ValidateCredentials(username, password);
        }
    }
}
