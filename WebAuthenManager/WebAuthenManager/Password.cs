﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace WebAuthenManager
{
    [Serializable]
    public class Password
    {
        private byte[] hashPassword = null;
        private byte[] salt = null;

        /// <summary>
        /// Base64 String of Salt for the password
        /// </summary>
        public string Salt
        {
            get
            {
                return Convert.ToBase64String(salt);
            }
            set
            {
                salt = Convert.FromBase64String(value);
            }
        }
        public string HashPassword
        {
            get
            {
                return Convert.ToBase64String(hashPassword);
            }
            set
            {
                hashPassword = Convert.FromBase64String(value);
            }
        }

        public void SetPassword(string newPass)
        {
            var crypter = new System.Security.Cryptography.SHA1CryptoServiceProvider();
            var buffer = System.Text.Encoding.ASCII.GetBytes(newPass);
            if (salt == null)
            {
                var hashByte = crypter.ComputeHash(buffer);
                this.hashPassword = hashByte;
            }
            else
            {
                var tempBuffer = buffer.ToList();
                tempBuffer.AddRange(salt);
                var saltedBuffer = tempBuffer.ToArray();
                var hashByte = crypter.ComputeHash(saltedBuffer);
                tempBuffer = hashByte.ToList();
                tempBuffer.AddRange(salt);
                this.hashPassword = tempBuffer.ToArray();
            }
        }

        public Password()
            : this(null)
        {
        }

        public Password(string initPass, bool isUseSalt = false)
        {
            if (isUseSalt)
            {
                var rng = new RNGCryptoServiceProvider();
                this.salt = new byte[64];
                rng.GetBytes(this.salt);
            }
            if (!string.IsNullOrEmpty(initPass)) 
                SetPassword(initPass);
            
        }

        public bool VerifyPassword(string password)
        {
            var crypter = new System.Security.Cryptography.SHA1CryptoServiceProvider();
            var buffer = System.Text.Encoding.ASCII.GetBytes(password);
            if (salt == null)
            {
                var hashByte = crypter.ComputeHash(buffer);
                return hashPassword.SequenceEqual(hashByte);
            }
            else
            {
                // 01234567
                var saltPart = hashPassword.Skip(hashPassword.Length - salt.Length).Take(salt.Length).ToArray();
                if (!saltPart.SequenceEqual(this.salt))
                    return false;
                var contentPart = hashPassword.Take(hashPassword.Length - salt.Length).ToArray();
                var tempBuffer = buffer.ToList();
                tempBuffer.AddRange(salt);
                var hashBytes = crypter.ComputeHash(tempBuffer.ToArray());
                return hashPassword.Take(hashBytes.Length).ToArray().SequenceEqual(hashBytes);
            }
        }
    }
}
