﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebHelper
{
    public class Constants
    {
        public static string AppPath
        {
            get
            {
                try
                {
                    var context = System.Web.HttpContext.Current;
                    string appPath = string.Format("{0}://{1}{2}{3}",
                                                      context.Request.Url.Scheme,
                                                      context.Request.Url.Host,
                                                      context.Request.Url.Port == 80
                                                        ? string.Empty : ":" + context.Request.Url.Port,
                                                      context.Request.ApplicationPath);
                    if (!appPath.EndsWith("/"))
                        appPath += "/";
                    return appPath;
                }
                catch (Exception)
                {
                    return string.Empty;
                }
            }
        }

    }
}
