﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace WebHelper
{
    [DataContract, Serializable]
    public class APIResult
    {
        [DataMember]
        public bool IsSuccess { get; set; }
        [DataMember]
        public object Result { get; set; }
        [DataMember]
        public string ErrorMessage { get; set; }

        private APIResult()
        {
        }

        public static APIResult CreateResult(object data)
        {
            var res = new APIResult()
            {
                IsSuccess = true,
                Result = data,
                ErrorMessage = null
            };
            return res;
        }

        public static APIResult ExceptionResult(Exception ex)
        {
            var res = new APIResult()
            {
                IsSuccess = false,
                Result = null,
                ErrorMessage = ex.Message
            };
            return res;
        }

        public static APIResult ErrorResult(string errorMsg)
        {
            var res = new APIResult()
            {
                IsSuccess = false,
                Result = null,
                ErrorMessage = errorMsg
            };
            return res;
        }
    }

    
}
