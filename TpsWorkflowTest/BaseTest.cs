﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TPSWorkflow;
using System.Web.Mvc;
using Moq;
using System.Web;
using System.Web.Routing;
using System.Linq;
using TPSWorkflow.DAL;
using System.Collections.Specialized;
using System.Web.SessionState;
using System.IO;
using System.Reflection;
using TPSWorkflow.DAL.DtoS;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Data.Entity;
using Ecobz.Utilities;


namespace TpsWorkflowTest
{
    [TestClass]
    public class BaseTest
    {
        protected const string webConfigForTestPath = @"D:\Development Projects\PTT\PTT Workflow\TPSWorkflow";
        protected const string userNameAdminForTest = "chris";

        public static HttpContext FakeHttpContext()
        {
            var httpRequest = new HttpRequest("", "http://stackoverflow/", "");
            var stringWriter = new StringWriter();
            var httpResponse = new HttpResponse(stringWriter);
            var httpContext = new HttpContext(httpRequest, httpResponse);

            var sessionContainer = new HttpSessionStateContainer("id", new SessionStateItemCollection(),
                                                    new HttpStaticObjectsCollection(), 10, true,
                                                    HttpCookieMode.AutoDetect,
                                                    SessionStateMode.InProc, false);

            httpContext.Items["AspSession"] = typeof(HttpSessionState).GetConstructor(
                                        BindingFlags.NonPublic | BindingFlags.Instance,
                                        null, CallingConventions.Standard,
                                        new[] { typeof(HttpSessionStateContainer) },
                                        null)
                                .Invoke(new object[] { sessionContainer });

            return httpContext;
        }

        protected static HttpSessionStateBase FakeSessionContext(IUsersManage userRepo)
        {
            var result = new MockHttpSession();
            var userChosen = userRepo.GetByAuthen("chris", "asdf");
            result["UserInfo"] = userChosen;
            result["Rights"] = (TPSWorkflow.DAL.UserRights)userRepo.GetUserRights(userChosen);
            result["IsMaster"] = true;
            return result;
        }
        protected static HttpSessionStateBase FakeSessionContextForApproved(IUsersManage userRepo)
        {
            var result = new MockHttpSession();
            var userChosen = userRepo.GetByCode("240337");
            result["UserInfo"] = userChosen;
            result["Rights"] = (TPSWorkflow.DAL.UserRights)userRepo.GetUserRights(userChosen);
            result["IsMaster"] = true;
            return result;
        }
        protected void SetupMockFuckingSession(Mock<HttpSessionState> state, IUsersManage userRepo)
        {
            var userChosen = userRepo.GetByAuthen("chris", "asdf");
            state.Setup(d => d["UserInfo"]).Returns(userChosen);
            state.Setup(d => d["Rights"]).Returns((TPSWorkflow.DAL.UserRights)userRepo.GetUserRights(userChosen));
            state.Setup(d => d["IsMaster"]).Returns(true);
        }

        protected int testVar = 2;
        protected TPSWorkflow.DAL.IssuesData tpsRepo;
        protected TPSWorkflow.DAL.IUsersManage userRepo;
        protected TPSWorkflow.DAL.IMailLogger mailLogRepo;
        protected TPSWorkflow.Controllers.IssuesController issueController;
        protected TPSWorkflow.DAL.TPSEntities entityContext;
        protected HttpSessionStateBase mockSession;

        protected List<group> SetupTestingGroupForTodayMeeting()
        {
            var groupToTest = this.entityContext.group.Find(1);
            groupToTest.IsShiftThisMonth = true;
            groupToTest.ShiftDate = DateTime.Now.Date;
            entityContext.SaveChanges();
            var today = DateTime.Now.Date;
            var toDel = this.entityContext.meeting.FirstOrDefault(c => c.GroupId == groupToTest.GroupId && EntityFunctions.TruncateTime(c.MeetingDate) == today);
            if (toDel != null)
            {
                entityContext.meeting.Remove(toDel);
                entityContext.SaveChanges();
            }
            var groupToTest2 = this.entityContext.group.Find(2);
            groupToTest2.IsShiftThisMonth = true;
            groupToTest2.ShiftDate = DateTime.Now.Date.AddDays(5);
            var thisDayOfWeek = (int)DateTime.Now.Date.DayOfWeek;
            groupToTest2.DayOfWeek = thisDayOfWeek;
            int week = 1;
            DateTime mDate = DateTime.MinValue;
            while (mDate.Date != DateTime.Now.Date && week <= 5)
            {
                mDate = this.tpsRepo.nextMeetingDateIncludeToday(thisDayOfWeek, week, 0);
                week++;
            }
            Assert.IsTrue(week <= 5, "Cannot resolve correct meeting date");
            this.entityContext.SaveChanges();
            var result = new List<group>()
            {
                groupToTest,
                groupToTest2
            };
            return result;
        }
        protected void SetMeetingDateToDayAfterToday(int numdays, group entity)
        {
            var targetDate = DateTime.Now.Date.AddDays(numdays);
            int dayOfWeek, week = 1;
            for (dayOfWeek = 0; dayOfWeek < 7; dayOfWeek++)
            {
                bool isBreak = false;
                for (week = 1; week <= 5; week++)
                {
                    if (tpsRepo.nextMeetingDateIncludeToday(dayOfWeek, week) == targetDate)
                    {
                        isBreak = true;
                        break;
                    }
                }
                if (isBreak)
                    break;
            }
            entity.DayOfWeek = dayOfWeek;
            entity.WeekOfMonth = week;
        }

        [TestMethod]
        public void Login()
        {
            var username = "chris";
            var password = "asdf";
            Mock<HttpRequestBase> request = new Mock<HttpRequestBase>();
            Mock<HttpContextBase> context = new Mock<HttpContextBase>();
            var requestParams = new NameValueCollection()
            {
                {"username",username},
                {"password",password}
            };

            request.Setup(d => d.Params).Returns(requestParams);

            context.SetupGet(d => d.Request).Returns(request.Object);
            context.Setup(d => d.Session).Returns(new MockHttpSession());

            HttpContext.Current = FakeHttpContext();
            var authenticator = new TPSWorkflow.Controllers.AuthenController(this.userRepo, this.tpsRepo);
            authenticator.ControllerContext = new ControllerContext(context.Object, new RouteData(), authenticator);
            try
            {
                authenticator.Authen();
            }
            catch (MethodAccessException)
            {
            }
        }
        [TestInitialize]
        public void InitialTest()
        {
            testVar = 3;
            tpsRepo = new TPSWorkflow.DAL.IssuesData();
            userRepo = new TPSWorkflow.DAL.UsersManage(10);
            mailLogRepo = new TPSWorkflow.DAL.MailLogger();
            this.mockSession = FakeSessionContext(userRepo);
            //var MockHttpContext = new Mock<HttpContext>();
            //var mockHttpSession = new Mock<HttpSessionState>();
            //this.SetupMockFuckingSession(mockHttpSession, userRepo);
            //MockHttpContext.SetupGet(d => d.Session).Returns(mockHttpSession.Object);
            //HttpContext.Current = MockHttpContext.Object;


            this.issueController = new TPSWorkflow.Controllers.IssuesController(this.tpsRepo, this.userRepo, this.mailLogRepo);
            this.entityContext = new TPSWorkflow.DAL.TPSEntities();
        }
    }
}
