﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TPSWorkflow;
using System.Web.Mvc;
using Moq;
using System.Web;
using System.Web.Routing;
using System.Linq;
using TPSWorkflow.DAL;
using System.Collections.Specialized;
using System.Web.SessionState;
using System.IO;
using System.Reflection;
using TPSWorkflow.DAL.DtoS;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Data.Entity;
using Ecobz.Utilities;

namespace TpsWorkflowTest
{
    [TestClass]
    public class MailLogTest:BaseTest
    {
        

        [TestMethod]
        public void Maillog_Should_log_mail_when_issue_info_complete()
        {
            MailLog mailLog = null;
            string billNo = "0130000069";
            try
            {
                Mock<HttpRequestBase> request = new Mock<HttpRequestBase>();
                Mock<HttpResponseBase> response = new Mock<HttpResponseBase>();
                Mock<HttpContextBase> context = new Mock<HttpContextBase>();
                var reqParams = new NameValueCollection();

                reqParams["issueid"] = "-1";
                
                for (int i = 0; i < billNo.Length; i++)
                {
                    reqParams["billNo" + i.ToString()] = billNo[i].ToString();
                }
                
                reqParams["repairNo"] = "110000026";
                reqParams["plantId"] = "G-03";
                reqParams["plantCode"] = "G-03-000  0-INSP";
                reqParams["txtMachineNo"] = "2";
                reqParams["txtMachineFullName"] = "300-PSV-045PRESSURESRV.ATLIQ.TANKB";
                reqParams["problemName"] = "105002944:";
                reqParams["problemDetail"] = "105002944:";
                reqParams["problemDate"] = "24-Jun-2012";
                reqParams["fixDate"] = "11-May-2015";
                reqParams["dueDate"] = "";
                reqParams["unitcode"] = "80000600";

                request.SetupGet(d => d.Params).Returns(reqParams);
                context.SetupGet(d => d.Request).Returns(request.Object);
                context.SetupGet(d => d.Session).Returns(this.mockSession);

                this.issueController.ControllerContext = new ControllerContext(context.Object, new RouteData(), this.issueController);
                this.issueController.InjectSessionForTesting(this.mockSession);

                try
                {
                    var result = this.issueController.SubmitIssueInfo();
                    Assert.IsTrue(result is RedirectToRouteResult);
                }
                catch (Exception ex)
                {
                    Assert.IsTrue(ex is MethodAccessException);
                }

                mailLog = this.entityContext.MailLog.Where(c => c.TimeStamp == this.entityContext.MailLog.Max(c2 => c2.TimeStamp)).FirstOrDefault();
                Assert.IsNotNull(mailLog);
                Assert.AreEqual(mailLog.MailType, (int)MailLogType.InfoToApprover);
                Assert.AreEqual(1, mailLog.GroupId);
            }
            finally
            {
                this.entityContext.MailLog.Remove(mailLog);
                var toDelIssue = this.entityContext.issues.Where(c => c.BillNo == "0130000069").FirstOrDefault();
                this.entityContext.issues.Remove(toDelIssue);
                this.entityContext.SaveChanges();
            }

        }

        [TestMethod]
        public void Maillog_Should_log_mail_when_approver_assigned_presenter()
        {
            MailLog mailLog = null;
            string billNo = "0110021392";
            try
            {
                Mock<HttpRequestBase> request = new Mock<HttpRequestBase>();
                Mock<HttpResponseBase> response = new Mock<HttpResponseBase>();
                Mock<HttpContextBase> context = new Mock<HttpContextBase>();
                var reqParams = new NameValueCollection();

                var testEntity = this.entityContext.issues.First(d => d.BillNo == billNo);
                testEntity.Status = (int)IssuesStatus.Waiting;
                testEntity.InchargeUserCode = string.Empty;
                this.entityContext.SaveChanges();
                var dtoToPost = testEntity.ToDTO();
                dtoToPost.StatusEnum = IssuesStatus.Approve;
                dtoToPost.InchargeUserCode = "240367";

                reqParams["json"] = Newtonsoft.Json.JsonConvert.SerializeObject(dtoToPost);


                request.SetupGet(d => d.Params).Returns(reqParams);
                context.SetupGet(d => d.Request).Returns(request.Object);
                context.SetupGet(d => d.Session).Returns(FakeSessionContextForApproved(this.userRepo));

                this.issueController.ControllerContext = new ControllerContext(context.Object, new RouteData(), this.issueController);
                this.issueController.InjectSessionForTesting(FakeSessionContextForApproved(this.userRepo));

                try
                {
                    var result = this.issueController.SubmitIssueApprove();
                    Assert.IsTrue(result is RedirectToRouteResult);
                }
                catch (Exception ex)
                {
                    Assert.IsTrue(ex is MethodAccessException);
                }

                mailLog = this.entityContext.MailLog.Where(c => c.TimeStamp == this.entityContext.MailLog.Max(c2 => c2.TimeStamp)).FirstOrDefault();
                Assert.IsNotNull(mailLog);
                Assert.AreEqual(mailLog.MailType, (int)MailLogType.ApproverAssignPresenter);
                Assert.AreEqual(testEntity.allowplant.groupId, mailLog.GroupId);
            }
            finally
            {
                this.entityContext.MailLog.Remove(mailLog);
                this.entityContext.SaveChanges();
            }
        }

        [TestMethod]
        public void Maillog_Should_log_mail_when_approver_cancel()
        {
            MailLog mailLog = null;
            string billNo = "0110021392";
            try
            {
                Mock<HttpRequestBase> request = new Mock<HttpRequestBase>();
                Mock<HttpResponseBase> response = new Mock<HttpResponseBase>();
                Mock<HttpContextBase> context = new Mock<HttpContextBase>();
                var reqParams = new NameValueCollection();

                var testEntity = this.entityContext.issues.First(d => d.BillNo == billNo);
                testEntity.Status = (int)IssuesStatus.Waiting;
                testEntity.InchargeUserCode = string.Empty;
                this.entityContext.SaveChanges();
                var dtoToPost = testEntity.ToDTO();
                dtoToPost.StatusEnum = IssuesStatus.Cancel;
                dtoToPost.CancelDetail = "ยกเลิกจ้า";

                reqParams["json"] = Newtonsoft.Json.JsonConvert.SerializeObject(dtoToPost);


                request.SetupGet(d => d.Params).Returns(reqParams);
                context.SetupGet(d => d.Request).Returns(request.Object);
                context.SetupGet(d => d.Session).Returns(FakeSessionContextForApproved(this.userRepo));


                this.issueController.ControllerContext = new ControllerContext(context.Object, new RouteData(), this.issueController);
                this.issueController.InjectSessionForTesting(FakeSessionContextForApproved(this.userRepo));

                try
                {
                    var result = this.issueController.SubmitIssueApprove();
                    Assert.IsTrue(result is RedirectToRouteResult);
                }
                catch (Exception ex)
                {
                    Assert.IsTrue(ex is MethodAccessException);
                }

                mailLog = this.entityContext.MailLog.Where(c => c.TimeStamp == this.entityContext.MailLog.Max(c2 => c2.TimeStamp)).FirstOrDefault();
                Assert.IsNotNull(mailLog);
                Assert.AreEqual(mailLog.MailType, (int)MailLogType.ApproverCancel);
                Assert.AreEqual(testEntity.allowplant.groupId, mailLog.GroupId);
            }
            finally
            {
                this.entityContext.MailLog.Remove(mailLog);
                this.entityContext.SaveChanges();
            }
        }

        [TestMethod]
        public void Maillog_Should_log_mail_when_approver_change_presenter()
        {
            //MailLog mailLog = null;
            string billNo = "0110021392";
            List<MailLog> mailLogs = null;
            try
            {
                Mock<HttpRequestBase> request = new Mock<HttpRequestBase>();
                Mock<HttpResponseBase> response = new Mock<HttpResponseBase>();
                Mock<HttpContextBase> context = new Mock<HttpContextBase>();
                var reqParams = new NameValueCollection();

                var testEntity = this.entityContext.issues.First(d => d.BillNo == billNo);
                testEntity.Status = (int)IssuesStatus.Approve;
                testEntity.InchargeUserCode = "240337";
                this.entityContext.SaveChanges();
                var dtoToPost = testEntity.ToDTO();
                dtoToPost.StatusEnum = IssuesStatus.Approve;
                dtoToPost.InchargeUserCode = "250391";

                reqParams["json"] = Newtonsoft.Json.JsonConvert.SerializeObject(dtoToPost);


                request.SetupGet(d => d.Params).Returns(reqParams);
                context.SetupGet(d => d.Request).Returns(request.Object);
                context.SetupGet(d => d.Session).Returns(FakeSessionContextForApproved(this.userRepo));

                this.issueController.ControllerContext = new ControllerContext(context.Object, new RouteData(), this.issueController);
                this.issueController.InjectSessionForTesting(FakeSessionContextForApproved(this.userRepo));

                try
                {
                    var result = this.issueController.SubmitIssueApprove();
                    Assert.IsTrue(result is RedirectToRouteResult);
                }
                catch (Exception ex)
                {
                    Assert.IsTrue(ex is MethodAccessException);
                }

                mailLogs = this.entityContext.MailLog.OrderByDescending(d => d.TimeStamp).Take(3).ToList();
                Assert.IsNotNull(mailLogs);
                Assert.AreEqual(mailLogs[0].MailType, (int)MailLogType.ApproverAssignPresenter);
                Assert.AreEqual(mailLogs[1].MailType, (int)MailLogType.ApproverAssignPresenter);
                Assert.AreEqual(mailLogs[2].MailType, (int)MailLogType.ApproverChangePresenter);
                foreach (var mailLog in mailLogs)
                {
                    Assert.AreEqual(testEntity.allowplant.groupId, mailLog.GroupId);
                }
            }
            finally
            {
                mailLogs.ForEach(mailLog =>
                    {
                        this.entityContext.MailLog.Remove(mailLog);
                        this.entityContext.SaveChanges();
                    });
            }
        }

        [TestMethod]
        public void Maillog_Should_log_mail_before_meeting_1_and_7()
        {
        }

        [TestMethod]
        public void Maillog_Should_log_mail_when_case_closed()
        {
            
            MailLog mailLog = null;
            try
            {
                Mock<HttpRequestBase> request = new Mock<HttpRequestBase>();
                Mock<HttpResponseBase> response = new Mock<HttpResponseBase>();
                Mock<HttpContextBase> context = new Mock<HttpContextBase>();
                var reqParams = new NameValueCollection();

                var testEntity = this.entityContext.issues.First(d => d.summary != null && d.summary.summary_km.Count == 0);
                testEntity.Status = (int)IssuesStatus.Ready;
                this.entityContext.SaveChanges();
                var dtoToPost = testEntity.ToDTO();
                dtoToPost.StatusEnum = IssuesStatus.ClosedWaitKM;
                dtoToPost.InchargeUserCode = "250391";
                dtoToPost.summary = testEntity.summary.ToDTO();
                dtoToPost.summary.StatusEnum = SummaryStatus.Final;

                reqParams["json"] = Newtonsoft.Json.JsonConvert.SerializeObject(dtoToPost);
                reqParams["issueid"] = dtoToPost.IssueId.ToString();
                reqParams["delFileIdsJson"] = "[]";

                request.SetupGet(d => d.Params).Returns(reqParams);
                context.SetupGet(d => d.Request).Returns(request.Object);
                context.SetupGet(d => d.Session).Returns(this.mockSession);

                var issueController = new TPSWorkflow.Controllers.IssuesController(this.tpsRepo, this.userRepo, this.mailLogRepo);
                issueController.ControllerContext = new ControllerContext(context.Object, new RouteData(), issueController);
                issueController.InjectSessionForTesting(this.mockSession);

                try
                {
                    var result = issueController.SubmitKM();
                    Assert.IsTrue(result is RedirectToRouteResult);
                }
                catch (Exception ex)
                {
                    Assert.IsTrue(ex is MethodAccessException);
                }

                mailLog = this.entityContext.MailLog.Where(c => c.TimeStamp == this.entityContext.MailLog.Max(c2 => c2.TimeStamp)).FirstOrDefault();
                Assert.IsNotNull(mailLog);
                Assert.AreEqual(mailLog.MailType, (int)MailLogType.CaseClosed);
                Assert.AreEqual(testEntity.allowplant.groupId, mailLog.GroupId);
            }
            finally
            {
                this.entityContext.MailLog.Remove(mailLog);
                this.entityContext.SaveChanges();
            }
        }

        [TestMethod]
        public void Maillog_Should_log_mail_when_case_pending()
        {
            MailLog mailLog = null;
            try
            {
                Mock<HttpRequestBase> request = new Mock<HttpRequestBase>();
                Mock<HttpResponseBase> response = new Mock<HttpResponseBase>();
                Mock<HttpContextBase> context = new Mock<HttpContextBase>();
                var reqParams = new NameValueCollection();

                var testEntity = this.entityContext.issues.First(d => d.summary != null && d.summary.summary_km.Count == 0);
                testEntity.Status = (int)IssuesStatus.Ready;
                this.entityContext.SaveChanges();
                var dtoToPost = testEntity.ToDTO();
                dtoToPost.InchargeUserCode = "250391";
                dtoToPost.summary = testEntity.summary.ToDTO();
                dtoToPost.summary.StatusEnum = SummaryStatus.Remeeting;

                reqParams["json"] = Newtonsoft.Json.JsonConvert.SerializeObject(dtoToPost);
                reqParams["issueid"] = dtoToPost.IssueId.ToString();
                reqParams["delFileIdsJson"] = "[]";

                request.SetupGet(d => d.Params).Returns(reqParams);
                context.SetupGet(d => d.Request).Returns(request.Object);
                context.SetupGet(d => d.Session).Returns(this.mockSession);

                var issueController = new TPSWorkflow.Controllers.IssuesController(this.tpsRepo, this.userRepo, this.mailLogRepo);
                issueController.ControllerContext = new ControllerContext(context.Object, new RouteData(), issueController);
                issueController.InjectSessionForTesting(this.mockSession);

                try
                {
                    var result = issueController.SubmitKM();
                    Assert.IsTrue(result is RedirectToRouteResult);
                }
                catch (Exception ex)
                {
                    Assert.IsTrue(ex is MethodAccessException);
                }

                mailLog = this.entityContext.MailLog.Where(c => c.TimeStamp == this.entityContext.MailLog.Max(c2 => c2.TimeStamp)).FirstOrDefault();
                Assert.IsNotNull(mailLog);
                Assert.AreEqual(mailLog.MailType, (int)MailLogType.CaseReMeeting);
                Assert.AreEqual(testEntity.allowplant.groupId, mailLog.GroupId);
            }
            finally
            {
                this.entityContext.MailLog.Remove(mailLog);
                this.entityContext.SaveChanges();
            }
        }

        [TestMethod]
        public void Maillog_Should_log_mail_when_case_km_completed()
        {
        }

        [TestMethod]
        public void Memo_issues_memo_Should_return_correct_memo_and_issues()
        {
            //throw new NotImplementedException();
            // Cannot run because memocontroller cannot call result method in test context. Don't know why.
            int meetingId = 129;
            try
            {
                Mock<HttpRequestBase> request = new Mock<HttpRequestBase>();
                Mock<HttpResponseBase> response = new Mock<HttpResponseBase>();
                Mock<HttpContextBase> context = new Mock<HttpContextBase>();
                var reqParams = new NameValueCollection();

                var testEntity = this.entityContext.meeting.First(d => d.MeetingId == meetingId);
                context.SetupGet(d => d.Request).Returns(request.Object);
                context.SetupGet(d => d.Session).Returns(this.mockSession);

                var memoController = new TPSWorkflow.Controllers.MemoController(this.tpsRepo, this.userRepo, this.mailLogRepo);
                memoController.ControllerContext = new ControllerContext(context.Object, new RouteData(), memoController);
                memoController.InjectSessionForTesting(this.mockSession);

                var resView = memoController.MemoIssues(meetingId);
                var targetIssue = new List<int>(){ 
                    92,
                    230,
                    91,
                    94,
                    133,
                    137,
                    227,
                    228,
                    231,
                };
                
                Assert.IsInstanceOfType(resView, typeof(ViewResult));
                var view = resView as ViewResult;
                Assert.IsInstanceOfType(view.Model, typeof(meetingDto));
                Assert.AreEqual(((meetingDto)view.Model).MeetingId, meetingId);
                Assert.IsInstanceOfType(view.ViewData["issues"], typeof(List<issuesDto>));
                var issuesList = (List<issuesDto>)view.ViewData["issues"];
                Assert.AreEqual(targetIssue.Count, issuesList.Count);
                foreach (var i in issuesList)
                {
                    Assert.IsTrue(targetIssue.Contains(i.IssueId));
                }
            }
            finally
            {
            }
        }

        [TestMethod]
        public void Memo_Should_get_correct_issues_for_GetIssuesCanBeAddedToMeeting()
        {
            int meetingIdToTest = 129;
            var result = this.tpsRepo.GetIssuesCanBeAddedToMeeting(meetingIdToTest);
            var cannotBeInResult = this.entityContext.meeting_issue.Where(c => c.MeetingId == meetingIdToTest).Select(c => c.IssueId).ToList();
            Assert.IsTrue(!result.Any(r => cannotBeInResult.Contains(r.IssueId)), "Some issue in meeting contain in result");
            var shouldBeInResult = new int[] {
                92,
                94,
                133
            };
            var idInResults = result.Select(d => d.IssueId).ToList();
            Assert.IsFalse(shouldBeInResult.Any(r => !idInResults.Contains(r)), "Some issue that should be display not display");
        }

        [TestMethod]
        public void Memo_Should_correctly_add_issue_to_meeting_for_new_issue()
        {
            var meetingId = 129;
            var issueId = 92;
            var toRemove = this.entityContext.meeting_issue.Where(c => c.IssueId == issueId);
            this.entityContext.meeting_issue.RemoveRange(toRemove);
            this.entityContext.SaveChanges();

            this.tpsRepo.AddIssuesToMeeting(meetingId, new List<int>() { issueId });

            var addedEntity = this.entityContext.meeting_issue.Where(c => c.MeetingId == meetingId && c.IssueId == issueId).ToList();
            Assert.AreEqual(1, addedEntity.Count, "Should have 1 relation between meeting, issue. Found " + addedEntity.Count);
            var entity = addedEntity.First();
            Assert.AreEqual((int)MeetingIssueType.Main, entity.AgendaType, "Should be define as main agenda in meeting");

            this.entityContext.meeting_issue.Remove(entity);
            this.entityContext.SaveChanges();
        }

        [TestMethod]
        public void Memo_Should_correctly_add_issue_to_meeting_for_pending_issue()
        {
            var meetingId = 129;
            var issueIds = new List<int>()
            {
                94,
                133
            };
            var oldMeetingId = 74;
            var toRemove = this.entityContext.meeting_issue.Where(c => issueIds.Contains(c.IssueId));
            this.entityContext.meeting_issue.RemoveRange(toRemove);
            this.entityContext.SaveChanges();
            var toAdd1 = new meeting_issue()
            {
                IssueId = issueIds[0],
                MeetingId = oldMeetingId,
                AgendaType = (int)MeetingIssueType.Main
            };
            var toAdd2 = new meeting_issue()
            {
                IssueId = issueIds[1],
                MeetingId = oldMeetingId,
                AgendaType = (int)MeetingIssueType.Main
            };
            this.entityContext.meeting_issue.Add(toAdd1);
            this.entityContext.meeting_issue.Add(toAdd2);
            this.entityContext.SaveChanges();

            this.tpsRepo.AddIssuesToMeeting(meetingId, issueIds);
            var addedEntity = this.entityContext.meeting_issue.Where(c => c.MeetingId == meetingId && issueIds.Contains(c.IssueId)).ToList();
            Assert.AreEqual(2, addedEntity.Count, "Should have 2 relation between meeting, issue. Found " + addedEntity.Count);
            var entity1 = addedEntity[0];
            Assert.AreEqual((int)MeetingIssueType.Pending, entity1.AgendaType, "Should be define as pending agenda in meeting");
            var entity2 = addedEntity[1];
            Assert.AreEqual((int)MeetingIssueType.Pending, entity2.AgendaType, "Should be define as pending agenda in meeting");

            this.entityContext.meeting_issue.Remove(entity1);
            this.entityContext.meeting_issue.Remove(entity2);
            this.entityContext.SaveChanges();
        }

        [TestMethod]
        public void Memo_Should_delete_issue_from_meeting_correctly()
        {
            
            /* MeetingId	IssueId	AgendaType
                129	230	0
                129	137	200
                129	227	200
                129	228	200
                129	231	200 */
            var meetingId = 129;
            var testIssue = 91;

            // If issue is not in meeting, throw exception
            this.entityContext.meeting_issue.RemoveRange(this.entityContext.meeting_issue.Where(c => c.MeetingId == meetingId & c.IssueId == testIssue));
            this.entityContext.SaveChanges();
            try
            {
                this.tpsRepo.DeleteIssuesFromMeeting(129, 91);
                Assert.Fail();
            }
            catch (InvalidDataException)
            {
            }

            try
            {
                testIssue = 230;
                // Check if delete completed
                this.tpsRepo.DeleteIssuesFromMeeting(meetingId, testIssue);
                Assert.AreEqual(0, this.entityContext.meeting_issue.Count(c => c.MeetingId == meetingId & c.IssueId == testIssue), "Relation was not deleted");
            }
            finally
            {
                if (this.entityContext.meeting_issue.Count(c => c.MeetingId == meetingId & c.IssueId == testIssue) == 0)
                {
                    var entity = new meeting_issue()
                    {
                        MeetingId = meetingId,
                        IssueId = testIssue,
                        AgendaType = 0
                    };
                    this.entityContext.meeting_issue.Add(entity);
                    this.entityContext.SaveChanges();
                }
            }
        }

        [TestMethod]
        public void Memo_Should_create_agenda_in_memo_correctly()
        {
            var meetingId = 129;
        }

        [TestMethod]
        public void Schedule_Should_created_and_unshift_meeting_when_meeting_occrued()
        {
            // Create meeting
            // Unshift meeting after sp_tps_resolvemeeting
            var groupsTest = this.SetupTestingGroupForTodayMeeting();
            var groupToTest = groupsTest[0];
            var groupToTest2 = groupsTest[1];

            var meetingCount = this.entityContext.meeting.Count();
            this.entityContext.Database.SqlQuery<object>("exec sp_tps_resolvemeeting").ToList();
            this.entityContext.Entry<group>(groupToTest).Reload();
            this.entityContext.Entry<group>(groupToTest2).Reload();

            Assert.AreEqual(meetingCount + 1, entityContext.meeting.Count(), "Meeting is not created correctly (maybe too much or too less)");
            Assert.AreEqual(false, groupToTest.IsShiftThisMonth, "Not reset shifting date correctly");
            Assert.AreEqual(null, groupToTest.ShiftDate, "Not reset shifting date correctly");
            Assert.IsNull(entityContext.meeting.Where(c => DbFunctions.TruncateTime(c.MeetingDate) == DateTime.Now && c.GroupId == groupToTest2.GroupId).FirstOrDefault(), "Should not create meeting which date is already shift");
            Assert.AreEqual(true, groupToTest2.IsShiftThisMonth, "Should not reset group schedule which is shift");
            Assert.AreEqual(DateTime.Now.Date.AddDays(5), groupToTest2.ShiftDate, "Should not reset group schedule which is shift");

            var lastMeeting = this.entityContext.meeting.Find(this.entityContext.meeting.AsQueryable().Max(d => d.MeetingId));
            Assert.AreEqual(groupToTest.GroupId, lastMeeting.GroupId);
           
        }

        [TestMethod]
        public void Schedule_Should_save_meeting_date_in_meeting_schedule()
        {
            var toTestGroup = this.entityContext.group.Find(1);
            var oldConfig = new
            {
                IsShift = toTestGroup.IsShiftThisMonth,
                DateShift = toTestGroup.ShiftDate
            };
            try
            {
                this.tpsRepo.UpdateShiftingDate(1, true, DateTime.Now.Date, "");
                this.entityContext.Entry<group>(toTestGroup).Reload();
                Assert.AreEqual(true, toTestGroup.IsShiftThisMonth);
                Assert.AreEqual(DateTime.Now.Date, toTestGroup.ShiftDate);
            }
            finally
            {
                toTestGroup.ShiftDate = oldConfig.DateShift;
                toTestGroup.IsShiftThisMonth = oldConfig.IsShift;
                this.entityContext.SaveChanges();
            }

        }

        [TestMethod]
        public void Schedule_should_send_mail_based_on_shift_dated()
        {
            // 1st Group have normal meeting date on next week, but shift date to another week
            // 2nd Group have normal meeting date on another week, but shift date to this week
            #region Setup group for test

            var allGroups = this.entityContext.group.ToList();
            foreach (var group in allGroups)
            {
                group.IsShiftThisMonth = true;
                group.ShiftDate = DateTime.Now.AddDays(10);
            }

            var groupToTest = this.entityContext.group.Find(2);
            groupToTest.IsShiftThisMonth = true;
            groupToTest.ShiftDate = DateTime.Now.Date.AddDays(14);
            entityContext.SaveChanges();
            var today = DateTime.Now.Date;
            var toDel = this.entityContext.meeting.FirstOrDefault(c => c.GroupId == groupToTest.GroupId && EntityFunctions.TruncateTime(c.MeetingDate) == today);
            if (toDel != null)
            {
                entityContext.meeting.Remove(toDel);
                entityContext.SaveChanges();
            }
            this.SetMeetingDateToDayAfterToday(7, groupToTest);

            var groupToTest2 = this.entityContext.group.Find(1);
            groupToTest2.IsShiftThisMonth = true;
            groupToTest2.ShiftDate = DateTime.Now.Date.AddDays(7);
            this.SetMeetingDateToDayAfterToday(14, groupToTest2);
            this.entityContext.SaveChanges();
            
            #endregion

            Assert.AreEqual(DateTime.Now.Date.AddDays(7), tpsRepo.nextMeetingDate(groupToTest.GroupId).Date);
            Assert.AreEqual(DateTime.Now.Date.AddDays(14), tpsRepo.nextMeetingDate(groupToTest2.GroupId).Date);

            var mailCount = this.entityContext.MailLog.Count();
            TpsConfigurator.ArgsResolver.ResolveCommandLineArgs(true, webConfigForTestPath, TpsConfigurator.ResolveMode.All);
            Assert.AreEqual(mailCount + 1, entityContext.MailLog.Count(), "Mail was not sent correctly (maybe more or less but it should send 1 mail)");
            var lastMailId = entityContext.MailLog.Max(c => c.MaillogId);
            var lastMail = entityContext.MailLog.FirstOrDefault(d => d.MaillogId == lastMailId);
            var plantList = groupToTest2.allowplant.Select(c=>" " + c.plantdisplaycode).ToArray();
            var plantsString = string.Join(",", plantList).Trim();
            Assert.IsTrue(lastMail.MailContent.Contains(plantsString), "Incorrect mail group was sent");
            Assert.AreEqual(groupToTest2.GroupId, lastMail.GroupId);

            allGroups = this.entityContext.group.ToList();
            foreach (var group in allGroups)
            {
                group.IsShiftThisMonth = false;
                group.ShiftDate = null;
            }
            this.entityContext.SaveChanges();
        }

        [TestMethod]
        public void Mail_from_tps_schedule_should_save_calendar()
        {
            var groupToTest = this.entityContext.group.Find(1);
            this.SetMeetingDateToDayAfterToday(7, groupToTest);
            groupToTest.IsShiftThisMonth = false;
            groupToTest.ShiftDate = null;
            var groupToTest2 = this.entityContext.group.Find(2);
            groupToTest2.IsShiftThisMonth = true;
            groupToTest2.ShiftDate = DateTime.Now.AddDays(1).Date;
            var oldCalendarId1 = groupToTest.LatestCalendarId;
            var oldCalendarId2 = groupToTest2.LatestCalendarId;
            this.entityContext.SaveChanges();
            TpsConfigurator.ArgsResolver.ResolveCommandLineArgs(true, webConfigForTestPath, TpsConfigurator.ResolveMode.All);

            this.entityContext.Entry<group>(groupToTest).Reload();
            this.entityContext.Entry<group>(groupToTest2).Reload();

            Assert.AreNotEqual(oldCalendarId1, groupToTest.LatestCalendarId);
            Assert.AreNotEqual(oldCalendarId2, groupToTest2.LatestCalendarId);

        }

        [TestMethod]
        public void Schedule_should_resend_calendar_if_meeting_is_shift_and_schedule_have_sent()
        {
            // Test if group already send calendar resend
            var groupToTest = this.entityContext.group.Find(1);
            groupToTest.IsShiftThisMonth = false;
            groupToTest.ShiftDate = null;
            this.SetMeetingDateToDayAfterToday(4, groupToTest);
            this.entityContext.SaveChanges();
            var mailCountBefore = this.entityContext.MailLog.Count();
            Mock<HttpContextBase> context = new Mock<HttpContextBase>();
            var testController = new TPSWorkflow.Controllers.SettingsController(this.tpsRepo, this.userRepo, this.mailLogRepo);
            context.SetupGet(d => d.Session).Returns(this.mockSession);
            this.issueController.ControllerContext = new ControllerContext(context.Object, new RouteData(), this.issueController);
            this.issueController.InjectSessionForTesting(this.mockSession);
            testController.UpdateShiftingDate(groupToTest.GroupId, true, DateTime.Now.Date.AddDays(14), "");
            Assert.IsTrue(mailCountBefore < this.entityContext.MailLog.Count(), "Mail not sent");
            var lastMailId = entityContext.MailLog.Max(c => c.MaillogId);
            var lastMail = entityContext.MailLog.FirstOrDefault(d => d.MaillogId == lastMailId);
            Assert.AreEqual((int)MailLogType.ShiftDate, lastMail.MailType, "Mail type that sent incorrect");

            
            
        }
        [TestMethod]
        public void Schedule_should_not_send_calendar_if_meeting_is_shift_and_schedule_have_not_sent()
        {
            // Test if group not sent calendar must not resend
            Mock<HttpContextBase> context = new Mock<HttpContextBase>();
            var testController = new TPSWorkflow.Controllers.SettingsController(this.tpsRepo, this.userRepo, this.mailLogRepo);
            context.SetupGet(d => d.Session).Returns(this.mockSession);
            var groupToTest = this.entityContext.group.Find(1);
            groupToTest.IsShiftThisMonth = false;
            groupToTest.ShiftDate = null;
            this.SetMeetingDateToDayAfterToday(10, groupToTest);
            this.entityContext.SaveChanges();
            this.entityContext.Entry<group>(groupToTest).Reload();
            foreach (var e in this.entityContext.ChangeTracker.Entries())
            {
                e.Reload();
            }
            var mailCountBefore = this.entityContext.MailLog.Count();
            testController.UpdateShiftingDate(groupToTest.GroupId, true, DateTime.Now.Date.AddDays(14), "");
            Assert.IsTrue(mailCountBefore == this.entityContext.MailLog.Count(), "Mail sent");
        }

        [TestMethod]
        public void Authen_should_save_log_when_login_as_another_user()
        {
            var userCodeForTest = "250102";
            Mock<HttpRequestBase> request = new Mock<HttpRequestBase>();
            Mock<HttpResponseBase> response = new Mock<HttpResponseBase>();
            Mock<HttpContextBase> context = new Mock<HttpContextBase>();
            var reqParams = new NameValueCollection();

            request.SetupGet(d => d.Params).Returns(reqParams);
            context.SetupGet(d => d.Request).Returns(request.Object);
            context.SetupGet(d => d.Session).Returns(this.mockSession);

            var systemController = new TPSWorkflow.Controllers.SystemController(this.tpsRepo, this.userRepo);
            systemController.ControllerContext = new ControllerContext(context.Object, new RouteData(), systemController);
            systemController.InjectSessionForTesting(this.mockSession);
            var countBefore = this.entityContext.activitylog.Count();
            try
            {
                systemController.AdminLogin(userCodeForTest);
            }
            catch (MethodAccessException)
            {
            }
            Assert.AreEqual(countBefore + 1, this.entityContext.activitylog.Count());
            var lastActivityLog = this.entityContext.activitylog.OrderByDescending(c => c.ActivityLogId).First();
            Assert.AreEqual((int)ActivityLogType.MockUser, lastActivityLog.LogType);
            Assert.AreEqual(ActivityLogType.MockUser.GetDescription(), lastActivityLog.Description);
            Assert.AreEqual(userNameAdminForTest, lastActivityLog.Usercode);
            Assert.AreEqual(userCodeForTest, lastActivityLog.AsUsercode);
        }

        [TestMethod]
        public void Authen_should_save_log_when_approve_and_cancel_as_anoter_user()
        {
            string billNo = "0110021392";
            var thisMockSession = FakeSessionContextForApproved(this.userRepo);
            thisMockSession["MockFrom"] = userNameAdminForTest;
            var usernameOfTargetUser = ((PersonalInfoDto)thisMockSession["UserInfo"]).CODE;
            Mock<HttpRequestBase> request = new Mock<HttpRequestBase>();
            Mock<HttpResponseBase> response = new Mock<HttpResponseBase>();
            Mock<HttpContextBase> context = new Mock<HttpContextBase>();
            var reqParams = new NameValueCollection();

            var testEntity = this.entityContext.issues.First(d => d.BillNo == billNo);
            testEntity.Status = (int)IssuesStatus.Waiting;
            testEntity.InchargeUserCode = string.Empty;
            this.entityContext.SaveChanges();
            var dtoToPost = testEntity.ToDTO();
            dtoToPost.StatusEnum = IssuesStatus.Approve;
            dtoToPost.InchargeUserCode = "240367";

            reqParams["json"] = Newtonsoft.Json.JsonConvert.SerializeObject(dtoToPost);


            request.SetupGet(d => d.Params).Returns(reqParams);
            context.SetupGet(d => d.Request).Returns(request.Object);
            context.SetupGet(d => d.Session).Returns(thisMockSession);

            this.issueController.ControllerContext = new ControllerContext(context.Object, new RouteData(), this.issueController);
            this.issueController.InjectSessionForTesting(thisMockSession);

            var lastActivityLogCount = this.entityContext.activitylog.Count();
            try
            {
                this.issueController.SubmitIssueApprove();
            }
            catch (MethodAccessException)
            {
            }
            Assert.AreEqual(lastActivityLogCount + 1, this.entityContext.activitylog.Count(), "Activity not log");
            var lastActivityLog = this.entityContext.activitylog.OrderByDescending(c => c.ActivityLogId).First();
            Assert.AreEqual((int)ActivityLogType.ApproveAsUser, lastActivityLog.LogType, "Activity log incorrectly");
            Assert.AreEqual(ActivityLogType.ApproveAsUser.GetDescription(), lastActivityLog.Description, "Activity log incorrectly");
            Assert.AreEqual(userNameAdminForTest, lastActivityLog.Usercode, "Activity log incorrectly");
            Assert.AreEqual(usernameOfTargetUser, lastActivityLog.AsUsercode, "Activity log incorrectly");
            Assert.AreEqual(dtoToPost.BillNo, lastActivityLog.BillNo, "Activity log incorrectly");

            testEntity.Status = (int)IssuesStatus.Waiting;
            testEntity.InchargeUserCode = string.Empty;
            this.entityContext.SaveChanges();
            dtoToPost.StatusEnum = IssuesStatus.Cancel;
            reqParams["json"] = Newtonsoft.Json.JsonConvert.SerializeObject(dtoToPost);


            request.SetupGet(d => d.Params).Returns(reqParams);
            context.SetupGet(d => d.Request).Returns(request.Object);
            context.SetupGet(d => d.Session).Returns(thisMockSession);

            this.issueController.ControllerContext = new ControllerContext(context.Object, new RouteData(), this.issueController);
            this.issueController.InjectSessionForTesting(thisMockSession);

            lastActivityLogCount = this.entityContext.activitylog.Count();
            try
            {
                this.issueController.SubmitIssueApprove();
            }
            catch (MethodAccessException)
            {
            }
            Assert.AreEqual(lastActivityLogCount + 1, this.entityContext.activitylog.Count(), "Activity not log");
            lastActivityLog = this.entityContext.activitylog.OrderByDescending(c => c.ActivityLogId).First();
            Assert.AreEqual((int)ActivityLogType.CancelAsUser, lastActivityLog.LogType, "Activity log incorrectly");
            Assert.AreEqual(ActivityLogType.CancelAsUser.GetDescription(), lastActivityLog.Description, "Activity log incorrectly");
            Assert.AreEqual(userNameAdminForTest, lastActivityLog.Usercode, "Activity log incorrectly");
            Assert.AreEqual(usernameOfTargetUser, lastActivityLog.AsUsercode, "Activity log incorrectly");
            Assert.AreEqual(dtoToPost.BillNo, lastActivityLog.BillNo, "Activity log incorrectly");
        }

        [TestMethod]
        public void Should_send_mail_for_issues_which_not_get_approved_for_more_than_7_days()
        {
            string billNo1 = "0110021722";
            string billNo2 = "0110021412";
            this.entityContext.Database.ExecuteSqlCommand("UPDATE Issues SET IsWarnApprover = 1");
            this.entityContext.SaveChanges();
            var targetIssues1 = this.entityContext.issues.Where(c => c.BillNo == billNo1).FirstOrDefault();
            var targetIssues2 = this.entityContext.issues.Where(c => c.BillNo == billNo2).FirstOrDefault();

            targetIssues1.ApproveDateTime = DateTime.Now - TimeSpan.FromDays(8);
            targetIssues1.Status = (int)IssuesStatus.Approve;
            targetIssues1.IsWarnApprover = false;
            targetIssues2.ApproveDateTime = DateTime.Now - TimeSpan.FromDays(8);
            targetIssues2.Status = (int)IssuesStatus.Approve;
            targetIssues2.IsWarnApprover = true;

            this.entityContext.SaveChanges();
            var mailLogBefore = this.entityContext.MailLog.Where(c => c.MailType == (int)MailLogType.AlertPresenter && c.GroupId == targetIssues2.allowplant.groupId).Count();
            TpsConfigurator.ArgsResolver.ResolveCommandLineArgs(true, webConfigForTestPath, TpsConfigurator.ResolveMode.Meeting);
            Assert.AreEqual(mailLogBefore + 1, this.entityContext.MailLog.Where(c => c.MailType == (int)MailLogType.AlertPresenter && c.GroupId == targetIssues2.allowplant.groupId).Count(), "Mail sent incorrectly");
            this.entityContext.Entry<issues>(targetIssues1).Reload();
            Assert.AreEqual(true, targetIssues1.IsWarnApprover, "Not shifting to warn after mail sent");

        }

        [TestMethod]
        public void Issues_should_save_closed_date()
        {

            MailLog mailLog = null;
            try
            {
                Mock<HttpRequestBase> request = new Mock<HttpRequestBase>();
                Mock<HttpResponseBase> response = new Mock<HttpResponseBase>();
                Mock<HttpContextBase> context = new Mock<HttpContextBase>();
                var reqParams = new NameValueCollection();

                var testEntity = this.entityContext.issues.First(d => d.summary != null && d.summary.summary_km.Count == 0);
                testEntity.Status = (int)IssuesStatus.Ready;
                testEntity.summary.Status = (int)SummaryStatus.None;
                testEntity.ClosedDate = null;
                this.entityContext.SaveChanges();
                var dtoToPost = testEntity.ToDTO();
                dtoToPost.StatusEnum = IssuesStatus.ClosedWaitKM;
                dtoToPost.InchargeUserCode = "250391";
                dtoToPost.summary = testEntity.summary.ToDTO();
                dtoToPost.summary.StatusEnum = SummaryStatus.Final;

                reqParams["json"] = Newtonsoft.Json.JsonConvert.SerializeObject(dtoToPost);
                reqParams["issueid"] = dtoToPost.IssueId.ToString();
                reqParams["delFileIdsJson"] = "[]";

                request.SetupGet(d => d.Params).Returns(reqParams);
                context.SetupGet(d => d.Request).Returns(request.Object);
                context.SetupGet(d => d.Session).Returns(this.mockSession);

                var issueController = new TPSWorkflow.Controllers.IssuesController(this.tpsRepo, this.userRepo, this.mailLogRepo);
                issueController.ControllerContext = new ControllerContext(context.Object, new RouteData(), issueController);
                issueController.InjectSessionForTesting(this.mockSession);

                try
                {
                    var result = issueController.SubmitKM();
                    Assert.IsTrue(result is RedirectToRouteResult);
                }
                catch (Exception ex)
                {
                    Assert.IsTrue(ex is MethodAccessException);
                }
                this.entityContext.Entry<issues>(testEntity).Reload();
                Assert.IsNotNull(testEntity.ClosedDate);
                Assert.IsTrue((DateTime.Now - testEntity.ClosedDate) < TimeSpan.FromMinutes(1));
                
            }
            finally
            {
                
            }
        }
    }
}


