﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace TpsWorkflowTest
{
    public class MockHttpSession : HttpSessionStateBase
    {
        Dictionary<string, object> m_SessionStorage = new Dictionary<string, object>();

        public override object this[string name]
        {
            get
            {
                try
                {
                    return m_SessionStorage[name];
                }
                catch(KeyNotFoundException)
                {
                    return null;
                }

            }
            set { m_SessionStorage[name] = value; }
        }
    }
}
