﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using TPSWorkflow.DAL;

namespace TpsWorkflowTest
{
    [TestClass]
    public class NewMailTest:BaseTest
    {
        [TestMethod]
        public void Should_change_location_correctly()
        {
            var entity = this.entityContext.TpsConfig.Where(c => c.ConfigKey == "LocationRoom").FirstOrDefault();
            Assert.IsNotNull(entity, "No config set");
            var repoLocation = this.tpsRepo.GetDefaultMeetingLocation();
            Assert.AreEqual(entity.ConfigValue, repoLocation, "Location get from repository mismatch");
            var targetRoomName="Testing Room";
            this.tpsRepo.SetDefaultMeetingLocation(targetRoomName);
            this.entityContext.Entry<TpsConfig>(entity).Reload();
            Assert.AreEqual(this.tpsRepo.GetDefaultMeetingLocation(), targetRoomName);
            this.tpsRepo.SetDefaultMeetingLocation(repoLocation);
        }

        [TestMethod]
        public void Should_reset_location_after_meeting_occured()
        {
            var groupId = 3;
            var targetGroup = this.entityContext.group.Where(c => c.GroupId == groupId).First();
            this.SetMeetingDateToDayAfterToday(0, targetGroup);
            var today = DateTime.Now.Date;
            this.entityContext.meeting.RemoveRange(this.entityContext.meeting.Where(c => c.GroupId == groupId && System.Data.Entity.DbFunctions.TruncateTime(c.MeetingDate) == today));
            targetGroup.Location = "ห้องสำหรับกลุ่ม 3 ทดสอบ";
            this.entityContext.SaveChanges();

            TpsConfigurator.ArgsResolver.ResolveCommandLineArgs(true, webConfigForTestPath, TpsConfigurator.ResolveMode.Meeting);
            this.entityContext.Entry<group>(targetGroup).Reload();
            Assert.AreEqual(string.Empty, targetGroup.Location);
        }

        [TestMethod]
        public void Should_use_group_location_when_send_mail_if_exists()
        {
            var groupId = 3;
            var targetGroup = this.entityContext.group.Where(c => c.GroupId == groupId).First();
            this.SetMeetingDateToDayAfterToday(7, targetGroup);
            targetGroup.Location = "ห้องสำหรับกลุ่ม 3 ทดสอบ";
            this.entityContext.SaveChanges();
            var mc = new MailScheduleCreator();
            var mailList = mc.CreateScheduleMail();
            var targetMail = mailList.Where(c => c.GroupId == groupId).FirstOrDefault();
            var contentType = targetMail.CalendarView.ContentType.CharSet;
            var encoding = System.Text.Encoding.UTF8;
            byte[] byteBuffer = new byte[targetMail.CalendarView.ContentStream.Length];
            targetMail.CalendarView.ContentStream.Read(byteBuffer,0,byteBuffer.Length);
            var contentString = encoding.GetString(byteBuffer);
            Assert.IsTrue(contentString.Contains("LOCATION:" + targetGroup.Location));
        }
    }
}
