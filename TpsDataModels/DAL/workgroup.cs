//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TPSWorkflow.DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class workgroup
    {
        public workgroup()
        {
            this.issues = new HashSet<issues>();
            this.approver = new HashSet<approver>();
            this.group = new HashSet<group>();
            this.presenter = new HashSet<presenter>();
        }
    
        public int WorkGroupId { get; set; }
        public string WorkGroupName { get; set; }
        public string unitcode { get; set; }
        public string WorkGroupNameTH { get; set; }
        public int IsMaster { get; set; }
    
        public virtual ICollection<issues> issues { get; set; }
        public virtual ICollection<approver> approver { get; set; }
        public virtual ICollection<group> group { get; set; }
        public virtual ICollection<presenter> presenter { get; set; }
    }
}
