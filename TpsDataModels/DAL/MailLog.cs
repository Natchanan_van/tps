//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TPSWorkflow.DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class MailLog
    {
        public long MaillogId { get; set; }
        public string SendToAddress { get; set; }
        public string SendCCAddress { get; set; }
        public string MailContent { get; set; }
        public int MailType { get; set; }
        public System.DateTime TimeStamp { get; set; }
        public string MailTypeDescription { get; set; }
        public bool IsSent { get; set; }
        public string Error { get; set; }
        public Nullable<int> GroupId { get; set; }
    }
}
