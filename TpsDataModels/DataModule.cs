﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TPSWorkflow.DAL
{
    public partial class PISEntities
    {
        public PISEntities(string connectionString):base(connectionString)
        {

        }
    }

    public partial class TPSEntities
    {
        public TPSEntities(string connectionString)
            : base(connectionString)
        {

        }
    }

    public partial class SAPEntities
    {
        public SAPEntities(string connectionString)
            : base(connectionString)
        {

        }
    }

    public abstract class DataModule
    {
        protected PISEntities pisContext;
        protected SAPEntities sapContext;
        protected TPSEntities tpsContext;

        public DataModule()
        {
            this.pisContext = new PISEntities();
            this.sapContext = new SAPEntities();
            this.tpsContext = new TPSEntities();
        }

        public DataModule(string tpsConnectionString)
        {
            this.pisContext = new PISEntities();
            this.sapContext = new SAPEntities();
            this.tpsContext = new TPSEntities(tpsConnectionString);
        }
        public DataModule(string tpsConnectionString, string pmConnectionString)
        {
            this.pisContext = new PISEntities(pmConnectionString);
            this.sapContext = new SAPEntities();
            this.tpsContext = new TPSEntities(tpsConnectionString);
        }


        public static DateTime nextMeetingDate(group entity)
        {
            if (entity.IsShiftThisMonth && entity.ShiftDate != null)
                return entity.ShiftDate.Value;
            var day = entity.DayOfWeek;
            var week = entity.WeekOfMonth;
            var p = DateTime.Now.AddDays(1);
            while (!(p.DayOfWeek == (DayOfWeek)day && (((p.Day - 1) / 7) + 1) == week))
            {
                p = p.AddDays(1);
            }
            return p;
        }
    }

  
}