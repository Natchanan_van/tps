﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TPSWorkflow.DAL.DtoS
{
    public enum IssuesStatus
    {
        New = 0,
        Waiting = 100,
        Approve = 200,
        Pending = 400,
        Ready = 450,
        ClosedWaitKM = 500,
        Recorded = 600,
        Cancel = 700,
        PendingAfterMeeting = 800,
        Deleted = 900
    }
}
