﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TPSWorkflow.DAL
{
    public enum MailLogType
    {
        [Description("ทั้งหมด")]
        Unknow = 0,
        [Description("ส่งข้อมูลหา Approver รอการ Approve")]
        InfoToApprover = 100,
        [Description("ถูกยกเลิกจาก Approver")]
        ApproverCancel = 200,
        [Description("Approver ได้ทำการตอบตกลงและมอบหมายงานให้ผู้นำเสนอ")]
        ApproverAssignPresenter = 300,
        [Description("Approver ได้ทำการเปลี่ยนผู้นำเสนอ")]
        ApproverChangePresenter = 400,
        [Description("นัดผู้เข้าร่วมประชุม")]
        AlertMeetingAttendees = 500,
        [Description("อนุมัติปิดเรื่อง")]
        CaseClosed = 600,
        [Description("แจ้งให้นำกลับเข้าประชุมใหม่")]
        CaseReMeeting = 700,
        [Description("แนบ KM")]
        CaseKm = 800,
        [Description("แนบ Memo การประชุม")]
        Memo = 900,
        [Description("เลื่อนวันประชุม")]
        ShiftDate = 1000,
        [Description("แจ้งเตือนให้กรอกข้อมูลนำเสนองานประชุม")]
        AlertPresenter = 1100
    }
    public interface IMailLogger
    {
        bool LogMail(string send, string cc, string content, MailLogType type, bool isSent, string err = "", int? groupId = null);
    }
}
