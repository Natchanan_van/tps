﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.SqlServer;
using System.Globalization;
using System.Linq;
using System.Net.Mail;
using System.Text;
using TPSWorkflow.DAL.DtoS;
using TPSWorkflow.Repositories;

namespace TPSWorkflow.DAL
{
    public enum GenerateType
    {
        Week,
        Tomorrow
    }
    public class MailScheduleCreator : DAL.DataModule, TPSWorkflow.Repositories.IMailScheduleCreator
    {
        public MailScheduleCreator(string tpsConnectionString):base(tpsConnectionString)
        {
            
        }
        public MailScheduleCreator(string tpsConnectionString,string pmConnectionString)
            : base(tpsConnectionString, pmConnectionString)
        {

        }
        public MailScheduleCreator()
            : base()
        {
        }

        private string timeString(DateTime time)
        {
            return time.ToString("HH:mm");
        }
        private Dictionary<int, string> groupIdCalendarCache = new Dictionary<int, string>();

        private List<group> getNeedScheduledGroup(DateTime targetDate)
        {
            var targetDateDayOfWeek = (int)targetDate.DayOfWeek;
            var targetDateWeekOfMonth = Convert.ToInt32(Math.Ceiling(targetDate.Day / 7.0));
            var groupNotSuspended = this.tpsContext.group.Where(c => !c.IsSuspended || c.SuspendedEnd < targetDate);
            var groupMeetingtargetDate = groupNotSuspended.Where(c => (!c.IsShiftThisMonth && c.DayOfWeek == targetDateDayOfWeek && c.WeekOfMonth == targetDateWeekOfMonth)
                || (c.IsShiftThisMonth && c.ShiftDate.HasValue && DbFunctions.TruncateTime(c.ShiftDate) == targetDate));

            return groupMeetingtargetDate.ToList();
        }
        private AlternateView genCalendarForGroup(group targetGroup, DateTime meetingDate, GenerateType genType)
        {
            var calendarId = genType == GenerateType.Week ? Guid.NewGuid().ToString() : targetGroup.LatestCalendarId;
            var timeStart = targetGroup.TimeStart.GetValueOrDefault(DateTime.MinValue);
            var timeEnd = targetGroup.TimeEnd.GetValueOrDefault(DateTime.MaxValue);
            var plants = string.Join(", ", targetGroup.allowplant.Select(c => c.plantdisplaycode).ToArray());
            var subject = "ระบบจัดการประชุม TPS : ขอโปรดร่วมเข้าประชุม ของ " + plants;
            var title = subject;
            var dayOfWeek = targetGroup.DayOfWeek;
            var weekOfMonth = targetGroup.WeekOfMonth;
            var nextDate = meetingDate;
            DateTime start, end;

            var isShiftThisMonth = targetGroup.IsShiftThisMonth;
            var shiftDate = targetGroup.ShiftDate;
            if (isShiftThisMonth && shiftDate.HasValue)
            {
                start = shiftDate.Value.Date + timeStart.TimeOfDay;
                end = shiftDate.Value.Date + timeEnd.TimeOfDay;
            }
            else
            {
                start = nextDate.Date + timeStart.TimeOfDay;
                end = nextDate.Date + timeEnd.TimeOfDay;
            }
            groupIdCalendarCache[targetGroup.GroupId] = calendarId;
            string location;
            if (string.IsNullOrEmpty(targetGroup.Location))
                location = this.tpsContext.TpsConfig.Where(c => c.ConfigKey == "LocationRoom").Select(c => c.ConfigValue).FirstOrDefault();
            else
                location = targetGroup.Location;
            
            var iCalView = Ecobz.Utilities.iCalHelper.CreateICalContent(end, start, location, title, string.Empty, calendarId);
            return iCalView;
        }
        protected List<string> getMailAddressToSendInGroup(group targetGroup)
        {
            return this.GetMailUserToSendInGroup(targetGroup).Select(c => c.EmailAddr).ToList();

        }
        public List<PersonalInfo> GetMailUserToSendInGroup(group targetGroup)
        {
            var usersInTps = pisContext.Database.SqlQuery<PersonalInfo>(string.Format(@"SELECT pTable.*
                            FROM dbo.personel_info as pTable
                            INNER JOIN dbo.unit as uTable ON uTable.unitcode = pTable.UNITCODE
                            where pTable.unitcode = '80000602'" + // ผบ.วบก. ทั้งหมด
                            "OR (pTable.MGMT = 'L'"+ // เฉพาะหัวหน้าเท่านั้น
	                        "AND ((pTable.unitcode = '80000597' AND ISNUMERIC(pTable.JOBGROUP) > 0 AND CAST(pTable.JOBGROUP as int) >= 14)"+ //-- ฝ่าย วบก.
	                         " OR (pTable.unitcode = '80000600' AND ISNUMERIC(pTable.JOBGROUP) > 0 AND CAST(pTable.JOBGROUP as int) >= 11)"+ //-- ส่วน บง.วบก.
	                         " OR (pTable.unitcode = '80000601' AND ISNUMERIC(pTable.JOBGROUP) > 0 AND CAST(pTable.JOBGROUP as int) >= 11)"+ //-- ส่วน ตร.วบก.
	                         " OR (pTable.unitcode = '80000603' AND ISNUMERIC(pTable.JOBGROUP) > 0 AND CAST(pTable.JOBGROUP as int) >= 11)"+ //-- ส่วน บฟ.วบก.
	                         " OR (pTable.unitcode = '80000604' AND ISNUMERIC(pTable.JOBGROUP) > 0 AND CAST(pTable.JOBGROUP as int) >= 11)))"+ //-- ส่วน บค.วบก.
                            "OR (pTable.MGMT = 'L'" + // เฉพาะหัวหน้าเท่านั้น
                            "AND (uTable.DUMMY_RELATIONSHIP like (SELECT DUMMY_RELATIONSHIP FROM dbo.unit WHERE unitcode = '" + targetGroup.unitcode + "') + '%'))")).ToList();
             
              /* Comment out by Arnonthawajjana
//            var usersInTps = pisContext.Database.SqlQuery<PersonalInfo>(string.Format(@"SELECT pTable.*
//                            FROM dbo.personel_info as pTable
//                            INNER JOIN dbo.Report_To as rTable ON pTable.CODE = rTable.CODE
//                            INNER JOIN dbo.unit as uTable ON uTable.unitcode = pTable.UNITCODE
//                            where (((rTable.BAND = 'AC1' AND uTable.DUMMY_RELATIONSHIP like '1-3-2-9-5%') OR (rTable.BAND = 'AD' AND uTable.DUMMY_RELATIONSHIP = '1-3-2-9-5'))   
//                            OR (pTable.unitcode = '80000597' AND ISNUMERIC(pTable.JOBGROUP) > 0 AND CAST(pTable.JOBGROUP as int) >= 13) 
//                            OR (pTable.unitcode = '80000600' AND ISNUMERIC(pTable.JOBGROUP) > 0 AND CAST(pTable.JOBGROUP as int) >= {0}) 
//                            OR (pTable.unitcode = '80000602') 
//                            OR (pTable.unitcode = '80000603' AND ISNUMERIC(pTable.JOBGROUP) > 0 AND CAST(pTable.JOBGROUP as int) >= {0}) 
//                            OR (pTable.unitcode = '80000604' AND ISNUMERIC(pTable.JOBGROUP) > 0 AND CAST(pTable.JOBGROUP as int) >= {0})
//                            OR (pTable.unitcode = '" + targetGroup.unitcode + "' AND ISNUMERIC(pTable.JOBGROUP) > 0 AND CAST(pTable.JOBGROUP as int) >= {0}))", 10)).ToList();*/
            //var usersInTps = from pTable in pisContext.PersonalInfo
            //                 join rTable in pisContext.Report_To on pTable.CODE equals rTable.CODE
            //                 join uTable in pisContext.WorkUnit on pTable.UNITCODE equals uTable.unitcode
            //                 where (rTable.BAND == "AC1" && uTable.DUMMY_RELATIONSHIP.StartsWith("1-3-2-9-5"))
            //                 || (rTable.BAND == "AD" && uTable.DUMMY_RELATIONSHIP == "1-3-2-9-5")
            //                 || (pTable.UNITCODE == "80000602" && SqlFunctions.IsNumeric(pTable.JOBGROUP) > 0 && int.Parse(pTable.JOBGROUP) >= 10)
            //                 || (pTable.UNITCODE == "80000597" && SqlFunctions.IsNumeric(pTable.JOBGROUP) > 0 && int.Parse(pTable.JOBGROUP) >= 13)
            //                 || (pTable.UNITCODE == "80000600" && SqlFunctions.IsNumeric(pTable.JOBGROUP) > 0 && int.Parse(pTable.JOBGROUP) >= 10)
            //                 || (pTable.UNITCODE == "80000603" && SqlFunctions.IsNumeric(pTable.JOBGROUP) > 0 && int.Parse(pTable.JOBGROUP) >= 10)
            //                 || (pTable.UNITCODE == "80000604" && SqlFunctions.IsNumeric(pTable.JOBGROUP) > 0 && int.Parse(pTable.JOBGROUP) >= 10)
            //                 select pTable;
            var pendingIssuePresenter = this.tpsContext.issues.Where(c => c.Status == (int)IssuesStatus.PendingAfterMeeting && c.summary.ResumMonth == DateTime.Now.Month && c.allowplant.groupId == targetGroup.GroupId).Select(c => c.InchargeUserCode).ToList();
            var meetingIssuePresenter = this.tpsContext.issues.Where(c => c.Status == (int)IssuesStatus.Ready && c.allowplant.groupId == targetGroup.GroupId).Select(c => c.InchargeUserCode).ToList();
            var additionalUserCode = targetGroup.group_mailing.Select(c => c.UserCode).ToList();
            additionalUserCode.AddRange(pendingIssuePresenter);
            additionalUserCode.AddRange(meetingIssuePresenter);
            var additionalUserMail = this.pisContext.PersonalInfo.Where(c => additionalUserCode.Contains(c.CODE));
            
            
            return usersInTps.Union(additionalUserMail).ToList();
        }
        protected MailDetail genMailDetailForGroup(group group, DateTime date, GenerateType genType)
        {
            var tableHeader = "<tr><td align=\"center\" bgcolor=\"#92CDDC\" border=\"1\">ลำดับ</td><td align=\"center\" bgcolor=\"#92CDDC\" border=\"1\">รายละเอียดการประชุม</td><td align=\"center\" bgcolor=\"#92CDDC\" border=\"1\">ผู้รับผิดชอบ</td></tr>";
            var tableContent = "<tr><td align=\"center\" border=\"1\">{0}</td><td border=\"1\">{1}</td><td align=\"center\" border=\"1\">{2}</td></tr>";
            var startTime = group.TimeStart.GetValueOrDefault(DateTime.MinValue);
            var nextTime = startTime + TimeSpan.FromMinutes(15);
            var plants = string.Join(", ", group.allowplant.Select(c => c.plantdisplaycode).ToArray());
            var subject = "ระบบจัดการประชุม TPS : ขอโปรดร่วมเข้าประชุม ของ " + plants + "";
            var toEmail = this.getMailAddressToSendInGroup(group);
            var sb = new StringBuilder();
            sb.AppendLine("<table>");
            sb.AppendLine("<tr><td colspan=\"10\">เรียน ผจ.วบก. ผจ.ส่วน สังกัด วบก. และท่านผู้เกี่ยวข้อง</td></tr>");
            sb.AppendLine("<br />");
            sb.AppendFormat("<tr><td colspan=\"10\">ระบบจัดการประชุม TPS ขอเรียนเชิญประชุม Technical Problem Solving {0} ในวันที่ {1} โดยมีวาระการประชุมดังนี้</td></tr>", plants, date.ToString("dd-MMM-yyyy", new System.Globalization.CultureInfo("th-TH")));
            sb.AppendLine();
            sb.AppendLine("<table border=\"1\" cellpadding=\"3\" cellspacing=\"0\" >");

            #region วาระที่ 1

            sb.AppendFormat("<tr><td colspan=\"3\" bgcolor=\"#002060\" border=\"1\"><font color=\"white\">วาระที่ 1: แจ้งเพื่อทราบ ({0}-{1})</font></td></tr>", timeString(startTime), timeString(nextTime));
            startTime = nextTime;
            nextTime = startTime + TimeSpan.FromMinutes(15);
            sb.AppendLine(tableHeader);
            sb.AppendFormat(tableContent, "1.1", "Maintenance Measurement", "-");
            sb.AppendFormat(tableContent, "1.2", "รายงานสอบสวนอุบัติเหตุ อุบัติการณ์", "-");
            sb.AppendFormat(tableContent, "1.3", "MOC (Management of Change)", "-");

            #endregion

            #region วาระที่ 2
            sb.AppendFormat("<tr><td colspan=\"3\" bgcolor=\"#002060\" border=\"1\"><font color=\"white\">วาระที่ 2: รับรองการประชุม ({0}-{1})</font></td></tr>", timeString(startTime), timeString(nextTime));
            startTime = nextTime;
            nextTime = startTime + TimeSpan.FromMinutes(15);
            sb.AppendLine(tableHeader);
            var latestMeetingOrder = this.tpsContext.meeting.Where(c => c.GroupId == group.GroupId).Max(d => d.MeetingOrder);
            var baseUrl = this.tpsContext.TpsConfig.Where(c => c.ConfigKey == "baseUrl").Select(c => c.ConfigValue).FirstOrDefault();
            baseUrl = baseUrl == null ? "" : baseUrl;
            var latestMeeting = this.tpsContext.meeting.Where(c => c.MeetingOrder == latestMeetingOrder && c.GroupId == group.GroupId).First();
            var content = string.Format("รับรองการประชุม ครั้งที่ {0} พื้นที่ {1} <br /> <a href=\"{2}\">Link to minute meeting</a>", latestMeeting.MeetingOrder + "/" + latestMeeting.MeetingDate.Year, plants, baseUrl + "Memo/ExportMemo?meetingid=" + latestMeeting.MeetingId);
            sb.AppendFormat(tableContent, "2.1", content, "-");
            #endregion

            #region วาระที่ 3

            sb.AppendLine("<tr><td colspan=\"3\" bgcolor=\"#002060\" border=\"1\"><font color=\"white\">วาระที่ 3: วาระการติดตามการประชุมครั้งที่ผ่านมา</font></td></tr>");
            sb.AppendLine(tableHeader);
            var pendingIssue = this.tpsContext.issues.Where(c => c.Status == (int)IssuesStatus.PendingAfterMeeting && c.summary.ResumMonth == date.Month && c.allowplant.groupId == group.GroupId).ToList();
            int subNumber = 1;
            if (pendingIssue.Count == 0)
            {
                sb.AppendFormat(tableContent, "3.1", "ไม่มีวาระติดตาม", "-");
            }
            else
            {
                nextTime = startTime + TimeSpan.FromMinutes(15);
                foreach (var issue in pendingIssue)
                {
                    nextTime = startTime + TimeSpan.FromMinutes(15);
                    var presenterName = this.pisContext.PersonalInfo.Where(c => c.CODE == issue.InchargeUserCode).Select(c => c.FNAME + " " + c.LNAME).FirstOrDefault();
                    if (string.IsNullOrEmpty(presenterName))
                        presenterName = "-";
                    var topicContent = string.Format("{0} ({1}-{2})", issue.ProblemName, timeString(startTime), timeString(nextTime));
                    sb.AppendFormat(tableContent, "3." + subNumber, topicContent, presenterName);
                    subNumber++;
                    startTime = nextTime;
                }
            }

            #endregion

            #region วาระที่ 4 เป็นต้นไป

            var agendaNo = 4;
            var meetingIssue = this.tpsContext.issues.Where(c => c.Status == (int)IssuesStatus.Ready && c.allowplant.groupId == group.GroupId)
                .GroupBy(d => d.allowplant.plantdisplaycode)
                .Select(c => new
                {
                    plant = c.Key,
                    issues = c.ToList()
                }).ToList();

            foreach (var plantIssue in meetingIssue)
            {
                sb.AppendFormat("<tr><td colspan=\"3\" bgcolor=\"#002060\" border=\"1\"><font color=\"white\">วาระที่ {0}: {1}</font></td></tr>", agendaNo, "การแก้ไขปัญหา TPS " + plantIssue.plant);
                sb.AppendLine(tableHeader);
                var subAgendaNo = 1;
                foreach (var issue in plantIssue.issues)
                {
                    nextTime = startTime + TimeSpan.FromMinutes(15);
                    var presenterName = this.pisContext.PersonalInfo.Where(c => c.CODE == issue.InchargeUserCode).Select(c => c.FNAME + " " + c.LNAME).FirstOrDefault();
                    if (string.IsNullOrEmpty(presenterName))
                        presenterName = "-";
                    var topicContent = string.Format("{0} ({1}-{2})", issue.ProblemName, timeString(startTime), timeString(nextTime));
                    sb.AppendFormat(tableContent, agendaNo + "." + subAgendaNo, topicContent, presenterName);
                    subAgendaNo++;
                    startTime = nextTime;
                }
                agendaNo++;
            }


            #endregion

            sb.AppendLine("</table><br />");
            sb.AppendLine("<tr><td colspan=\"10\">จึงเรียนมาเพื่อโปรดเข้าร่วมประชุม ระบบจัดการประชุม TPS <br /> ระบบจัดการประชุม TPS</td></tr>");
            return new MailDetail()
                {
                    Body = sb.ToString(),
                    CC = new List<string>(),
                    Subject = subject,
                    To = toEmail,
                    CalendarView = this.genCalendarForGroup(group, date, genType),
                    GroupId = group.GroupId,
                    MailType = MailLogType.AlertMeetingAttendees
                };
        }
        protected MailDetail genMailForWaitingPresenterEnterDetail(issues entity)
        {
            var to = this.pisContext.PersonalInfo.Where(c => c.CODE == entity.InchargeUserCode).FirstOrDefault();
            if (to == null || string.IsNullOrEmpty(to.EmailAddr))
                return null;
            var subject = string.Format("ระบบจัดการประชุม TPS : ขอแจ้งเตือนโปรดดำเนินการกรอกรายละเอียดวาระการประชุม (ใบแจ้งซ่อมเลขที่ {0})", entity.BillNo);
            var sb = new StringBuilder();
            sb.AppendLine("เรียน " + to.INAME + " " + to.FNAME + " " + to.LNAME + "<br />");
            var baseUrl = this.tpsContext.TpsConfig.Where(c => c.ConfigKey == "baseUrl").Select(c => c.ConfigValue).FirstOrDefault();
            baseUrl = baseUrl == null ? "" : baseUrl;
            var link = baseUrl + "Issues/SolveDetail/" + entity.IssueId;
            sb.AppendFormat("&emsp;&emsp;&emsp;&emsp;ระบบจัดการประชุม TPS ขอแจ้งว่า ผจ. ส่วน {0} มอบหมายให้ท่าน เป็นผู้นำเสนอ ในการประชุม Technical Problem Solving {1} ในวันที่ {2} <br /> ตามใบแจ้งซ่อมเลขที่ {3} ท่านสามารถกรอกรายละเอียด และแนบ File Presentation ตาม <a href=\"{4}\">Link</a>",
                entity.workgroup.WorkGroupNameTH, entity.allowplant.plantdisplaycode, DataModule.nextMeetingDate(entity.allowplant.group).ToString("dd-MMM-yyyy", new CultureInfo("th-TH")), entity.BillNo, link);
            entity.IsWarnApprover = true;
            this.tpsContext.SaveChanges();
            return new MailDetail()
            {
                GroupId = entity.allowplant.groupId.GetValueOrDefault(-1),
                Body = sb.ToString(),
                CalendarView = null,
                Subject = subject,
                To = new List<string>() { to.EmailAddr },
                MailType = MailLogType.AlertPresenter
            };
            
            //ระบบจัดการประชุม TPS ขอแจ้งว่า ผจ. ส่วน ' + @WorkgroupNameTh + ' มอบหมายให้ท่าน เป็นผู้นำเสนอ ในการประชุม Technical Problem Solving ' + @plantDisplay + ' ในวันที่ ' + @nextDateStr + ' <br /> ตามใบแจ้งซ่อมเลขที่ ' + @billNo + ' ท่านสามารถกรอกรายละเอียด และแนบ File Presentation ตาม <a href="' + @approveLink + '">Link</a>';
        }
        public List<MailDetail> CreateScheduleMail()
        {
           
            var result = new List<MailDetail>();
            var nextWeek = DateTime.Now.Date.AddDays(7);
            var targetGroupNextWeek = this.getNeedScheduledGroup(nextWeek);
            foreach (var group in targetGroupNextWeek)
            {
                result.Add(this.genMailDetailForGroup(group, nextWeek, GenerateType.Week));
            }
            var nextDay = DateTime.Now.Date.AddDays(1);
            var targetGroupNextDay = this.getNeedScheduledGroup(nextDay);
            foreach (var group in targetGroupNextDay)
            {
                result.Add(this.genMailDetailForGroup(group, nextDay, GenerateType.Tomorrow));
            }
            var day7daybefore = DateTime.Now.Date - TimeSpan.FromDays(7);
            var notReadyIssues = this.tpsContext.issues.Where(c => c.Status == (int)IssuesStatus.Approve && 
                !c.IsWarnApprover && c.ApproveDateTime.HasValue &&
                DbFunctions.TruncateTime(c.ApproveDateTime) < day7daybefore).ToList();
            foreach (var issue in notReadyIssues)
            {
                result.Add(this.genMailForWaitingPresenterEnterDetail(issue));
            }
            return result;
        }
        public void UpdateCalendarId(int groupId)
        {
            try
            {
                var entity = this.tpsContext.group.First(d => d.GroupId == groupId);
                entity.LatestCalendarId = groupIdCalendarCache[groupId];
                this.tpsContext.SaveChanges();
            }
            catch
            {
            }
        }
    }
}
