﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TPSWorkflow.DAL;

namespace TPSWorkflow.Repositories
{
    /// <summary>
    /// Create mail content for schedule meeting
    /// </summary>
    public interface IMailScheduleCreator
    {
        List<MailDetail> CreateScheduleMail();
    }

    public class MailDetail
    {
        public string Subject { get; set; }
        public string Body { get; set; }
        public List<string> To { get; set; }
        public List<string> CC { get; set; }
        public System.Net.Mail.AlternateView CalendarView { get; set; }
        public int GroupId { get; set; }
        public MailLogType MailType { get; set; } 
    }
}
