﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ecobz.Utilities;

namespace TPSWorkflow.DAL
{
    public class MailLogger : DataModule, IMailLogger
    {
        public bool LogMail(string send, string cc, string content, MailLogType type, bool isSent, string err = "",int? groupId = null)
        {
            var d = new MailLog()
            {
                Error = err,
                IsSent = isSent,
                MailContent = content,
                MailType = (int)type,
                MailTypeDescription = type.GetDescription(),
                SendCCAddress = cc,
                SendToAddress = send,
                TimeStamp = DateTime.Now,
                GroupId = groupId
            };
            this.tpsContext.MailLog.Add(d);
            return this.tpsContext.SaveChanges() > 0;
        }
        public MailLogger()
            : base()
        {
        }

        public MailLogger(string tpsConnectionString):base()
        {
            if (!string.IsNullOrEmpty(tpsConnectionString))
            {
                this.tpsContext = new TPSEntities(tpsConnectionString);
            }
        }
    }
}