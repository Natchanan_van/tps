DECLARE @isMigrate int;
DECLARE @migrationName nvarchar(50);

SET @migrationName = 'AddGroupIdToMailLog'
SET @isMigrate = (SELECT Count(0) FROM Migrations WHERE MigrationName = @migrationName)

IF @isMigrate = 0
BEGIN
	/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
	BEGIN TRANSACTION;
	
	ALTER TABLE dbo.MailLog ADD GroupId int;

	INSERT INTO Migrations (MigrationName) VALUES (@migrationName);
	COMMIT;
END
ELSE
	PRINT 'Migration ignore because this migration is already implemented!!'