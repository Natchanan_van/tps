DECLARE @isMigrate int;
DECLARE @migrationName nvarchar(50);

SET @migrationName = 'CreateMailLog'
SET @isMigrate = (SELECT Count(0) FROM Migrations WHERE MigrationName = @migrationName)

IF @isMigrate = 0
BEGIN
	/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
	BEGIN TRANSACTION;
	CREATE TABLE dbo.MailLog
		(
		MaillogId bigint NOT NULL IDENTITY (1, 1),
		SendToAddress nvarchar(MAX) NULL,
		SendCCAddress nvarchar(MAX) NULL,
		MailContent ntext NULL,
		MailType int NOT NULL,
		TimeStamp datetime NOT NULL,
		MailTypeDescription nvarchar(MAX) NULL
		)  ON [PRIMARY]
		TEXTIMAGE_ON [PRIMARY];

	ALTER TABLE dbo.MailLog ADD CONSTRAINT
		DF_MailLog_TimeStamp DEFAULT GETDATE() FOR TimeStamp

	ALTER TABLE dbo.MailLog ADD CONSTRAINT
		PK_MailLog_1 PRIMARY KEY CLUSTERED 
		(
		MaillogId
		) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]


	ALTER TABLE dbo.MailLog SET (LOCK_ESCALATION = TABLE)

	COMMIT

	INSERT INTO Migrations (MigrationName) VALUES (@migrationName);
END
ELSE
	PRINT 'Migration ignore because this migration is already implemented!!'