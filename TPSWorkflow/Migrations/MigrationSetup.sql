-- First database migration practice approach

-- Create Database migration table

CREATE TABLE dbo.Migrations
	(
	MigrationName nvarchar(50) NOT NULL,
	MigratedTime datetime NOT NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Migrations ADD CONSTRAINT
	DF_Migrations_MigratedTime DEFAULT GETDATE() FOR MigratedTime
GO
ALTER TABLE dbo.Migrations ADD CONSTRAINT
	PK_Migrations PRIMARY KEY CLUSTERED 
	(
	MigrationName
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO

