DECLARE @isMigrate int;
DECLARE @migrationName nvarchar(50);

SET @migrationName = 'ActivityLog'
SET @isMigrate = (SELECT Count(0) FROM Migrations WHERE MigrationName = @migrationName)

IF @isMigrate = 0
BEGIN
	/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
	BEGIN TRANSACTION;
	
	CREATE TABLE dbo.activitylog
	(
	ActivityLogId int NOT NULL IDENTITY (1, 1) PRIMARY KEY,
	LogType int NOT NULL,
	Description nvarchar(MAX) NULL,
	Usercode nvarchar(MAX) NULL,
	AsUsercode nvarchar(MAX) NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]

	INSERT INTO Migrations (MigrationName) VALUES (@migrationName);
	COMMIT;
END
ELSE
	PRINT 'Migration ignore because this migration is already implemented!!'