﻿using ReportManagement;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TPSWorkflow.Helpers;

namespace TPSWorkflow
{
    [HandleError(ExceptionType = typeof(TpsUnauthorizedException), View = "Unauthorize")]
    public class BaseController : PdfViewController
    {
        //
        // GET: /Base/

        protected SessionHelper tpsSession;

        public BaseController():base()
        {
            try
            {
                this.tpsSession = SessionHelper.GetInstance(new HttpSessionStateWrapper(System.Web.HttpContext.Current.Session));
            }
            catch
            {
                try
                {
                    this.tpsSession = SessionHelper.GetInstance(this.HttpContext.Session);
                }
                catch
                {
                }
            }
        }

        public ActionResult testError()
        {
            throw new TpsUnauthorizedException("Test");
        }

        public string GetHtmlString(string viewToRender, ViewDataDictionary viewData)
        {
            var controllerContext = this.ControllerContext;
            var result = ViewEngines.Engines.FindView(this.ControllerContext, viewToRender, null);

            StringWriter output;
            using (output = new StringWriter())
            {
                var viewContext = new ViewContext(controllerContext, result.View, viewData, controllerContext.Controller.TempData, output);
                result.View.Render(viewContext, output);
                result.ViewEngine.ReleaseView(controllerContext, result.View);
            }
            return output.ToString();
        }

        public virtual void InjectSessionForTesting(HttpSessionStateBase session)
        {
            this.tpsSession = SessionHelper.GetInstance(session);
        }

    }
}
