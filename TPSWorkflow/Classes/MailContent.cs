﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web;
using TPSWorkflow.DAL;
using TPSWorkflow.DAL.DtoS;

namespace TPSWorkflow
{
    public class MailContentCreator
    {
        protected IIssuesData context;
        protected IUsersManage usersContext;

        public MailContentCreator(IIssuesData context, IUsersManage usersContext)
        {
            this.context = context;
            this.usersContext = usersContext;
        }



        private void AppendIssueDetail(StringBuilder sb, issuesDto issue)
        {
            //sb.AppendLine("<table>");
            //sb.AppendFormat("<tr><td>เลขที่ใบแจ้งซ่อม</td><td>{0}</td><tr>", issue.BillNo);
            //sb.AppendLine("<br />");
            //sb.AppendFormat("<เลขที่ใบสั่งซ่อม : {0}", issue.RepairBillNo);
            //sb.AppendLine("<br />");
            //sb.AppendFormat("โรงแยกก๊าซ : {0}", issue.plantshortcode);
            //sb.AppendLine("<br />");
            //sb.AppendFormat("เครื่องจักร : {0}", issue.MachineFullName);
            //sb.AppendLine("<br />");
            //sb.AppendFormat("กลุ่มงานรับผิดชอบ : {0}", issue.WorkGroupNameTh);
            //sb.AppendLine("<br />");
            //sb.AppendFormat("วันที่พบปัญหา : {0}", issue.ProblemDate.TpsMailDateTime());
            //sb.AppendLine("<br />");
            //sb.AppendFormat("ชื่อปัญหา : {0}", issue.ProblemName);
            //sb.AppendLine("<br />");
            //sb.AppendFormat("รายละเอียดปัญหา : {0}", issue.ProblemDetail);
            //sb.AppendLine("<br />");
            //sb.AppendFormat("วันที่เริ่มแก้ไข : {0}", issue.RepairDate.HasValue ? issue.RepairDate.Value.TpsMailDateTime() : string.Empty);
            //sb.AppendLine("<br />");
            //sb.AppendFormat("วันที่ดำเนินการเสร็ข : {0}", issue.RepairDueDate.HasValue ? issue.RepairDueDate.Value.TpsMailDateTime() : string.Empty);
            sb.AppendLine("<br />");
            sb.AppendLine("<table border=\"1\">");
            sb.AppendFormat("<tr><td width=\"200\">เลขที่ใบแจ้งซ่อม</td><td>{0}</td></tr>", issue.BillNo);
            sb.AppendFormat("<tr><td>เลขที่ใบสั่งซ่อม</td><td>{0}</td></tr>", issue.RepairBillNo);
            sb.AppendFormat("<tr><td>โรงแยกก๊าซ</td><td>{0}</td></tr>", issue.PlantDisplayCode + ". " + issue.plantcode);
            sb.AppendFormat("<tr><td>เครื่องจักร</td><td>{0} : {1}</td></tr>", issue.MachineNo, issue.MachineFullName);
            sb.AppendFormat("<tr><td>กลุ่มงานรับผิดชอบ</td><td>{0}</td></tr>", issue.WorkGroupNameTh);
            sb.AppendFormat("<tr><td>วันที่พบปัญหา</td><td>{0}</td></tr>", issue.ProblemDate.TpsMailDateTime());
            sb.AppendFormat("<tr><td>ชื่อปัญหา</td><td>{0}</td></tr>", issue.ProblemName);
            sb.AppendFormat("<tr><td>รายละเอียดปัญหา</td><td>{0}</td></tr>", issue.ProblemDetail);
            sb.AppendFormat("<tr><td>วันที่เริ่มแก้ไข</td><td>{0}</td></tr>", issue.RepairDate.HasValue ? issue.RepairDate.Value.TpsMailDateTime() : string.Empty);
            sb.AppendFormat("<tr><td>วันที่ดำเนินการเสร็จ</td><td>{0}</td></tr>", issue.RepairDueDate.HasValue ? issue.RepairDueDate.Value.TpsMailDateTime() : string.Empty);
            sb.AppendFormat("</table>");
        }
        private void AppendIssueSummary(StringBuilder sb, issuesDto issue)
        {
            var mt = this.context.GetSummaryById(issue.IssueId).Finalize;
            //sb.AppendLine("<h1>มติที่ประชุม</h1>");
            //sb.AppendLine("<br />");
            sb.AppendLine("<span style=\"font-weight:bold;text-decoration:underline;font-size:larger;\">มติที่ประชุม</span>");
            sb.AppendLine("<p>" + mt + "</p>");
            sb.AppendLine("<br />");
        }

        public MailResult CreatedBillMail(int issuesId)
        {
            #region Get Address

            var issue = this.context.GetById(issuesId);
            var approvers = this.context.GetAllApprover(issue.unitcode).Where(d => !string.IsNullOrEmpty(d.EmailAddr)).Select(r => new MailAddress(r.EmailAddr, r.FullName)).ToList();
            var cc = this.context.GetCCApprover(issue.unitcode).Where(d => !string.IsNullOrEmpty(d.EmailAddr)).Select(r => new MailAddress(r.EmailAddr, r.FullName)).ToList();

            #endregion Get Address

            #region Content

            var sb = new StringBuilder();
            var subject = string.Format("ระบบจัดการประชุม TPS : ขอโปรดพิจารณาอนุมัติวาระการประชุม จากใบแจ้งซ่อมเลขที่ {0}", issue.BillNo);
            var subjectForCC = string.Format("ระบบจัดการประชุม TPS : ขอโปรดทราบใบแจ้งซ่อมเลขที่ {0}", issue.BillNo);
            var title = string.Format("เรียน ผจ. ส่วน {0}", issue.WorkGroupNameTh);
            var titleCC = string.Format("สำเนาเรียน วิศวกร ส่วน {0}", issue.WorkGroupNameTh);
           
            sb.AppendLine("<br />");
            sb.AppendLine("<br />");
            sb.AppendFormat("&emsp;&emsp;&emsp;&emsp;ระบบจัดการประชุม TPS ขอเรียนแจ้งว่า อุปกรณ์ {0} เกิดการ Breakdown ตามใบแจ้งซ่อมเลขที่ {1} ", issue.MachineFullName, issue.BillNo);
            string link = WebHelper.Constants.AppPath + "Issues/Approve/" + issue.IssueId.ToString();
            sb.AppendLine("<br />");
            sb.AppendFormat("ขอความอนุเคราะห์ ส่วน {0} พิจารณาอนุมัติวาระการประชุม ตาม <a href=\"{1}\">Link</a>", issue.WorkGroupNameTh, link);
            sb.AppendLine("<br />");
            sb.AppendLine("<br />");

            this.AppendIssueDetail(sb, issue);

            sb.AppendLine("<br />");
            sb.AppendLine("<br />");
            sb.AppendLine("จึงเรียนมาเพื่อโปรดพิจารณา <br />");
            sb.AppendLine("ระบบจัดการประชุม TPS <br />");

            #endregion Content

            var res = new MailResult()
            {
                BCC = new List<MailAddress>(),
                CC = cc,
                Send = approvers,
                Content = title + sb.ToString(),
                ContentCC = titleCC + sb.ToString(),
                Subject = subject,
                SubjectCC = subjectForCC
            };
            res.ResolveAddress();
            return res;
        }

        public MailResult ApprovedBillMail(int issuesId)
        {
            #region Get Address

            var issue = this.context.GetById(issuesId);
            var presenterObj = this.usersContext.GetByCode(issue.InchargeUserCode);
            if (string.IsNullOrEmpty(presenterObj.EmailAddr))
            {
                throw new TpsUnauthorizedException("ไม่มีอีเมล์ของ Presenter ในระบบ");
            }
            var persenter = new List<MailAddress>() { new MailAddress(presenterObj.EmailAddr, presenterObj.FullName) };
            var cc = new List<MailAddress>();
            var creator = this.usersContext.GetByCode(issue.CreatedBy);
            if (creator != null)
            {
                try
                {
                    if (!string.IsNullOrEmpty(creator.EmailAddr))
                    {
                        cc.Add(new MailAddress(creator.EmailAddr, creator.FullName));
                    }
                }
                catch (Exception)
                {
                }
                finally
                {
                }
            }
            var facilitators = this.usersContext.GetByWorkGroup(this.context.GetMasterUnitCode()).Where(d => !string.IsNullOrEmpty(d.EmailAddr));
            cc.AddRange(facilitators.Select(r => new MailAddress(r.EmailAddr, r.FullName)).ToList());

            #endregion Get Address

            #region Content

            var sb = new StringBuilder();
            var subject = string.Format("ระบบจัดการประชุม TPS : ขอโปรดดำเนินการกรอกรายละเอียดวาระการประชุม (ใบแจ้งซ่อมเลขที่ {0})", issue.BillNo);
            var subjectForCc = string.Format("ระบบจัดการประชุม TPS : วาระการประชุมของท่านได้รับการอนุมัติ (ใบแจ้งซ่อมเลขที่ {0})", issue.BillNo);
            sb.AppendFormat("เรียน {0}", presenterObj.FullName);
            sb.AppendLine("<br /><br />");
            var p = this.context.nextMeetingDate(issue.plantshortcode);
            var meetingDate = p.TpsMailDateTime();
            string link = WebHelper.Constants.AppPath + "Issues/SolveDetail/" + issue.IssueId.ToString();
            sb.AppendFormat("&emsp;&emsp;&emsp;&emsp;ระบบจัดการประชุม TPS ขอแจ้งว่า ผจ. ส่วน {0} มอบหมายให้ท่าน เป็นผู้นำเสนอ ในการประชุม Technical Problem Solving {1} ในวันที่ {2} <br /> ตามใบแจ้งซ่อมเลขที่ {3} ท่านสามารถกรอกรายละเอียด และแนบ File Presentation ตาม <a href=\"{4}\">Link</a> ", issue.WorkGroupNameTh, issue.PlantDisplayCode, meetingDate, issue.BillNo, link);
            sb.AppendLine("<br />");
            
            this.AppendIssueDetail(sb, issue);
            sb.AppendLine("<br />");
            sb.AppendLine("จึงเรียนมาเพื่อโปรดพิจารณา <br />");
            sb.AppendLine("ระบบจัดการประชุม TPS <br />");

            #endregion Content

            var res = new MailResult()
            {
                BCC = new List<MailAddress>(),
                CC = cc,
                Send = persenter,
                Content = sb.ToString(),
                Subject = subject,
                SubjectCC = subjectForCc,
                ContentCC = sb.ToString()
            };
            res.ResolveAddress();
            return res;
        }
        public MailResult ChangeApproverMail(string oldPresenterCode, string billNo, string workgroupnameTh)
        {
            #region Get Address

            
            var presenterObj = this.usersContext.GetByCode(oldPresenterCode);
            if (string.IsNullOrEmpty(presenterObj.EmailAddr))
            {
                throw new TpsUnauthorizedException("ไม่มีอีเมล์ของ Presenter ในระบบ");
            }
            var persenter = new List<MailAddress>() { new MailAddress(presenterObj.EmailAddr, presenterObj.FullName) };
            //var cc = new List<MailAddress>();
            //var creator = this.usersContext.GetByCode(issue.CreatedBy);
            //if (creator != null)
            //{
            //    if (!string.IsNullOrEmpty(creator.EmailAddr))
            //    {
            //        cc.Add(new MailAddress(creator.EmailAddr, creator.FullName));
            //    }
            //}
            //var facilitators = this.usersContext.GetByWorkGroup(this.context.GetMasterUnitCode()).Where(d => !string.IsNullOrEmpty(d.EmailAddr));
            //cc.AddRange(facilitators.Select(r => new MailAddress(r.EmailAddr, r.FullName)).ToList());

            #endregion Get Address

            #region Content

            var sb = new StringBuilder();
            var subject = string.Format("ระบบจัดการประชุม TPS : ขอแจ้งเปลี่ยนผู้นำเสนอการประชุม (ใบแจ้งซ่อมเลขที่ {0})", billNo);
            sb.AppendFormat("เรียน {0}", presenterObj.FullName);
            sb.AppendLine("<br /><br />");
            sb.AppendFormat("ระบบจัดการประชุม TPS ขอแจ้งว่า ผจ. ส่วน {0} ขอแจ้งเปลี่ยนผู้นำเสนอ  (ใบแจ้งซ่อมเลขที่ {1}) เป็นท่านอื่น", workgroupnameTh, billNo);
            sb.AppendLine("<br />");
            sb.AppendLine("จึงเรียนมาเพื่อโปรดพิจารณา <br />");
            sb.AppendLine("ระบบจัดการประชุม TPS <br />");

            #endregion Content

            var res = new MailResult()
            {
                BCC = new List<MailAddress>(),
                Send = persenter,
                Content = sb.ToString(),
                Subject = subject,
                CC = new List<MailAddress>()
            };
            res.ResolveAddress();
            return res;
        }

        public MailResult CancelBillMail(int issuesId)
        {
            #region Get Address

            var issue = this.context.GetById(issuesId);
            var send = new List<MailAddress>();
            var creator = this.usersContext.GetByCode(issue.CreatedBy);
            if (creator != null && !string.IsNullOrEmpty(creator.EmailAddr))
            {
                send.Add(new MailAddress(creator.EmailAddr, creator.FullName));
            }
            if (creator == null)
                creator = new PersonalInfoDto();
            var facilitators = this.usersContext.GetByWorkGroup(this.context.GetMasterUnitCode()).Where(d => !string.IsNullOrEmpty(d.EmailAddr));
            send.AddRange(facilitators.Select(r => new MailAddress(r.EmailAddr, r.FULLNAMETH)).ToList());
            var cc = new List<MailAddress>();

            #endregion Get Address

            #region Content

            var sb = new StringBuilder();
            var subject = string.Format("ระบบจัดการประชุม TPS : วาระการประชุมของท่านถูกยกเลิก  (ใบแจ้งซ่อมเลขที่ {0})", issue.BillNo);
            var link = WebHelper.Constants.AppPath + "Issues/EditInfo/" + issue.IssueId;
            sb.AppendFormat("เรียน {0} <br/>", creator.FullName);
            sb.AppendFormat("&emsp;&emsp;&emsp;&emsp;วาระการประชุมของท่านถูกยกเลิกเนื่องจาก {1} ท่านสามารถกรอกรายละเอียดเพิ่มเติมได้ตาม <a href=\"{0}\">Link</a> <br /> <br />", link, issue.CancelDetail);
            sb.AppendFormat("จึงเรียนมาเพื่อโปรดดำเนินการ <br />");
            sb.AppendFormat("ระบบจัดการประชุม TPS <br />");
            var content = sb.ToString();

            #endregion Content

            var res = new MailResult()
            {
                BCC = new List<MailAddress>(),
                CC = cc,
                Send = send,
                Content = content,
                Subject = subject
            }; 
            res.ResolveAddress();
            return res;
        }

        //public MailResult ReadyMail(int issuesId)
        //{

        //}

        public MailResult ClosedMail(int issuesId)
        {
            #region Get Address

            var issue = this.context.GetById(issuesId);
            var send = new List<MailAddress>();
            var cc = new List<MailAddress>();
            var bcc = new List<MailAddress>();
            var presenter = this.usersContext.GetByCode(issue.InchargeUserCode);
            if (presenter != null)
            {
                send.Add(new MailAddress(presenter.EmailAddr, presenter.FullName));
            }

            #endregion Get Address

            #region Content

            var meeting = this.context.GetLatestMeetingByIssueId(issuesId);
            DateTime meetingDate;
            if (meeting == null)
            {
                meetingDate = this.context.nextMeetingDate(issue.plantshortcode, 0);
            }
            else
            {
                meetingDate = meeting.MeetingDate;
            }
                

            var subject = string.Format("ระบบจัดการประชุม TPS : ขอโปรดจัดทำ Knowledge Management (ใบแจ้งซ่อมเลขที่ {0})", issue.BillNo);
            var sb = new StringBuilder();
            sb.AppendFormat("เรียน {0}", this.usersContext.GetByCode(issue.InchargeUserCode).FullName);
            sb.AppendLine("<br /><br />");
            sb.AppendFormat("&emsp;&emsp;&emsp;&emsp;ระบบจัดการประชุม TPS ขอแจ้งให้ท่านทราบว่า มติที่ประชุม Technical Problem Solving {0} ในวันที่ {1} ได้มีการพิจารณาอนุมัติปิดวาระเรียบร้อยแล้ว ", issue.PlantDisplayCode, meetingDate.TpsMailDateTime());
            sb.AppendFormat(" ขอให้ท่านดำเนินการจัดทำ Knowledge Management  และแนบ File ตาม <a href=\"{0}\">Link</a>", WebHelper.Constants.AppPath + "Issues/KM/" + issuesId);
            sb.AppendLine("<br />");
            sb.AppendLine("<br />");
            this.AppendIssueSummary(sb, issue);
            sb.AppendLine("<br />");
            sb.AppendLine("จึงเรียนมาเพื่อโปรดพิจารณา <br />");
            sb.AppendLine("ระบบจัดการประชุม TPS <br />");

            #endregion

            var res = new MailResult()
            {
                BCC = new List<MailAddress>(),
                CC = cc,
                Send = send,
                Content = sb.ToString(),
                Subject = subject
            };
            res.ResolveAddress();
            return res;
        }

        public MailResult RecordedMail(int issueId)
        {
            #region Get Address

            var issue = this.context.GetById(issueId);
            var send = new List<MailAddress>();
            var cc = new List<MailAddress>();
            var bcc = new List<MailAddress>();

            var presenter = this.usersContext.GetByCode(issue.InchargeUserCode);
            if (string.IsNullOrEmpty(presenter.EmailAddr))
            {
                throw new TpsUnauthorizedException("ไม่มีอีเมล์ของ Presenter ในระบบ");
            }
            send.Add(new MailAddress(presenter.EmailAddr, presenter.FullName));


            #endregion Get Address

            #region Conent

            var meeting = this.context.GetLatestMeetingByIssueId(issueId);
            DateTime meetingDate;
            if (meeting == null)
            {
                meetingDate = this.context.nextMeetingDate(issue.plantshortcode, 0);
            }
            else
            {
                meetingDate = meeting.MeetingDate;
            }

            var subject = string.Format("ระบบจัดการประชุม TPS : ขอแจ้งปิดวาระการประชุม (ใบแจ้งซ่อมเลขที่ {0})", issue.BillNo);
            var sb = new StringBuilder();
            sb.AppendFormat("เรียน {0}", this.usersContext.GetByCode(issue.InchargeUserCode).FullName);
            sb.AppendLine("<br /><br />");
            sb.AppendFormat("&emsp;&emsp;&emsp;&emsp;ระบบจัดการประชุม TPS ขอแจ้งให้ท่านทราบว่า มติที่ประชุม Technical Problem Solving {0} ในวันที่ {1} ", issue.PlantDisplayCode, meetingDate.TpsMailDateTime());
            sb.AppendLine("<br />");
            sb.AppendFormat(" ให้สามารถปิดวาระดังกล่าวได้ ท่านสามารถเข้าตรวจสอบรายละเอียดมติที่ประชุม ตาม <a href=\"{0}\">Link</a> ", WebHelper.Constants.AppPath + "Issues/KM/" + issueId);
            sb.AppendLine("<br />");
            sb.AppendLine("<br />");
            this.AppendIssueSummary(sb, issue);
            sb.AppendLine("จึงเรียนมาเพื่อโปรดพิจารณา <br />");
            sb.AppendLine("ระบบจัดการประชุม TPS <br />");
            #endregion

            var res = new MailResult()
            {
                BCC = new List<MailAddress>(),
                CC = cc,
                Send = send,
                Content = sb.ToString(),
                Subject = subject
            };
            res.ResolveAddress();
            return res;
        }

        public MailResult ClosedToPendingMail(int issueId)
        {
            #region Get Address

            var issue = this.context.GetById(issueId);
            var send = new List<MailAddress>();
            var cc = new List<MailAddress>();
            var facilitators = this.usersContext.GetByWorkGroup(this.context.GetMasterUnitCode()).Where(r => !string.IsNullOrEmpty(r.EmailAddr));
            cc.AddRange(facilitators.Select(r => new MailAddress(r.EmailAddr, r.FullName)));
            var creator = this.usersContext.GetByCode(issue.CreatedBy);
            if (creator != null && !string.IsNullOrEmpty(creator.EmailAddr))
            {
                cc.Add(new MailAddress(creator.EmailAddr, creator.FullName));
            }
            var bcc = new List<string>();

            var presenter = this.usersContext.GetByCode(issue.InchargeUserCode);
            if (!string.IsNullOrEmpty(presenter.EmailAddr))
            {
                send.Add(new MailAddress(presenter.EmailAddr, presenter.FullName));
            }
            else
            {
                //.Where(r => !string.IsNullOrEmpty(r.EmailAddr))
                throw new TpsUnauthorizedException("ไม่มีอีเมล์ของ Presenter ในระบบ");
            }

            #endregion

            #region Content

            var meeting = this.context.GetLatestMeetingByIssueId(issueId);
            DateTime meetingDate;
            if (meeting == null)
            {
                meetingDate = this.context.nextMeetingDate(issue.plantshortcode, 0);
            }
            else
            {
                meetingDate = meeting.MeetingDate;
            }

            var subject = string.Format("ระบบจัดการประชุม TPS : ขอโปรดดำเนินการกรอกรายละเอียดวาระการประชุมเพิ่มเติม (ใบแจ้งซ่อมเลขที่ {0})", issue.BillNo);
            var subjectForCc = string.Format("ระบบจัดการประชุม TPS : วาระการประชุมของท่านได้รับการอนุมัติ (ใบแจ้งซ่อมเลขที่ {0})", issue.BillNo);
            var sb = new StringBuilder();
            sb.AppendFormat("เรียน {0}", this.usersContext.GetByCode(issue.InchargeUserCode).FullName);
            sb.AppendLine("<br /><br />");
            sb.AppendFormat("&emsp;&emsp;&emsp;&emsp;ระบบจัดการประชุม TPS ขอแจ้งว่าตามมติที่ ประชุม Technical Problem Solving {0} ในวันที่ {1} ตามใบแจ้งซ่อมเลขที่ {2}", issue.PlantDisplayCode, meetingDate.TpsMailDateTime(), issue.BillNo);

            string link = WebHelper.Constants.AppPath + "Issues/SolveDetail/" + issue.IssueId;
            //            ขอแจ้งให้ท่านกรอกรายละเอียดเพิ่ม และแก้ไข File Presentation ตาม Link \\...  และเข้ามานำเสนอในที่ประชุม Technical Problem Solving [กลุ่มโรงงาน] ในวันที่ [วันที่ประชุม(ครั้งถัดไป)] อีกครั้ง
            //[แสดงรายละเอียดทั้งหมดของวาระ]
            //[แสดงบันทึกมติที่ประชุม]
            var sum = this.context.GetSummaryById(issueId);

            sb.AppendFormat("<br /> ขอแจ้งให้ท่านกรอกรายละเอียดเพิ่ม และแก้ไข File Presentation ตาม <a href=\"{0}\">Link</a> และเข้ามานำเสนอในที่ประชุม Technical Problem Solving {1} ในวันที่ {2} อีกครั้ง", link, issue.PlantDisplayCode, this.context.nextMeetingDateOnMonth(issue.GroupId, sum.ResumMonth.GetValueOrDefault(1)).TpsMailDateTime());
            sb.AppendLine("<br />");
            this.AppendIssueDetail(sb, issue);
            sb.AppendLine("<br />");
            sb.AppendLine("<br />");
            this.AppendIssueSummary(sb, issue);
            sb.AppendLine("<br />");
            sb.AppendLine("จึงเรียนมาเพื่อโปรดพิจารณา <br />");
            sb.AppendLine("ระบบจัดการประชุม TPS <br />");
            #endregion

            var res = new MailResult()
            {
                BCC = new List<MailAddress>(),
                CC = cc,
                Send = send,
                Content = sb.ToString(),
                Subject = subject,
                SubjectCC = subjectForCc,
                ContentCC = sb.ToString()
            };
            res.ResolveAddress();
            return res;
        }

        public MailResult ShiftedMail(int groupId, DateTime shiftDate, DateTime oldMeetingDate)
        {
            var result = new MailResult();
            var groupData = this.context.GetGroupById(groupId);
            var allowPlants = this.context.GetPlantByGroup(groupId);
            var plants = allowPlants.Select(d => " " + d.plantdisplaycode).ToArray();
            var plantsString = string.Join(",", plants).Trim();
            var subject = string.Format("ระบบจัดการประชุม TPS : ขอเลื่อนงานประชุม Technical Problem Solving {0}", plantsString);
            result.Subject = subject;
            var sb = new StringBuilder();
            sb.AppendLine("เรียน ผจ.วบก. ผจ.ส่วน สังกัด วบก. และท่านผู้เกี่ยวข้อง <br />");
            sb.AppendFormat("ระบบจัดการประชุม TPS ขอแจ้งเลื่อนงานประชุม Technical Problem Solving {0} ในวันที่ {1} เป็นวันที่ {2} จึงเรียนมาเพื่อทราบ",
                plantsString,
                oldMeetingDate.ToString("dd-MMM-yyyy"),
                shiftDate.ToString("dd-MMM-yyyy"));
            result.Content = sb.ToString();
            result.Send = this.context.GetMailByGroup(groupId).Select(c => new MailAddress(c.EmailAddr)).ToList();
            result.CC = new List<MailAddress>();
            return result;
        }
    }

    public class MailResult
    {
        public List<MailAddress> Send { get; set; }

        public List<MailAddress> CC { get; set; }

        public List<MailAddress> BCC { get; set; }

        public string Subject { get; set; }

        public string SubjectCC { get; set; }

        public string Content { get; set; }

        public string ContentCC { get; set; }

        public void ResolveAddress()
        {
            this.Send = this.Send.GroupBy(r => r.Address).Select(r => r.First()).ToList();
            List<int> toRemoveIndex = new List<int>();
            for (int i = 0; i < this.CC.Count; i++)
            {
                if (this.Send.Count(d => d.Address == this.CC[i].Address) > 0)
                    toRemoveIndex.Add(i);
            }
            toRemoveIndex.ForEach(d => this.CC.RemoveAt(d));
            this.CC = this.CC.GroupBy(d => d.Address).Select(d => d.First()).ToList();
        }
    }
}