﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Web;
using TPSWorkflow.DAL;
using TPSWorkflow.DAL.DtoS;

namespace TPSWorkflow
{
    public enum MailType
    {
        Production = 0,
        TestPtt = 1,
        TestDev = 2
    }

    public class TpsMailGenerator
    {
        public List<TpsAgenda> Agendas { get; set; }

        public string GenerateHtmlBody()
        {
            if (this.Agendas == null)
                return null;
            var sb = new System.Text.StringBuilder();
            sb.AppendLine("<table border=\"1\" style=\"width:100%; border:solid black 1px; border-collapse:collapse;\">");
            int index = 1;
            foreach (var agenda in this.Agendas)
            {
                sb.AppendLine("<tr style=\"background-color:rgb(84,141,212);\">");
                sb.AppendFormat("<td style=\"width:200px; padding:0px; border:solid black 1px;\">วาระที่ {0}</td><td style=\" padding:0px; border:solid black 1px;\">{1}</td>", index, agenda.Topic);
                sb.AppendLine("</tr>");
                if (agenda.SubAgenda != null)
                {
                    sb.AppendLine("<tr style=\"background-color:rgb(219, 229, 241);\">");
                    sb.AppendLine("<td style=\"border:solid black 1px; padding:0px;\" >ลำดับ</td><td style=\"border:solid black 1px; padding:0px;\" >รายละเอียดการประชุม</td>");
                    sb.AppendLine("</tr>");
                    int subindex = 1;
                    foreach (var subAg in agenda.SubAgenda)
                    {
                        sb.AppendLine("<tr>");
                        sb.AppendFormat("<td style=\"border:solid black 1px; padding:0px;\" >{0}.{1}</td><td>{2}</td style=\"border:solid black 1px; padding:0px;\" >", index, subindex, subAg.Topic);
                        sb.AppendLine("</tr>");
                    }
                }
                index++;
            }

            sb.AppendLine("</table>");
            return sb.ToString();
        }

        public static string ResolveNewLineForHtml(string src)
        {
            if (src == null)
                return null;
            return src.Replace("\n", "<br />\n");
        }

        public static string HtmlSubAgFromIssue(issuesDto toMeet, string commentString = "")
        {
            var sb = new StringBuilder();
            var problemString = ResolveNewLineForHtml(toMeet.ProblemName);
            sb.AppendFormat("เรื่อง : {0} {1}", problemString, commentString);
            sb.AppendLine("<br />");
            var workGroup = ResolveNewLineForHtml(toMeet.WorkGroupNameTh);
            sb.AppendFormat("หน่วยงาน : {0}", workGroup);
            sb.AppendLine("<br />");
            var problemDetail = ResolveNewLineForHtml(toMeet.ProblemDetail);
            sb.AppendFormat("รายละเอียด : {0}", problemDetail);
            sb.AppendLine("<br />");
            var possibleCauses = ResolveNewLineForHtml(toMeet.problem.PossibleCauses);
            sb.AppendFormat("สาเหตุ : {0}", possibleCauses);
            sb.AppendLine("<br />");
            var solveDirection = ResolveNewLineForHtml(toMeet.problem.SolveDirection);
            sb.AppendFormat("การแก้ไขเบื้องต้น : {0}", solveDirection);
            sb.AppendLine("<br />");
            var summary = toMeet.summary == null ? new summaryDto() : toMeet.summary;
            var suggestion = ResolveNewLineForHtml(summary.Suggestion);
            sb.AppendFormat("ข้อเสนอแนะ : {0}", suggestion);
            sb.AppendLine("<br />");
            var finalize = ResolveNewLineForHtml(summary.Finalize);
            sb.AppendFormat("มติที่ประชุม: {0}", finalize);
            sb.AppendLine("<br />");
            
            string final = string.Empty;
            switch (toMeet.StatusEnum)
            {
                case IssuesStatus.PendingAfterMeeting:
                    final = string.Format("นำกลับเข้าที่ประชุมใหม่เดือน {0}", CultureInfo.GetCultureInfo("th-TH").DateTimeFormat.GetMonthName(toMeet.summary.ResumMonth.GetValueOrDefault(1)));
                    break;
                case IssuesStatus.Recorded:
                case IssuesStatus.ClosedWaitKM:
                    final = "ปิด"; break;
                default:
                    final="ยังมิได้ลงผล";break;
            }
            sb.AppendFormat("สถานะ : {0}", final);
            return sb.ToString();
        }
    }

    public class TpsAgenda
    {
        public string Topic { get; set; }
        public int IssueId { get; set; }

        public List<TpsAgenda> SubAgenda { get; set; }

        public TpsAgenda()
        {
            this.SubAgenda = new List<TpsAgenda>();
        }
    }

    public class MailWriter
    {
        private const string mailSender = "TPS System";

        protected string serverName = string.Empty;
        protected string username = string.Empty;
        protected string password = string.Empty;
        protected string htmlContent = string.Empty;
        protected SmtpClient smtp = null;
        protected string mailName = string.Empty;
        protected MailAddress fromAddress = null;
        protected MailMessage myMessage = null;
        protected List<LinkedResource> linkPic = null;
        protected IMailLogger mailLogger = null;

        protected MailType GetMailConfig
        {
            get
            {
                return MailWriter.MailConfig;
            }
        }

        public static MailType MailConfig
        {
            get
            {
                switch (Properties.Settings.Default.IsTestMail)
                {
                    case 'y': return MailType.TestPtt;
                    case 'n': return MailType.Production;
                    default:
                        return MailType.TestDev;
                }
            }
        }

        protected void ResolveMailForEnvironment(MailResult content, bool isCc)
        {
            if (this.GetMailConfig == MailType.TestDev)
            {
                this.ClearReciever();
                this.ClearCC();
                this.AddReciever("chakrit.lj@gmail.com", "Chakrit");
                this.AddReciever("kanda@fireoneone.com", "Kanda");
                var mailToList = string.Join(";", content.Send.Select(d => d.Address).ToArray());
                var mailCCList = string.Join(";", content.CC.Select(d => d.Address).ToArray());
                if (isCc)
                {
                    content.ContentCC += string.Format("\r\n CC: {0}", mailCCList);
                    this.SetBody(content.ContentCC);
                }
                else
                {
                    content.Content += string.Format("\r\n To : {0}", mailToList);
                    this.SetBody(content.Content);
                }
            }
            else if (this.GetMailConfig == MailType.TestPtt)
            {
                this.ClearReciever();
                this.ClearCC();
                if (!isCc)
                {
                    if (!string.IsNullOrEmpty(Properties.Settings.Default.TestMailTo.Trim()))
                    {
                        var recieverMails = Properties.Settings.Default.TestMailTo.Split(';');
                        foreach (var m in recieverMails)
                        {
                            this.AddReciever(m, "TPS User");
                        }
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(Properties.Settings.Default.TestMailCc.Trim()))
                    {
                        var ccMails = Properties.Settings.Default.TestMailCc.Split(';');
                        foreach (var m in ccMails)
                        {
                            this.AddReciever(m, "TPS User");
                        }
                    }
                }
               
                var mailToList = string.Join(";", content.Send.Select(d => d.Address).ToArray());
                var mailCCList = string.Join(";", content.CC.Select(d => d.Address).ToArray());
                if (isCc)
                {
                    content.ContentCC += string.Format("\r\n CC: {0}", mailCCList);
                    this.SetBody(content.ContentCC);
                }
                else
                {
                    content.Content += string.Format("\r\n To : {0}", mailToList, mailCCList);
                    this.SetBody(content.Content);
                }
            }
        }

        public List<string> mailReciever
        {
            get
            {
                return this.myMessage.To.Select(r => r.Address).ToList();
            }
        }

        private string Body { get; set; }

        private MailWriter()
        {
            this.linkPic = new List<LinkedResource>();
        }

        public static MailWriter Create(string serverName, string username, string password, IMailLogger mailLog)
        {
            var res = new MailWriter();

            if (!string.IsNullOrEmpty(username))
            {
                res.fromAddress = new MailAddress(username, mailSender);
            }
            //var toAddress = new MailAddress("chakrit.lj@ecobz.com", "To Name");
            string fromPassword = password;

#if (!GGMAIL)
            res.smtp = new SmtpClient
            {
                Host = serverName,
                Port = 25,
                EnableSsl = false,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                //UseDefaultCredentials = false,
                Timeout = 180000,
                //Credentials = null
            };
#else   
            res.smtp = new SmtpClient
            {
                Host = serverName,
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Timeout = 180000,
                Credentials = null
            };

#endif

            if (string.IsNullOrEmpty(password))
            {
                res.smtp.UseDefaultCredentials = false;
                res.smtp.Credentials = null;
            }
            else
            {
                res.smtp.Credentials = new NetworkCredential(res.fromAddress.Address, fromPassword);
            }
            res.myMessage = new MailMessage();
            if (res.fromAddress != null)
            {
                res.myMessage.From = res.fromAddress;
            }
            res.mailLogger = mailLog;
            return res;
        }

        public void SetBody(string htmlBody)
        {
            this.myMessage.Body = htmlBody;
            this.htmlContent = htmlBody;
        }

        public void SetSubject(string subject)
        {
            this.myMessage.Subject = subject;
        }

        public void AddReciever(string targetMail, string targetName)
        {
            if (string.IsNullOrEmpty(targetMail))
            {
                return;
            }
            var toAddress = new MailAddress(targetMail, targetName);
            this.myMessage.To.Add(toAddress);
        }

        public void ClearReciever()
        {
            this.myMessage.To.Clear();
        }
        
        public void AddCC(string targetMail, string targetName)
        {
            var toAddress = new MailAddress(targetMail, targetName);
            this.myMessage.CC.Add(toAddress);
        }

        public void ClearCC()
        {
            this.myMessage.CC.Clear();
        }

        public void SetImage(string index, string imagePath, string imageType)
        {
            var pic = new LinkedResource(imagePath, imageType);
            pic.ContentId = index;
            this.linkPic.Add(pic);

        }

        public void Send(MailLogType mailType, int? groupId)
        {

            AlternateView htmlView = AlternateView.CreateAlternateViewFromString(htmlContent);
            htmlView.ContentType = new System.Net.Mime.ContentType("text/html");
            foreach (var pic in this.linkPic)
            {
                htmlView.LinkedResources.Add(pic);
            }
            myMessage.AlternateViews.Add(htmlView);
            
            try
            {
                ServicePointManager.ServerCertificateValidationCallback = delegate(object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
                {
                    return true;
                };
                smtp.Send(myMessage);
                var mailLogger = new TextMailLogger(TPSPathHelper.GetLogFilePath());
                mailLogger.AddLog(this.myMessage.To.Select(c => c.Address).ToList(),
                    this.myMessage.CC.Select(c => c.Address).ToList(),
                    this.myMessage.Subject,
                    true);
                this.mailLogger.LogMail(string.Join(",", myMessage.To.Select(c => c.Address).ToArray()),
                                            string.Join(",", myMessage.CC.Select(c => c.Address).ToArray()),
                                            myMessage.Body,
                                            mailType, true, "", groupId);
            }
            catch (SmtpException ex)
            {
                this.mailLogger.LogMail(string.Join(",", myMessage.To.Select(c => c.Address).ToArray()),
                                        string.Join(",", myMessage.CC.Select(c => c.Address).ToArray()),
                                        myMessage.Body,
                                        mailType, false, ex.Message, groupId);
                throw;
            }
            

        }

        public void SetByContentAll(MailResult content)
        {
            content.Send.ForEach(r => this.AddReciever(r.Address, r.DisplayName));
            content.CC.ForEach(r => this.AddCC(r.Address, r.DisplayName));
            this.SetSubject(content.Subject);
            this.SetBody(content.Content);
            this.ResolveMailForEnvironment(content,false); 
        }

        public void SetByContentTo(MailResult content)
        {
            content.Send.ForEach(r => this.AddReciever(r.Address, r.DisplayName));
            this.SetSubject(content.Subject);
            this.SetBody(content.Content);
            this.ResolveMailForEnvironment(content, false);
        }

        public void SetByContentCC(MailResult content)
        {
            content.CC.ForEach(r => this.AddReciever(r.Address, r.DisplayName));
            this.SetSubject(content.SubjectCC);
            if (string.IsNullOrEmpty(content.ContentCC))
            {
                this.SetBody(content.Content);
            }
            else
            {
                this.SetBody(content.ContentCC);
            }
            this.ResolveMailForEnvironment(content, true);
        }

        public void AddAlternateView(AlternateView view)
        {
            this.myMessage.AlternateViews.Add(view);
        }
    }

    
}