﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using TPSWorkflow.DAL;
using System.Net.Mail;
using TPSWorkflow.DAL.DtoS;
using Ecobz.Utilities;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;
using System.Text;

namespace TPSWorkflow.Controllers
{
    public class SystemController : BaseController
    {
        protected IIssuesData issuesDataAccess = null;
        protected IUsersManage userManage = null;


        public SystemController(IIssuesData access, IUsersManage user)
        {
            this.issuesDataAccess = access;

            this.userManage = user;
        }

        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            base.OnActionExecuted(filterContext);
            this.issuesDataAccess.SetPermission(this.tpsSession.userInfo, this.userManage);
        }

        public override void InjectSessionForTesting(HttpSessionStateBase session)
        {
            base.InjectSessionForTesting(session);
            this.issuesDataAccess.SetPermission(this.tpsSession.userInfo, this.userManage);
        }

        private IDictionary<string, string> GetDictionary(allowplantDto data, bool isIncludeUrl)
        {
            var res = new Dictionary<string, string>();
            res.Add("plantshortcode", data.plantshortcode);
            res.Add("plantdisplaycode", data.plantdisplaycode);

            if (isIncludeUrl)
            {
                res.Add("url", string.Format("System/EditPlant?id={0}", data.plantshortcode));
                res.Add("delurl", string.Format("System/DelPlant?id={0}", data.plantshortcode));
            }
            return res;
        }

        private List<string> GetPlantFieldsDisplayName()
        {
            return new List<string>() { "Plant Id", "Plant Display Name" };
        }

        #region Plant

        [TPSAuthen]
        public ActionResult Plant()
        {
            if (this.tpsSession.userInfo == null || this.tpsSession.userInfo.TpsType != TpsUserType.SuperAdmin)
            {
                return Content("Only Super Admin have permission to edit plant");
            }
            var data = this.issuesDataAccess.GetSystemAllowPlants();
            var dataLists = data.Select(d => GetDictionary(d, true)).ToArray();
            var fLists = dataLists.FirstOrDefault().Select(d => d.Key).ToList();
            var v = View("SimpleTable");
            v.ViewData["fields"] = fLists;
            v.ViewData["records"] = dataLists;
            return v;
        }

        [TPSAuthen]
        public ActionResult EditPlant()
        {
            var id = Request.Params["id"];
            var data = GetDictionary(this.issuesDataAccess.GetSystemAllowPlant(id), false);

            var fLists = data.Select(r => r.Key).ToList();
            var v = View("SimpleEdit");
            v.ViewData["fields"] = fLists;
            v.ViewData["data"] = data;
            v.ViewData["submiturl"] = "System/SubmitPlant";
            return v;
        }

        [TPSAuthen]
        public ActionResult SubmitPlant()
        {
            var d = ReflectionUtils.NameValueCollectionToObject<allowplantDto>(Request.Params);
            if (this.issuesDataAccess.EditPlant(d.plantshortcode, d) != null)
            {
                return RedirectToAction("Plant");
            }
            else
            {
                throw new TpsUnauthorizedException("Id Incorrect");
            }
        }

        [TPSAuthen]
        public ActionResult AddPlant()
        {
            var orgdata = GetDictionary(this.issuesDataAccess.GetSystemAllowPlants().FirstOrDefault(), false);
            var data = new Dictionary<string, string>();
            foreach (var key in orgdata.Keys)
            {
                data.Add(key, string.Empty);
            }
            var fLists = data.Select(r => r.Key).ToList();
            var v = View("SimpleEdit");
            v.ViewData["fields"] = fLists;
            v.ViewData["data"] = data;
            v.ViewData["submiturl"] = "System/SubmitPlant";
            return v;

        }
        [TPSAuthen]
        public ActionResult DelPlant()
        {
            var id = Request.Params["id"];
            this.issuesDataAccess.DeletePlant(id);
            return RedirectToAction("Plant");
        }
        #endregion

        public ActionResult CheckMode()
        {
            string mode = "";
#if TESTMAIL
            mode = "TEST EMAIL";
#endif
            return Json(mode, JsonRequestBehavior.AllowGet);
        }

        public ActionResult TestEmail()
        {
            return null;
            //var x = MailWriter.Create("e2k10-hubrelay.ptt.corp", "tpsmeeting_test@pttplc.com", string.Empty);
            ////x.mailReciever.Add("chakrit.lj@gmail.com");
            //x.SetBody("My Body");
            //x.SetSubject("My Subject");
            //x.AddReciever("chakrit.lj@gmail.com", "To My Dear");
            //x.Send();
            //return null;

            //var smtp = new System.Net.Mail.SmtpClient("e2k10-hubrelay.ptt.corp", 25);
            //smtp.UseDefaultCredentials = false;
            //smtp.Credentials = null;
            //smtp.EnableSsl = false;
            //ServicePointManager.ServerCertificateValidationCallback = delegate(object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
            //{
            //    return true;
            //};
            //var message = new System.Net.Mail.MailMessage()
            //{
            //    From = new System.Net.Mail.MailAddress("example234@ptt.corp"),
            //    Subject = "Hello",
            //    Body = "TPS TESTING",
            //};

            //message.To.Add(new MailAddress("chakrit.lj@gmail.com"));
            //smtp.Send(message);
            //return Json(System.Net.CredentialCache.DefaultNetworkCredentials, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetServerPath()
        {
            return Content(Server.MapPath("~/"));
        }

        [HttpPost]
        public ActionResult AdminLogin(string mockuser)
        {
            var userInfo = this.tpsSession.userInfo;
            if (userInfo == null)
                return null;
            if (userInfo.UserType == UserSource.TPS && userInfo.TpsType == TpsUserType.SuperAdmin)
            {
                var authenRes = this.userManage.GetByUsername(mockuser);
                if (authenRes != null)
                {
                    this.userManage.LogUserMock(userInfo.Username, authenRes.CODE, Request.UserHostAddress);
                    this.tpsSession.userInfo = authenRes;
                    this.tpsSession.Rights = userManage.GetUserRights(authenRes);
                    this.tpsSession.IsMaster = authenRes.UNITCODE == this.issuesDataAccess.GetMasterUnitCode();
                    this.tpsSession.MockFromUser = userInfo.Username;
                    return Json("Success");
                }
            }

            return Json("Authentication Failed");
        }

        public ActionResult MinDate()
        {
            return Json(System.Data.SqlTypes.SqlDateTime.MinValue.Value, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetActivitiesLog()
        {
            var userInfo = this.tpsSession.userInfo;
            if (userInfo == null)
                return null;
            var startDateString = Request.Params["start"];
            DateTime startDate = System.Data.SqlTypes.SqlDateTime.MinValue.Value;
            DateTime endDate = System.Data.SqlTypes.SqlDateTime.MaxValue.Value;
            var endDateString = Request.Params["end"];
            if (!string.IsNullOrEmpty(startDateString))
                startDate = startDateString.ParseDatePickerString();
            if (!string.IsNullOrEmpty(endDateString))
                endDate = endDateString.ParseDatePickerString();
            if (userInfo.UserType == UserSource.TPS && userInfo.TpsType == TpsUserType.SuperAdmin)
            {
                var activities = this.issuesDataAccess.GetActivities(startDate, endDate);
                var sb = new StringBuilder();

                sb.AppendLine("วันและเวลา, Activity,ผู้สวมสิทธิ์, IP เครื่องที่ล็อกอิน, รหัสพนักงานผู้ถูกสวมสิทธิ์, ใบแจ้งซ่อมที่เกี่ยวข้อง");
                foreach (var entity in activities)
                {
                    sb.AppendFormat("{0},{1},{2},{3},{4},{5}", entity.LogTimeStamp.ToString(), entity.Description, entity.Usercode, entity.IPAddress, entity.AsUsercode, entity.BillNo);
                    sb.AppendLine();
                }
                return File(Encoding.UTF8.GetBytes(sb.ToString()), "text/csv", "ActivityLog.csv");
                
            }
            return Content("No permission");
        }
    }
}