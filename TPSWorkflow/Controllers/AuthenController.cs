﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TPSWorkflow.DAL;

namespace TPSWorkflow.Controllers
{

 
    [Authorize]
    [AllowAnonymous]
    public class AuthenController : BaseController
    {
        //
        // GET: /Authen/
        protected IUsersManage usersManage;
        protected IIssuesData tpsDal;

        private bool validateBruteForceAttach()
        {
            int attempts = 0;

            if (System.Web.HttpContext.Current.Session["attemps"] != null)
            {
                attempts = (int)System.Web.HttpContext.Current.Session["attemps"];
            }
            else
            {
                System.Web.HttpContext.Current.Session.Add("attemps", 0);
            }
            attempts++;
            System.Web.HttpContext.Current.Session["attemps"] = attempts;
            if (attempts >= 2)
            {
                System.Web.HttpContext.Current.Session["attemps"] = 0;
                return true;
            }
            else
            {
                return false;
            }
        }

        public AuthenController(IUsersManage usersManage, IIssuesData issueData)
        {
            this.usersManage = usersManage;
            this.tpsDal = issueData;
           
            try
            {
                string winUsername;
                try
                {
                    winUsername = System.Web.HttpContext.Current.User.Identity.Name;
                }
                catch
                {
                    winUsername = string.Empty;
                }
//#if DEVELOPING
//                winUsername = "490009";
//#endif

                if (!string.IsNullOrEmpty(winUsername) && this.tpsSession.userInfo == null)
                {
                    var domainSlash = winUsername.IndexOf('\\');
                    if (domainSlash >= 0)
                    {
                        winUsername = winUsername.Substring(domainSlash + 1);
                    }
                    var pInfo = usersManage.GetByUsername(winUsername);
                    if (pInfo != null)
                    {
                        this.tpsSession.userInfo = pInfo;
                        this.tpsSession.Rights = usersManage.GetUserRights(pInfo);
                        this.tpsSession.IsMaster = pInfo.UNITCODE == tpsDal.GetMasterUnitCode();
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        [Authorize]
        public ActionResult Index()
        {
            Response.TrySkipIisCustomErrors = true;
            if (this.tpsSession.userInfo != null)
            {
                var returnUrl = Request.Params["returnUrl"];
                if (string.IsNullOrEmpty(returnUrl))
                    returnUrl = TempData["returnUrl"] as string;
                if (!string.IsNullOrEmpty(returnUrl))
                {
                    if (returnUrl.Contains("http") || returnUrl.Contains("https"))
                    {
                        return Redirect(returnUrl);
                    }
                    else
                    {
                        return Redirect(WebHelper.Constants.AppPath + returnUrl);
                    }
                }
                return RedirectToAction("Index", "Summary");
            }
            var returnView = View();
            if (TempData["ErrorMessage"] != null)
                returnView.ViewData["ErrMessage"] = TempData["ErrorMessage"];
            else
                returnView.ViewData["ErrMessage"] = string.Empty;
            var winUsername = System.Web.HttpContext.Current.User.Identity.Name;
            returnView.ViewData["Authen"] = winUsername;
            return returnView;
        }

        public ActionResult Relogin()
        {
            var returnView = View("Index");
            if (TempData["ErrorMessage"] != null)
                returnView.ViewData["ErrMessage"] = TempData["ErrorMessage"];
            else
                returnView.ViewData["ErrMessage"] = string.Empty;
            var winUsername = System.Web.HttpContext.Current.User.Identity.Name;
            returnView.ViewData["Authen"] = winUsername;
            return returnView;
        }


        [AllowAnonymous]
        
        public ActionResult Authen()
        {
            if (Session["nextLoginDateTime"] != null)
            {
                var nextDateTime = (DateTime)Session["nextLoginDateTime"];
                if (DateTime.Now > nextDateTime)
                {
                    Session["nextLoginDateTime"] = null;
                }
                else
                {
                    return Content("Your user has been lockout due to too many wrong passwords sent");
                }
            }
            string returnUrl = null;
            try
            {
                var username = Request.Params["username"];
                var password = Request.Params["password"];
                returnUrl = Request.Params["returnUrl"];

                var authenRes = usersManage.GetByAuthen(username, password);
                if (authenRes != null)
                {
                    this.tpsSession.ClearData();
                    this.tpsSession.userInfo = authenRes;
                    this.tpsSession.Rights = usersManage.GetUserRights(authenRes);
                    
                    this.tpsSession.IsMaster = authenRes.UNITCODE == tpsDal.GetMasterUnitCode() || authenRes.TpsType == DAL.DtoS.TpsUserType.SuperAdmin; 
                }
                else
                {
                    //if (Properties.Settings.Default.IsTestMail == 'y')
                    //{
                    //    authenRes = usersManage.GetByUsername(username);
                    //    if (authenRes != null)
                    //    {
                    //        this.tpsSession.userInfo = authenRes;
                    //        this.tpsSession.Rights = usersManage.GetUserRights(authenRes);
                    //        this.tpsSession.IsMaster = authenRes.UNITCODE == tpsDal.GetMasterUnitCode();
                    //        TempData["returnUrl"] = returnUrl;
                    //        return RedirectToAction("Index");
                    //    }
                    //    else
                    //    {
                    //        if (validateBruteForceAttach())
                    //        {
                    //            System.Web.HttpContext.Current.Session["nextLoginDateTime"] = DateTime.Now.AddMinutes(5);
                    //        }
                    //    }
                    //}
                    //else
                    //{
                    if (validateBruteForceAttach())
                    {
                        System.Web.HttpContext.Current.Session["nextLoginDateTime"] = DateTime.Now.AddMinutes(5);
                    }
                    //}

                    TempData["ErrorMessage"] = "ไม่มีผู้ใช้อยู่ในระบบ";
                }

                
            }
            catch (Exception ex)
            {
                TempData["ErrorMessage"] = ex.Message;
            }
            TempData["returnUrl"] = returnUrl;
            return RedirectToAction("Index");
        }

        [TPSAuthen]
        public ActionResult TestNoAuthen()
        {
            return null;
        }

        [AllowAnonymous]
        public ActionResult InvokeScheduler()
        {
            var exeFile = Server.MapPath("~/TpsConfigurator.exe");
            var startInfo = new System.Diagnostics.ProcessStartInfo(exeFile);
            var p = new System.Diagnostics.Process();
            p.StartInfo = startInfo;
            p.StartInfo.UserName = "Administrator";
            var pwd = new System.Security.SecureString();
            foreach (var c in "ecobzThKv")
            {
                pwd.AppendChar(c);
            }
            p.StartInfo.Password = pwd;
            p.StartInfo.UseShellExecute = false;
            p.StartInfo.CreateNoWindow = false;
            if (p.Start())
            {
                return Content(exeFile);
            }
            else
            {
                return Content("Cannot Start Process");
            }
            
        }

        [AllowAnonymous]
        public ActionResult TestAuthen()
        {
            var winUsername = System.Web.HttpContext.Current.User.Identity.Name;
            var sb = new System.Text.StringBuilder();
            sb.AppendFormat("Windows Username = {0} <br/>", winUsername);
            string winUsernameProcessed = string.Empty;
            var domainSlash = winUsername.IndexOf('\\');
            if (domainSlash >= 0)
            {
                winUsernameProcessed = winUsername.Substring(domainSlash + 1);
            }
            else
            {
                winUsernameProcessed = winUsername;
            }
            sb.AppendFormat("Username without domain = {0} <br />", winUsernameProcessed);
            return Content(sb.ToString(), "Text/HTML");
        }
    }
}
