﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using TPSWorkflow.DAL;
using TPSWorkflow.DAL.DtoS;

namespace TPSWorkflow.Controllers
{
    [TPSAuthen]
    public class MemoController : BaseController
    {
        //
        // GET: /Memo/

        protected IIssuesData issuesDataAccess = null;
        protected IUsersManage userManage = null;
        protected IMailLogger mailLogger = null;

        protected string GetMeetingInfoString(meetingDto meeting)
        {
            string plantsString = meeting.Plants;
            string meetingInfoString = string.Format("ครั้งที่ {0}/{1} โรงแยกก๊าซ {2}", meeting.MeetingOrder, (meeting.MeetingDate.Year + 543), plantsString);
            return meetingInfoString;
        }

        protected bool CheckEditPermission()
        {
            var perm = this.userManage.GetUserRights(tpsSession.userInfo);
#if DEVELOPING
            perm |= UserRights.SystemAdministrator;
#endif
            if (this.tpsSession.userInfo.UNITCODE == this.issuesDataAccess.GetMasterUnitCode())
            {
                return true;
            }
            return false;
        }

        public MemoController(IIssuesData access, IUsersManage user, IMailLogger mailLog)
        {
            this.issuesDataAccess = access;
            this.mailLogger = mailLog;
            this.userManage = user;
        }
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);
            this.issuesDataAccess.SetPermission(this.tpsSession.userInfo, this.userManage);
        }

        public override void InjectSessionForTesting(HttpSessionStateBase session)
        {
            base.InjectSessionForTesting(session);
            this.issuesDataAccess.SetPermission(this.tpsSession.userInfo, this.userManage);
        }

        public ActionResult Index()
        {
            var resView = View("Table");
            resView.ViewData["groups"] = this.issuesDataAccess.GetAllGroups();
            resView.ViewData["isCanBreakDown"] = (this.tpsSession.userInfo.UNITCODE == this.issuesDataAccess.GetMasterUnitCode() || this.tpsSession.userInfo.TpsType == TpsUserType.SuperAdmin);
            return resView;
        }

        public ActionResult TableSource()
        {

                var tablePrms = Ecobiz.WebUI.DataTables.ServerTableParams.RetrieveFromRequest(Request.Params);
                var dateStr = Request.Params["datesearch"];
                var dateEnd = Request.Params["dateend"];
                var groupStr = Request.Params["groupSearch"];
                var statusSearch = Request.Params["statusSearch"];
                int statusInt = 0;
                int.TryParse(statusSearch, out statusInt);
                var searchPrms = new MemoSearchParams()
                {
                    SortField = (MemoField)tablePrms.SortCols[0].ColumnNo,
                    IsDesc = tablePrms.SortCols[0].Direction == Ecobiz.WebUI.DataTables.SortDirection.Descending,
                    StatusSearch = (MemoStatus)statusInt
                };
                if (!string.IsNullOrEmpty(dateStr))
                    searchPrms.dateSearchStart = DateTime.ParseExact(dateStr, "dd-MMM-yyyy", new CultureInfo("en-US"));
                if (!string.IsNullOrEmpty(dateEnd))
                    searchPrms.dateSearchEnd = DateTime.ParseExact(dateEnd, "dd-MMM-yyyy", new CultureInfo("en-US"));
                if (!string.IsNullOrEmpty(groupStr))
                    searchPrms.GroupId = int.Parse(Request.Params["groupSearch"]);
                else
                    searchPrms.GroupId = -1;
                searchPrms.IsFilterActive = null;
                int allCount = 0;
                var res = this.issuesDataAccess.GetBySearchAndSort(searchPrms, tablePrms.Start, tablePrms.Count, out allCount);
                if (res == null)
                    return null;
                if (searchPrms.SortField == MemoField.UpdateBy)
                {
                    res = res.OrderBy(d => d.memo.UpdateByFullName).ToList();
                }

                var tableRes = Ecobiz.WebUI.DataTables.TableData<DAL.DtoS.meetingDto>.CreateData(tablePrms.Echo, allCount, allCount, res);

               return Json(tableRes, JsonRequestBehavior.AllowGet);

        }

        public ActionResult Edit(int id)
        {
            this.issuesDataAccess.ResolveMeetingIssuesRelationType(id);
            var memo = this.issuesDataAccess.GetMeetingById(id);
            if (memo == null)
                throw new KeyNotFoundException();
            var resView = View("Edit", memo);
            resView.ViewData["isEditable"] = this.CheckEditPermission();
            var mailTemplate = this.issuesDataAccess.GetMailTemplate();
            resView.ViewData["mailTemplate"] = mailTemplate;
            resView.ViewData["Agendas"] = this.AgendasFromMeeting(memo, GetMeetingInfoString(memo));

            return resView;
        }

        public ActionResult SubmitMemo()
        {
            var jsonText = Request.Params["json"];
            var submitDtos = Newtonsoft.Json.JsonConvert.DeserializeObject<meetingDto>(jsonText);
            var isShiftStatus = Request.Params["Preview"] != "1";
            try
            {
                if (!this.CheckEditPermission())
                {
                    throw new TpsUnauthorizedException("You don't have permission to save memo");
                }
                if (this.issuesDataAccess.SaveMemo(submitDtos.memo, isShiftStatus) && this.issuesDataAccess.UpdateAttendees(submitDtos.MeetingId, submitDtos.meeting_attendees))
                {
                    return Json(WebHelper.APIResult.CreateResult("Success"));
                }
                else
                {
                    return Json(WebHelper.APIResult.ErrorResult("Unexpected error"));
                }
            }
            catch (Exception ex)
            {
                return Json(WebHelper.APIResult.ExceptionResult(ex));
            }
        }

        public ActionResult testView()
        {
            //var viewResult = View("MemoMail");
            //string x;
            //using (var sw = new StringWriter())
            //{
            //    var viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
            //    viewResult.View.Render(viewContext, sw);
            //    x = sw.GetStringBuilder().ToString();
            //}
            return null;

        }

        public ActionResult ExportMemo()
        {
            var meetingId = int.Parse(Request.Params["meetingId"]);
            var mailSender = MailWriter.Create(Properties.Settings.Default.MailServer, Properties.Settings.Default.MailUser, Properties.Settings.Default.MailPassword, this.mailLogger);
            var meeting = this.issuesDataAccess.GetMeetingById(meetingId);
            var isPreviewText = Request.Params["IsPreview"];
            var isPreview = !string.IsNullOrEmpty(isPreviewText);
            //if (!meeting.IsMeetingReady())
            //{
            //    return Content("Meeting is not ready for exporting");
            //}
            if (!meeting.IsMemo && !isPreview)
            {
                return Content("Meeting is not ready for exporting");
            }
            if (meeting.IsForcePdf == true)
            {
                if (!System.IO.File.Exists(meeting.PdfLocalPath))
                    return Content("Meeting PDF not found");
                return File(meeting.PdfLocalPath, MimeMapping.GetMimeMapping(meeting.PdfLocalPath));
            }
            var ags = this.AgendasFromMeeting(meeting, GetMeetingInfoString(meeting));
            
            var mailTeamplate = this.issuesDataAccess.GetMailTemplate();
            var groupSetting = this.issuesDataAccess.GetGroupById(meeting.GroupId);
            var viewModel = new MemoMailViewModel()
            {
                Agendas = ags,
                MailTemplate = mailTeamplate,
                Mode = MailViewMode.PDF,
                LastMeetingId = this.issuesDataAccess.GetBeforeMeetingId(meeting),
                meetingInfo = meeting,
                StartMeetingTimeString = groupSetting.TimeStart.Value.ToString("HH:mm"),
                EndMeetingTimeString = groupSetting.TimeEnd.Value.ToString("HH:mm"),
                IsPreview = isPreview
            };
            var ToList = this.issuesDataAccess.GetMailByMeeting(meeting.MeetingId).ToList();
            viewModel.Attendees = ToList;

            Response.Charset = "UTF-8";
            return ViewPdf(string.Empty, "MemoMail", viewModel, "application/octet-stream");
            //return View("MeMoMail", ags);
            //return new RazorPDF.PdfResult(ags, "MemoMail");
        }

        private List<TpsAgenda> AgendasFromMeeting(meetingDto meeting, string meetingInfoString)
        {

            foreach (var mtg in meeting.meeting_issue)
            {
                mtg.issues.problem = this.issuesDataAccess.GetProblemDetailByIssue(mtg.IssueId);
            }
            var mailContent = new TpsMailGenerator()
            {
                Agendas = new List<TpsAgenda>()
            };
            var mailTemplate = this.issuesDataAccess.GetMailTemplate();
            var agenda1 = new TpsAgenda()
            {
                Topic = mailTemplate.Text1,
                SubAgenda = new List<TpsAgenda>()
            };
            //var agenda11 = new TpsAgenda()
            //{
            //    Topic = meeting.memo.Memo1
            //};
            //agenda1.SubAgenda.Add(agenda11);
            foreach (var subAg1 in meeting.memo.SubMemo.Where(r => r.MainNumber == 1).OrderBy(r => r.SubNumber))
            {
                var thisAg = new TpsAgenda()
                {
                   Topic =  TpsMailGenerator.ResolveNewLineForHtml(subAg1.Text)
                };
                agenda1.SubAgenda.Add(thisAg);
            }

            mailContent.Agendas.Add(agenda1);

            var lastId = this.issuesDataAccess.GetBeforeMeetingId(meeting);
            string topic = string.Empty;
            if (lastId != -1)
            {
                var ag2infoStr = this.GetMeetingInfoString(this.issuesDataAccess.GetMeetingById(lastId));
                topic = string.Format("{0} ดังนี้", ag2infoStr);
            }
            var agenda2 = new TpsAgenda()
            {
                Topic = topic,
                SubAgenda = new List<TpsAgenda>()
            };
            #region Special For Agenda 2

            var subAg2 = new TpsAgenda()
            {
                Topic = TpsMailGenerator.ResolveNewLineForHtml(meeting.memo.Memo2)
            };
            agenda2.SubAgenda.Add(subAg2);

            #endregion

            mailContent.Agendas.Add(agenda2);

            var agenda3 = new TpsAgenda()
            {
                Topic = "การติดตามงาน และการดำเนินการตามมติที่ประชุมครั้งที่ผ่านมา"
            };
            if (meeting.meeting_issue.Where(d => d.TypeEnum == MeetingIssueType.Pending || d.TypeEnum == MeetingIssueType.KM).Count() == 0)
            {
                agenda3.Topic = "ไม่มีวาระติดตาม";
                agenda3.SubAgenda = null;
            }
            else
            {
                foreach (var pending in meeting.meeting_issue.Where(d => d.TypeEnum == MeetingIssueType.Pending).Select(d => d.issues))
                {                   
                    var thisSummary = this.issuesDataAccess.GetSummaryById(pending.IssueId);
                    pending.summary = thisSummary;
                    var comment = string.Format("(ครั้งที่ {0})", thisSummary.PendingTimeNo);
                    var thisAgenda = new TpsAgenda()
                    {
                        Topic = TpsMailGenerator.HtmlSubAgFromIssue(pending, comment),
                        IssueId = pending.IssueId
                    };
                    agenda3.SubAgenda.Add(thisAgenda);
                }
                foreach (var pending in meeting.meeting_issue.Where(d => d.TypeEnum == MeetingIssueType.KM).Select(d => d.issues))
                {
                    var thisSummary = this.issuesDataAccess.GetSummaryById(pending.IssueId);
                    pending.summary = thisSummary;
                    var thisAgenda = new TpsAgenda()
                    {
                        Topic = TpsMailGenerator.HtmlSubAgFromIssue(pending, "(ติดตาม KM)"),
                        IssueId = pending.IssueId
                    };
                    agenda3.SubAgenda.Add(thisAgenda);
                }
            }
            mailContent.Agendas.Add(agenda3);


            #region Problems After Agenda 4

            var allMainIssue = meeting.meeting_issue.Where(d => d.TypeEnum == MeetingIssueType.Main).Select(d => d.issues);
            var eachIssue = allMainIssue.GroupBy(r => r.plantshortcode).Select(r => new { plantCode = r.Key, issue = r.ToList() }).ToList();
            foreach (var plantIssue in eachIssue.OrderBy(r => r.plantCode))
            {
               
                var thisAgenda = new TpsAgenda()
                {
                    Topic = string.Format("การแก้ไขปัญหา TPS {0}", this.issuesDataAccess.GetSystemAllowPlant(plantIssue.plantCode).plantdisplaycode)
                };
                thisAgenda.SubAgenda = new List<TpsAgenda>();
                foreach (var issue in plantIssue.issue)
                {
                    var thisSummary = this.issuesDataAccess.GetSummaryById(issue.IssueId);
                    issue.summary = thisSummary;
                    var subAg = new TpsAgenda()
                    {
                        Topic = TpsMailGenerator.HtmlSubAgFromIssue(issue),
                        IssueId = issue.IssueId
                    };
                    thisAgenda.SubAgenda.Add(subAg);
                }
                mailContent.Agendas.Add(thisAgenda);
            }

            //foreach (var toMeet in meeting.meeting_issue.Where(d => d.TypeEnum == MeetingIssueType.Main).Select(d => d.issues))
            //{
            //    var thisAgenda = new TpsAgenda()
            //    {
            //        Topic = toMeet.ProblemName
            //    };

            //    var subAg = new TpsAgenda()
            //    {
            //        Topic = TpsMailGenerator.HtmlSubAgFromIssue(toMeet)
            //    };
            //    thisAgenda.SubAgenda.Add(subAg);
            //    mailContent.Agendas.Add(thisAgenda);
            //}
            #endregion

            #region Last Memo

            var lastAg = new TpsAgenda()
            {
                Topic = "เรื่องอื่นๆ (ถ้ามี)"
            };
            foreach (var subAg in meeting.memo.SubMemo.Where(r => r.MainNumber >= 4).OrderBy(r => r.SubNumber))
            {
                var thisAg = new TpsAgenda()
                {
                    Topic = subAg.Text.Replace("\n","<br />")
                };
                lastAg.SubAgenda.Add(thisAg);
            }
            mailContent.Agendas.Add(lastAg);

            #endregion

            return mailContent.Agendas;
        }
        private List<TpsAgenda> agendaForSummary(int meetingId)
        {
            var meeting = this.issuesDataAccess.GetMeetingById(meetingId);
            foreach (var mtg in meeting.meeting_issue)
            {
                mtg.issues.problem = this.issuesDataAccess.GetProblemDetailByIssue(mtg.IssueId);
            }
            var mailContent = new TpsMailGenerator()
            {
                Agendas = new List<TpsAgenda>()
            };
            var mailTemplate = this.issuesDataAccess.GetMailTemplate();
            var agenda1 = new TpsAgenda()
            {
                Topic = mailTemplate.Text1,
                SubAgenda = new List<TpsAgenda>()
            };
            //var agenda11 = new TpsAgenda()
            //{
            //    Topic = meeting.memo.Memo1
            //};
            //agenda1.SubAgenda.Add(agenda11);
            foreach (var subAg1 in meeting.memo.SubMemo.Where(r => r.MainNumber == 1).OrderBy(r => r.SubNumber))
            {
                var thisAg = new TpsAgenda()
                {
                    Topic = subAg1.Text
                };
                agenda1.SubAgenda.Add(thisAg);
            }

            mailContent.Agendas.Add(agenda1);

            var lastId = this.issuesDataAccess.GetBeforeMeetingId(meeting);
            string topic = string.Empty;
            if (lastId != -1)
            {
                var ag2infoStr = this.GetMeetingInfoString(this.issuesDataAccess.GetMeetingById(lastId));
                topic = string.Format("{0} ดังนี้", ag2infoStr);
            }
            var agenda2 = new TpsAgenda()
            {
                Topic = topic,
                SubAgenda = new List<TpsAgenda>()
            };

            mailContent.Agendas.Add(agenda2);

            var agenda3 = new TpsAgenda()
            {
                Topic = "การติดตามงาน และการดำเนินการตามมติที่ประชุมครั้งที่ผ่านมา"
            };
            if (meeting.meeting_issue.Where(d => d.TypeEnum == MeetingIssueType.Pending || d.TypeEnum == MeetingIssueType.KM).Count() == 0)
            {
                agenda3.Topic = "ไม่มีวาระติดตาม";
                agenda3.SubAgenda = null;
            }
            else
            {
                foreach (var pending in meeting.meeting_issue.Where(d => d.TypeEnum == MeetingIssueType.Pending).Select(d => d.issues))
                {
                    var thisAgenda = new TpsAgenda()
                    {
                        Topic = pending.ProblemName
                    };
                    agenda3.SubAgenda.Add(thisAgenda);
                }
                foreach (var pending in meeting.meeting_issue.Where(d => d.TypeEnum == MeetingIssueType.KM).Select(d => d.issues))
                {
                    var thisAgenda = new TpsAgenda()
                    {
                        Topic = pending.ProblemName
                    };
                    agenda3.SubAgenda.Add(thisAgenda);
                }
            }
            mailContent.Agendas.Add(agenda3);


            #region Problems After Agenda 4

            var allMainIssue = meeting.meeting_issue.Where(d => d.TypeEnum == MeetingIssueType.Main).Select(d => d.issues);
            var eachIssue = allMainIssue.GroupBy(r => r.plantshortcode).Select(r => new { plantCode = r.Key, issue = r.ToList() }).ToList();
            foreach (var plantIssue in eachIssue.OrderBy(r => r.plantCode))
            {
                var thisAgenda = new TpsAgenda()
                {
                    Topic = string.Format("การแก้ไขปัญหา TPS {0}", this.issuesDataAccess.GetSystemAllowPlant(plantIssue.plantCode).plantdisplaycode)
                };
                thisAgenda.SubAgenda = new List<TpsAgenda>();
                foreach (var issue in plantIssue.issue)
                {
                    var subAg = new TpsAgenda()
                    {
                        Topic = issue.ProblemName
                    };
                    thisAgenda.SubAgenda.Add(subAg);
                }
                mailContent.Agendas.Add(thisAgenda);
            }

            //foreach (var toMeet in meeting.meeting_issue.Where(d => d.TypeEnum == MeetingIssueType.Main).Select(d => d.issues))
            //{
            //    var thisAgenda = new TpsAgenda()
            //    {
            //        Topic = toMeet.ProblemName
            //    };

            //    var subAg = new TpsAgenda()
            //    {
            //        Topic = TpsMailGenerator.HtmlSubAgFromIssue(toMeet)
            //    };
            //    thisAgenda.SubAgenda.Add(subAg);
            //    mailContent.Agendas.Add(thisAgenda);
            //}
            #endregion

            #region Last Memo

            var lastAg = new TpsAgenda()
            {
                Topic = "เรื่องอื่นๆ (ถ้ามี)"
            };
            mailContent.Agendas.Add(lastAg);

            #endregion

            return mailContent.Agendas;
        }

        public ActionResult SendMemo()
        {
            var meetingId = int.Parse(Request.Params["meetingId"]);
            var mailSender = MailWriter.Create(Properties.Settings.Default.MailServer, Properties.Settings.Default.MailUser, Properties.Settings.Default.MailPassword, this.mailLogger);
            this.issuesDataAccess.ResolveMeetingIssuesRelationType(meetingId);
            var meeting = this.issuesDataAccess.GetMeetingById(meetingId);

            if (!meeting.IsMeetingReady())
            {
                return Content("Meeting is not ready for sending");
            }
            var meetingInfoString = this.GetMeetingInfoString(meeting);
            var ags = this.AgendasFromMeeting(meeting, meetingInfoString);

            mailSender.SetSubject("ระบบจัดการประชุม TPS : เพื่อโปรดทราบสรุปรายงานการประชุม " + meetingInfoString);
            //var view = View("MemoMail");
            //view.ViewData["model"] = mailContent.Agendas;
            //return view;
            var mailTeamplate = this.issuesDataAccess.GetMailTemplate();
            var groupSetting = this.issuesDataAccess.GetGroupById(meeting.GroupId);
            var viewModel = new MemoMailViewModel()
            {
                Agendas = ags,
                MailTemplate = mailTeamplate,
                LastMeetingId = this.issuesDataAccess.GetBeforeMeetingId(meeting),
                meetingInfo = meeting,
                StartMeetingTimeString = groupSetting.TimeStart.Value.ToString("HH:mm"),
                EndMeetingTimeString = groupSetting.TimeEnd.Value.ToString("HH:mm"),
                IsUsePdf = meeting.IsForcePdf,
            };
            if (viewModel.IsUsePdf)
            {
                viewModel.pdfLink = WebHelper.Constants.AppPath + string.Format("Memo/ExportMemo?meetingId={0}", meeting.MeetingId);
            }
            var viewData = new ViewDataDictionary();
            var ToList = this.issuesDataAccess.GetMailByMeeting(meeting.MeetingId).ToList();

            viewModel.Attendees = ToList;
            viewModel.Mode = MailViewMode.Html;

            viewData["model"] = viewModel;
            var x = this.GetHtmlString("MemoMail", viewData);
            x += "<br /> <br /> จึงเรียนมาเพื่อโปรดทราบ " +
                    "<br /> ระบบจัดการประชุม TPS";
            var regEx = new System.Text.RegularExpressions.Regex("<img src=\".+\" />");
            var match = regEx.Match(x);
            if (match.Success)
            {
                x = x.Replace(match.Captures[0].Value, "<img src=\"cid:Pic1\">");
                mailSender.SetImage("Pic1", Server.MapPath("~/Content/Images/ptt_logo_doc.jpg"), System.Net.Mime.MediaTypeNames.Image.Jpeg);
            }


            if (MailWriter.MailConfig != MailType.Production)
            {
                if (MailWriter.MailConfig == MailType.TestDev)
                {
                    var sb = new StringBuilder(x);
                    sb.AppendFormat("<br /> To : {0}", string.Join(";", ToList.Select(d => d.EmailAddr).ToArray()));
                    x = sb.ToString();
                    mailSender.SetBody(x);
                    mailSender.AddReciever("chakrit.lj@gmail.com", "Chakrit");
                    mailSender.AddReciever("kanda@fireoneone.com", "Kanda");
                    mailSender.Send(MailLogType.Memo, meeting.GroupId);
                }
                else if (MailWriter.MailConfig == MailType.TestPtt)
                {
                    /*
                     * <img src="http://localhost/TPSWorkflow/Content/Images/ptt_logo_doc.jpg" />
                     */
                    var sb = new StringBuilder(x);
                    sb.AppendFormat("<br /> To : {0}", string.Join(";", ToList.Select(d => d.EmailAddr).ToArray()));
                    x = sb.ToString();
                    mailSender.SetBody(x);
                    if (!string.IsNullOrEmpty(Properties.Settings.Default.TestMailTo.Trim()))
                    {
                        var recieverMails = Properties.Settings.Default.TestMailTo.Split(';');
                        foreach (var m in recieverMails)
                        {
                            mailSender.AddReciever(m, "TPS User");
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(Properties.Settings.Default.TestMailCc.Trim()))
                        {
                            var ccMails = Properties.Settings.Default.TestMailCc.Split(';');
                            foreach (var m in ccMails)
                            {
                                mailSender.AddReciever(m, "TPS User");
                            }
                        }
                    }
                    mailSender.SetBody(x);
                    mailSender.Send(MailLogType.Memo, meeting.GroupId);
                }
            }
            else
            {
                mailSender.SetBody(x);
                ToList.ForEach(c => mailSender.AddReciever(c.EmailAddr, c.FullName));
                mailSender.Send(MailLogType.Memo, meeting.GroupId);
            }
            this.issuesDataAccess.SetSentMail(meeting.MeetingId, true);
            TempData["SyncMessage"] = "ส่งสำเร็จเรียบร้อยแล้วครับ";
            return RedirectToAction("Index");

        }

        [HttpPost]
        public ActionResult RemoveMeeting(int id)
        {
            if (!this.CheckEditPermission())
            {
                throw new TpsUnauthorizedException("You don't have permission to delete memo");
            }
            try
            {
                this.issuesDataAccess.DeleteMeeting(id);
                return Json(WebHelper.APIResult.CreateResult("Success"));
            }
            catch (Exception ex)
            {
                return Json(WebHelper.APIResult.ExceptionResult(ex));
            }
        }

        [Obsolete]
        private ActionResult Create()
        {
            if (!this.CheckEditPermission())
            {
                return Content("No permission");
            }
            var resView = View("Create");
            var dates = this.issuesDataAccess.GetAllMeetingDates(false).Select(c => c.ToString("dd-MMM-yyyy", new CultureInfo("en-US")));
            resView.ViewData["Dates"] = dates;
            return resView;
        }

        [HttpPost]
        public ActionResult GetUnmemoMeetingByDate()
        {
            try
            {

                if (!this.CheckEditPermission())
                {
                    var eResult = WebHelper.APIResult.ErrorResult("No permission");
                    return Json(eResult);
                }
                var searchPrms = new MemoSearchParams()
                {
                    IsFilterActive = false,

                };
                DateTime dateBuffer;
                if (!DateTime.TryParseExact(Request.Params["date"], "dd-MMM-yyyy", new CultureInfo("en-US"), DateTimeStyles.AssumeUniversal, out dateBuffer))
                {
                    return Json(WebHelper.APIResult.ErrorResult("Date is not correct"));
                }
                searchPrms.dateSearchStart = dateBuffer;
                searchPrms.GroupId = -1;
                var countRes = 0;
                var result = this.issuesDataAccess.GetBySearchAndSort(searchPrms, -1, -1, out countRes);
                result.ForEach(d =>
                {
                    this.issuesDataAccess.ResolveAttendeesInfo(d);
                });
                return Json(WebHelper.APIResult.CreateResult(result));
            }
            catch (Exception ex)
            {
                return Json(WebHelper.APIResult.ExceptionResult(ex));
            }
        }

        [HttpPost]
        public ActionResult GetAgendas(int id)
        {
            try
            {
                var agendas = this.agendaForSummary(id);
                return Json(WebHelper.APIResult.CreateResult(agendas));
            }
            catch (Exception ex)
            {
                return Json(WebHelper.APIResult.ExceptionResult(ex));
            }
        }

        [HttpPost]
        public ActionResult CreateMeeting(int id)
        {
            try
            {
                if (!this.CheckEditPermission())
                    throw new UnauthorizedAccessException("No permission");
                this.issuesDataAccess.CreateMemoInMeeting(id);
                return Json(WebHelper.APIResult.CreateResult("Success"));
            }
            catch (Exception ex)
            {
                return Json(WebHelper.APIResult.ExceptionResult(ex));
            }
        }

        public ActionResult MemoIssues(int id)
        {
            var memo = this.issuesDataAccess.GetMeetingById(id);
            if (memo == null)
            {
                throw new KeyNotFoundException();
            }
            if (!this.CheckEditPermission())
            {
                throw new UnauthorizedAccessException();
            }
            var resView = View("MemoIssues", memo);
            resView.ViewData["issues"] = this.issuesDataAccess.GetIssuesByMeeting(id);
            return resView;

        }

        [HttpPost]
        public ActionResult GetIssuesToAddToMeetingDt(int meetingId)
        {
            if (!this.CheckEditPermission())
            {
                throw new UnauthorizedAccessException();
            }
            var meeting = this.issuesDataAccess.GetMeetingById(meetingId);
            var prms = Ecobiz.WebUI.DataTables.ServerTableParams.RetrieveFromRequest(this.Request.Params);
            if (meeting.isSentMail)
            {
                return Json(Ecobiz.WebUI.DataTables.TableData<issuesMeetingDto>.CreateData(prms.Echo, 0, 0, new List<issuesMeetingDto>()));
            }
            var issues = this.issuesDataAccess.GetIssuesCanBeAddedToMeeting(meetingId);
            var result = Ecobiz.WebUI.DataTables.TableData<issuesMeetingDto>.CreateData(prms.Echo, issues.Count, issues.Count, issues);
            return Json(result);
        }

        [HttpPost]
        public ActionResult AddIssuesToMeeting()
        {
            
            if (!this.CheckEditPermission())
            {
                throw new UnauthorizedAccessException();
            }
            try
            {
                int meetingId;
                List<int> issuesToAdd;
                try
                {
                    meetingId = int.Parse(Request.Params["meetingId"]);
                    issuesToAdd = Newtonsoft.Json.JsonConvert.DeserializeObject<List<int>>(Request.Params["issueIds"]);
                }
                catch
                {
                    throw new InvalidDataException("Parameter incorrect type");
                }
                var meeting = this.issuesDataAccess.GetMeetingById(meetingId);
                if (meeting.isSentMail)
                    throw new Exception("Cannot change issues of sent memo");
                this.issuesDataAccess.AddIssuesToMeeting(meetingId, issuesToAdd);
                return Json(WebHelper.APIResult.CreateResult("Success"));
            }
            catch (Exception ex)
            {
                return Json(WebHelper.APIResult.ExceptionResult(ex));
            }
        }

        [HttpPost]
        public ActionResult DeleteIssuesFromMeeting()
        {
            if (!this.CheckEditPermission())
            {
                throw new UnauthorizedAccessException();
            }
            try
            {
                int meetingId;
                int issueToDel;
                try
                {
                    meetingId = int.Parse(Request.Params["meetingId"]);
                    issueToDel = int.Parse(Request.Params["issueId"]);
                }
                catch
                {
                    throw new InvalidDataException("Parameter incorrect type");
                }
                var meeting = this.issuesDataAccess.GetMeetingById(meetingId);
                if (meeting.isSentMail)
                    throw new Exception("Cannot change issues of sent memo");
                this.issuesDataAccess.DeleteIssuesFromMeeting(meetingId, issueToDel);
                return Json(WebHelper.APIResult.CreateResult("Success"));
            }
            catch (Exception ex)
            {
                return Json(WebHelper.APIResult.ExceptionResult(ex));
            }
        }
    }
}