﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TPSWorkflow.Controllers
{
    public class ImageController : Controller
    {
        //
        // GET: /Image/
        public ActionResult Temp()
        {
            var filename = Request.Params["name"];
            return File(TPSPathHelper.GetTempLocationForFileName(filename), MimeMapping.GetMimeMapping(filename));
        }

        public ActionResult Avatars()
        {
            var filename = Request.Params["name"];
            return File(TPSPathHelper.GetAvatarLocationForFileName(filename), MimeMapping.GetMimeMapping(filename));
        }
	}
}