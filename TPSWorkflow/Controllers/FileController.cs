﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TPSWorkflow.DAL;

namespace TPSWorkflow.Controllers
{
    public class FileController : BaseController
    {
        //
        // GET: /File/
        protected IIssuesData issuesDataAccess = null;
        protected IUsersManage userManage = null;

        public FileController(IIssuesData access, IUsersManage user)
        {
            this.issuesDataAccess = access;
            this.issuesDataAccess.SetPermission(this.tpsSession.userInfo, user);
            this.userManage = user;
        }
        public ActionResult KM()
        {
            var path = this.issuesDataAccess.GetKmFileById(int.Parse(Request.Params["id"]));
            return File(path.LocalPath, System.Net.Mime.MediaTypeNames.Application.Octet, path.FileName);
            
        }

        public ActionResult Summary()
        {
            var path = this.issuesDataAccess.GetProblemFileById(int.Parse(Request.Params["id"]));
            return File(path.LocalPath, System.Net.Mime.MediaTypeNames.Application.Octet, path.FileName);
        }
	}
}