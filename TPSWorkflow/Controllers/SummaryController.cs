﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ecobiz.WebUI.DataTables;
using TPSWorkflow.DAL;
using TPSWorkflow.DAL.DtoS;
using WebHelper;

namespace TPSWorkflow.Controllers
{
    [TPSAuthen]
    public class SummaryController : BaseController
    {
        //
        // GET: /Summary/
        protected IIssuesData issuesDataAccess = null;
        protected IUsersManage userManage = null;

        public SummaryController(IIssuesData dataModule, IUsersManage user)
        {
            this.issuesDataAccess = dataModule;
            this.issuesDataAccess.SetPermission(this.tpsSession.userInfo, user);
            this.userManage = user;
        }

        private IssuesSearchParams getSearchPrms(System.Collections.Specialized.NameValueCollection requestPrms)
        {
            #region Gather Search Parameters

            var searchPrms = new IssuesSearchParams()
            {
                AllSearch = requestPrms["SearchTerm"],
                SortField = IssuesField.UpdateDate
            };
            var startDate = DateTime.ParseExact(requestPrms["startDate"].ToString(), "d-M-yyyy", System.Globalization.CultureInfo.GetCultureInfo("en-US"));
            var endDate = DateTime.ParseExact(requestPrms["endDate"].ToString(), "d-M-yyyy", System.Globalization.CultureInfo.GetCultureInfo("en-US"));
            searchPrms.StartDate = startDate;
            searchPrms.EndDate = endDate.AddDays(1);

            var groupFilteringObj = requestPrms["groupSearch"];
            var groupFiltering = groupFilteringObj == null ? "-1" : groupFilteringObj.ToString();
            int groupId = -1;
            int.TryParse(groupFiltering, out groupId);
            searchPrms.GroupId = groupId;
            var isOnlyMe = requestPrms["isOnlyMe"];
            var isOnlyMeBool = false;
            if (isOnlyMe == null || !(bool.TryParse(isOnlyMe, out isOnlyMeBool)) || isOnlyMeBool == false)
            {
                searchPrms.IsOnlyForUser = false;
            }
            else
            {
                searchPrms.IsOnlyForUser = true;
                searchPrms.CreatorId = tpsSession.userInfo.CODE;
                var isUserIsApprover = this.userManage.IsApproverInUnitCode(this.tpsSession.userInfo.CODE, this.tpsSession.userInfo.UNITCODE);
                searchPrms.IsSearcherIsApprover = isUserIsApprover;
                searchPrms.SearcherUnitcode = tpsSession.userInfo.UNITCODE;
            }

            return searchPrms;

            #endregion
        }

        private bool IsCanEditFromSummary()
        {
            try
            {
                return (this.tpsSession.userInfo.unit.DUMMY_RELATIONSHIP.StartsWith(IssuesData.GetVBGDummyRelation()));
            }
            catch (Exception)
            {
                return true;
            }
        }

        private bool IsCanRecover()
        {
            return this.tpsSession.userInfo.UNITCODE == this.issuesDataAccess.GetMasterUnitCode();
        }

        public ActionResult Index()
        {
            // ***** Should refractor to use the same methodology as search
            var resView = View("IndexNoAjax");
            var groupsData= this.issuesDataAccess.GetAllGroups();
            groupsData.ForEach(c => c.allowplant = this.issuesDataAccess.GetPlantByGroup(c.GroupId));
            resView.ViewData["groups"] = groupsData;
#if DEVELOPING
            resView.ViewData["usrWgId"] = "all";
#else
            resView.ViewData["usrWgId"] = this.tpsSession.userInfo.UNITCODE;
#endif
            var rights = this.userManage.GetUserRights(this.tpsSession.userInfo);
            resView.ViewData["isEditable"] = this.IsCanEditFromSummary();
            resView.ViewData["isCanBreakDown"] = (this.tpsSession.userInfo.UNITCODE == this.issuesDataAccess.GetMasterUnitCode() || this.tpsSession.userInfo.TpsType == TpsUserType.SuperAdmin);
            resView.ViewData["isCanRecover"] = (this.tpsSession.userInfo.UNITCODE == this.issuesDataAccess.GetMasterUnitCode() || this.tpsSession.userInfo.TpsType == TpsUserType.SuperAdmin);
            var tableData = new Dictionary<IssuesStatus, List<issuesDto>>();
            var isUserIsApprover = this.userManage.IsApproverInUnitCode(this.tpsSession.userInfo.CODE, this.tpsSession.userInfo.UNITCODE);
            var val = Enum.GetValues(typeof(IssuesStatus));
            var countDict = new Dictionary<IssuesStatus, int>();
            var isOnlyForUser = this.tpsSession.userInfo.UNITCODE == this.issuesDataAccess.GetMasterUnitCode() ? false : true;
            foreach (var status in val.Cast<IssuesStatus>())
            {
                var searchPrms = new IssuesSearchParams()
                {
                    EndDate = DateTime.Now.AddDays(1),
                    AllSearch = string.Empty,
                    IsDesc = true,
                    SortField = IssuesField.UpdateDate,
                    GroupId = -1,
                    StartDate = DateTime.Now.AddMonths(-1),
                    IsFilterStatus = true,
                    Status = status,
                    IsOnlyForUser = isOnlyForUser,
                    CreatorId = tpsSession.userInfo.CODE,
                    IsSearcherIsApprover = isUserIsApprover,
                    SearcherUnitcode = tpsSession.userInfo.UNITCODE
                };
                if (status == IssuesStatus.Pending)
                {
                    searchPrms.Statuses = new List<IssuesStatus>()
                    {
                        IssuesStatus.PendingAfterMeeting,
                        IssuesStatus.Pending
                    };
                }
                int x;
                tableData.Add(status, this.issuesDataAccess.GetBySearchAndSort(searchPrms, -1, -1, out x));
                countDict.Add(status, tableData[status].Count);
            }
            resView.ViewData["countDict"] = countDict;
            resView.ViewData["tableData"] = tableData;
            var oldContent = new Dictionary<string, string>();
            oldContent.Add("SearchTerm", string.Empty);
            oldContent.Add("startDate", DateTime.Now.AddMonths(-1).ToString("d-M-yyyy", System.Globalization.CultureInfo.GetCultureInfo("en-US")));
            oldContent.Add("endDate", DateTime.Now.ToString("d-M-yyyy", System.Globalization.CultureInfo.GetCultureInfo("en-US")));
            oldContent.Add("groupSearch", "-1");
            oldContent.Add("isShowOnlyMe", isOnlyForUser.ToString());
            resView.ViewData["oldContent"] = oldContent;
            this.issuesDataAccess.SetBaseUrl();
            return resView;
        }

        public ActionResult Search()
        {

            var searchPrms = this.getSearchPrms(Request.Params);

            var resView = View("IndexNoAjax");
            var groupsData = this.issuesDataAccess.GetAllGroups(); 
            groupsData.ForEach(c => c.allowplant = this.issuesDataAccess.GetPlantByGroup(c.GroupId));
            resView.ViewData["groups"] = groupsData;
            
#if DEVELOPING
            resView.ViewData["usrWgId"] = "all";
#else
            resView.ViewData["usrWgId"] = this.tpsSession.userInfo.UNITCODE;
#endif
            var rights = this.userManage.GetUserRights(this.tpsSession.userInfo);
            resView.ViewData["isEditable"] = this.IsCanEditFromSummary();
            resView.ViewData["isCanBreakDown"] = (this.tpsSession.userInfo.UNITCODE == this.issuesDataAccess.GetMasterUnitCode() || this.tpsSession.userInfo.TpsType == TpsUserType.SuperAdmin);
            var tableData = new Dictionary<IssuesStatus, List<issuesDto>>();
            var val = Enum.GetValues(typeof(IssuesStatus));
            var countDict = new Dictionary<IssuesStatus, int>();
            foreach (var status in val.Cast<IssuesStatus>())
            {
                if (status == IssuesStatus.Pending)
                {
                    searchPrms.Statuses = new List<IssuesStatus>()
                    {
                        IssuesStatus.PendingAfterMeeting,
                        IssuesStatus.Pending
                    };
                }
                else
                {
                    searchPrms.Status = status;
                    searchPrms.Statuses = null;
                }
                searchPrms.IsFilterStatus = true;

                int count;
                tableData.Add(status, this.issuesDataAccess.GetBySearchAndSort(searchPrms, -1, -1, out count));
                countDict.Add(status, count);
            }
            resView.ViewData["tableData"] = tableData;
            resView.ViewData["countDict"] = countDict;
            var oldContent = new Dictionary<string, string>();
            oldContent.Add("SearchTerm", Request.Params["SearchTerm"]);
            oldContent.Add("startDate", Request.Params["startDate"]);
            oldContent.Add("endDate", Request.Params["endDate"]);
            oldContent.Add("groupSearch", Request.Params["groupSearch"]);
            oldContent.Add("isShowOnlyMe", searchPrms.IsOnlyForUser.ToString());
            resView.ViewData["oldContent"] = oldContent;
            this.issuesDataAccess.SetBaseUrl();
            return resView;
        }

        public ActionResult TableSource()
        {
            try
            {
                ServerTableParams retPrms = Ecobiz.WebUI.DataTables.ServerTableParams.RetrieveFromRequest(Request.Params, true, "issueSearchKey");
                var searchPrms = new IssuesSearchParams()
                {
                    AllSearch = retPrms.SearchTerm,
                };
                if (retPrms.SortCols != null && retPrms.SortCols.Count > 0)
                {
                    switch (retPrms.SortCols.FirstOrDefault().ColumnNo)
                    {
                        case 0:
                            searchPrms.SortField = IssuesField.Group;
                            break;
                        case 1:
                            searchPrms.SortField = IssuesField.ProblemDate;
                            break;
                        case 2:
                            searchPrms.SortField = IssuesField.BillNo;
                            break;
                        case 3:
                            searchPrms.SortField = IssuesField.ProblemName;
                            break;
                        case 4:
                            searchPrms.SortField = IssuesField.PlantCode;
                            break;
                        case 5:
                            searchPrms.SortField = IssuesField.UnitCode;
                            break;
                        case 6:
                            searchPrms.SortField = IssuesField.UpdateDate;
                            break;
                        default:
                            searchPrms.SortField = IssuesField.Group;
                            break;
                    }
                    searchPrms.IsDesc = retPrms.SortCols.FirstOrDefault().Direction == SortDirection.Descending;
                }
                else
                {
                    searchPrms.SortField = IssuesField.UpdateDate;
                    searchPrms.IsDesc = false;
                }
                var startDate = DateTime.ParseExact(retPrms.AdditionalParams["startDate"].ToString(), "d-M-yyyy", System.Globalization.CultureInfo.GetCultureInfo("en-US"));
                var endDate = DateTime.ParseExact(retPrms.AdditionalParams["endDate"].ToString(), "d-M-yyyy", System.Globalization.CultureInfo.GetCultureInfo("en-US"));
                searchPrms.StartDate = startDate;
                searchPrms.EndDate = endDate.AddDays(1);
                
                var statusFilterString = retPrms.AdditionalParams["issuestatus"].ToString();
                var groupFiltering = retPrms.AdditionalParams["groupSearch"].ToString();
                var filterStatus = IssuesStatus.New;
                searchPrms.IsFilterStatus = Enum.TryParse<IssuesStatus>(statusFilterString, out filterStatus);
                searchPrms.Status = filterStatus;
                if (filterStatus == IssuesStatus.Pending)
                {
                    searchPrms.Statuses = new List<IssuesStatus>()
                    {
                        IssuesStatus.PendingAfterMeeting,
                        IssuesStatus.Pending
                    };
                }
                int groupId = -1;
                int.TryParse(groupFiltering, out groupId);
                searchPrms.GroupId = groupId;
                


                int allCount= -1;
                var qRes = this.issuesDataAccess.GetBySearchAndSort(searchPrms, retPrms.Start,retPrms.Count, out allCount);

                var tableRes = Ecobiz.WebUI.DataTables.TableData<DAL.DtoS.issuesDto>.CreateData(retPrms.Echo, allCount, qRes.Count, qRes);
                return Json(tableRes, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public ActionResult Recover()
        {
            if (!IsCanRecover())
            {
                return Content("No Permission to Recover");
            }
            var delIssues = this.issuesDataAccess.GetDeletedIssues(Request.Params["searchTerm"]);
            var resView = View("Recover");
            resView.ViewData["breakdown"] = delIssues.Where(r => r.IsUnbreakdown).ToList();
            resView.ViewData["deleted"] = delIssues.Where(d => !d.IsUnbreakdown).ToList();
            return resView;
        }

        public ActionResult RecoverSearch()
        {
            var searchPrms = this.getSearchPrms(Request.Params);
            if (!IsCanRecover())
            {
                return Content("No Permission to Recover");
            }
            var delIssues = this.issuesDataAccess.GetSearchedDeletedIssues(searchPrms.AllSearch, searchPrms.StartDate, searchPrms.EndDate);
            var resView = View("Recover");
            resView.ViewData["breakdown"] = delIssues.Where(r => r.IsUnbreakdown).ToList();
            resView.ViewData["deleted"] = delIssues.Where(d => !d.IsUnbreakdown).ToList();
            var oldContent = new Dictionary<string, string>();
            oldContent.Add("SearchTerm", Request.Params["SearchTerm"]);
            oldContent.Add("startDate", Request.Params["startDate"]);
            oldContent.Add("endDate", Request.Params["endDate"]);
            resView.ViewData["oldContent"] = oldContent;
            return resView;
        }

        
    }
}
