﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TPSWorkflow.DAL;
using TPSWorkflow.DAL.DtoS;
using TPSWorkflow.Helpers;
using WebHelper;

namespace TPSWorkflow.Controllers
{
    [TPSAuthen]
    public class IssuesController : BaseController
    {
        //
        // GET: /Issues/

        protected IIssuesData issuesDataAccess = null;
        protected IUsersManage userManage = null;
        protected IMailLogger mailLogger = null;
        private PersonalInfoDto thisUser
        {
            get
            {
                return this.tpsSession.userInfo;
            }
        }

        //[TPSAuthen]
        public IssuesController(IIssuesData access, IUsersManage user, IMailLogger mailLogger)
        {
            this.issuesDataAccess = access;
            
            this.userManage = user;
            this.mailLogger = mailLogger;

            
        }

        public override void InjectSessionForTesting(HttpSessionStateBase session)
        {
            base.InjectSessionForTesting(session);
            this.issuesDataAccess.SetPermission(this.tpsSession.userInfo, this.userManage);
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);
            this.issuesDataAccess.SetPermission(this.tpsSession.userInfo, this.userManage);
        }

        #region Permission Checker

        private void CheckForEditPermission(string unitCode)
        {
            var userRight = this.userManage.GetUserRights(this.tpsSession.userInfo);

#if DEVELOPING
            userRight |= UserRights.SystemAdministrator;
#endif
            if (!(userRight.HasFlag(UserRights.SystemAdministrator) || (userRight.HasFlag(UserRights.CreateNewProblem) && this.tpsSession.userInfo.UNITCODE == unitCode)))
            {
                throw new Exception("คุณไม่มีสิทธิ์ที่จะสร้างใบแจ้งซ่อมในหน่วยงานที่เลือก");
            }
        }
        private void CheckForApprovePermission(string unitCode)
        {
            var userRight = this.userManage.GetUserRights(this.tpsSession.userInfo);
            if (!this.userManage.IsApproverInUnitCode(this.tpsSession.userInfo.CODE, unitCode))
            {
                throw new TpsUnauthorizedException("คุณไม่มีสิทธิ์ในการอนุมัติ");
            }
//#if DEVELOPING
//            userRight |= UserRights.SystemAdministrator;
//#endif
//            if (this.issuesDataAccess.GetCustomApproverInUnit(unitCode).Count(r=>r.CODE==this.tpsSession.userInfo.CODE) > 0)
//            {
//                userRight |= UserRights.ApprovedProblem;
//            }
//            if (!((userRight.HasFlag(UserRights.ApprovedProblem) && this.tpsSession.userInfo.UNITCODE == unitCode) ||
//                userRight.HasFlag(UserRights.ApprovedProblemAcrossUnit)))
//            {
                
//            }
        }
        private bool IsCanRecover()
        {
            return this.tpsSession.userInfo.UNITCODE == this.issuesDataAccess.GetMasterUnitCode();
        }
        private bool CheckForEditDetailPermission(issuesDto issue)
        {
            var userRight = this.userManage.GetUserRights(this.tpsSession.userInfo);
#if DEVELOPING
            userRight |= UserRights.SystemAdministrator;
#endif
            if (!(userRight.HasFlag(UserRights.SystemAdministrator) ||
                (userRight.HasFlag(UserRights.EditProblemCause) && this.tpsSession.userInfo.UNITCODE == issue.unitcode)))
            {
                return false;
            }
            if (issue.InchargeUserCode != this.tpsSession.userInfo.CODE)
            {
#if DEVELOPING
                return true;
#endif
                return false;
            }
            return true;
        }
        private bool CheckForSummaryPermission(string unitCode)
        {
            var userRight = this.userManage.GetUserRights(this.tpsSession.userInfo);
#if DEVELOPING
            userRight |= UserRights.SystemAdministrator;
#endif
            if (!(userRight.HasFlag(UserRights.SystemAdministrator) ||
                (userRight.HasFlag(UserRights.AssignMeetingResult) && this.tpsSession.userInfo.UNITCODE == unitCode)))
            {
                return false;
            }

            return true;
        }
        private bool CheckForEditKmPermission()
        {
            var userRight = this.userManage.GetUserRights(this.tpsSession.userInfo);
#if DEVELOPING
            userRight |= UserRights.SystemAdministrator;
#endif  
            if (!(userRight.HasFlag(UserRights.AssignMeetingReport) || userRight.HasFlag(UserRights.SystemAdministrator))) 
            {
                return false;
            }
            return true;
        }
        private bool IsCanEditInfo(issuesDto edittedIssue)
        {
            if (edittedIssue.StatusEnum == IssuesStatus.New)
            {
                var rights = this.userManage.GetUserRights(this.tpsSession.userInfo);
                if (rights.HasFlag(UserRights.CreateNewProblem))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            if (edittedIssue.StatusEnum == IssuesStatus.Cancel)
            {
                return this.tpsSession.userInfo.UNITCODE == this.issuesDataAccess.GetMasterUnitCode()
                    || this.tpsSession.userInfo.CODE == edittedIssue.CreatedBy
                    || (this.tpsSession.userInfo.UNITCODE == edittedIssue.unitcode && IsCanApprove(edittedIssue.unitcode));
            }
            else
            {
                
                return (this.tpsSession.userInfo.UNITCODE == this.issuesDataAccess.GetMasterUnitCode() || this.tpsSession.userInfo.CODE == edittedIssue.CreatedBy
                || (this.tpsSession.userInfo.UNITCODE == edittedIssue.unitcode && IsCanApprove(edittedIssue.unitcode)));
            }
            
        }
        private bool IsCanBreakdown()
        {
            return (this.tpsSession.userInfo.UNITCODE == this.issuesDataAccess.GetMasterUnitCode() || this.tpsSession.userInfo.TpsType == TpsUserType.SuperAdmin);
        }
        private bool IsCanApprove(string unitCode)
        {
            var userRight = this.userManage.GetUserRights(this.tpsSession.userInfo);

#if DEVELOPING
            userRight |= UserRights.SystemAdministrator;
#endif
            if (this.issuesDataAccess.GetCustomApproverInUnit(unitCode).Count(r => r.CODE == this.tpsSession.userInfo.CODE) > 0)
            {
                userRight |= UserRights.ApprovedProblem;
            }
            if (!((userRight.HasFlag(UserRights.ApprovedProblem) && this.tpsSession.userInfo.UNITCODE == unitCode) ||
                userRight.HasFlag(UserRights.ApprovedProblemAcrossUnit)))
            {
                return false;
            }
            return true;
        }
        private bool IsCanUnbreakdown()
        {
            return thisUser.UNITCODE == this.issuesDataAccess.GetMasterUnitCode();
        }

        #endregion

        #region DataSource
        [HttpPost]
        public ActionResult GetBillDataFromSap()
        {
            try
            {
                if (Request.Params["billNo"] == null)
                    return Json(APIResult.ErrorResult("ไม่ระบุใบแจ้งซ่อม"));
                var billNo = Request.Params["billNo"];
                if (this.issuesDataAccess.GetByBillNo(billNo) != null)
                    return Json(APIResult.ErrorResult("ใบแจ้งซ่อมซ้ำในระบบ"));
                var issues = this.issuesDataAccess.GetSapDataFromBillNo(billNo);
                if (issues == null)
                    return Json(APIResult.ErrorResult("ไม่พบใบแจ้งซ่อม"));
                
                return Json(APIResult.CreateResult(issues));
            }
            catch (Exception ex)
            {
                return Json(APIResult.ExceptionResult(ex));
            }
        }
        [HttpPost, TPSAuthen]
        public ActionResult GetMachineFromId()
        {
            var machineNo = Request.Params["machineNo"];
            if (string.IsNullOrEmpty(machineNo))
                return Json("");
            int d;
            if (int.TryParse(machineNo, out d))
            {
                //var machines = this.issuesDataAccess.GetAllMachines();
                //var res = machines.Where(r => r.MachineCode == d).Select(x => x.MachineName).FirstOrDefault();
                //if (res == null)
                //    res = string.Empty;
                var res = string.Empty;
                var machine = this.issuesDataAccess.GetMachineByCode(d);
                if (machine != null)
                    res = machine.MachineName;
                return Json(res);
            }
            return Json("");
        }


        public ActionResult IssueDataTable()
        {
            var tablePrms = Ecobiz.WebUI.DataTables.ServerTableParams.RetrieveFromRequest(Request.Params);
            var status = (IssuesStatus)int.Parse(Request.Params["issuestatus"]);
            var searchPrms = new IssuesSearchParams()
            {
                AllSearch = tablePrms.SearchTerm,
                IsFilterStatus = true,
                Status = status,
                SortField = (IssuesField)tablePrms.SortCols[0].ColumnNo,
                IsDesc = tablePrms.SortCols[0].Direction == Ecobiz.WebUI.DataTables.SortDirection.Descending
            };
            int allCount = 0;
            var res = this.issuesDataAccess.GetBySearchAndSort(searchPrms, tablePrms.Start, tablePrms.Count, out allCount);
            if (res == null)
                return null;
            var tableRes = Ecobiz.WebUI.DataTables.TableData<issuesDto>.CreateData(tablePrms.Echo, allCount, res.Count, res);
            return Json(tableRes, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Some of mail which using across controllers

        protected void sendWaitingMail(issuesDto thisIssue)
        {
            var mailWriter = MailWriter.Create(Properties.Settings.Default.MailServer, Properties.Settings.Default.MailUser, Properties.Settings.Default.MailPassword, this.mailLogger);
            var mailCreator = new MailContentCreator(this.issuesDataAccess, this.userManage);
            var mailContent = mailCreator.CreatedBillMail(this.issuesDataAccess.GetByBillNo(thisIssue.BillNo).IssueId);
            mailWriter.SetByContentTo(mailContent);
            mailWriter.Send(MailLogType.InfoToApprover, thisIssue.GroupId);
            mailWriter.SetByContentCC(mailContent);
            mailWriter.Send(MailLogType.InfoToApprover, thisIssue.GroupId);
        }

        #endregion

        #region Issues Editor Step 1

        private void PrepareIssueInfoData(ViewResult issueInfoView)
        {
            issueInfoView.ViewData["plants"] = this.issuesDataAccess.GetAllPlantCode();
            issueInfoView.ViewData["workgroups"] = this.issuesDataAccess.GetAllWorkGroup();
            issueInfoView.ViewData["machines"] = this.issuesDataAccess.GetAllMachines();
            issueInfoView.ViewData["issueid"] = -1;   
        }
        public ActionResult New()
        {
            var resView = View("IssueInfo", null);
            this.PrepareIssueInfoData(resView);
            resView.ViewData["isEditable"] = true;
            return resView;
        }
        [HttpPost]
        public ActionResult SubmitIssueInfo()
        {
            try
            {
                
                #region Check Permission

                var userRight = this.userManage.GetUserRights(this.tpsSession.userInfo);

#if DEVELOPING
                userRight |= UserRights.SystemAdministrator;
#endif




                #endregion

                var issueId = Request.Params["issueid"];
                issuesDto thisIssue = null;
                if (issueId == "-1")
                {
                    if (!(userRight.HasFlag(UserRights.SystemAdministrator) || (userRight.HasFlag(UserRights.CreateNewProblem))))
                    {
                        throw new Exception("คุณไม่มีสิทธิ์ที่จะสร้างใบแจ้งซ่อมในหน่วยงานที่เลือก");
                    }
                    thisIssue = new issuesDto()
                    {
                        IssueId = -1
                    };
                    string billNo = string.Empty;
                    for (int i = 0; i < 12; i++)
                    {
                        billNo += Request.Params["billNo" + i];
                    }
                    thisIssue.BillNo = billNo;
                    thisIssue.CreatedBy = this.tpsSession.userInfo.CODE;
                }
                else
                {
                    thisIssue = this.issuesDataAccess.GetById(int.Parse(issueId));
                    if (thisIssue == null || !IsCanEditInfo(thisIssue) || thisIssue.IsDeleted)
                        throw new TpsUnauthorizedException("คุณไม่มีสิทธิ์ที่จะแก้ไขใบแจ้งซ่อมในหน่วยงานที่เลือก");
                    
                    if (string.IsNullOrEmpty(thisIssue.CreatedBy))
                    {
                        thisIssue.CreatedBy = this.tpsSession.userInfo.CODE;
                        thisIssue.CreatedDate = DateTime.Now;
                    }
                }
                if (thisIssue.Status >= (int)IssuesStatus.Approve && thisIssue.StatusEnum != IssuesStatus.Cancel)
                {
                    throw new TpsUnauthorizedException("ไม่สามารถแก้ไขใบแจ้งซ่อมที่ Approved แล้วได้");
                }
                thisIssue.RepairBillNo = Request.Params["repairNo"];
                thisIssue.plantshortcode = Request.Params["plantId"];
                thisIssue.plantcode = Request.Params["plantCode"];
                thisIssue.MachineNo = Request.Params["txtMachineNo"];
                thisIssue.MachineFullName = Request.Params["txtMachineFullName"];


                thisIssue.ProblemName = Request.Params["problemName"];
                thisIssue.ProblemDetail = Request.Params["problemDetail"];
                thisIssue.RepairDate = null;
                thisIssue.unitcode = Request.Params["unitcode"];
                thisIssue.RepairDueDate = null;
                try
                {
                    thisIssue.ProblemDate = DateTime.ParseExact(Request.Params["problemDate"], "dd-MMM-yyyy", new CultureInfo("en-US"));
                    if (!string.IsNullOrEmpty(Request.Params["fixDate"]))
                    {
                        thisIssue.RepairDate = DateTime.ParseExact(Request.Params["fixDate"], "dd-MMM-yyyy", new CultureInfo("en-US"));
                    }
                    if (!string.IsNullOrEmpty(Request.Params["dueDate"]))
                        thisIssue.RepairDueDate = DateTime.ParseExact(Request.Params["dueDate"], "dd-MMM-yyyy", new CultureInfo("en-US"));
                }
                catch (FormatException)
                {
                    throw new TpsUnauthorizedException("Cannot Format Date");
                }


                var priorStatus = thisIssue.StatusEnum;
                if (this.issuesDataAccess.SavePartialForStep1(thisIssue))
                {
                    TempData["SyncMessage"] = "บันทึกสำเร็จเรียบร้อยแล้วครับ";
                    thisIssue.GroupId = this.issuesDataAccess.GetGroupByPlantCode(thisIssue.plantshortcode).GroupId;
                    if (thisIssue.IssueId == -1 || priorStatus == IssuesStatus.New)
                    {
                        this.sendWaitingMail(thisIssue);
                        return RedirectToAction("Index", "Summary");
                    }
                    else
                    {
                        if (thisIssue.StatusEnum == IssuesStatus.Cancel)
                        {
                            return RedirectToAction("EditInfo", new { controller = "Issues", action = "EditInfo", id = thisIssue.IssueId });
                        }
                        return RedirectToAction("Approve", new { controller = "Issues", action = "Approve", id = thisIssue.IssueId });
                    }
                }
                else
                {
                    throw new Exception("Unexpected Error");
                }
            }
            catch (System.Net.Mail.SmtpException ex1)
            {
                TempData["SyncMessage"] = "บันทึกสำเร็จเรียบร้อยแล้ว แต่พบข้อผิดพลาดไม่สามารถส่งอีเมล์เข้าหาผู้ร่วมประชุมได้";
                TempData["HiddenMessage"] = ex1.Message + " | " + ex1.StackTrace;
                return RedirectToAction("Index", "Summary");
            }
            catch (Exception)
            {
                throw;
            }
        }
        [TPSAuthen]
        public ActionResult EditInfo(int id)
        {
            var userRight = this.userManage.GetUserRights(this.tpsSession.userInfo);

#if DEVELOPING
            userRight |= UserRights.SystemAdministrator;
#endif
            var edittedIssue = this.issuesDataAccess.GetById(id);
            if (edittedIssue == null || edittedIssue.IsDeleted)
            {
                return Content("Issues have been deleted from the system. Please contact server administrator");
            }
                
            bool isEditable = true;
            //if (this.tpsSession.userInfo.UNITCODE != edittedIssue.unitcode || !(userRight.HasFlag(UserRights.SystemAdministrator) || (userRight.HasFlag(UserRights.CreateNewProblem))))
            //{
            //    isEditable = false;
            //}

            if (!IsCanEditInfo(edittedIssue))
            {
                isEditable = false;
            }


            if (edittedIssue.Status >= (int)IssuesStatus.Approve && edittedIssue.StatusEnum != IssuesStatus.Cancel && edittedIssue.StatusEnum != IssuesStatus.Pending)
            {
                isEditable = false;
            }
            var resView = View("IssueInfo", edittedIssue);
            this.PrepareIssueInfoData(resView);
            resView.ViewData["issueDto"] = edittedIssue;
            resView.ViewData["isEditable"] = isEditable;
            resView.ViewData["isCanUnbreakDown"] = IsCanUnbreakdown();
            return resView;
        }
        [TPSAuthen]
        public ActionResult ReApprove()
        {
            var issueId = int.Parse(Request.Params["issueid"]);
            var edittedIssue = this.issuesDataAccess.GetById(issueId);
            if (edittedIssue == null)
                throw new TpsUnauthorizedException("You don't have permission to edit issue");
            if (!IsCanEditInfo(edittedIssue) || edittedIssue.StatusEnum != IssuesStatus.Cancel)
            {
                throw new TpsUnauthorizedException("You don't have permission to edit issue");
            }
            if (this.issuesDataAccess.ReApproveIssues(issueId))
            {
                var mailWriter = MailWriter.Create(Properties.Settings.Default.MailServer, Properties.Settings.Default.MailUser, Properties.Settings.Default.MailPassword, this.mailLogger);
                var mailCreator = new MailContentCreator(this.issuesDataAccess, this.userManage);
                var mailContent = mailCreator.CreatedBillMail(edittedIssue.IssueId);
                mailWriter.SetByContentTo(mailContent);
                mailWriter.Send(MailLogType.InfoToApprover, edittedIssue.GroupId);
                mailWriter.SetByContentCC(mailContent);
                mailWriter.Send(MailLogType.InfoToApprover, edittedIssue.GroupId);
                return Json(WebHelper.APIResult.CreateResult("Success"));
            }
            else
            {
                return Json(WebHelper.APIResult.ErrorResult("Error"));
            }
        }
        #endregion

        #region Issues Editor Step 2

        public ActionResult Approve(int id)
        {
            var issueId = id;
            var thisIssue = this.issuesDataAccess.GetById(issueId);
            var resView = View("IssueApprove", thisIssue);
            if (thisIssue == null || thisIssue.IsDeleted)
                throw new TpsUnauthorizedException("You don't have permission to edit issue");
            if ((int)thisIssue.StatusEnum <= (int)IssuesStatus.Waiting || thisIssue.StatusEnum==IssuesStatus.Cancel || thisIssue.StatusEnum == IssuesStatus.Pending || thisIssue.StatusEnum == IssuesStatus.Approve)
            {
                try
                {
                    this.CheckForApprovePermission(thisIssue.unitcode);
                    resView.ViewData["isEditable"] = true;
                }
                catch (Exception)
                {
                    resView.ViewData["isEditable"] = false;
                }
            }
            else
            {
                resView.ViewData["isEditable"] = false;
            }
            var users = this.userManage.GetByWorkGroup(thisIssue.unitcode);
            users.AddRange(this.issuesDataAccess.GetCustomPresenterInUnit(thisIssue.unitcode));
            
            resView.ViewData["users"] = users;
            
            return resView;
        }
        [HttpPost]
        public ActionResult SubmitIssueApprove()
        {
            var jsonText = Request.Params["json"];
            var thisIssue = Newtonsoft.Json.JsonConvert.DeserializeObject<issuesDto>(jsonText);
            var oldIssue = this.issuesDataAccess.GetById(thisIssue.IssueId);
            if (oldIssue == null)
            {
                return Content("Issue Id Invalid");
            }
            var olderPresenterCode = oldIssue.InchargeUserCode;
            thisIssue.ApproverCode = this.tpsSession.userInfo.CODE;
            this.CheckForApprovePermission(thisIssue.unitcode);
            if (this.issuesDataAccess.SavePartialForStep2(thisIssue))
            {
                try
                {
                    thisIssue.GroupId = this.issuesDataAccess.GetGroupByPlantCode(thisIssue.plantshortcode).GroupId;
                    TempData["SyncMessage"] = "บันทึกสำเร็จเรียบร้อยแล้วครับ";
                    if (thisIssue.StatusEnum == IssuesStatus.Approve)
                    {
                        
                        var presenter = this.userManage.GetByCode(thisIssue.InchargeUserCode);
                        if (!string.IsNullOrEmpty(presenter.EmailAddr))
                        {
                            if (!string.IsNullOrEmpty(olderPresenterCode))
                            {
                                var mailWriterForChange = MailWriter.Create(Properties.Settings.Default.MailServer, Properties.Settings.Default.MailUser, Properties.Settings.Default.MailPassword, this.mailLogger);
                                var mailCreatorForChange = new MailContentCreator(this.issuesDataAccess, this.userManage);
                                var mailContentForChange = mailCreatorForChange.ChangeApproverMail(olderPresenterCode, thisIssue.BillNo, thisIssue.WorkGroupNameTh);
                                mailWriterForChange.SetByContentTo(mailContentForChange);
                                mailWriterForChange.Send(MailLogType.ApproverChangePresenter, thisIssue.GroupId);
                            }
                            var mailWriter = MailWriter.Create(Properties.Settings.Default.MailServer, Properties.Settings.Default.MailUser, Properties.Settings.Default.MailPassword, this.mailLogger);
                            var mailCreator = new MailContentCreator(this.issuesDataAccess, this.userManage);
                            var mailContent = mailCreator.ApprovedBillMail(thisIssue.IssueId);
                            mailWriter.SetByContentTo(mailContent);
                            mailWriter.Send(MailLogType.ApproverAssignPresenter, thisIssue.GroupId);
                            mailWriter.SetByContentCC(mailContent);
                            mailWriter.Send(MailLogType.ApproverAssignPresenter, thisIssue.GroupId);
                        }
                        if (!string.IsNullOrEmpty(tpsSession.MockFromUser))
                        {
                            this.userManage.LogUserApprove(tpsSession.MockFromUser, this.tpsSession.userInfo.CODE, thisIssue.BillNo, Request.UserHostAddress);
                        }
                        return RedirectToAction("Approve", new { controller = "Issues", action = "Approve", id = thisIssue.IssueId });
                    }
                    else
                    {
                        if (thisIssue.StatusEnum == IssuesStatus.Cancel)
                        {
                            var mailWriter = MailWriter.Create(Properties.Settings.Default.MailServer, Properties.Settings.Default.MailUser, Properties.Settings.Default.MailPassword, this.mailLogger);
                            var mailCreator = new MailContentCreator(this.issuesDataAccess, this.userManage);
                            var mailContent = mailCreator.CancelBillMail(thisIssue.IssueId);
                            mailWriter.SetByContentAll(mailContent);
                            mailWriter.Send(MailLogType.ApproverCancel, thisIssue.GroupId);
                        }
                        if (!string.IsNullOrEmpty(tpsSession.MockFromUser))
                        {
                            this.userManage.LogUserCancel(tpsSession.MockFromUser, this.tpsSession.userInfo.CODE, thisIssue.BillNo, Request.UserHostAddress);
                        }
                        return RedirectToAction("Approve", new { controller = "Issues", action = "Approve", id = thisIssue.IssueId });
                    }
                }
                catch (System.Net.Mail.SmtpException)
                {
                    TempData["SyncMessage"] = "บันทึกสำเร็จเรียบร้อยแล้วครับ แต่พบข้อผิดพลาดไม่สามารถส่งอีเมล์เข้าหาผู้ร่วมประชุมได้";
                    return RedirectToAction("Approve", new { controller = "Issues", action = "Approve", id = thisIssue.IssueId });
                }
            }
            else
            {
                throw new Exception("Unexpected Error");
            }
        }

        #endregion

        #region Issues Editor Step 3

        private bool IsIssueCanNotSolveDetail(issuesDto thisIssue)
        {
            var isPermission = (thisIssue.StatusEnum == IssuesStatus.New || 
                thisIssue.StatusEnum == IssuesStatus.Cancel || 
                thisIssue.StatusEnum == IssuesStatus.Waiting);
            var isStillPending = false;
            if (thisIssue.StatusEnum == IssuesStatus.Pending)
            {
                isStillPending = true;
                var summary = this.issuesDataAccess.GetSummaryById(thisIssue.IssueId);
                if (summary != null && summary.StatusEnum == SummaryStatus.Remeeting)
                {
                    isStillPending = false;
                }
            }
            isPermission = isPermission || isStillPending;
            return (isPermission);
            
        }
        private bool IsThisUserCanSolveDetail(issuesDto thisIssue)
        {
            return (thisIssue.InchargeUserCode == this.tpsSession.userInfo.CODE) || this.tpsSession.userInfo.UNITCODE == this.issuesDataAccess.GetMasterUnitCode();
        }
        public ActionResult SolveDetail(int id)
        {
            var thisIssue = this.issuesDataAccess.GetById(id);
            if (thisIssue == null || thisIssue.IsDeleted)
            {
                throw new TpsUnauthorizedException("You don't have permission to edit issue");
            }
            var viewModel = this.issuesDataAccess.GetProblemDetailByIssue(id);
            thisIssue.problem = viewModel;
            if (IsIssueCanNotSolveDetail(thisIssue))
            {
                //throw new TpsUnauthorizedException("Issue is not in state to solve yet. Please Approved Issue first");
                return RedirectToAction("Approve", new { controller = "Issues", action = "Approve", id = thisIssue.IssueId });
            }
            
            var resView = View("IssueDetail", thisIssue);
            var isEditable = this.IsThisUserCanSolveDetail(thisIssue);
            isEditable = isEditable && (thisIssue.StatusEnum != IssuesStatus.Recorded && thisIssue.StatusEnum != IssuesStatus.ClosedWaitKM);
            resView.ViewData["IsEditable"] = isEditable;

            return resView;
        }

        public ActionResult SubmitIssuesDetailFile()
        {
            try
            {
                var issueId = int.Parse(Request.Params["issueId"]);
                var thisIssue = this.issuesDataAccess.GetById(issueId);
                if (thisIssue == null)
                {
                    throw new TpsUnauthorizedException("You don't have permission to edit issue");
                }
                if (!this.IsThisUserCanSolveDetail(thisIssue))
                {
                    throw new TpsUnauthorizedException("You don't have permission to edit issue");
                }
                if (this.IsIssueCanNotSolveDetail(thisIssue))
                {
                    throw new TpsUnauthorizedException("Please Approved Issue first");
                }
                if (thisIssue.StatusEnum == IssuesStatus.Recorded || thisIssue.StatusEnum == IssuesStatus.ClosedWaitKM)
                {
                    throw new TpsUnauthorizedException("Issue is closed.");
                }
                var uploadFile = Request.Files["Filedata"];
                var isUploadSuccess = this.issuesDataAccess.UploadFileToProblem(issueId, uploadFile);
                if (isUploadSuccess)
                {
                    return Json(APIResult.CreateResult("File Upload Success"), JsonRequestBehavior.AllowGet);
                }
                else
                    throw new Exception("Upload Failed");
            }
            catch (Exception ex)
            {
                return Json(APIResult.ExceptionResult(ex), JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult SubmitIssueDetail(int id)
        {
            
            var thisProblem = Newtonsoft.Json.JsonConvert.DeserializeObject<problemDto>(Request.Params["json"]);
            var toDelFileId = Newtonsoft.Json.JsonConvert.DeserializeObject<List<int>>(Request.Params["delFileIdsJson"]);
            var thisIssue = this.issuesDataAccess.GetById(id);
            if (!this.IsThisUserCanSolveDetail(thisIssue))
            {
                throw new TpsUnauthorizedException("You don't have permission to edit issue");
            }
            if (this.IsIssueCanNotSolveDetail(thisIssue))
            {
                throw new TpsUnauthorizedException("Issue is closed.");
            }
            
            toDelFileId.ForEach(c =>
                this.issuesDataAccess.DeleteFileFromProblem(c)
                );
            
            if (this.issuesDataAccess.SavePartialForStep3(thisProblem))
            {
                TempData["SyncMessage"] = "บันทึกสำเร็จเรียบร้อยแล้วครับ";
                return RedirectToAction("KM", new { controller = "Issues", action = "SolveDetail", id = thisProblem.IssueId });
            }
            else
            {
                throw new Exception("Unexpected Error");
            }
        }

        #endregion

        #region Issues Editor Step 4
        private bool IsCanKm(issuesDto issue)
        {
            return (issue.InchargeUserCode == thisUser.CODE || thisUser.CODE == issuesDataAccess.GetMasterUnitCode());
        }
        public ActionResult KM(int id)
        {
            var issue = this.issuesDataAccess.GetById(id);
            if (issue == null || issue.IsDeleted)
            {
                throw new TpsUnauthorizedException("You don't have permission to view issue");
            }
            var isEditAble = true;

            #region Check Permission

            if (!this.CheckForEditKmPermission())
            {
                isEditAble = false;
            }

            #endregion

            // Check If Already Meeting
            if (this.issuesDataAccess.GetLatestMeetingByIssueId(id) == null)
            {
                //isEditAble = false;
            }

            var KMSummary = this.issuesDataAccess.GetSummaryById(id);
            issue.summary = KMSummary;

            var resview = View("IssueKM", issue);
            resview.ViewData["isEditable"] = isEditAble;
            bool isKmAble = isEditAble;
            if (!isEditAble)
            {
                if (this.IsCanKm(issue))
                {
                    isKmAble = true;
                }
            }
            resview.ViewData["isKmAble"] = isKmAble;

            #region Get Meeting Date
            string meetingDate = string.Empty;
            if (issue.StatusEnum == IssuesStatus.Ready)
            {
                meetingDate = "ใบแจ้งซ่อมยังมิได้นำเข้าวาระการประชุม";
            }
            else
            {
                meetingDate = this.issuesDataAccess.GetMeetingDateByIssues(issue.IssueId).TpsReportDateTime();
            }
            resview.ViewData["meetingDate"] = meetingDate;
            if (issue.StatusEnum==IssuesStatus.Ready)
            {
                resview.ViewData["nextMeetingDate"] = "ยังมิได้นัดการประชุมครั้งถัดไป";
            }
            else if (issue.StatusEnum == IssuesStatus.PendingAfterMeeting)
            {
                resview.ViewData["nextMeetingDate"] = this.issuesDataAccess.nextMeetingDateOnMonth(issue.GroupId, issue.summary.ResumMonth.GetValueOrDefault(1)).TpsReportDateTime();
            }
            else
            {
                resview.ViewData["nextMeetingDate"] = "วาระถูกสรุปเรียบร้อยแล้วครับ";
            }
            
            #endregion

            return resview;
        }

        public ActionResult SubmitKM()
        {
            var issue = this.issuesDataAccess.GetById(int.Parse(Request.Params["issueid"]));
            if (issue == null || !this.CheckForSummaryPermission(issue.unitcode))
            {
                return RedirectToAction("Index", new { controller = "Summary", action = "Index" });
            }
            var statusBeforeUpdate = issue.StatusEnum;
            var thisKM = Newtonsoft.Json.JsonConvert.DeserializeObject<issuesDto>(Request.Params["json"]);
            var toDelFileId = Newtonsoft.Json.JsonConvert.DeserializeObject<List<int>>(Request.Params["delFileIdsJson"]);
            toDelFileId.ForEach(c => this.issuesDataAccess.DeleteFileFromKM(c));
            if (this.issuesDataAccess.SavePartialForStep4(thisKM.summary))
            {
                var newIssue = this.issuesDataAccess.GetById(int.Parse(Request.Params["issueid"]));
                if (statusBeforeUpdate == IssuesStatus.Ready && newIssue.StatusEnum == IssuesStatus.ClosedWaitKM)
                {
                    var mailWriter = MailWriter.Create(Properties.Settings.Default.MailServer, Properties.Settings.Default.MailUser, Properties.Settings.Default.MailPassword, this.mailLogger);
                    var mailCreator = new MailContentCreator(this.issuesDataAccess, this.userManage);
                    var mailContent = mailCreator.ClosedMail(newIssue.IssueId);
                    mailWriter.SetByContentAll(mailContent);
                    mailWriter.Send(MailLogType.CaseClosed, newIssue.GroupId);
                }
                if (newIssue.StatusEnum == IssuesStatus.Recorded && statusBeforeUpdate != IssuesStatus.Recorded)
                {
                    var mailWriter = MailWriter.Create(Properties.Settings.Default.MailServer, Properties.Settings.Default.MailUser, Properties.Settings.Default.MailPassword, this.mailLogger);
                    var mailCreator = new MailContentCreator(this.issuesDataAccess, this.userManage);
                    var mailContent = mailCreator.RecordedMail(newIssue.IssueId);
                    mailWriter.SetByContentAll(mailContent);
                    mailWriter.Send(MailLogType.CaseKm, newIssue.GroupId);
                }
                if (newIssue.StatusEnum == IssuesStatus.PendingAfterMeeting && statusBeforeUpdate != IssuesStatus.PendingAfterMeeting)
                {
                    var mailWriter = MailWriter.Create(Properties.Settings.Default.MailServer, Properties.Settings.Default.MailUser, Properties.Settings.Default.MailPassword, this.mailLogger);
                    var mailCreator = new MailContentCreator(this.issuesDataAccess, this.userManage);
                    var mailContent = mailCreator.ClosedToPendingMail(newIssue.IssueId);
                    mailWriter.SetByContentTo(mailContent);
                    mailWriter.Send(MailLogType.CaseReMeeting, newIssue.GroupId);
                    mailWriter.SetByContentCC(mailContent);
                    mailWriter.Send(MailLogType.CaseReMeeting, newIssue.GroupId);
                    
                }
                TempData["SyncMessage"] = "บันทึกสำเร็จเรียบร้อยแล้วครับ";
                return RedirectToAction("Index", new { controller = "Summary", action = "Index" });
            }
            else
            {
                throw new Exception("Unexpected Error");
            }
        }


        public ActionResult SubmitIssuesKMFile()
        {
            try
            {
                var issueId = int.Parse(Request.Params["issueId"]);
                var thisIssue = this.issuesDataAccess.GetById(issueId);
                if (thisIssue == null)
                {
                    throw new TpsUnauthorizedException("You don't have permission to edit issue");
                }
                if (!this.CheckForEditDetailPermission(thisIssue) && !CheckForSummaryPermission(thisIssue.unitcode))
                {
                    throw new TpsUnauthorizedException("You don't have permission to edit issue");
                }
                var statusBeforeUpdate = thisIssue.StatusEnum;

                var uploadFile = Request.Files["Filedata"];
                var isUploadSuccess = this.issuesDataAccess.UploadFileToKM(issueId, uploadFile);
                if (isUploadSuccess)
                {
                    var newIssue = this.issuesDataAccess.GetById(int.Parse(Request.Params["issueid"]));
                    if (statusBeforeUpdate == IssuesStatus.Ready && newIssue.StatusEnum == IssuesStatus.ClosedWaitKM)
                    {
                        var mailWriter = MailWriter.Create(Properties.Settings.Default.MailServer, Properties.Settings.Default.MailUser, Properties.Settings.Default.MailPassword, this.mailLogger);
                        var mailCreator = new MailContentCreator(this.issuesDataAccess, this.userManage);
                        var mailContent = mailCreator.ClosedMail(newIssue.IssueId);
                        mailWriter.SetByContentAll(mailContent);
                        mailWriter.Send(MailLogType.CaseClosed, newIssue.GroupId);
                    }
                    if (newIssue.StatusEnum == IssuesStatus.Recorded && statusBeforeUpdate != IssuesStatus.Recorded)
                    {
                        var mailWriter = MailWriter.Create(Properties.Settings.Default.MailServer, Properties.Settings.Default.MailUser, Properties.Settings.Default.MailPassword, this.mailLogger);
                        var mailCreator = new MailContentCreator(this.issuesDataAccess, this.userManage);
                        var mailContent = mailCreator.RecordedMail(newIssue.IssueId);
                        mailWriter.SetByContentAll(mailContent);
                        mailWriter.Send(MailLogType.CaseKm, newIssue.GroupId);
                    }
                    if (newIssue.StatusEnum == IssuesStatus.PendingAfterMeeting && statusBeforeUpdate != IssuesStatus.PendingAfterMeeting)
                    {
                        var mailWriter = MailWriter.Create(Properties.Settings.Default.MailServer, Properties.Settings.Default.MailUser, Properties.Settings.Default.MailPassword, this.mailLogger);
                        var mailCreator = new MailContentCreator(this.issuesDataAccess, this.userManage);
                        var mailContent = mailCreator.ClosedToPendingMail(newIssue.IssueId);
                        mailWriter.SetByContentTo(mailContent);
                        mailWriter.Send(MailLogType.CaseReMeeting, newIssue.GroupId);
                        mailWriter.SetByContentCC(mailContent);
                        mailWriter.Send(MailLogType.CaseReMeeting, newIssue.GroupId);
                    }
                    return Json(APIResult.CreateResult("File Upload Success"), JsonRequestBehavior.AllowGet);
                }
                else
                    throw new Exception("Upload Failed");
            }
            catch (Exception ex)
            {
                return Json(APIResult.ExceptionResult(ex), JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        public ActionResult GetAllBill()
        {
            #region Check Permission

            var userRight = this.userManage.GetUserRights(this.tpsSession.userInfo);

#if DEVELOPING
            userRight |= UserRights.SystemAdministrator;
#endif
            if (!(userRight.HasFlag(UserRights.SystemAdministrator) || (userRight.HasFlag(UserRights.CreateNewProblem) && this.tpsSession.userInfo.UNITCODE == Request.Params["unitcode"])))
            {
                throw new Exception("คุณไม่มีสิทธิ์ที่จะสร้างใบแจ้งซ่อมในหน่วยงานที่เลือก");
            }

            #endregion

            return Json(this.issuesDataAccess.GetAllSapBill(), JsonRequestBehavior.AllowGet);
        }

        public ActionResult ApproveWantedMail()
        {
            var unitcode = Request.Params["unitcode"];
            var mailList = this.issuesDataAccess.GetAllApprover(unitcode).Select(r => r.EmailAddr).ToList();
            return Json(mailList, JsonRequestBehavior.AllowGet);
        }
        public ActionResult CCMail()
        {
            var unitcode = Request.Params["unitcode"];
            var mailList = this.issuesDataAccess.GetCCApprover(unitcode).Select(r => r.EmailAddr).ToList();
            return Json(mailList, JsonRequestBehavior.AllowGet);
        }
        public ActionResult PresenterMail()
        {
            var issueid = int.Parse(Request.Params["issueid"]);
            var presenterCode = this.issuesDataAccess.GetById(issueid).InchargeUserCode;
            var user = this.userManage.GetByCode(presenterCode);
            return Json(user.EmailAddr, JsonRequestBehavior.AllowGet);

        }
        public ActionResult ForceMemoScheduler()
        {
            if (!IsCanBreakdown())
                return null;
            try
            {
                var res = TpsConfigurator.ArgsResolver.ResolveCommandLineArgs(true, Server.MapPath("~"), TpsConfigurator.ResolveMode.Meeting);
                return Json(WebHelper.APIResult.CreateResult(res.BreakDownCount));
            }
            catch (Exception ex)
            {
                return Json(WebHelper.APIResult.ExceptionResult(ex));
            }
        }
        public ActionResult ForceBreakdownScheduler()
        {
            if (!IsCanBreakdown())
                return null;
            try
            {
                var res = TpsConfigurator.ArgsResolver.ResolveCommandLineArgs(true, Server.MapPath("~"), TpsConfigurator.ResolveMode.Breakdown);
                return Json(WebHelper.APIResult.CreateResult(res.BreakDownCount));
            }
            catch (Exception ex)
            {
                return Json(WebHelper.APIResult.ExceptionResult(ex));
            }
        }
        public ActionResult Unbreakdown(int issueId, string reason)
        {
            var test = 0;
            if (!IsCanUnbreakdown())
            {
                return Json(APIResult.ErrorResult("คุณไม่มีสิทธิ์ในการแก้ไข"));
            }
            if (this.issuesDataAccess.Unbreakdown(issueId, reason, thisUser.CODE))
            {
                return Json(APIResult.CreateResult("Success"));
            }
            else
            {
                return Json(APIResult.ErrorResult("ใบแจ้งซ่อมดังกล่าวไม่อยู่ในสภาพที่สามารถทำการ Unbreakdown ได้ กรุณาตรวจสอบว่าใบแจ้งซ่อมดังกล่าวเป็นใบที่พึ่ง Breakdown และยังไม่ได้ถูกลบออกจากระบบ"));
            }
        }

        #region Recycle bin management

        public ActionResult Delete(int issueId, string reason)
        {
            var edittedIssue = this.issuesDataAccess.GetById(issueId);
            if (edittedIssue == null || edittedIssue.IsDeleted || edittedIssue.IsUnbreakdown || !IsCanApprove(edittedIssue.unitcode))
            {
                return Json(APIResult.ErrorResult("No Permission"));
            }
            if (this.issuesDataAccess.Delete(issueId, reason, thisUser.CODE))
            {
                return Json(APIResult.CreateResult("Success"));
            }
            else
            {
                return Json(APIResult.ErrorResult("ไม่สามารถลบได้"));
            }
        }

        [HttpPost]
        public ActionResult RecoverIssue(int issueId)
        {
            if (!IsCanRecover())
            {
                return Json(APIResult.ErrorResult("No Permission to recover"));
            }
            if (this.issuesDataAccess.RecoverIssue(issueId))
            {
                var thisIssue = this.issuesDataAccess.GetById(issueId);
                if (thisIssue.StatusEnum == IssuesStatus.Waiting)
                {
                    this.sendWaitingMail(thisIssue);
                }
                return Json(APIResult.CreateResult("Success"));
            }
            else
            {
                return Json(APIResult.ErrorResult("Cannot Recover"));
            }
        }

        [HttpPost]
        public ActionResult DestroyIssue(int issueId)
        {
            if (!IsCanRecover())
            {
                return Json(APIResult.ErrorResult("No Permission to recover"));
            }
            if (this.issuesDataAccess.DestroyIssue(issueId))
            {
                return Json(APIResult.CreateResult("Success"));
            }
            else
            {
                return Json(APIResult.ErrorResult("Cannot Recover"));
            }
        }

        #endregion

        public ActionResult GetNextMeetingDate(int groupId)
        {
            try
            {
                if (!tpsSession.IsAuthen)
                    return Content("No permission");
                var monthNoStr = Request.Params["monthNo"];
                DateTime nextMeetingDate;
                int monthNo = 0;
                if (!int.TryParse(monthNoStr, out monthNo))
                {
                    nextMeetingDate = this.issuesDataAccess.nextMeetingDate(groupId);
                }
                else
                {
                    nextMeetingDate = this.issuesDataAccess.nextMeetingDateOnMonth(groupId, monthNo);
                }
                return Json(APIResult.CreateResult(nextMeetingDate.TpsReportDateTime()), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(APIResult.ExceptionResult(ex), JsonRequestBehavior.AllowGet);
            }
        }
        
    }
}

