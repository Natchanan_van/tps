﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TPSWorkflow.Controllers
{
    public class LogoutController : BaseController
    {
        //
        // GET: /Logout/
        public ActionResult Index()
        {
            this.tpsSession.Destroy();
            return RedirectToAction("Index", "Authen");
        }

        public ActionResult L()
        {
            this.tpsSession.Destroy();
            return Content("Logout Success");
        }
	}
}