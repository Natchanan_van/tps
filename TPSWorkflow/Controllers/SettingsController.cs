﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using TPSWorkflow.DAL;
using TPSWorkflow.DAL.DtoS;

namespace TPSWorkflow.Controllers
{
    [TPSAuthen]
    public class SettingsController : BaseController
    {
        //
        // GET: /Settings/
        protected IIssuesData issuesDataAccess = null;
        protected IUsersManage userManage = null;
        protected IMailLogger mailLogger = null;

        public SettingsController(IIssuesData access, IUsersManage user, IMailLogger mailLogger)
        {
            this.issuesDataAccess = access;
            
            this.userManage = user;
            this.mailLogger = mailLogger;
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);
            this.issuesDataAccess.SetPermission(this.tpsSession.userInfo, this.userManage);
        }

        private bool CheckAdminPermission()
        {
            var userRights = this.userManage.GetUserRights(this.tpsSession.userInfo);
#if DEVELOPING
            userRights |= UserRights.SystemAdministrator;
#endif
            return userRights.HasFlag(UserRights.Administrator) || this.tpsSession.userInfo.UNITCODE == this.issuesDataAccess.GetMasterUnitCode();
        }

        public bool UpdateShiftingDate(int groupId, bool isShift, DateTime? shiftDate, string location)
        {
            var oldMeetingDate = this.issuesDataAccess.nextMeetingDateIncludeShiftDate(groupId);
            var isUpdateSuccess = this.issuesDataAccess.UpdateShiftingDate(groupId, isShift, shiftDate, location);
            if (!isUpdateSuccess)
                return false;
            if (isShift && shiftDate != null && shiftDate > DateTime.Now && (shiftDate - DateTime.Now) <= TimeSpan.FromDays(7) && shiftDate.Value.Date != oldMeetingDate.Date)
            {
                var mailWriter = MailWriter.Create(Properties.Settings.Default.MailServer, Properties.Settings.Default.MailUser, Properties.Settings.Default.MailPassword, this.mailLogger);
                var groupData = this.issuesDataAccess.GetGroupById(groupId);
                var mailContentCreator = new MailContentCreator(this.issuesDataAccess, this.userManage);
                var content = mailContentCreator.ShiftedMail(groupId, shiftDate.Value, oldMeetingDate);
                var timeStart = shiftDate.Value.Date + groupData.TimeStart.GetValueOrDefault(DateTime.Now.Date).TimeOfDay;
                var timeEnd = shiftDate.Value.Date + groupData.TimeEnd.GetValueOrDefault(DateTime.Now.Date).TimeOfDay;

                if (string.IsNullOrEmpty(groupData.Location))
                    location = this.issuesDataAccess.GetDefaultMeetingLocation();
                else
                    location = groupData.Location;
                System.Net.Mail.AlternateView iCalView;
                if (oldMeetingDate < DateTime.Now.Date.AddDays(7))
                    iCalView = Ecobz.Utilities.iCalHelper.CreateICalContent(timeEnd, timeStart, location, content.Subject, string.Empty, groupData.LatestCalendarId);
                else
                {
                    var newCalendarId = this.issuesDataAccess.GenerateNewCalendarId(groupId);
                    iCalView = Ecobz.Utilities.iCalHelper.CreateICalContent(timeEnd, timeStart, location, content.Subject, string.Empty, newCalendarId);
                }
                mailWriter.AddAlternateView(iCalView);
                mailWriter.SetByContentTo(content);
                mailWriter.Send(MailLogType.ShiftDate, groupId);
            }
            return true;
        }
       
        #region Approver

        public ActionResult DynApprover()
        {
            if (!CheckAdminPermission())
                throw new TpsUnauthorizedException("You don't have permission to edit setting");
            
            var wgData = this.issuesDataAccess.GetAllWorkGroup();
            var approverData = (from wg in wgData
                                select new { key = wg.unitcode, val = this.issuesDataAccess.GetMainApprover(wg.unitcode) }).ToDictionary(d => d.key, d2 => d2.val);
            var viewModel = new ApproverViewModel()
            {
                WorkGroups = wgData,
                MainApprover = approverData
            };
            var resView = View("DynamicApprover", viewModel);
            return resView;
        }
        public ActionResult GetApproverList()
        {
            try
            {
                if (!CheckAdminPermission())
                    throw new TpsUnauthorizedException("You don't have permission to edit setting");
                var approverList = this.issuesDataAccess.GetCustomApproverInUnit(Request.Params["unitCode"]);
                return Json(WebHelper.APIResult.CreateResult(approverList));
            }
            catch (Exception ex)
            {
                return Json(WebHelper.APIResult.ExceptionResult(ex));
            }
        }
        public ActionResult GetPresenterList()
        {
            try
            {
                if (!CheckAdminPermission())
                    throw new TpsUnauthorizedException("You don't have permission to edit setting");
                var approverList = this.issuesDataAccess.GetCustomPresenterInUnit(Request.Params["unitCode"]);
                return Json(WebHelper.APIResult.CreateResult(approverList));
            }
            catch (Exception ex)
            {
                return Json(WebHelper.APIResult.ExceptionResult(ex));
            }
        }
        public ActionResult GetUsersList()
        {
            try
            {
                if (!CheckAdminPermission())
                    throw new TpsUnauthorizedException("You don't have permission to edit setting");
                var userList = this.userManage.GetByWorkGroup(Request.Params["unitCode"], Request.Params["searchTerm"]);
                var approverListCode = this.issuesDataAccess.GetCustomApproverInUnit(Request.Params["unitCode"]).Select(r=>r.CODE).ToList();
                var userListNoApprover = userList.Where(r => !approverListCode.Contains(r.CODE)).ToList();
                
                return Json(WebHelper.APIResult.CreateResult(userListNoApprover));
            }
            catch (Exception ex)
            {
                return Json(WebHelper.APIResult.ExceptionResult(ex));
            }
        }
        public ActionResult GetAllUsersListPresenter()
        {
            try
            {
                if (!CheckAdminPermission())
                    throw new TpsUnauthorizedException("You don't have permission to edit setting");
                var userList = this.userManage.GetAllVBG(Request.Params["searchTerm"]);
                var approverListCode = this.issuesDataAccess.GetCustomPresenterInUnit(Request.Params["unitCode"]).Select(r => r.CODE).ToList();
                var userListNoApprover = userList.Where(r => !approverListCode.Contains(r.CODE)).ToList();
                return Json(WebHelper.APIResult.CreateResult(userListNoApprover));
            }
            catch (Exception ex)
            {
                return Json(WebHelper.APIResult.ExceptionResult(ex));
            }
        }
        public ActionResult GetAllUsersListApprover()
        {
            try
            {
                if (!CheckAdminPermission())
                    throw new TpsUnauthorizedException("You don't have permission to edit setting");
                var userList = this.userManage.GetAllVBG(Request.Params["searchTerm"]);
                var approverListCode = this.issuesDataAccess.GetCustomApproverInUnit(Request.Params["unitCode"]).Select(r => r.CODE).ToList();
                var userListNoApprover = userList.Where(r => !approverListCode.Contains(r.CODE)).ToList();
                return Json(WebHelper.APIResult.CreateResult(userListNoApprover));
            }
            catch (Exception ex)
            {
                return Json(WebHelper.APIResult.ExceptionResult(ex));
            }
        }
        public ActionResult AddApprovers()
        {
            try
            {
                if (!CheckAdminPermission())
                    throw new TpsUnauthorizedException("You don't have permission to edit setting");
                var codeArray = Newtonsoft.Json.JsonConvert.DeserializeObject<List<string>>(Request.Params["userCodes"]);
                var unitCode = Request.Params["unitcode"];
                if (this.issuesDataAccess.AddApproversByCode(codeArray, unitCode))
                    return Json(WebHelper.APIResult.CreateResult(null));
                else
                    throw new Exception("Unexpected Error");
                
            }
            catch (Exception ex)
            {
                return Json(WebHelper.APIResult.ExceptionResult(ex));
            }
        }
        public ActionResult DelApprover()
        {
            try
            {
                if (!CheckAdminPermission())
                    throw new TpsUnauthorizedException("You don't have permission to edit setting");
                var usercode = Request.Params["usercode"];
                var unitCode = Request.Params["unitcode"];
                if (this.issuesDataAccess.DeleteApprover(usercode,unitCode))
                    return Json(WebHelper.APIResult.CreateResult(null));
                else
                    throw new Exception("Unexpected Error");

            }
            catch (Exception ex)
            {
                return Json(WebHelper.APIResult.ExceptionResult(ex));
            }
        }
        public ActionResult AddPresenter()
        {
            try
            {
                if (!CheckAdminPermission())
                    throw new TpsUnauthorizedException("You don't have permission to edit setting");
                var codeArray = Newtonsoft.Json.JsonConvert.DeserializeObject<List<string>>(Request.Params["userCodes"]);
                var unitCode = Request.Params["unitcode"];
                if (this.issuesDataAccess.AddPresenterByCode(codeArray, unitCode))
                    return Json(WebHelper.APIResult.CreateResult(null));
                else
                    throw new Exception("Unexpected Error");

            }
            catch (Exception ex)
            {
                return Json(WebHelper.APIResult.ExceptionResult(ex));
            }
        }
        public ActionResult DelPresenter()
        {
            try
            {
                if (!CheckAdminPermission())
                    throw new TpsUnauthorizedException("You don't have permission to edit setting");
                var usercode = Request.Params["usercode"];
                var unitCode = Request.Params["unitcode"];
                if (this.issuesDataAccess.DeletePresenter(usercode, unitCode))
                    return Json(WebHelper.APIResult.CreateResult(null));
                else
                    throw new Exception("Unexpected Error");

            }
            catch (Exception ex)
            {
                return Json(WebHelper.APIResult.ExceptionResult(ex));
            }
        }
        #endregion

        #region Scheduler

        public ActionResult Scheduler()
        {
            if (!CheckAdminPermission())
                throw new TpsUnauthorizedException("You don't have permission to edit setting");
            var isCanShift = this.userManage.isCanShiftMeeting(this.tpsSession.userInfo);
            var groupData = this.issuesDataAccess.GetAllGroups();
            var plantsData = this.issuesDataAccess.GetAllPlantCode();
            groupData.ForEach(c => c.allowplant = this.issuesDataAccess.GetPlantByGroup(c.GroupId));
            groupData.ForEach(c => c.group_mailing = this.issuesDataAccess.GetMailingByGroup(c.GroupId));
            groupData.ForEach(c => c.Location = string.IsNullOrEmpty(c.Location) ? string.Empty : c.Location);
            var dates = groupData.Select(r => this.issuesDataAccess.nextMeetingDateIncludeShiftDate(r.GroupId)).ToList();

            #region Resolve Suspended Date

            foreach (var g in groupData)
            {
                if (g.IsSuspended && g.SuspendedEnd != null && DateTime.Now > g.SuspendedEnd.GetValueOrDefault(DateTime.MinValue))
                {
                    g.IsSuspended = false;
                }
            }

            #endregion

            var viewModel = new SchedulerViewModel()
            {
                Groups = groupData,
                AllPlants = plantsData,
                MeetingDates = dates
            };
            var resView = View("Scheduler", viewModel);
            resView.ViewData["isCanShiftMeeting"] = isCanShift;
            resView.ViewData["defaultRoom"] = this.issuesDataAccess.GetDefaultMeetingLocation();
            return resView;
        }
        public ActionResult GetMailer()
        {
            try
            {
                if (!CheckAdminPermission())
                    throw new TpsUnauthorizedException("You don't have permission to edit setting");
                var mailerList = this.issuesDataAccess.GetMailingUserDetailByGroup(int.Parse(Request.Params["groupid"]));
                return Json(WebHelper.APIResult.CreateResult(mailerList));
            }
            catch (Exception ex)
            {
                return Json(WebHelper.APIResult.ExceptionResult(ex));
            }
        }
        public ActionResult GetMailableUserList()
        {
            try
            {
                if (!CheckAdminPermission())
                    throw new TpsUnauthorizedException("You don't have permission to edit setting");
                var list = this.issuesDataAccess.GetMailableByGroup(Request.Params["unitcode"], Request.Params["searchterm"]);
                return Json(WebHelper.APIResult.CreateResult(list));
            }
            catch (Exception ex)
            {
                return Json(WebHelper.APIResult.ExceptionResult(ex));
            }
        }
        public ActionResult SubmitScheduler()
        {
            if (!CheckAdminPermission())
                throw new TpsUnauthorizedException("You don't have permission to edit setting");
            var isCanShift = this.userManage.isCanShiftMeeting(this.tpsSession.userInfo);
            var groupId = int.Parse(Request.Params["groupId"]);
            var newGroups = Newtonsoft.Json.JsonConvert.DeserializeObject<List<string>>(Request.Params["newGroups"]);
            var addUsers = Newtonsoft.Json.JsonConvert.DeserializeObject<List<string>>(Request.Params["addUsers"]);
            var delUsers = Newtonsoft.Json.JsonConvert.DeserializeObject<List<string>>(Request.Params["delUsers"]);
            var dayInWeek = int.Parse(Request.Params["dayInWeek"]);
            var weekmonth = int.Parse(Request.Params["weekmonth"]);
            var starttimehr = int.Parse(Request.Params["starttimehr"]);
            var starttimemin = int.Parse(Request.Params["starttimemin"]);
            var endtimehr = int.Parse(Request.Params["endtimehr"]);
            var endtimemin = int.Parse(Request.Params["endtimemin"]);
            var isSuspended = int.Parse(Request.Params["isSuspended"]);
            var locationRoom = Request.Params["meetingLocation"];
            DateTime? suspendDate = null;
            if (isSuspended != 0)
            {
                suspendDate = DateTime.ParseExact(Request.Params["susDate"], "dd-MMM-yyyy", new System.Globalization.CultureInfo("en-US"));
            }
            var isShift = int.Parse(Request.Params["isShift"]);
            DateTime? shiftDate = null;
            if (isShift != 0)
            {
                shiftDate = DateTime.ParseExact(Request.Params["shiftDate"], "dd-MMM-yyyy", new System.Globalization.CultureInfo("en-US"));
            }

            try
            {
                if (newGroups.Count > 0)
                {
                    if (!this.issuesDataAccess.UpdatePlantsInGroup(groupId, newGroups))
                        throw new Exception("Unable to update plants");
                }
                if (!this.issuesDataAccess.AddCustomMailers(groupId, addUsers))
                    throw new Exception("Unable to add mailer");
                if (!this.issuesDataAccess.DelCustomMailers(groupId, delUsers))
                    throw new Exception("Unable to delete mailer");
                
                if (!this.issuesDataAccess.UpdateDayAndWeekInGroup(groupId, dayInWeek, weekmonth, isSuspended == 1, suspendDate, isCanShift))
                    throw new Exception("Unable to update meeting day");

                if (!this.UpdateShiftingDate(groupId, isShift == 1, shiftDate, locationRoom))
                    throw new Exception("Unable to update shifting date");

                if (locationRoom != null)
                    locationRoom = locationRoom.Trim();
                
                var time = new TimeSpan(starttimehr, starttimemin, 0);
                var startTime = DateTime.Now.Date + time;
                time = new TimeSpan(endtimehr, endtimemin, 0);
                var endTime = DateTime.Now.Date + time;
                this.issuesDataAccess.UpdateTime(groupId, startTime, endTime);
                TempData["SyncMessage"] = "บันทึกสำเร็จเรียบร้อยแล้วครับ";
                if (!string.IsNullOrEmpty(Request.Params["currentGroup"]))
                {
                    TempData["currentGroup"] = Request.Params["currentGroup"];
                }
                return RedirectToAction("Scheduler");
                
            }
            catch (Exception ex)
            {
                throw ex;
            }
           

        }
        

        #endregion

        #region TemplateEmail

        public ActionResult TemplateEmail()
        {
            if (!CheckAdminPermission())
                throw new TpsUnauthorizedException("You don't have permission to edit setting");
            var resultView = View("Mail", this.issuesDataAccess.GetMailTemplate());
            resultView.ViewData["LocationRoom"] = this.issuesDataAccess.GetDefaultMeetingLocation();
            return resultView;
        }
        public ActionResult SubmitTemplateEmail()
        {
            if (!CheckAdminPermission())
                throw new TpsUnauthorizedException("You don't have permission to edit setting");
            var k = new mail_templateDto()
            {
                Text1 = Request.Params["text1"],
                Text2 = Request.Params["text2"],
                Text3 = Request.Params["text3"],
                Text4 = Request.Params["text4"],
            };
            this.issuesDataAccess.UpdateMailTemplate(k);
            this.issuesDataAccess.SetDefaultMeetingLocation(Request.Params["locationRoom"]);
            TempData["SyncMessage"] = "บันทึกสำเร็จเรียบร้อยแล้วครับ";
            return RedirectToAction("TemplateEmail");
        }

        #endregion

        #region Custom Users

        public ActionResult Users()
        {
            if (!CheckAdminPermission())
                throw new TpsUnauthorizedException("You don't have permission to edit setting");
            var tpsUsers = this.issuesDataAccess.GetAllTpsUser();
            var viewModel = new UsersViewModel()
            {
                TPSUsers = tpsUsers
            };
            var resView = View("UsersList", viewModel);
            return resView;

        }
        public ActionResult UserEdit(int id)
        {
            if (!CheckAdminPermission())
                throw new TpsUnauthorizedException("You don't have permission to edit setting");
            PersonalInfoDto viewModel = null;
            if (id == -1)
            {
                viewModel = new PersonalInfoDto()
                {
                    TpsUserId = -1
                };
            }
            else
            {
                viewModel = this.issuesDataAccess.GetTpsUserById(id);
                if (viewModel.TpsType == TpsUserType.SuperAdmin && tpsSession.userInfo.TpsType != TpsUserType.SuperAdmin)
                {
                    throw new TpsUnauthorizedException("You don't have permission to edit setting");
                }
            }

            var resView = View("UserEdit", viewModel);
            resView.ViewData["wgList"] = this.issuesDataAccess.GetAllWorkGroup();
            return resView;
        }
        public ActionResult UploadTempPic()
        {
            if (!CheckAdminPermission())
                throw new TpsUnauthorizedException("You don't have permission to edit setting");
            var file = Request.Files["Filedata"];
            var guid = Guid.NewGuid().ToString();
            var newFileName = string.Format("{0}.{1}", guid, System.IO.Path.GetExtension(file.FileName).Trim('.'));
            file.SaveAs(TPSPathHelper.GetTempLocationForFileName(newFileName));

            var resObj = new
            {
                Url = Url.Content("~/Image/Temp?name=" + Url.Encode(newFileName)),
                FileTempId = newFileName
            };
            return Json(WebHelper.APIResult.CreateResult(resObj));
        }
        public ActionResult SubmitUser()
        {
            if (!CheckAdminPermission())
                throw new TpsUnauthorizedException("You don't have permission to edit setting");
            var userInfo = Newtonsoft.Json.JsonConvert.DeserializeObject<PersonalInfoDto>(Request.Params["json"]);
            var password = Request.Params["password"];
            var imgTempName = Request.Params["imgtempname"];
            if (tpsSession.userInfo.TpsType != TpsUserType.SuperAdmin && userInfo.TpsType == TpsUserType.SuperAdmin)
            {
                return Content("You don't have permission to create super admin user");
            }

            var newUserInfo = this.issuesDataAccess.UpdateUser(userInfo, password, imgTempName);
            if (!string.IsNullOrEmpty(imgTempName))
            {
                System.IO.File.Move(TPSPathHelper.GetTempLocationForFileName(imgTempName), TPSPathHelper.GetAvatarLocationForFileName(imgTempName));
            }
            tpsSession.userInfo = newUserInfo;
            TempData["SyncMessage"] = "บันทึกสำเร็จเรียบร้อยแล้วครับ";
            return RedirectToAction("Users");
        }
        [HttpPost]
        public ActionResult DelUser()
        {
            if (!CheckAdminPermission())
                throw new TpsUnauthorizedException("You don't have permission to edit setting");
            try
            {
                var userId = int.Parse(Request.Params["userId"]);
                var user = this.issuesDataAccess.GetTpsUserById(userId);
                if (user.TpsType == TpsUserType.SuperAdmin)
                {
                    return Content("Cannot Delete Super Admin");
                }
                if (!this.issuesDataAccess.DeleteTpsUser(userId))
                {
                    throw new Exception("Unexpected Error");
                }
                return Json(WebHelper.APIResult.CreateResult(null));
            }
            catch (Exception ex)
            {
                return Json(WebHelper.APIResult.ExceptionResult(ex));
            }
        }

        #endregion

        #region Mail Log
        public ActionResult MailHistory()
        {
            if (!CheckAdminPermission())
                throw new TpsUnauthorizedException("You don't have permission to edit setting");
            var resView = View("MailHistory");
            resView.ViewData["groupList"] = this.issuesDataAccess.GetAllGroups();
            return resView;
        }
        public ActionResult MailHistoryData()
        {
            if (!CheckAdminPermission())
                return Content("");
            var dt=Ecobiz.WebUI.DataTables.ServerTableParams.RetrieveFromRequest(Request.Params);
            var searchTerm = Request.Params["searchTerm"];
            var startDateTime = Request.Params["startDateTime"];
            var endDateTime = Request.Params["endDateTime"];
            var mailLogType = Request.Params["mailLogType"];
            int groupId = -1;
            int.TryParse(Request.Params["groupId"], out groupId);
            int mailCount = 0;
            List<MailLogDto> result=null;
            int rowCount = 0;
            if (string.IsNullOrEmpty(startDateTime) || string.IsNullOrEmpty(endDateTime))
                result = this.issuesDataAccess.GetMailLogSearch(dt.Start, dt.Count, out rowCount, searchTerm, (MailLogType)int.Parse(mailLogType));
            else
            {
                DateTime startDate, endDate;
                DateTime.TryParseExact(startDateTime, "d-M-yyyy", new CultureInfo("en-US"), DateTimeStyles.AssumeLocal, out startDate);
                DateTime.TryParseExact(endDateTime, "d-M-yyyy", new CultureInfo("en-US"), DateTimeStyles.AssumeLocal, out endDate);
                endDate = endDate.AddDays(1);
                result = this.issuesDataAccess.GetMailLogSearch(dt.Start, dt.Count, out rowCount, searchTerm, (MailLogType)int.Parse(mailLogType), startDate, endDate, groupId);
                mailCount = this.issuesDataAccess.GetMailLogCount();
            }
            var res = Ecobiz.WebUI.DataTables.TableData<MailLogDto>.CreateData(dt.Echo, mailCount, rowCount, result);
            return Json(res, JsonRequestBehavior.AllowGet);
        }
        #endregion

        public ActionResult GetAllUsers()
        {
            if (!CheckAdminPermission())
                throw new TpsUnauthorizedException("You don't have permission to edit setting");

            var st = Request.Params["searchTerm"];

            return Json(WebHelper.APIResult.CreateResult(this.userManage.GetAll(st).ToList()));
        }

        public ActionResult ActivityLog()
        {
            var userInfo = this.tpsSession.userInfo;
            if (userInfo == null)
                return null;
            if (userInfo.UserType == UserSource.TPS && userInfo.TpsType == TpsUserType.SuperAdmin)
            {
                return View("ActivityLog");

            }
            return Content("No permission");
        }
    }
}