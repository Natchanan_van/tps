﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace TPSWorkflow.DAL.DtoS
{
    public class MachineDto
    {
        public int MachineCode { get; set; }
        public string MachineName { get; set; }
        public string MachineFullName
        {
            get
            {
                return string.Format("{0} {1}", MachineCode, MachineName);
            }
        }
    }
    [DataContract()]
    public partial class MailLogDto
    {
        [DataMember()]
        public Int64 MaillogId { get; set; }

        [DataMember()]
        public String SendToAddress { get; set; }

        [DataMember()]
        public String SendCCAddress { get; set; }

        [DataMember()]
        public String MailContent { get; set; }

        [DataMember()]
        public Int32 MailType { get; set; }

        [DataMember()]
        public DateTime TimeStamp { get; set; }

        [DataMember()]
        public String MailTypeDescription { get; set; }

        [DataMember()]
        public Boolean IsSent { get; set; }

        [DataMember()]
        public String Error { get; set; }

        [DataMember()]
        public int? GroupId { get; set; }

        public MailLogDto()
        {
        }

        public MailLogDto(Int64 maillogId, String sendToAddress, String sendCCAddress, String mailContent, Int32 mailType, DateTime timeStamp, String mailTypeDescription, Boolean isSent, String error)
        {
            this.MaillogId = maillogId;
            this.SendToAddress = sendToAddress;
            this.SendCCAddress = sendCCAddress;
            this.MailContent = mailContent;
            this.MailType = mailType;
            this.TimeStamp = timeStamp;
            this.MailTypeDescription = mailTypeDescription;
            this.IsSent = isSent;
            this.Error = error;
        }
    }

    /// <summary>
    /// Assembler for <see cref="MailLog"/> and <see cref="MailLogDto"/>.
    /// </summary>
    public static partial class MailLogAssembler
    {
        /// <summary>
        /// Invoked when <see cref="ToDTO"/> operation is about to return.
        /// </summary>
        /// <param name="dto"><see cref="MailLogDto"/> converted from <see cref="MailLog"/>.</param>
        static partial void OnDTO(this MailLog entity, MailLogDto dto);

        /// <summary>
        /// Invoked when <see cref="ToEntity"/> operation is about to return.
        /// </summary>
        /// <param name="entity"><see cref="MailLog"/> converted from <see cref="MailLogDto"/>.</param>
        static partial void OnEntity(this MailLogDto dto, MailLog entity);

        /// <summary>
        /// Converts this instance of <see cref="MailLogDto"/> to an instance of <see cref="MailLog"/>.
        /// </summary>
        /// <param name="dto"><see cref="MailLogDto"/> to convert.</param>
        public static MailLog ToEntity(this MailLogDto dto)
        {
            if (dto == null) return null;

            var entity = new MailLog();

            entity.MaillogId = dto.MaillogId;
            entity.SendToAddress = dto.SendToAddress;
            entity.SendCCAddress = dto.SendCCAddress;
            entity.MailContent = dto.MailContent;
            entity.MailType = dto.MailType;
            entity.TimeStamp = dto.TimeStamp;
            entity.MailTypeDescription = dto.MailTypeDescription;
            entity.IsSent = dto.IsSent;
            entity.Error = dto.Error;
            entity.GroupId = dto.GroupId;

            dto.OnEntity(entity);

            return entity;
        }

        /// <summary>
        /// Converts this instance of <see cref="MailLog"/> to an instance of <see cref="MailLogDto"/>.
        /// </summary>
        /// <param name="entity"><see cref="MailLog"/> to convert.</param>
        public static MailLogDto ToDTO(this MailLog entity)
        {
            if (entity == null) return null;

            var dto = new MailLogDto();

            dto.MaillogId = entity.MaillogId;
            dto.SendToAddress = entity.SendToAddress;
            dto.SendCCAddress = entity.SendCCAddress;
            dto.MailContent = entity.MailContent;
            dto.MailType = entity.MailType;
            dto.TimeStamp = entity.TimeStamp;
            dto.MailTypeDescription = entity.MailTypeDescription;
            dto.IsSent = entity.IsSent;
            dto.Error = entity.Error;
            dto.GroupId = entity.GroupId == 0 ? null : entity.GroupId;

            entity.OnDTO(dto);

            return dto;
        }

        /// <summary>
        /// Converts each instance of <see cref="MailLogDto"/> to an instance of <see cref="MailLog"/>.
        /// </summary>
        /// <param name="dtos"></param>
        /// <returns></returns>
        public static List<MailLog> ToEntities(this IEnumerable<MailLogDto> dtos)
        {
            if (dtos == null) return null;

            return dtos.Select(e => e.ToEntity()).ToList();
        }

        /// <summary>
        /// Converts each instance of <see cref="MailLog"/> to an instance of <see cref="MailLogDto"/>.
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        public static List<MailLogDto> ToDTOs(this IEnumerable<MailLog> entities)
        {
            if (entities == null) return null;

            return entities.Select(e => e.ToDTO()).ToList();
        }

    }
}