﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace TPSWorkflow.DAL.DtoS
{
    public enum UserSource
    {
        TPS = 1,
        PIS = 0
    }
    public enum TpsUserType
    {
        NonTPS = 0,
        Staff = 1,
        Admin = 2,
        SuperAdmin = 3
    }
    public enum MeetingIssueType
    {
        Pending = 0,
        KM = 100,
        Main = 200
    }
    public enum AttendedType
    {
        Attended = 0,
        NotAttended = -100
    }
    
    public partial class issuesDto
    {
        [DataMember()]
        public int GroupId { get; set; }
        [DataMember()]
        public string GroupName { get; set; }
        [DataMember()]
        public IssuesStatus StatusEnum
        {
            get
            {
                return (IssuesStatus)this.Status;
            }
            set
            {
                this.Status = (int)value;
            }
        }
        [DataMember()]
        public string StatusText
        {
            get
            {
                return this.StatusEnum.ToString();
            }
            set
            {

            }
        }
        [DataMember()]
        public string WorkGroupName
        {
            get;
            set;
        }
        [DataMember()]
        public int WorkGroupId
        {

            get;
            set;
        }
        [DataMember()]
        public string PlantDisplayCode { get; set; }
        [DataMember]
        public string WorkGroupNameTh { get; set; }
        //[DataMember()]
        //public string UnitName { get; set; }
    }

    public partial class issuesMeetingDto:issuesDto
    {
        [DataMember()]
        public bool IsHadMeeting { get; set; }
    }

    public partial class meeting_issueDto
    {
        [DataMember]
        public MeetingIssueType TypeEnum
        {
            get
            {
                return (MeetingIssueType)this.AgendaType;
            }
            set
            {
                this.AgendaType = (int)value;
            }
        }
    }

    public partial class summaryDto
    {

        public SummaryStatus StatusEnum
        {
            get
            {
                return (SummaryStatus)this.Status;
            }
            set
            {
                this.Status = (int)value;
            }
        }
        [DataMember]
        public int ReSumForMonth { get; set; }
    }

    [Serializable]
    public partial class PersonalInfoDto
    {
        [DataMember]
        public string WorkgroupName { get; set; }
        [DataMember]
        public string WorkgroupNameTh { get; set; }
        [DataMember]
        public UserSource UserType { get; set; }
        [DataMember]
        public TpsUserType TpsType { get; set; }
        [DataMember]
        public string ImageUrl { get; set; }
        [DataMember]
        public string Username { get; set; }
        [DataMember]
        public string FullName
        {
            get
            {
                return string.Format("{0} {1} {2}", this.INAME, this.FNAME, this.LNAME);
            }
        }
        [DataMember]
        public int TpsUserId { get; set; }
        [DataMember]
        public string ImageTempName { get; set; }
        [DataMember]
        public string Department { get; set; }

        public string GetPosCode(IUsersManage usersResolver)
        {
            throw new NotImplementedException();

        }
    }

    [Serializable]
    public partial class WorkUnitDto
    {
    }

    public partial class memoDto
    {
        [DataMember]
        public string UpdateByFullName { get; set; }

    }

    public partial class meetingDto
    {
        [DataMember]
        public string GroupName { get; set; }

        public bool IsMeetingReady()
        {
            if (this.meeting_issue == null)
                return false;
            List<issuesDto> allIssues = this.meeting_issue.Select(d => d.issues).ToList();
            return allIssues.All(d => d.StatusEnum == IssuesStatus.PendingAfterMeeting || d.StatusEnum == IssuesStatus.ClosedWaitKM || d.StatusEnum == IssuesStatus.Recorded);

        }

        [DataMember]
        public bool IsReady { get; set; }

        [DataMember]
        public bool IsForcePdf { get; set; }
        [DataMember]
        public string PdfLocalPath { get; set; }
        [DataMember]
        public bool IsMemo { get; set; }
    }

    public partial class meeting_attendeesDto
    {
        [DataMember]
        public PersonalInfoDto UserInfo { get; set; }
        [DataMember]
        public AttendedType Attended { get; set; }
    }

    

    public enum SummaryStatus
    {
        None = 0,
        Remeeting = 1,
        Final = 2
    }

    public enum IssueCreateType
    {
        Manual = 0,
        Automatic = 5
    }

    public static partial class issuesAssembler
    {
        static partial void OnEntity(this issuesDto dto, issues entity)
        {

            if (entity.allowplant != null)
                dto.PlantDisplayCode = entity.allowplant.plantdisplaycode;
            if (entity.workgroup != null)
            {
                dto.GroupName = entity.workgroup.WorkGroupName;
                dto.WorkGroupId = entity.workgroup.WorkGroupId;
                dto.WorkGroupName = entity.workgroup.WorkGroupName;
                dto.WorkGroupNameTh = entity.workgroup.WorkGroupNameTH;
            }
        }

        public static issues ToEntity(this issuesDto dto, issues srcEntity)
        {
            if (dto == null)
                return null;

            var entity = srcEntity;

            //entity.IssueId = dto.IssueId;
            entity.BillNo = dto.BillNo;
            entity.CreatedDate = dto.CreatedDate;
            entity.ModifiedDate = dto.ModifiedDate;

            entity.Topic = dto.Topic;
            entity.unitcode = dto.unitcode;
            entity.plantcode = dto.plantcode;
            entity.ProblemDate = dto.ProblemDate;
            entity.ProblemName = dto.ProblemName;
            entity.ProblemDetail = dto.ProblemDetail;
            entity.RepairDate = dto.RepairDate;
            entity.RepairDueDate = dto.RepairDueDate;
            entity.Status = dto.Status;
            entity.CreatedType = dto.CreatedType;

            dto.OnEntity(entity);

            return entity;
        }

        static partial void OnDTO(this issues entity, issuesDto dto)
        {
            if (dto.unitcode != null && entity.workgroup != null)
            {
                dto.WorkGroupId = entity.workgroup.WorkGroupId;
                dto.WorkGroupName = entity.workgroup.WorkGroupName;
                dto.WorkGroupNameTh = entity.workgroup.WorkGroupNameTH;
            }

        }


        public static issuesDto ToDTO(this issues entity, group groupEntity, workgroup wgEntity)
        {
            var d = entity.ToDTO();
            if (groupEntity != null)
            {
                d.GroupName = groupEntity.GroupName;
                d.GroupId = groupEntity.GroupId;
            }
            if (wgEntity != null)
            {
                d.WorkGroupName = wgEntity.WorkGroupName;
                d.WorkGroupNameTh = wgEntity.WorkGroupNameTH;
                d.WorkGroupId = wgEntity.WorkGroupId;
            }
            if (entity.allowplant != null)
                d.PlantDisplayCode = entity.allowplant.plantdisplaycode;
            return d;
        }

    }

    public static partial class problemAssembler
    {
        /// <summary>
        /// Invoked when <see cref="ToDTO"/> operation is about to return.
        /// </summary>
        /// <param name="dto"><see cref="problemDto"/> converted from <see cref="problem"/>.</param>
        static partial void OnDTO(this problem entity, problemDto dto)
        {
            dto.problem_file = entity.problem_file.ToList().ToDTOs();
        }
    }

    public static partial class summaryAssembler
    {
        /// <summary>
        /// Invoked when <see cref="ToDTO"/> operation is about to return.
        /// </summary>
        /// <param name="dto"><see cref="summaryDto"/> converted from <see cref="summary"/>.</param>
        static partial void OnDTO(this summary entity, summaryDto dto)
        {
            dto.summary_km = entity.summary_km.ToList().ToDTOs();
            dto.ReSumForMonth = entity.ResumMonth.GetValueOrDefault(0) - DateTime.Now.Month;
            if (dto.ReSumForMonth < 0)
            {
                // Year loop
                dto.ReSumForMonth = entity.ResumMonth.GetValueOrDefault(0) + 12 - DateTime.Now.Month;
            }
            if (dto.ReSumForMonth < 0)
                dto.ReSumForMonth = 0;
        }
    }

    public static partial class PersonalInfoAssembler
    {
        /// <summary>
        /// Invoked when <see cref="ToDTO"/> operation is about to return.
        /// </summary>
        /// <param name="dto"><see cref="PersonalInfoDto"/> converted from <see cref="PersonalInfo"/>.</param>
        static partial void OnDTO(this PersonalInfo entity, PersonalInfoDto dto)
        {
            dto.unit = entity.unit.ToDTO();
            dto.UserType = UserSource.PIS;
        }
        public static PersonalInfoDto ToGenericDto(this tpsusers entity)
        {
            AutoMapper.Mapper.CreateMap<tpsusers, PersonalInfoDto>();
            var res = AutoMapper.Mapper.Map<PersonalInfoDto>(entity);
            res.TpsUserId = entity.UserId;
            res.UserType = UserSource.TPS;
            res.TpsType = (TpsUserType)entity.TpsType;
            res.ImageUrl = entity.ImageUrl;
            res.Username = entity.Username;
            res.Department = entity.Department;
            if (res.TpsType == TpsUserType.Admin)
            {
                res.POSNAME = "Admin (External User)";
            }
            else
            {
                res.POSNAME = "Staff (External User)";
            }
            return res;
        }



    }

    public static partial class meetingAssembler
    {
        /// <summary>
        /// Invoked when <see cref="ToDTO"/> operation is about to return.
        /// </summary>
        /// <param name="dto"><see cref="meetingDto"/> converted from <see cref="meeting"/>.</param>
        static partial void OnDTO(this meeting entity, meetingDto dto)
        {
            if (entity.memo != null)
                dto.memo = entity.memo.ToDTO();
            dto.GroupName = entity.group.GroupName;
            if (entity.meeting_attendees != null)
                dto.meeting_attendees = entity.meeting_attendees.ToDTOs();
            dto.IsForcePdf = entity.IsForcePdf == true;
            dto.PdfLocalPath = entity.PdfLocalPath;
            dto.IsMemo = entity.IsMemo;
        }


    }

    public static partial class memoAssembler
    {
        static partial void OnDTO(this memo entity, memoDto dto)
        {
            if (entity.SubMemo != null)
                dto.SubMemo = entity.SubMemo.ToDTOs();
        }
    }

    public static partial class meeting_attendeesAssembler
    {
        static partial void OnDTO(this meeting_attendees entity, meeting_attendeesDto dto)
        {
            dto.Attended = (AttendedType)entity.AttendedType;
        }
    }
}
