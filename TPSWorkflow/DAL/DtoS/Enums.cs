﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TPSWorkflow.DAL
{
    [Flags]
    public enum UserRights
    {
        /// <summary>
        /// No Rights
        /// </summary>
        None = 0,
        /// <summary>
        /// สิทธิ์เข้าดู
        /// </summary>
        Open = 1 << 0,
        /// <summary>
        /// สิทธิ์ในการสร้างวาระใหม่
        /// </summary>
        CreateNewProblem = 1 << 1,
        /// <summary>
        /// สิทธิ์อนุมัติ
        /// </summary>
        ApprovedProblem = 1 << 2,

        /// <summary>
        /// สิทธิ์ในการลงสาเหตุ
        /// </summary>
        EditProblemCause = 1 << 3,
        /// <summary>
        /// สิทธิ์ลงผลการประชุม
        /// </summary>
        AssignMeetingResult = 1 << 4,
        /// <summary>
        /// สิทธิ์ในการเขียนรายงานการประชุม
        /// </summary>
        AssignMeetingReport = 1 << 5,
        /// <summary>
        /// สิทธิ์การส่งเมล์เข้าร่วมประชุม
        /// </summary>
        MailMeeting = 1 << 7,
        SystemAdministrator = 1 << 8,
        ApprovedProblemAcrossUnit = 1 << 9,
        Administrator = 1 << 10
    }

    [Flags]
    public enum GroupRights
    {
        None = 0,
        Approve = 1 << 0,
        RecieveMail = 1 << 1
    }
}