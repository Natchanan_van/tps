﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TPSWorkflow.DAL.DtoS;

namespace TPSWorkflow.DAL
{
    public interface IUsersManage
    {
        IEnumerable<PersonalInfoDto> GetAll(string searchTerm);
        PersonalInfoDto GetById(string id);
        PersonalInfoDto GetByCode(string code);
        List<PersonalInfoDto> GetByCodes(IEnumerable<string> codes);
        UserRights GetUserRights(PersonalInfoDto user);
        PersonalInfoDto GetByAuthen(string username, string password);
        PersonalInfoDto GetByUsername(string username);
        List<PersonalInfoDto> GetByWorkGroup(string unitCode);
        List<PersonalInfoDto> GetByWorkGroup(string unitCode, string searchTerm);
        string GetPositionByUserId(string userCode);
        bool isCanShiftMeeting(PersonalInfoDto userInfo);
        List<PersonalInfoDto> GetAllVBG(string searchTerm);
        bool IsApproverInUnitCode(string userCode, string unitCode);
        void LogUserMock(string srcUser, string asUser, string ipAddress);
        void LogUserApprove(string srcUser, string asUser, string billNo, string ipAddress);
        void LogUserCancel(string srcUser, string asUser, string billNo, string ipAddress);
    }
}
