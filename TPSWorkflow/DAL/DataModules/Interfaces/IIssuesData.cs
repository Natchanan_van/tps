﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TPSWorkflow.DAL.DtoS;

namespace TPSWorkflow.DAL
{
    public interface IIssuesData
    {
        void SetPermission(PersonalInfoDto pInfo, IUsersManage permissionResolver);
        List<issuesDto> GetBySearchAndSort(IssuesSearchParams prms, int start, int count, out int allCount);
        issuesDto GetById(int id);
        issuesDto GetByBillNo(string billNo);
        Dictionary<IssuesStatus, int> GetCountByStatus();
        List<issuesDto> GetDeletedIssues(string searchTerm);
        List<issuesDto> GetSearchedDeletedIssues(string searchTerm, DateTime startDate, DateTime endDate);

        problemDto GetProblemDetailByIssue(int issueId);
        summaryDto GetSummaryById(int issueId);


        List<allowplantDto> GetAllPlantCode();
        List<allowplantDto> GetPlantByGroup(int groupId);
        issuesDto GetSapDataFromBillNo(string billNo);
        List<string> GetAllSapBill();

        List<workgroupDto> GetAllWorkGroup();
        List<PersonalInfoDto> GetCustomApproverInUnit(string unitcode);
        List<PersonalInfoDto> GetCustomPresenterInUnit(string unitcode);
        List<PersonalInfoDto> GetMainApprover(string unitcode);
        string GetMasterUnitCode();
        bool RecoverIssue(int issueId);
        bool DestroyIssue(int issueId);

        List<MachineDto> GetAllMachines();
        List<groupDto> GetAllGroups();

        #region Meetings
        meetingDto GetMeetingById(int meetingId);
        void SetSentMail(int meetingId, bool isSend);
        void DeleteMeeting(int id);
        bool SaveMemo(memoDto toSave, bool isChangeStatus);
        List<meetingDto> GetAllMeetings();
        List<meetingDto> GetBySearchAndSort(MemoSearchParams prms, int start, int count, out int allCount);
        int GetBeforeMeetingId(meetingDto currentMeeting);
        bool UpdateAttendees(int meetingId, List<meeting_attendeesDto> newList);
        void ResolveAttendeesInfo(meetingDto meeting);
        void CreateMemoInMeeting(int meetingId);
        List<issuesDto> GetIssuesByMeeting(int meetingId);
        List<issuesMeetingDto> GetIssuesCanBeAddedToMeeting(int meetingId);
        void AddIssuesToMeeting(int meetingId, List<int> issuesId);
        void DeleteIssuesFromMeeting(int meetingId, int issueId);
        void ResolveMeetingIssuesRelationType(int meetingId);
        #endregion

        bool SavePartialForStep1(issuesDto toSave);
        bool SavePartialForStep2(issuesDto toSave);
        bool SavePartialForStep3(problemDto toSave);
        bool SavePartialForStep4(summaryDto toSave);
        bool ReApproveIssues(int issueId);

        bool UploadFileToProblem(int issueId, System.Web.HttpPostedFileBase uploadFile);
        bool DeleteFileFromProblem(int fileId);

        bool UploadFileToKM(int issueId, System.Web.HttpPostedFileBase uploadFile);
        bool DeleteFileFromKM(int KmId);

        bool AddApproversByCode(List<string> codeArray, string unitcode);
        bool DeleteApprover(string usercode, string unitcode);

        bool AddPresenterByCode(List<string> codeArray, string unitcode);
        bool DeletePresenter(string usercode, string unitcode);

        List<group_mailingDto> GetMailingByGroup(int groupId);
        List<PersonalInfoDto> GetMailingUserDetailByGroup(int groupId);
        List<PersonalInfoDto> GetMailByMeeting(int meetingId);

        #region Group

        bool UpdateDayAndWeekInGroup(int groupId, int day, int week, bool isSuspended, DateTime? suspendDedTime, bool isShifting = true);
        bool UpdateTime(int groupId, DateTime startTime, DateTime endTime);
        bool UpdatePlantsInGroup(int groupId, List<string> newPlantCodes);
        bool UpdateShiftingDate(int groupId, bool isShift, DateTime? shiftDate, string location);
        bool AddCustomMailers(int groupId, List<string> userCodes);
        bool DelCustomMailers(int groupId, List<string> userCodes);
        groupDto GetGroupById(int groupId);
        string GetCalendarId(int groupId);
        string GenerateNewCalendarId(int groupId);

        #endregion

        bool UpdateMailTemplate(mail_templateDto dto);
        mail_templateDto GetMailTemplate();

        #region TPS Users
        PersonalInfoDto GetTpsUserById(int id);
        List<PersonalInfoDto> GetAllTpsUser();
        PersonalInfoDto UpdateUser(PersonalInfoDto userInfo, string password = null, string imgTempName = null);
        bool DeleteTpsUser(int id);
        List<PersonalInfoDto> GetMailByGroup(int groupId);
        List<PersonalInfoDto> GetMailableByGroup(string unitcode, string search);
        #endregion

        List<allowplantDto> GetSystemAllowPlants();
        allowplantDto GetSystemAllowPlant(string code);
        allowplantDto EditPlant(string code, allowplantDto dto);
        void DeletePlant(string code);

        List<PersonalInfoDto> GetAllApprover(string unitcode);
        List<PersonalInfoDto> GetCCApprover(string unitcode);

        #region Mail History
        List<MailLogDto> GetMailLogSearch(int start, int count, out int allCount, string searchTerm);
        List<MailLogDto> GetMailLogSearch(int start, int count, out int allCount, string searchTerm, MailLogType mailType);
        List<MailLogDto> GetMailLogSearch(int start, int count, out int allCount, string searchTerm, MailLogType mailType, DateTime startTime, DateTime endTime, int groupId);
        int GetMailLogCount();
        #endregion

        summary_kmDto GetKmFileById(int id);
        problem_fileDto GetProblemFileById(int id);
        groupDto GetGroupByPlantCode(string plantcode);
        meetingDto GetLatestMeetingByIssueId(int issueId);
        DateTime nextMeetingDate(string plantshortcode, int pendingMonth = 0);
        DateTime nextMeetingDate(int groupId, int pendingMonth = 0);
        DateTime nextMeetingDateOnMonth(int groupId, int onMonth);
        DateTime nextMeetingDate(int? dayOfWeek, int? weekOfMonth, int pendingMonth = 0);
        DateTime nextMeetingDateIncludeToday(int? dayOfWeek, int? weekOfMonth, int pendingMonth = 0);
        DateTime nextMeetingDateIncludeShiftDate(int groupId, int pendingMonth = 0);
        List<DateTime> GetAllMeetingDates(bool? isActive);

        MachineDto GetMachineByCode(int machineCode);
        void SetBaseUrl();

        bool Unbreakdown(int issueId, string reason, string userCode);
        bool Delete(int issueId, string reason, string userCode);

        DateTime GetMeetingDateByIssues(int issueId);
        List<activitylog> GetActivities();
        List<activitylog> GetActivities(DateTime startDate, DateTime endDate);

        void SetDefaultMeetingLocation(string location);
        string GetDefaultMeetingLocation();
    }
}
