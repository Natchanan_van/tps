﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TPSWorkflow.DAL.DtoS;
using TPSWorkflow.DAL;
using Ecobz.Utilities;
using System.Data.Entity.SqlServer;
using System.IO;

namespace TPSWorkflow.DAL
{
    public enum IssuesField
    {
        Group = 0,
        ProblemDate = 200,
        ProblemName = 300,
        PlantCode = 400,
        UnitCode = 500,
        BillNo = 600,
        UpdateDate = 700
    }

    public enum MemoField
    {
        None = 0,
        Group = 4,
        MeetingDate = 1,
        UpdateBy = 3,
        UpdateDate = 2
    }

    public class IssuesSearchParams : SearchSortParams
    {
        public string AllSearch { get; set; }
        public IssuesField SortField { get; set; }
        public bool IsDesc { get; set; }
        public IssuesStatus Status { get; set; }
        public List<IssuesStatus> Statuses { get; set; }
        public bool IsFilterStatus { get; set; }
        public int GroupId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public bool IsOnlyForUser { get; set; }
        public string CreatorId { get; set; }
        public bool IsSearcherIsApprover { get; set; }
        public string SearcherUnitcode { get; set; }

        public IssuesSearchParams()
        {
            this.StartDate = System.Data.SqlTypes.SqlDateTime.MinValue.Value;
            this.EndDate = System.Data.SqlTypes.SqlDateTime.MinValue.Value;
        }
    }
    public enum MemoStatus
    {
        All = 0,
        Draft = 1,
        Editted = 2,
        Sent = 3
    }

    public class MemoSearchParams : SearchSortParams
    {
        public DateTime? dateSearchStart { get; set; }
        public DateTime? dateSearchEnd { get; set; }
        public MemoField SortField { get; set; }
        public bool IsDesc { get; set; }
        public int GroupId { get; set; }
        public bool? IsFilterActive { get; set; }
        public MemoStatus StatusSearch { get; set; }
    }

    public class IssuesData:DataModule, IIssuesData,IDisposable
    {
        private UserRights userPermission = UserRights.None;
        private PersonalInfoDto userInfo = null;
        private IUsersManage userResolver = null;

        /// <summary>
        /// รหัสของ ผยก. (Dummy เดิม 1-3-2-9)
        /// </summary>
        /// <returns></returns>
        public static string GetPYGUDummyRelation()
        {
            var dbContext = new PISEntities();
            var res = dbContext.WorkUnit.Where(r => r.unitcode == Properties.Settings.Default.ViewerUnitCode).Select(r => r.DUMMY_RELATIONSHIP).FirstOrDefault();
            if (string.IsNullOrEmpty(res))
                res = "1-3-2-9";
            return res;
        }

        /// <summary>
        /// รหัสของ วบก. (Dummy เดิม 1-3-2-9-5)
        /// </summary>
        /// <returns></returns>
        public static string GetVBGDummyRelation()
        {
            var dbContext = new PISEntities();
            var res = dbContext.WorkUnit.Where(r => r.unitcode == Properties.Settings.Default.VBGUnitCode).Select(r => r.DUMMY_RELATIONSHIP).FirstOrDefault();
            if (string.IsNullOrEmpty(res))
                res = "1-3-2-9-5";
            return res;
        }

        public void SetPermission(PersonalInfoDto pInfo, IUsersManage permissionResolver)
        {
            this.userInfo = pInfo;
            this.userPermission = permissionResolver.GetUserRights(this.userInfo);
            this.userResolver = permissionResolver;
        }

        #region Issues

        public List<issuesDto> GetBySearchAndSort(IssuesSearchParams prms, int start, int count, out int allCount)
        {
            try
            {
                #region Check Permission

                allCount = -1;
                if (this.userPermission == UserRights.None)
                    return new List<issuesDto>();

                #endregion

                var query = tpsContext.issues.AsQueryable();

               

                if (!string.IsNullOrEmpty(prms.AllSearch))
                {
                    query = query.Where(d => d.BillNo.Contains(prms.AllSearch) ||
                        d.ProblemName.Contains(prms.AllSearch) ||
                        d.plantcode.Contains(prms.AllSearch) ||
                        d.allowplant.group.GroupName.Contains(prms.AllSearch) ||
                        d.workgroup.WorkGroupName.Contains(prms.AllSearch)
                        );
                }

                if (prms.IsFilterStatus)
                {
                    if (prms.Statuses != null && prms.Statuses.Count > 0)
                    {
                        var intArray = prms.Statuses.Select(d => (int)d).ToList();
                        query = query.Where(d => intArray.Contains(d.Status));
                    }
                    else
                    {
                        query = query.Where(d => d.Status == (int)prms.Status);
                    }
                }
                if (!(prms.EndDate == System.Data.SqlTypes.SqlDateTime.MinValue.Value && prms.StartDate == System.Data.SqlTypes.SqlDateTime.MinValue.Value))
                {
                    query = query.Where(d => d.ModifiedDate >= prms.StartDate && d.ModifiedDate <= prms.EndDate);
                }
                if (prms.GroupId > 0)
                {
                    query = query.Where(d => d.allowplant.groupId == prms.GroupId);
                }
                if (prms.IsOnlyForUser && !string.IsNullOrEmpty(prms.CreatorId))
                {
                    if (prms.IsSearcherIsApprover)
                    {
                        query = query.Where(d => d.InchargeUserCode == prms.CreatorId || d.unitcode == prms.SearcherUnitcode).Where(c => c.Status != (int)IssuesStatus.New);
                    }
                    else
                    {
                        query = query.Where(d => d.InchargeUserCode == prms.CreatorId).Where(c => c.Status != (int)IssuesStatus.New); ;
                    }

                }

                switch (prms.SortField)
                {
                    case IssuesField.Group:
                        query = query.OrderedByMultiField(d => d.allowplant.group.GroupName, prms.IsDesc);
                        break;
                    case IssuesField.ProblemName:
                        query = query.OrderedByMultiField(d => d.ProblemName, prms.IsDesc);
                        break;
                    case IssuesField.PlantCode:
                        query = query.OrderedByMultiField(d => d.allowplant.plantdisplaycode, prms.IsDesc);
                        break;
                    case IssuesField.BillNo:
                        query = query.OrderedByMultiField(d => d.BillNo, prms.IsDesc);
                        break;
                    case IssuesField.UnitCode:
                        query = query.OrderedByMultiField(d => d.workgroup.WorkGroupNameTH, prms.IsDesc);
                        break;
                    case IssuesField.UpdateDate:
                        query = query.OrderedByMultiField(d => d.ModifiedDate, prms.IsDesc);
                        break;
                    default:
                        query = query.OrderedByMultiField(d => d.ProblemDate, prms.IsDesc);
                        break;
                }
                allCount = query.Count();
                if (start >= 0)
                {
                    query = query.Skip(start);
                }
                if (count >= 0)
                {
                    query = query.Take(count);
                }
                return query.ToList().Select(d => d.ToDTO(d.allowplant.group, d.workgroup)).ToList();
           
            }
            catch (Exception)
            {
                throw;
                //allCount = -1;
                //return null;
            }

            //throw new NotImplementedException();
        }
        public issuesDto GetById(int id)
        {
            var issue = this.tpsContext.issues.Where(d => d.IssueId == id).FirstOrDefault();
            if (issue == null)
                return null;
            #region Check Permission

            //if (issue.unitcode != userInfo.UNITCODE)
            //{
            //    throw new TpsUnauthorizedException("Cannot Access Accross Unit");
            //}

            //if (!userPermission.HasFlag(UserRights.Open))
            //{
            //    throw new TpsUnauthorizedException("You don't have permission to open any issue");
            //}

            #endregion

            return issue.ToDTO(issue.allowplant.group, issue.workgroup);
        }
        public issuesDto GetByBillNo(string billNo)
        {
            return this.tpsContext.issues.Where(d => d.BillNo == billNo).FirstOrDefault().ToDTO();
        }
        public Dictionary<IssuesStatus, int> GetCountByStatus()
        {
            var res = new Dictionary<IssuesStatus, int>();
            var val = Enum.GetValues(typeof(IssuesStatus));
            foreach (var status in val.Cast<IssuesStatus>())
            {
                res.Add(status, this.tpsContext.issues.Where(d => d.Status == (int)status).Count());
            }
            return res;
        }
        public List<issuesDto> GetDeletedIssues(string searchTerm)
        {
            return this.GetSearchedDeletedIssues(searchTerm, DateTime.Now.AddMonths(-1), DateTime.Now.AddDays(1));
        }
        public List<issuesDto> GetSearchedDeletedIssues(string searchTerm, DateTime startDate, DateTime endDate)
        {
            var query = this.tpsContext.issues.Where(r => r.IsDeleted);
            if (!string.IsNullOrEmpty(searchTerm))
            {
                query = query.Where(d => d.BillNo.Contains(searchTerm) ||
                       d.ProblemName.Contains(searchTerm) ||
                       d.plantcode.Contains(searchTerm) ||
                       d.allowplant.group.GroupName.Contains(searchTerm) ||
                       d.workgroup.WorkGroupName.Contains(searchTerm)
                       );
            }
            query = query.Where(r => r.ModifiedDate >= startDate && r.ModifiedDate <= endDate);
            return query.ToList().Select(d => d.ToDTO(d.allowplant.group, d.workgroup)).ToList();
        }
        public DateTime GetMeetingDateByIssues(int issueId)
        {
            var issue = this.tpsContext.issues.FirstOrDefault(d => d.IssueId == issueId);
            if (issue == null || issue.meeting_issue == null || issue.meeting_issue.Count == 0)
            {
                return nextMeetingDate(issue.ToDTO().GroupId);
            }
            else
            {
                return issue.meeting_issue.Max(d => d.meeting.MeetingDate);
            }
        }


        public bool SavePartialForStep1(issuesDto toSave)
        {
            try
            {
                issues entity = null;
                if (toSave.IssueId < 0)
                {
                    if (this.tpsContext.issues.Count(d => d.BillNo == toSave.BillNo) > 0)
                    {
                        throw new Exception("ใบแจ้งซ่อมซ้ำในระบบ");
                    }
                    entity = this.tpsContext.issues.Create();
                    entity.BillNo = toSave.BillNo;
                    this.tpsContext.issues.Add(entity);
                    entity.ApproverCode = string.Empty;
                    entity.CancelDetail = string.Empty;
                    entity.CreatedDate = DateTime.Now;
                    entity.CreatedType = (int)IssueCreateType.Manual;
                    entity.Status = (int)IssuesStatus.Waiting;
                    entity.unitcode = toSave.unitcode;
                    entity.CreatedBy = toSave.CreatedBy;
                }
                else
                {
                    entity = this.tpsContext.issues.Where(d => d.IssueId == toSave.IssueId).First();
                    if (entity.IsDeleted)
                    {
                        return false;
                    }
                    if (string.IsNullOrEmpty(entity.CreatedBy))
                    {
                        entity.CreatedBy = toSave.CreatedBy;
                    }
                }

                entity.unitcode = toSave.unitcode;
                entity.RepairBillNo = toSave.RepairBillNo;
                entity.plantshortcode = toSave.plantshortcode;
                entity.plantcode = toSave.plantcode;
                entity.MachineNo = toSave.MachineNo;
                entity.ProblemDate = toSave.ProblemDate;
                entity.ProblemName = toSave.ProblemName;
                entity.MachineFullName = toSave.MachineFullName;
                
                entity.ProblemDetail = toSave.ProblemDetail;
                entity.RepairDate = toSave.RepairDate;
                entity.RepairDueDate = toSave.RepairDueDate;
                if (entity.Status == (int)IssuesStatus.New)
                    entity.Status = (int)IssuesStatus.Waiting;

                if (entity.Status == (int)IssuesStatus.Cancel)
                {
                    entity.IsCancelAndEditted = true;
                }
                else
                {
                    entity.IsCancelAndEditted = false;
                }
                entity.ModifiedDate = DateTime.Now;
                

                this.tpsContext.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool SavePartialForStep2(issuesDto toSave)
        {
            //#region Check Permission

            //if (!(this.userPermission.HasFlag(UserRights.ApprovedProblem) || this.userPermission.HasFlag(UserRights.ApprovedProblemAcrossUnit)))
            //    throw new TpsUnauthorizedException("User don't have permission to approve problem");
            //if (!this.userPermission.HasFlag(UserRights.ApprovedProblemAcrossUnit) && this.userInfo.UNITCODE != toSave.unitcode)
            //    throw new TpsUnauthorizedException("User don't have permission to approve problem across unit");
            //#endregion

            var thisEntity = tpsContext.issues.Where(d => d.IssueId == toSave.IssueId).FirstOrDefault();
            if (thisEntity == null || thisEntity.IsDeleted)
                return false;
            var entityStatus = (IssuesStatus)thisEntity.Status;
            if (!(entityStatus == IssuesStatus.New || entityStatus == IssuesStatus.Pending || entityStatus == IssuesStatus.Waiting || entityStatus == IssuesStatus.Approve || entityStatus == IssuesStatus.Cancel))
                throw new TpsUnauthorizedException("Cannot re-approved issue");

            if (thisEntity.Status == (int)IssuesStatus.Cancel || thisEntity.Status <= (int)IssuesStatus.Pending)
            {
                thisEntity.Status = (int)toSave.StatusEnum;
            }


            if (toSave.StatusEnum == IssuesStatus.Approve)
            {
                thisEntity.ApproverCode = toSave.ApproverCode;
                thisEntity.InchargeUserCode = toSave.InchargeUserCode;
                thisEntity.CancelDetail = string.Empty;
                thisEntity.PendingMonth = null;
                thisEntity.ApproveDateTime = DateTime.Now;
            }
            else if (toSave.StatusEnum == IssuesStatus.Cancel)
            {
                thisEntity.CancelDetail = toSave.CancelDetail;
                thisEntity.PendingMonth = null;
                thisEntity.ApproverCode = string.Empty;
                thisEntity.InchargeUserCode = string.Empty;
            }
            else if (toSave.StatusEnum == IssuesStatus.Pending)
            {
                if (toSave.PendingMonth == null)
                {
                    throw new TpsUnauthorizedException("ไม่สามารถกำหนด Pending โดยไม่ใส่เดือนได้");
                }
                thisEntity.PendingMonth = toSave.PendingMonth;
                thisEntity.ApproverCode = string.Empty;
                thisEntity.InchargeUserCode = string.Empty;
                thisEntity.CancelDetail = string.Empty;
            }

            thisEntity.ModifiedDate = DateTime.Now;
            if (thisEntity.problem == null)
            {
                thisEntity.problem = new problem();
            }


            try
            {

                return (tpsContext.SaveChanges() > 0);
            }
            catch (Exception ex)
            {
                throw ex;
            }

           
        }
        public bool SavePartialForStep3(problemDto toSave)
        {
            var thisProblem = this.tpsContext.problem.Where(d => d.IssueId == toSave.IssueId).FirstOrDefault();
            if (thisProblem == null || thisProblem.issues.IsDeleted)
                return false;
            thisProblem.BaseProblem = toSave.BaseProblem;
            thisProblem.FixDetail = toSave.FixDetail;
            thisProblem.PossibleCauses = toSave.PossibleCauses;
            thisProblem.ProblemCauses = toSave.ProblemCauses;
            thisProblem.SolveDirection = toSave.SolveDirection;
            thisProblem.RealCauses = toSave.RealCauses;
            thisProblem.LongTermPrevention = toSave.LongTermPrevention;
            thisProblem.issues.ModifiedDate = DateTime.Now;
            thisProblem.issues.Status = (int)IssuesStatus.Ready;

            var issue = this.GetById(toSave.IssueId);
            var nextDate = this.nextMeetingDate(issue.plantshortcode);
            //var meeting = this.tpsContext.meeting.Where(r => r.GroupId == issue.GroupId && r.MeetingDate == nextDate);
            //meeting meetingEntity = null;
            //if (meeting.Count() == 0)
            //{
            //    meetingEntity = new meeting();
            //    meetingEntity.MeetingDate = nextDate;
            //    meetingEntity.CreatedDate = DateTime.Now;
            //    meetingEntity.GroupId = issue.GroupId;
            //    this.tpsContext.meeting.Add(meetingEntity);
            //}
            //else
            //{
            //    meetingEntity = meeting.FirstOrDefault();
            //}
            //meetingEntity.meeting_issue.Add(new meeting_issue() { IssueId = issue.IssueId, AgendaType = 0 });
            //meetingEntity.ModifiedDate = DateTime.Now;

            this.tpsContext.SaveChanges();
            return true;
        }
        public bool SavePartialForStep4(summaryDto toSave)
        {
            var thisProblem = this.tpsContext.summary.Where(d => d.IssueId == toSave.IssueId).FirstOrDefault();
            if (thisProblem == null || thisProblem.issues.IsDeleted)
                return false;
            var oldStatus = (SummaryStatus)thisProblem.Status;
            thisProblem.Suggestion = toSave.Suggestion;
            thisProblem.Finalize = toSave.Finalize;
            thisProblem.ModifiedTime = DateTime.Now;
            thisProblem.issues.ModifiedDate = DateTime.Now;
            thisProblem.Status = toSave.Status;
            if (toSave.StatusEnum == SummaryStatus.Final)
            {
                if (thisProblem.summary_km.Count == 0)
                    thisProblem.issues.Status = (int)IssuesStatus.ClosedWaitKM;
                else
                    thisProblem.issues.Status = (int)IssuesStatus.Recorded;
                if (oldStatus == SummaryStatus.None)
                {
                    thisProblem.issues.ClosedDate = DateTime.Now;
                }
            }
            else if (toSave.StatusEnum == SummaryStatus.Remeeting)
            {
                thisProblem.issues.Status = (int)IssuesStatus.PendingAfterMeeting;
            }
            
            //thisProblem.ReSumDate = toSave.ReSumDate;
            thisProblem.LastEditUser = this.userInfo.CODE;
            if (toSave.ReSumForMonth > 0)
            {
                thisProblem.ResumMonth = DateTime.Now.AddMonths(toSave.ReSumForMonth).Month;
            }

            this.tpsContext.SaveChanges();
            return true;
        }
        public bool ReApproveIssues(int issueId)
        {
            var entity = this.tpsContext.issues.Where(r => r.IssueId == issueId).FirstOrDefault();
            if (entity == null)
                return false;
            entity.Status = (int)IssuesStatus.Waiting;
            entity.IsCancelAndEditted = false;
            entity.CancelDetail = string.Empty;
            entity.PendingMonth = null;
            entity.ApproverCode = string.Empty;
            entity.ModifiedDate = DateTime.Now;
            try
            {
                tpsContext.SaveChanges();
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }
        public bool RecoverIssue(int issueId)
        {
            var thisEntity = this.tpsContext.issues.SingleOrDefault(r => r.IssueId == issueId);
            if (thisEntity == null || !thisEntity.IsDeleted || !(thisEntity.Status == (int)IssuesStatus.Deleted))
            {
                return false;
            }
            thisEntity.IsDeleted = false;
            if (thisEntity.IsUnbreakdown)
            {
                thisEntity.IsUnbreakdown = false;
                thisEntity.Status = (int)IssuesStatus.New;
            }
            else
            {
                thisEntity.Status = (int)IssuesStatus.Waiting;
            }
            this.tpsContext.SaveChanges();
            return true;
        }
        public bool DestroyIssue(int issueId)
        {
            var thisEntity = this.tpsContext.issues.SingleOrDefault(r => r.IssueId == issueId);
            if (thisEntity == null || !thisEntity.IsDeleted || !(thisEntity.Status == (int)IssuesStatus.Deleted))
            {
                return false;
            }
            this.tpsContext.issues.Remove(thisEntity);
            this.tpsContext.SaveChanges();
            return true;
        }

        #endregion

        #region Problem

        public problemDto GetProblemDetailByIssue(int issueId)
        {
            var issue = this.tpsContext.issues.Where(d => d.IssueId == issueId).FirstOrDefault();
            if (issue == null)
                return null;
           

            problem thisProblem;
            if (issue.problem == null)
            {
                var x = new problem();
                issue.problem = x;
                tpsContext.SaveChanges();
                thisProblem = x;
            }
            else
                thisProblem = issue.problem;
            return thisProblem.ToDTO();
        }

        public bool UploadFileToProblem(int issueId, System.Web.HttpPostedFileBase uploadFile)
        {
            var issue = this.tpsContext.issues.Where(d => d.IssueId == issueId).FirstOrDefault();
            if (issue == null)
                return false;

            var problem = issue.problem;
            var fileUpload = new problem_file()
            {
                FileName = uploadFile.FileName,
                IssueId=issueId,
                Type = 0,
                UploadedBy = this.userInfo.CODE,
                UploadedDate = DateTime.Now,
            };
            fileUpload.LocalPath = TPSPathHelper.ResolvePathFromProblem(fileUpload.ToDTO());
            uploadFile.SaveAs(fileUpload.LocalPath);
            problem.problem_file.Add(fileUpload);
            return tpsContext.SaveChanges() > 0;
        }

        public bool DeleteFileFromProblem(int fileId)
        {
            var thisFile = this.tpsContext.problem_file.Where(d => d.FileId == fileId).FirstOrDefault();
            if (thisFile == null)
                return false;
            System.IO.File.Delete(thisFile.LocalPath);
            this.tpsContext.problem_file.Remove(thisFile);
            return this.tpsContext.SaveChanges() > 0;
        }

        #endregion

        #region KM

        public summaryDto GetSummaryById(int issueId)
        {
            var km = this.tpsContext.summary.Where(d => d.IssueId == issueId).FirstOrDefault();
            if (km == null)
            {
                try
                {

                    var thisSummary = new summary();
                    var thisIssue = this.tpsContext.issues.Where(d => d.IssueId == issueId).FirstOrDefault();
                    thisIssue.summary = thisSummary;
                    thisSummary.LastEditUser = userInfo.CODE;
                    thisSummary.ModifiedTime = DateTime.Now;
                    thisSummary.CreatedTime = DateTime.Now;
                    thisSummary.Status = 0;
                    thisSummary.Finalize = string.Empty;
                    thisSummary.IssueId = issueId;
                    thisSummary.ResumMonth = 0;
                    thisSummary.Suggestion = string.Empty;
                    this.tpsContext.SaveChanges();
                    km = thisSummary;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                
            }
            return km.ToDTO();
        }

        public bool UploadFileToKM(int issueId, System.Web.HttpPostedFileBase uploadFile)
        {
            var issue = this.tpsContext.issues.Where(d => d.IssueId == issueId).FirstOrDefault();
            if (issue == null)
                return false;

            var summary = issue.summary;
            var fileUpload = new summary_km()
            {
                FileName = uploadFile.FileName,
                IssueId = issueId,
                UploadedBy = this.userInfo.CODE,
                UploadedDate = DateTime.Now,
            };
            fileUpload.LocalPath = TPSPathHelper.ResolvePathFromKM(fileUpload.ToDTO());
            uploadFile.SaveAs(fileUpload.LocalPath);
            this.tpsContext.summary_km.Add(fileUpload);
            if (issue.Status == (int)IssuesStatus.ClosedWaitKM)
            {
                issue.Status = (int)IssuesStatus.Recorded;
            }
            return tpsContext.SaveChanges() > 0;
        }

        public bool DeleteFileFromKM(int KmId)
        {
            var thisFile = this.tpsContext.summary_km.Where(d => d.KmId == KmId).FirstOrDefault();
            if (thisFile == null)
                return false;
            System.IO.File.Delete(thisFile.LocalPath);
            this.tpsContext.summary_km.Remove(thisFile);
            return this.tpsContext.SaveChanges() > 0;
        }

        

        #endregion

        #region Plant Code

        public List<allowplantDto> GetAllPlantCode()
        {
            return this.tpsContext.allowplant.ToList().ToDTOs().ToList();
        }
        public List<allowplantDto> GetPlantByGroup(int groupId)
        {
            return this.tpsContext.allowplant.Where(d => d.groupId == groupId).ToList().ToDTOs();
        }

        #endregion

        #region Bill Data (SAP)

        /// <summary>
        /// เปลี่ยน EQDto และ NotiDto เป็น Issue
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="eqDto"></param>
        /// <returns></returns>
        private issuesDto SapDtoToIssueDto(PM_NOTIDto dto)
        {
            //          select n.QMNUM as เลขที่ใบแจ้งซ่อม,n.AUFNR as เลขที่ใบสั่งซ่อม,n.TPLNR as โรงแยกก๊าซ 
            //,cast(n.EQUNR as int) as เครื่องจักร ,o.GEWRK as กลุ่มงานรับผิดชอบ ,n.QMDAT as วันที่พบปัญหา
            //,n.QMTXT as ชื่อปัญหา	 ,n.INGRP as กลุ่มที่ต้องcc ,n.MSAUS as Breakdown			
            //from [dbo].[PM_NOTI] n 
            //inner join PM_EQ o on  n.EQUNR = o.EQUNR
            //where o.GEWRK in ('GENG', 'GMECH' , 'GINSP', 'GINST', 'GELEC', 'GPLAN', 'GTURN')
            //  and left(n.TPLNR,4) in ('G-01','G-02','G-03' , 'G-05','G-06' ,'G-23' ,'G-01' ,'P-01') 

            if (dto == null)
                return null;
            var eqDto = this.sapContext.PM_EQ.Where(d => d.EQUNR == dto.EQUNR).FirstOrDefault();
            //var groupName = eqDto == null ? string.Empty : eqDto.GEWRK;
            var groupName = string.Empty;
            if (string.IsNullOrEmpty(groupName))
            {
                switch (dto.INGRP)
                {
                    case "G01":
                        groupName = "บค.วบก.";
                        break;
                    case "G02":
                        groupName = "บง.วบก.";
                        break;
                    case "G03":
                        groupName = "บฟ.วบก.";
                        break;
                    case "G04":
                        groupName = "บง.วบก.";
                        break;
                    case "G05":
                        groupName = "ผบ.วบก.";
                        break;
                    case "G08":
                        groupName = "วก.วบก.";
                        break;
                    case "G10":
                        groupName = "ซญ.วบก.";
                        break;
                    case "G11":
                        groupName = "ตร.วบก.";
                        break;
                }
            }
            var unitCode = this.tpsContext.workgroup.Where(d => d.WorkGroupName == groupName || d.WorkGroupNameTH == groupName).Select(d => d.unitcode).FirstOrDefault();
            
            var res = new issuesDto();
            res.BillNo = dto.QMNUM;
            res.RepairBillNo = dto.AUFNR.EmptyIfNull().TrimStart('0');
            res.plantcode = dto.TPLNR;
            //if (dto.TPLNR.EmptyIfNull().Length >= 4)
            //{
            //    res.plantshortcode = dto.TPLNR.EmptyIfNull().Substring(0, 4);
            //}
            group tpsGroup = null;
            var matchedGroups = this.tpsContext.allowplant.Where(d => dto.TPLNR.StartsWith(d.plantsearch)).ToList();
            if (matchedGroups != null && matchedGroups.Count > 0)
            {
                allowplant longestPlant = null;
                foreach (var g in matchedGroups)
                {
                    if (longestPlant == null || longestPlant.plantsearch.Length < g.plantsearch.Length)
                    {
                        longestPlant = g;
                    }
                }
                if (longestPlant != null)
                {
                    tpsGroup = longestPlant.group;
                    res.plantshortcode = longestPlant.plantshortcode;
                }
            }
            
            
            
            int buffer = 0;
            res.MachineNo = dto.EQUNR;
            var bString = buffer.ToString();
            if (res.MachineNo != null)
            {
                res.MachineFullName = (from p in this.sapContext.PM_EQ
                                       where p.EQUNR.EndsWith(bString)
                                       select p.SHTXT).FirstOrDefault();
            }

            res.ProblemDate = dto.QMDAT.GetValueOrDefault(DateTime.Now);
            res.ProblemName = dto.QMTXT;

            if (tpsGroup != null)
            {

                res.GroupId = tpsGroup.GroupId;
                res.GroupName = tpsGroup.GroupName;
            }

            res.unitcode = unitCode;
            //res.unitcode = groupName;

            #region New 3 Field

            var thisOrderEntity = this.sapContext.PM_ORDER.Where(d => dto.QMNUM.Contains(d.QMNUM)).FirstOrDefault();
            if (thisOrderEntity != null)
            {
                res.ProblemDetail = thisOrderEntity.KTEXT;
                res.RepairDate = thisOrderEntity.GSTRP;
                res.RepairDueDate = thisOrderEntity.GLTRP;
            }

            #endregion
            return res;
        }
        public List<string> GetAllSapBill()
        {
            //var query = this.sapContext.PM_NOTI.AsQueryable();
            var query = from noti in this.sapContext.PM_NOTI
                    join d in this.sapContext.PM_EQ on noti.EQUNR equals d.EQUNR into myjoin
                    from myjoin2 in myjoin.DefaultIfEmpty()
                    where //(new string[] { "GENG", "GMECH", "GINSP", "GINST", "GELEC", "GPLAN", "GTURN" }.Contains(myjoin2.GEWRK)) 
                    new string[] { "G-01", "G-02", "G-03", "G-05", "G-06", "G-23", "G-01", "P-01" }.Contains(noti.TPLNR.Substring(0, 4))
                    select noti.QMNUM;
            return query.ToList();
        }
        public issuesDto GetSapDataFromBillNo(string billNo)
        {
  //          select n.QMNUM as เลขที่ใบแจ้งซ่อม,n.AUFNR as เลขที่ใบสั่งซ่อม,n.TPLNR as โรงแยกก๊าซ 
  //,cast(n.EQUNR as int) as เครื่องจักร ,o.GEWRK as กลุ่มงานรับผิดชอบ ,n.QMDAT as วันที่พบปัญหา
  //,n.QMTXT as ชื่อปัญหา	 ,n.INGRP as กลุ่มที่ต้องcc ,n.MSAUS as Breakdown			
  //from [dbo].[PM_NOTI] n 
  //inner join PM_EQ o on  n.EQUNR = o.EQUNR
  //where o.GEWRK in ('GENG', 'GMECH' , 'GINSP', 'GINST', 'GELEC', 'GPLAN', 'GTURN')
  //  and left(n.TPLNR,4) in ('G-01','G-02','G-03' , 'G-05','G-06' ,'G-23' ,'G-01' ,'P-01') 
            if (billNo.Length > 10)
            {
                billNo = billNo.Substring(0, 10);
            }
            else if (billNo.Length < 10)
            {
                return null;
            }
            var stringToSearchArray = this.tpsContext.allowplant.Select(d => d.plantsearch).ToArray();

            var query = this.sapContext.PM_NOTI.AsQueryable();
            query = from noti in query
                    join d in this.sapContext.PM_EQ on noti.EQUNR equals d.EQUNR into myjoin
                    from myjoin2 in myjoin.DefaultIfEmpty()
                    //where (new string[] { "GENG", "GMECH", "GINSP", "GINST", "GELEC", "GPLAN", "GTURN" }.Contains(myjoin2.GEWRK)) 
                    //new string[] { "G-01", "G-02", "G-03", "G-05", "G-06", "G-23", "G-01", "P-01" }.Contains(noti.TPLNR.Substring(0, 4))
                    select noti;
                    

            query = query.Where(d => d.QMNUM.EndsWith(billNo));
            var data = query.FirstOrDefault();
            if (data == null || !stringToSearchArray.Any(d => data.TPLNR.StartsWith(d)))
                return null;
            var dtoData = data.ToDTO();
            var issueData = this.SapDtoToIssueDto(dtoData);
            return issueData;
        }
        public List<MachineDto> GetAllMachines()
        {
            return (from machine in this.sapContext.PM_EQ
                    where machine.EQUNR.EndsWith("3104605")
                    select machine).ToList().Select(d => new MachineDto()
                   {
                       MachineCode = int.Parse(d.EQUNR),
                       MachineName = d.SHTXT
                   }).ToList();
        }

        public MachineDto GetMachineByCode(int machineCode)
        {
            var machineCodeStr = machineCode.ToString();
            var machineEntity = this.sapContext.PM_EQ.SqlQuery(string.Format(
                "SELECT * FROM PM_EQ WHERE SUBSTRING(EQUNR, PATINDEX('%[^0]%', EQUNR+'.'), LEN(EQUNR)) = '{0}' ", machineCodeStr)
                ).FirstOrDefault();
            if (machineEntity != null)
            {
                return new MachineDto()
                {
                    MachineCode = Convert.ToInt32(machineEntity.EQUNR),
                    MachineName = machineEntity.SHTXT
                };
            }
            return null;
        }

        #endregion

        #region Meeting and Memo

        private void initAttendee(meeting meetingEntity)
        {
            #region Initialize Attendee
            var mailGroup = this.GetMailByGroup(meetingEntity.GroupId);
            var allPresenterCode = meetingEntity.meeting_issue.Select(r => r.issues).Where(d => d.InchargeUserCode != null).Select(d => d.InchargeUserCode).Where(r=>!string.IsNullOrEmpty(r)).ToList();
            //mailGroup.AddRange(allPresenterCode.Select(d => this.userResolver.GetByCode(d)));
            mailGroup.AddRange((from user in this.pisContext.PersonalInfo
                                where allPresenterCode.Contains(user.CODE)
                                select user).ToList().ToDTOs());
            mailGroup.ForEach(d =>
            {
                var attendee = new meeting_attendees()
                {
                    MeetingId = meetingEntity.MeetingId,
                    AttendeeCode = d.CODE
                };
                this.tpsContext.meeting_attendees.Add(attendee);
            });
            this.tpsContext.SaveChanges();
            #endregion
        }
        public void ResolveMeetingIssuesRelationType(int meetingId)
        {
            var toResolve = this.tpsContext.meeting_issue.Where(c => c.MeetingId == meetingId).ToList();
            foreach (var entity in toResolve)
            {
                this.resolveMeetingIssueRelationType(entity);
            }
            this.tpsContext.SaveChanges();
        }
        private void resolveMeetingIssueRelationType(meeting_issue entity)
        {
            if (this.tpsContext.meeting_issue.Where(c => c.IssueId == entity.IssueId && c.MeetingId < entity.MeetingId).Count() == 0)
            {
                entity.AgendaType = (int)MeetingIssueType.Main;
            }
            else
            {
                entity.AgendaType = (int)MeetingIssueType.Pending;
            }
        }
        public void ResolveMeeting()
        {
            var noMemoMeetings = this.tpsContext.meeting.Where(d => d.memo == null).ToList();
            noMemoMeetings.ForEach(d =>
                {
                    var thisMemo = new memo()
                    {
                        CreatedDate = DateTime.Now,
                        ModifiedDate = DateTime.Now,
                        MeetingId = d.MeetingId,
                        Memo1 = string.Empty,
                        Memo2 = string.Empty,
                        Memo3 = string.Empty
                    };
                    this.tpsContext.memo.Add(thisMemo);
                });
            tpsContext.SaveChanges();
        }
        public List<meetingDto> GetAllMeetings()
        {
            return this.tpsContext.meeting.ToList().ToDTOs();
        }
        public List<meetingDto> GetBySearchAndSort(MemoSearchParams prms, int start, int count, out int allCount)
        {
            this.ResolveMeeting();
            allCount = -1;

            var groupAllList = this.tpsContext.group.AsQueryable().Select(e => e.GroupId);

            var thisQuery = this.tpsContext.meeting.Where(e => groupAllList.Contains(e.GroupId));

           // thisQuery = thisQuery.Where(e => e.GroupId == groupAllList);
            
            if (prms.dateSearchStart != null && prms.dateSearchEnd != null)
            {
                var thisDate = prms.dateSearchStart.Value.Date;
                var tmr = prms.dateSearchEnd.Value.Date.AddDays(1);
                thisQuery = thisQuery.Where(d => d.MeetingDate >= thisDate && d.MeetingDate < tmr);
            }
            if (prms.IsFilterActive != null)
            {
                var filterBoolean = prms.IsFilterActive.GetValueOrDefault(false);
                thisQuery = thisQuery.Where(c => c.IsMemo == filterBoolean);
            }
            switch (prms.StatusSearch)
            {
                case MemoStatus.Draft:
                    thisQuery = thisQuery.Where(c => !c.IsMemo); break;
                case MemoStatus.Editted:
                    thisQuery = thisQuery.Where(c => c.IsMemo && !(c.IsSentMail == true)); break;
                case MemoStatus.Sent:
                    thisQuery = thisQuery.Where(c => c.IsMemo && c.IsSentMail == true); break;
                case MemoStatus.All:
                default:
                    break;
            }
            if (prms.GroupId >= 0)
            {
                thisQuery = thisQuery.Where(d => d.GroupId == prms.GroupId);
            }
              
            //thisQuery = thisQuery.OrderBy(d => 0);
            switch (prms.SortField)
            {
                case MemoField.Group:
                    thisQuery = thisQuery.OrderByExt(d => d.GroupId, prms.IsDesc);
                    break;
                case MemoField.MeetingDate:
                    thisQuery = thisQuery.OrderByExt(d => d.MeetingDate, prms.IsDesc);
                    break;
                case MemoField.UpdateBy:
                    thisQuery = thisQuery.OrderByExt(d => d.GroupId, prms.IsDesc);
                    break;
                case MemoField.UpdateDate:
                    thisQuery = thisQuery.OrderByExt(d => d.memo.ModifiedDate, prms.IsDesc);
                    break;
                default:
                    thisQuery = thisQuery.OrderByExt(d => d.MeetingDate, true);
                    break;
            }
                                            

            allCount = thisQuery.Count();
            if (start >= 0 && count >= 0)
                thisQuery = thisQuery.Skip(start).Take(count);

            var res = thisQuery.ToList().ToDTOs();
            res.ForEach(d =>
            {
                if (!string.IsNullOrEmpty(d.memo.UpdateByUserCode))
                {
                    var updateUser = this.userResolver.GetByCode(d.memo.UpdateByUserCode);
                    if (updateUser != null)
                    {
                        d.memo.UpdateByFullName = updateUser.FullName;
                    }
                     
                }
                d.IsReady = this.tpsContext.issues.Where(r => r.meeting_issue.Count(r2 => r2.MeetingId == d.MeetingId) > 0).All(d2 => d2.Status == (int)IssuesStatus.PendingAfterMeeting || d2.Status == (int)IssuesStatus.ClosedWaitKM || d2.Status == (int)IssuesStatus.Recorded);
                //d.memo.UpdateByFullName = this.pisContext.PersonalInfo.Where(d2 => d2.CODE == d.memo.UpdateByUserCode && new string[] { "A", "I", "B", "J" }.Contains(d2.WSTCODE)).Select(d3 => d3.INAME + " " + d3.FNAME + " " + d3.LNAME).FirstOrDefault();
            });
            return res;
        }
        public meetingDto GetMeetingById(int meetingId)
        {
            var resEntity = this.tpsContext.meeting.Where(d => d.MeetingId == meetingId).FirstOrDefault();
            if (resEntity.meeting_attendees.Count == 0)
            {
                initAttendee(resEntity);
                
            }
            var res = resEntity.ToDTO();
            var usersInMeeting = this.userResolver.GetByCodes(res.meeting_attendees.Select(c => c.AttendeeCode));
            res.meeting_attendees.ForEach(r =>
                {
                    r.UserInfo = usersInMeeting.Where(c => c.CODE == r.AttendeeCode).FirstOrDefault();
                });
            if (!string.IsNullOrEmpty(res.memo.UpdateByUserCode))
            {
                var uppateUser=this.userResolver.GetByCode(res.memo.UpdateByUserCode);
                if (uppateUser != null)
                    res.memo.UpdateByFullName = uppateUser.FullName;  //this.pisContext.PersonalInfo.Where(d2 => d2.CODE == res.memo.UpdateByUserCode && new string[] { "A", "I", "B", "J" }.Contains(d2.WSTCODE)).Select(d3 => d3.INAME + " " + d3.FNAME + " " + d3.LNAME).FirstOrDefault();
            }
            
            res.meeting_issue = this.tpsContext.meeting_issue.Where(d => d.MeetingId == res.MeetingId).ToDTOs();
            res.meeting_issue.ForEach(d =>
                {
                    d.issues = this.tpsContext.issues.Where(dd => dd.IssueId == d.IssueId).FirstOrDefault().ToDTO();
                });
            return res;
        }
        public bool SaveMemo(memoDto toSave, bool isChangeStatus)
        {
            var memoEntity = this.tpsContext.memo.Where(d => d.MeetingId == toSave.MeetingId).FirstOrDefault();
            memoEntity.Memo1 = toSave.Memo1;
            memoEntity.Memo2 = toSave.Memo2;
            memoEntity.Memo3 = toSave.Memo3;
            memoEntity.ModifiedDate = DateTime.Now;
            memoEntity.UpdateByUserCode = this.userInfo.CODE;
            if (isChangeStatus)
                memoEntity.meeting.IsMemo = true;
            toSave.SubMemo.ForEach(submemo =>
                {
                    var curEntity = memoEntity.SubMemo.Where(d => d.MainNumber == submemo.MainNumber && d.SubNumber == submemo.SubNumber).FirstOrDefault();
                    if (curEntity==null){
                        curEntity = new SubMemo();
                        curEntity.MemoAgendaId = 0;
                        curEntity.MeetingId = memoEntity.MeetingId;
                        this.tpsContext.SubMemo.Add(curEntity);
                    }
                    curEntity.MainNumber = submemo.MainNumber;
                    curEntity.SubNumber = submemo.SubNumber;
                    curEntity.Text = submemo.Text;
                });
            var toDel = from submemo in memoEntity.SubMemo
                        where (toSave.SubMemo.Where(r => r.MainNumber == submemo.MainNumber && r.SubNumber == submemo.SubNumber).Count() == 0)
                        select submemo;
            toDel.ToList().ForEach(d => this.tpsContext.SubMemo.Remove(d));
            


            this.tpsContext.SaveChanges();
            return true;
        }
        public meetingDto GetLatestMeetingByIssueId(int issueId)
        {
            var meetingList = from mtg in this.tpsContext.meeting
                              where mtg.meeting_issue.Count(d => d.IssueId == issueId) > 0
                              select mtg;
            var thisMtg = meetingList.Where(r => r.MeetingDate == meetingList.Max(d => d.MeetingDate)).FirstOrDefault();
            return thisMtg.ToDTO();
        }
        public DateTime nextMeetingDate(string plantshortcode, int pendingMonth = 0)
        {
            var groupObj = this.GetGroupByPlantCode(plantshortcode);
            if (groupObj == null)
                return DateTime.MinValue;
            var day = groupObj.DayOfWeek;
            var week = groupObj.WeekOfMonth;
            var p = DateTime.Now.AddDays(1);
            p = p.AddMonths(pendingMonth);
            while (!(p.DayOfWeek == (DayOfWeek)day && ((p.Day / 7) + 1) == week))
            {
                p = p.AddDays(1);
            }
            return p;
        }
        public DateTime nextMeetingDate(int groupId, int pendingMonth = 0)
        {
            var groupObj = this.GetGroupById(groupId);
            if (groupObj == null)
                return DateTime.MinValue;
            return this.nextMeetingDate(groupObj.DayOfWeek, groupObj.WeekOfMonth, pendingMonth);
        }
        public DateTime nextMeetingDate(int? dayOfWeek, int? weekOfMonth, int pendingMonth = 0)
        {
            var day = dayOfWeek;
            var week = weekOfMonth;
            var p = DateTime.Now.AddDays(1);
            p = p.AddMonths(pendingMonth);
            while (!(p.DayOfWeek == (DayOfWeek)day && (((p.Day - 1) / 7) + 1) == week))
            {
                p = p.AddDays(1);
            }
            return p;
        }
        public DateTime nextMeetingDateIncludeToday(int? dayOfWeek, int? weekOfMonth, int pendingMonth = 0)
        {
            var day = dayOfWeek;
            var week = weekOfMonth;
            var p = DateTime.Now.Date;
            p = p.AddMonths(pendingMonth);
            while (!(p.DayOfWeek == (DayOfWeek)day && (((p.Day - 1) / 7) + 1) == week))
            {
                p = p.AddDays(1);
            }
            return p;
        }
        public DateTime nextMeetingDateOnMonth(int groupId, int onMonth)
        {
            var groupObj = this.GetGroupById(groupId);
            if (groupObj == null)
                return DateTime.MinValue;
            var day = groupObj.DayOfWeek;
            var week = groupObj.WeekOfMonth;
            DateTime p;
            if (onMonth < DateTime.Now.Month)
            {
                p = new DateTime(DateTime.Now.Year + 1, onMonth, 1);
            }
            else
            {
                p = new DateTime(DateTime.Now.Year, onMonth, 1);
            }
            while (!(p.DayOfWeek == (DayOfWeek)day && (((p.Day - 1) / 7) + 1) == week))
            {
                p = p.AddDays(1);
            }
            return p;
        }
        public DateTime nextMeetingDateIncludeShiftDate(int groupId, int pendingMonth = 0)
        {
            var groupObj = this.GetGroupById(groupId);
            if (groupObj == null)
                return DateTime.MinValue;
            if (groupObj.IsShiftThisMonth)
                return groupObj.ShiftDate.GetValueOrDefault(DateTime.MinValue);
            return this.nextMeetingDate(groupId, pendingMonth);
        }
        public bool UpdateAttendees(int meetingId, List<meeting_attendeesDto> newList)
        {
            var entity = this.tpsContext.meeting.Where(r => r.MeetingId == meetingId).FirstOrDefault();
            if (entity == null)
                return false;
            foreach (var att in newList)
            {
                if (entity.meeting_attendees.Where(r => r.AttendeeCode == att.AttendeeCode).Count() == 0)
                {
                    entity.meeting_attendees.Add(new meeting_attendees()
                    {
                        AttendeeCode = att.AttendeeCode,
                        AttendedType = (int)att.Attended
                    });
                }
            }
            var toDel = entity.meeting_attendees.Where(r => r.MeetingId == meetingId && newList.Where(d => d.AttendeeCode == r.AttendeeCode).Count() == 0);
            toDel.ToList().ForEach(d => this.tpsContext.meeting_attendees.Remove(d));

            foreach (var attendee in entity.meeting_attendees.ToList())
            {
                var toUpdate = newList.FirstOrDefault(c => c.AttendeeCode == attendee.AttendeeCode);
                if (toUpdate != null)
                    attendee.AttendedType = (int)toUpdate.Attended;
            }
            this.tpsContext.SaveChanges();
            return true;
        }
        public List<PersonalInfoDto> GetMailByMeeting(int meetingId)
        {
            var t = this.tpsContext.meeting_attendees.Where(r => r.MeetingId == meetingId).ToList();
            if (t == null || t.Count == 0)
            {
                var meetingEntity = this.tpsContext.meeting.Where(r => r.MeetingId == meetingId).FirstOrDefault();
                if (meetingEntity != null)
                {
                    initAttendee(meetingEntity);
                    t = this.tpsContext.meeting_attendees.Where(r => r.MeetingId == meetingId).ToList();
                }
                else
                {
                    return new List<PersonalInfoDto>();
                }
            }

            var res = t.Select(t2 => userResolver.GetByCode(t2.AttendeeCode)).ToList().Where(d => d != null).ToList();
            res.ForEach(d => 
                {
                    d.WorkgroupNameTh = d.unit.unitabbr;
                });
            return res;
        }
        public int GetBeforeMeetingId(meetingDto currentMeeting)
        {
            var query = from mtg in this.tpsContext.meeting
                        where mtg.GroupId == currentMeeting.GroupId && mtg.MeetingDate < currentMeeting.MeetingDate
                        orderby mtg.MeetingDate descending
                        select mtg.MeetingId;
            if (query.Count() > 0)
            {
                return query.FirstOrDefault();
            }
            else
            {
                return -1;
            }

        }
        public void DeleteMeeting(int id)
        {
            var toDel = this.tpsContext.meeting.Where(r => r.MeetingId == id).FirstOrDefault();
            if (toDel != null)
                this.tpsContext.meeting.Remove(toDel);
            this.tpsContext.SaveChanges();
            return;
        }
        public void SetSentMail(int meetingId, bool isSend)
        {
            var entity = this.tpsContext.meeting.FirstOrDefault(d => d.MeetingId == meetingId);
            if (entity == null)
                return;
            entity.IsSentMail = isSend;
            tpsContext.SaveChanges();
            return;
        }
        public List<DateTime> GetAllMeetingDates(bool? isActive)
        {
            var query = tpsContext.meeting.AsQueryable();
            if (isActive != null)
            {
                var bitToSearch = isActive.GetValueOrDefault(false);
                query = query.Where(c => c.IsMemo == bitToSearch);
            }
            return query.Select(c => c.MeetingDate).Distinct().ToList();
        }
        public void ResolveAttendeesInfo(meetingDto meeting)
        {
            var userCodeList = meeting.meeting_attendees.Select(c => c.AttendeeCode).ToList();
            var users = this.pisContext.PersonalInfo.Where(c => userCodeList.Contains(c.CODE)).ToDTOs();
            meeting.meeting_attendees.ForEach(attEntity =>
                {
                    var thisUser = users.Where(c => c.CODE == attEntity.AttendeeCode).FirstOrDefault();
                    attEntity.UserInfo = thisUser;
                });
        }
        public void CreateMemoInMeeting(int meetingId)
        {
            var entity = this.tpsContext.meeting.Where(c => c.IsMemo == false && c.MeetingId == meetingId).FirstOrDefault();
            if (entity == null)
                return;
            entity.IsMemo = true;
            this.tpsContext.SaveChanges();
            return;
        }
        public List<issuesDto> GetIssuesByMeeting(int meetingId)
        {
            return this.tpsContext.meeting_issue.Where(c => c.MeetingId == meetingId)
                .Select(d => d.issues).ToList()
                .Select(c => c.ToDTO(c.allowplant.group, c.workgroup)).ToList();
        }
        public void AddIssuesToMeeting(int meetingId, List<int> issuesId)
        {
            foreach (var issueId in issuesId)
            {
                if (this.tpsContext.meeting_issue.Count(d => d.IssueId == issueId && d.MeetingId == meetingId) > 0)
                {
                    throw new System.Data.DuplicateNameException("Meeting already have these issues");
                }
            }
            foreach (var issueId in issuesId)
            {
                var thisRelation = this.tpsContext.meeting_issue.Create();
                thisRelation.MeetingId = meetingId;
                thisRelation.IssueId = issueId;
                thisRelation.meeting = this.tpsContext.meeting.Find(meetingId);
                thisRelation.issues = this.tpsContext.issues.Find(issueId);
                this.resolveMeetingIssueRelationType(thisRelation);
                this.tpsContext.meeting_issue.Add(thisRelation);
            }
            this.tpsContext.SaveChanges();
        }
        public void DeleteIssuesFromMeeting(int meetingId, int issueId)
        {
            var toDel = tpsContext.meeting_issue.Where(c => c.MeetingId == meetingId && c.IssueId == issueId);
            if (toDel.Count() == 0)
                throw new InvalidDataException("Deleted data is not actually exists in database");
            tpsContext.meeting_issue.RemoveRange(toDel);
            tpsContext.SaveChanges();
        }
        #endregion


        public void Dispose()
        {
            this.tpsContext.Dispose();
            this.sapContext.Dispose();
            this.pisContext.Dispose();
            
        }


        #region Workgroup and Approver

        public List<workgroupDto> GetAllWorkGroup()
        {
            return this.tpsContext.workgroup.ToList().ToDTOs();
        }
        public List<PersonalInfoDto> GetMainApprover(string unitcode)
        {
            
            //return usersInUnit.Where(d => this.userResolver.GetUserRights(d.ToDTO()).HasFlag(UserRights.ApprovedProblem)).FirstOrDefault().ToDTO();
            //var user = (from u in this.pisContext.PersonalInfo
            //            join r in this.pisContext.Report_To on u.CODE equals r.CODE
            //            select u);
            //var user2 = this.pisContext.PersonalInfo.Join(this.pisContext.Report_To, r => r.CODE, u => u.CODE, (r2, u2) => new { user = r2, report = u2 }).ToList();
            //var approver = user2.Where(d => d.report.BAND == "AC1" || Convert.ToInt32(d.report.JOBGROUP) >= 9).Select(d => d.user).FirstOrDefault();
            //return approver.ToDTO();
            //return user.Where(d => int.Parse(d.JOBGROUP) >= 9 || ).FirstOrDefault().ToDTO();
            //var query = string.Format("select p.* from dbo.Report_To r inner join dbo.personel_info p on p.CODE = r.code where (r.Band = 'AC1' OR r.JOBGROUP >=9) and unitcode = '{0}'", unitcode);
            var query = string.Format("select DISTINCT(p.CODE) as TEST, p.* from dbo.Report_To r inner join dbo.personel_info p on p.CODE = r.code where (r.Band = 'AC1' OR r.JOBGROUP >=10) and unitcode = '{0}'", unitcode);
            //select DISTINCT(p.CODE) as TEST, p.* from dbo.Report_To r inner join dbo.personel_info p on p.CODE = r.code where (r.Band = 'AC1' OR r.JOBGROUP >=9) and unitcode = '80000602'
            var user = this.pisContext.PersonalInfo.SqlQuery(query).Where(d => new string[] { "A", "I", "B", "J" }.Contains(d.WSTCODE)).ToList();
            user.ForEach(d => d.POSNAME = this.pisContext.positions.Where(r => r.poscode == d.POSCODE).Select(r => r.posname).FirstOrDefault());
            return user.ToDTOs();

            
        }
        public List<PersonalInfoDto> GetCustomApproverInUnit(string unitcode)
        {
            var cusApprover = this.tpsContext.approver.Where(d => d.unitcode == unitcode).Select(c => c.code).ToList();
            var res = this.pisContext.PersonalInfo.Where(d => cusApprover.Contains(d.CODE) && new string[] { "A", "I", "B", "J" }.Contains(d.WSTCODE)).ToList();
            res.ForEach(d => d.POSNAME = this.pisContext.positions.Where(r => r.poscode == d.POSCODE).Select(r => r.posname).FirstOrDefault());
            return res.ToDTOs();
        }
        public bool AddApproversByCode(List<string> usercodeArray, string unitcode)
        {
            
            #region Check code consistency

            var actualCodeArray = (from code in usercodeArray
                                   where this.pisContext.PersonalInfo.Count(d => d.CODE == code) > 0
                                   select code).ToList();

            #endregion

            actualCodeArray.ForEach(code =>
            {
                if (this.tpsContext.approver.Count(d => d.unitcode == unitcode && d.code == code) == 0)
                {
                    this.tpsContext.approver.Add(new approver()
                    {
                        unitcode = unitcode,
                        code = code
                    });
                }
            });
            this.tpsContext.SaveChanges();
            return true;
        }
        public bool DeleteApprover(string usercode, string unitcode)
        {
            var toDel = this.tpsContext.approver.Where(d => d.unitcode == unitcode && d.code == usercode).FirstOrDefault();
            if (toDel == null)
                return false;
            this.tpsContext.approver.Remove(toDel);
            return this.tpsContext.SaveChanges() > 0;
        }
        public string GetMasterUnitCode()
        {
            return this.tpsContext.workgroup.Where(r => r.IsMaster > 0).Select(d => d.unitcode).FirstOrDefault();
        }
        public List<PersonalInfoDto> GetCustomPresenterInUnit(string unitcode)
        {
            var cusApprover = this.tpsContext.presenter.Where(d => d.unitcode == unitcode).Select(c => c.code).ToList();
            var res = this.pisContext.PersonalInfo.Where(d => cusApprover.Contains(d.CODE) && new string[] { "A", "I", "B", "J" }.Contains(d.WSTCODE)).ToList();
            res.ForEach(d => d.POSNAME = this.pisContext.positions.Where(r => r.poscode == d.POSCODE).Select(r => r.posname).FirstOrDefault());
            return res.ToDTOs();
        }
        public bool AddPresenterByCode(List<string> usercodeArray, string unitcode)
        {
            #region Check code consistency

            var actualCodeArray = (from code in usercodeArray
                                   where this.pisContext.PersonalInfo.Count(d => d.CODE == code) > 0
                                   select code).ToList();

            #endregion

            actualCodeArray.ForEach(code =>
            {
                if (this.tpsContext.presenter.Count(d => d.unitcode == unitcode && d.code == code) == 0)
                {
                    this.tpsContext.presenter.Add(new presenter()
                    {
                        unitcode = unitcode,
                        code = code
                    });
                }
            });
            this.tpsContext.SaveChanges();
            return true;
        }
        public bool DeletePresenter(string usercode, string unitcode)
        {
            var toDel = this.tpsContext.presenter.Where(d => d.unitcode == unitcode && d.code == usercode).FirstOrDefault();
            if (toDel == null)
                return false;
            this.tpsContext.presenter.Remove(toDel);
            return this.tpsContext.SaveChanges() > 0;
        }
        #endregion

        #region Group and Meeting Scheduler Settings

        public List<group_mailingDto> GetMailingByGroup(int groupId)
        {
            return this.tpsContext.group_mailing.Where(d => d.GroupId == groupId).ToDTOs();
        }
        public List<PersonalInfoDto> GetMailingUserDetailByGroup(int groupId)
        {
            var mailings = this.GetMailingByGroup(groupId);
            var users = from mail in mailings
                        select this.userResolver.GetByCode(mail.UserCode);
            return users.ToList();
        }
        public bool UpdateDayAndWeekInGroup(int groupId, int day, int week, bool isSuspended, DateTime? suspendDedTime, bool isShifting=true)
        {
            var thisEntity = this.tpsContext.group.Where(d => d.GroupId == groupId).FirstOrDefault();
            if (thisEntity == null)
                return false;
            thisEntity.ModifiedDate = DateTime.Now;
            thisEntity.DayOfWeek = day;
            thisEntity.WeekOfMonth = week;
            if (isShifting)
            {
                thisEntity.IsSuspended = isSuspended;
                if (isSuspended)
                {
                    thisEntity.SuspendedStart = DateTime.Now;
                    thisEntity.SuspendedEnd = suspendDedTime;
                }
                else
                {
                    thisEntity.SuspendedEnd = null;
                    thisEntity.SuspendedStart = null;
                }
            }
            this.tpsContext.SaveChanges();
            return true;
        }
        public bool UpdateTime(int groupId, DateTime startTime, DateTime endTime)
        {
            var thisEntity = this.tpsContext.group.Where(d => d.GroupId == groupId).FirstOrDefault();
            if (thisEntity == null)
                return false;
            thisEntity.ModifiedDate = DateTime.Now;

            thisEntity.TimeEnd = endTime;
            thisEntity.TimeStart = startTime;

            this.tpsContext.SaveChanges();
            return true;
        }
        public bool UpdatePlantsInGroup(int groupId, List<string> newPlantCodes)
        {
            var thisEntity = this.tpsContext.group.Where(d => d.GroupId == groupId).FirstOrDefault();
            if (thisEntity == null)
                return false;
            thisEntity.ModifiedDate = DateTime.Now;

            this.tpsContext.allowplant.Where(d => d.groupId == groupId).ToList().ForEach(r => r.groupId = null);
            newPlantCodes.ForEach(plantCode =>
            {
                var plantEntity = this.tpsContext.allowplant.Where(r => r.plantshortcode == plantCode).FirstOrDefault();
                plantEntity.groupId = groupId;
            });
            

            this.tpsContext.SaveChanges();
            return true;
        }
        public bool AddCustomMailers(int groupId, List<string> userCodes)
        {
            userCodes.ForEach(userCode =>
                {
                    if (this.tpsContext.group_mailing.Count(r => r.GroupId == groupId && r.UserCode == userCode) == 0)
                    {
                        this.tpsContext.group_mailing.Add(new group_mailing()
                        {
                            GroupId = groupId,
                            UserCode = userCode
                        });
                    }
                });
            this.tpsContext.SaveChanges();
            return true;
        }
        public bool DelCustomMailers(int groupId, List<string> userCodes)
        {
            var toDels = this.tpsContext.group_mailing.Where(d => d.GroupId == groupId && userCodes.Contains(d.UserCode));
            toDels.ToList().ForEach(d => this.tpsContext.group_mailing.Remove(d));
            this.tpsContext.SaveChanges();
            return true;
        }
        public groupDto GetGroupByPlantCode(string plantcode)
        {
            var k = this.tpsContext.allowplant.Where(r => r.plantshortcode == plantcode).Select(d => d.group).FirstOrDefault();
            if (k == null)
                return null;
            var res = k.ToDTO();
            return res;
        }
        public groupDto GetGroupById(int groupId)
        {
            return this.tpsContext.group.Where(g => g.GroupId == groupId).FirstOrDefault().ToDTO();
        }
        public bool UpdateShiftingDate(int groupId, bool isShift, DateTime? shiftDate, string location)
        {
            var groupToUpdate = this.tpsContext.group.FirstOrDefault(c => c.GroupId == groupId);
            if (groupToUpdate == null)
                return false;
            groupToUpdate.IsShiftThisMonth = isShift;
            groupToUpdate.ShiftDate = shiftDate;
            groupToUpdate.Location = location;
            this.tpsContext.SaveChanges();
            return true;
        }
        public string GenerateNewCalendarId(int groupId)
        {
            var newId = Guid.NewGuid().ToString();
            var entity = this.tpsContext.group.Find(groupId);
            entity.LatestCalendarId = newId;
            this.tpsContext.SaveChanges();
            return newId;
        }
        public string GetCalendarId(int groupId)
        {
            return this.tpsContext.group.Where(c => c.GroupId == groupId).Select(c => c.LatestCalendarId).FirstOrDefault();
        }

        #endregion

        #region Mail Setting

        public mail_templateDto GetMailTemplate()
        {
            if (this.tpsContext.mail_template.Count() == 0)
            {
                this.tpsContext.mail_template.Add(new mail_template()
                {
                    MailTemplateId = 1,
                    Text1 = "วาระที่ 1:",
                    Text2 = "วาระที่ 2:",
                    Text3 = "วาระที่ 3:",
                    Text4 = "วาระที่ 4:",
                });
                this.tpsContext.SaveChanges();
            }
            return this.tpsContext.mail_template.First().ToDTO();
        }
        public bool UpdateMailTemplate(mail_templateDto dto)
        {
            var entity = this.tpsContext.mail_template.First();
            entity.Text1 = dto.Text1;
            entity.Text2 = dto.Text2;
            entity.Text3 = dto.Text3;
            entity.Text4 = dto.Text4;
            this.tpsContext.SaveChanges();
            return true;
        }

        #endregion

        #region Internal Users

        public PersonalInfoDto GetTpsUserById(int id)
        {
            var thisUser = this.tpsContext.tpsusers.Where(d => d.UserId == id).FirstOrDefault().ToGenericDto();
            return thisUser;
        }
        public List<PersonalInfoDto> GetAllTpsUser()
        {
            return this.tpsContext.tpsusers.ToList().Select(r => r.ToGenericDto()).ToList();
        }
        public PersonalInfoDto UpdateUser(PersonalInfoDto userInfo, string password = null, string imgTempName = null)
        {
            try
            {

                var thisEntity = userInfo.TpsUserId < 0 ? new tpsusers() : this.tpsContext.tpsusers.Where(d => d.UserId == userInfo.TpsUserId).FirstOrDefault();
                
                AutoMapper.Mapper.CreateMap<PersonalInfoDto, tpsusers>();
                AutoMapper.Mapper.Map<PersonalInfoDto, tpsusers>(userInfo, thisEntity);
                if (!string.IsNullOrEmpty(password))
                {
                    var p = new WebAuthenManager.Password(password);
                    thisEntity.PASSWORD = p.HashPassword;
                }
                if (!string.IsNullOrEmpty(imgTempName))
                {
                    thisEntity.ImageUrl = "/Image/Avatars?name=" + imgTempName;
                }
                else
                {
                    thisEntity.ImageUrl = string.Empty;
                }
                if (userInfo.TpsUserId < 0)
                {
                    this.tpsContext.tpsusers.Add(thisEntity);
                }
                thisEntity.CODE = string.Empty;
                this.tpsContext.SaveChanges();
                thisEntity.CODE = "TP" + thisEntity.UserId;
                this.tpsContext.SaveChanges();

                return thisEntity.ToGenericDto();
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException ex)
            {
                throw ex;
            }
        }
        public bool DeleteTpsUser(int id)
        {
            var toDel = this.tpsContext.tpsusers.Where(d => d.UserId == id).FirstOrDefault();
            if (toDel == null) return false;
            this.tpsContext.tpsusers.Remove(toDel);
            this.tpsContext.SaveChanges();
            return true;
        }

        #endregion

        public List<int> GetAllMachineCode()
        {
            throw new NotImplementedException();
        }

        public List<groupDto> GetAllGroups()
        {
            return this.tpsContext.group.ToList().ToDTOs().ToList();
        }

        public List<PersonalInfoDto> GetMailByGroup(int groupId)
        {
            //var vbgDummy = IssuesData.GetVBGDummyRelation();
            //var query = from user in this.pisContext.PersonalInfo
            //            join reportTo in this.pisContext.Report_To on user.CODE equals reportTo.CODE
            //            where (reportTo.BAND.ToUpper().Contains("AC1") && user.unit.DUMMY_RELATIONSHIP.StartsWith(vbgDummy))
            //            || (reportTo.BAND.ToUpper().Contains("AD") && user.unit.DUMMY_RELATIONSHIP == vbgDummy) && new string[] { "A", "I", "B", "J" }.Contains(user.WSTCODE)
            //            select user;
            //var group = this.tpsContext.group.Where(d => d.GroupId == groupId).FirstOrDefault();
            //if (group == null) 
            //    return null;
            //var userTps = group.group_mailing.Select(d => d.UserCode).ToList();
            //var res = query.ToList().ToDTOs();
            //res.ForEach(d => d.POSNAME = pisContext.positions.Where(d2 => d2.poscode == d.POSCODE).Select(d2 => d2.posname).FirstOrDefault());
            //var extraUser = from user in this.pisContext.PersonalInfo
            //                where userTps.Contains(user.CODE) && new string[] {"A","I","B","J"}.Contains(user.WSTCODE)
            //                select user;
            //res.AddRange(extraUser.ToList().ToDTOs());
            //var masterCode=this.GetMasterUnitCode();
            //var headUser = this.pisContext.PersonalInfo.Where(r => r.UNITCODE == masterCode && new string[] { "A", "I", "B", "J" }.Contains(r.WSTCODE));
            //res.AddRange(headUser.ToList().ToDTOs());
            //return res;
            var mailFinder = new MailScheduleCreator();
            return mailFinder.GetMailUserToSendInGroup(this.tpsContext.group.Find(groupId)).Select(c => c.ToDTO()).ToList();
        }

        public List<PersonalInfoDto> GetMailableByGroup(string unitcode, string search)
        {
            var pygDummy = GetPYGUDummyRelation();
            var query = from user in this.pisContext.PersonalInfo
                        where user.unit.DUMMY_RELATIONSHIP.StartsWith(pygDummy) && new string[] { "A", "I", "B", "J" }.Contains(user.WSTCODE)
                        select user;
            if (!string.IsNullOrWhiteSpace(search))
            {
                query = query.Where(d => d.FNAME.Contains(search) || d.LNAME.Contains(search) || d.CODE.Contains(search));
            }
            var res = query.ToList().ToDTOs();
            //res.ForEach(d => d.POSNAME = this.pisContext.personel_info_tps.Where(r => r.CODE == d.CODE).Select(r => r.POSNAME).FirstOrDefault());
            return res;
        }

        #region System

        public List<allowplantDto> GetSystemAllowPlants()
        {
            return this.tpsContext.allowplant.ToList().ToDTOs();
        }

        public allowplantDto GetSystemAllowPlant(string code)
        {
            return this.tpsContext.allowplant.Where(d => d.plantshortcode == code).FirstOrDefault().ToDTO();
        }
        public void DeletePlant(string code)
        {
            var entity = this.tpsContext.allowplant.Where(r => r.plantshortcode == code).FirstOrDefault();
            this.tpsContext.allowplant.Remove(entity);
            this.tpsContext.SaveChanges();

        }

        public allowplantDto EditPlant(string code, allowplantDto dto)
        {
           

            var entity = this.tpsContext.allowplant.Where(r => r.plantshortcode == code).FirstOrDefault();
            if (entity == null)
            {
                entity = this.tpsContext.allowplant.Create();
                entity.plantshortcode = dto.plantshortcode;
                this.tpsContext.allowplant.Add(entity);
            }
            entity.plantdisplaycode = dto.plantdisplaycode;
            try
            {
                this.tpsContext.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return entity.ToDTO();
        }

        #endregion

        #region Mail Resolver

        public List<PersonalInfoDto> GetAllApprover(string unitcode)
        {
            var res = this.GetMainApprover(unitcode);
            res.AddRange(this.GetCustomApproverInUnit(unitcode));
            return res;
        }

        public List<PersonalInfoDto> GetCCApprover(string unitcode)
        {
            var res = this.pisContext.PersonalInfo.Where(d => d.POSNAME == "วิศวกร" && d.UNITCODE == unitcode && new string[] { "A", "I", "B", "J" }.Contains(d.WSTCODE)).ToList().ToDTOs();
            return res;
        }

        #endregion

        #region File

        public summary_kmDto GetKmFileById(int id)
        {
            return this.tpsContext.summary_km.Where(d => d.KmId == id).FirstOrDefault().ToDTO();
        }

        public problem_fileDto GetProblemFileById(int id)
        {
            return this.tpsContext.problem_file.Where(d => d.FileId == id).FirstOrDefault().ToDTO();
        }

        #endregion

        public void SetBaseUrl()
        {
            var entity = this.tpsContext.TpsConfig.Where(d => d.ConfigKey == "BaseUrl").FirstOrDefault();
            if (entity == null)
            {
                entity = new TpsConfig();
                entity.ConfigKey = "BaseUrl";
                this.tpsContext.TpsConfig.Add(entity);
            }
            entity.ConfigValue = WebHelper.Constants.AppPath;
            this.tpsContext.SaveChanges();
        }

        public string GetLogConfigPath()
        {
            var entity = this.tpsContext.TpsConfig.Where(d => d.ConfigKey == "LogPath").FirstOrDefault();
            if (entity == null)
            {
                entity = new TpsConfig();
                entity.ConfigKey = "LogPath";
                entity.ConfigValue = TPSPathHelper.LogDefaultPath();
                this.tpsContext.TpsConfig.Add(entity);
                this.tpsContext.SaveChanges();
            }
            return entity.ConfigValue;
        }

        public bool Unbreakdown(int issueId, string reason, string userCode)
        {
            var thisEntity = this.tpsContext.issues.SingleOrDefault(r => r.IssueId == issueId);
            if (thisEntity == null)
            {
                return false;
            }
            if (thisEntity.IsDeleted || thisEntity.IsUnbreakdown)
            {
                return false;
            }
            if (thisEntity.Status != (int)IssuesStatus.New)
            {
                return false;
            }

            thisEntity.Status = (int)IssuesStatus.Deleted;
            thisEntity.IsDeleted = true;
            thisEntity.DeletedReason = reason;
            thisEntity.IsUnbreakdown = true;
            thisEntity.DeletedBy = userCode;
            this.tpsContext.SaveChanges();

            return true;
        }

        public bool Delete(int issueId, string reason, string userCode)
        {
            var thisEntity = this.tpsContext.issues.SingleOrDefault(r => r.IssueId == issueId);
            if (thisEntity == null)
            {
                return false;
            }
            if (thisEntity.IsDeleted || thisEntity.IsUnbreakdown)
            {
                return false;
            }
            if (thisEntity.Status != (int)IssuesStatus.Waiting)
            {
                return false;
            }

            thisEntity.Status = (int)IssuesStatus.Deleted;
            thisEntity.IsDeleted = true;
            thisEntity.DeletedReason = reason;
            thisEntity.IsUnbreakdown = false;
            thisEntity.DeletedBy = userCode;
            this.tpsContext.SaveChanges();

            return true;
        }

        public void SetDefaultMeetingLocation(string location)
        {
            var config = this.tpsContext.TpsConfig.Where(c => c.ConfigKey == "LocationRoom").FirstOrDefault();
            if (config == null)
            {
                config = new TpsConfig();
                config.ConfigKey = "LocationRoom";
                this.tpsContext.TpsConfig.Add(config);
            }
            config.ConfigValue = location;
            this.tpsContext.SaveChanges();
        }
        public string GetDefaultMeetingLocation()
        {
            var config = this.tpsContext.TpsConfig.Where(c => c.ConfigKey == "LocationRoom").FirstOrDefault();
            if (config == null)
            {
                config = new TpsConfig();
                config.ConfigKey = "LocationRoom";
                config.ConfigKey = "PTT";
                this.tpsContext.TpsConfig.Add(config);
                this.tpsContext.SaveChanges();
            }
            return config.ConfigValue;
        }

        public List<issuesMeetingDto> GetIssuesCanBeAddedToMeeting(int meetingId)
        {
            var meeting = this.tpsContext.meeting.Find(meetingId);
            var minTime=System.Data.SqlTypes.SqlDateTime.MinValue.Value;
            var issues = this.tpsContext.issues.Where(c => c.allowplant.groupId == meeting.GroupId &&
                (c.Status == (int)IssuesStatus.Ready || c.Status == (int)IssuesStatus.PendingAfterMeeting ||
                ((SqlFunctions.DateDiff("month", c.ApproveDateTime, DateTime.Now) < 2) && (c.Status == (int)IssuesStatus.ClosedWaitKM || c.Status == (int)IssuesStatus.Recorded))));
            var selector = issues.Where(c => !c.meeting_issue.Any(d => d.MeetingId == meetingId && d.IssueId == c.IssueId))
                .Select(c => new
                {
                    data = c,
                    isHadMeeting = c.meeting_issue.Count(d => d.MeetingId < meetingId) > 0
                });

            var resultDtos = selector.ToList().Select(c => new
            {
                entity = c.data.ToDTO(c.data.allowplant.group, c.data.workgroup),
                isHadMeeting = c.isHadMeeting
            }).ToList();
            var result = new List<issuesMeetingDto>();
            foreach (var r in resultDtos)
            {
                var item = new issuesMeetingDto();
                Ecobz.Utilities.ReflectionUtils.CopyBasicProperty(r.entity, item);
                item.IsHadMeeting = r.isHadMeeting;
                result.Add(item);
            }
            return result;
        }

        #region Log

        public List<activitylog> GetActivities()
        {
            return this.tpsContext.activitylog.OrderByDescending(c => c.ActivityLogId).ToList();
        }
        public List<activitylog> GetActivities(DateTime startDate, DateTime endDate)
        {
            return this.tpsContext.activitylog.Where(c => c.LogTimeStamp >= startDate && c.LogTimeStamp <= endDate).OrderByDescending(c => c.LogTimeStamp).ToList();
        }

        //public void SaveLogForMockupAnotherUser(string userCode, string 

        #endregion

        #region Mail history
        public List<MailLogDto> GetMailLogSearch(int start, int count, out int allCount, string searchTerm)
        {
            return GetMailLogSearch(start, count, out allCount, searchTerm, MailLogType.Unknow);
        }

        public List<MailLogDto> GetMailLogSearch(int start, int count, out int allCount, string searchTerm, MailLogType mailType)
        {
            return GetMailLogSearch(start, count, out allCount, searchTerm, mailType, System.Data.SqlTypes.SqlDateTime.MinValue.Value, System.Data.SqlTypes.SqlDateTime.MaxValue.Value, 0);
        }

        public List<MailLogDto> GetMailLogSearch(int start, int count, out int allCount, string searchTerm, MailLogType mailType, DateTime startTime, DateTime endTime, int groupId)
        {
            var query = this.tpsContext.MailLog.AsQueryable();
            if (!string.IsNullOrEmpty(searchTerm))
                query = query.Where(c => c.SendToAddress.Contains(searchTerm) || c.SendCCAddress.Contains(searchTerm));
            if (mailType != MailLogType.Unknow)
                query = query.Where(c => c.MailType == (int)mailType);
            if (groupId > 0)
            {
                query = query.Where(c => c.GroupId == groupId);
            }
            query = query.Where(c => c.TimeStamp >= startTime && c.TimeStamp <= endTime);
            query = query.OrderByDescending(c => c.TimeStamp);
            allCount = query.Count();
            query = query.Skip(start).Take(count);
            var result = query.ToList();
            return result.ToDTOs();
        }

        public int GetMailLogCount()
        {
            return this.tpsContext.MailLog.Count();
        }
        #endregion
    }
}
