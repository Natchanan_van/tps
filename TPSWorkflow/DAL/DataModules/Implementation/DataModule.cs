﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TPSWorkflow.DAL
{
    public partial class PISEntities
    {
        public PISEntities(string connectionString):base(connectionString)
        {

        }
    }

    public partial class TPSEntities
    {
        public TPSEntities(string connectionString)
            : base(connectionString)
        {

        }
    }

    public partial class SAPEntities
    {
        public SAPEntities(string connectionString)
            : base(connectionString)
        {

        }
    }

    public abstract class DataModule
    {
        protected PISEntities pisContext;
        protected SAPEntities sapContext;
        protected TPSEntities tpsContext;

        public DataModule()
        {
            this.pisContext = new PISEntities();
            this.sapContext = new SAPEntities();
            this.tpsContext = new TPSEntities();
        }
    }
}