﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using TPSWorkflow.DAL.DtoS;
using Ecobz.Utilities;

namespace TPSWorkflow.DAL
{
    public enum ActivityLogType
    {
        [Description("ไม่ทราบที่มา")]
        Unknow=0,
        [Description("จำลองผู้ใช้")]
        MockUser=100,
        [Description("อนุมัติใบแจ้งซ่อมแทนผู้ใช้")]
        ApproveAsUser=200,
        [Description("ยกเลิกใบแจ้งซ่อมแทนผู้ใช้")]
        CancelAsUser=300,
    }
    public class UsersManage : DataModule, IUsersManage
    {
        private Dictionary<string, string> AbbrCodeDict = new Dictionary<string, string>()
        {
            {"วบก.","80000597"},
            {"ซญ.วบก.","80000598"},
            {"วก.วบก.", "80000599"},
            {"บง.วบก.","80000600"},
            {"ตร.วบก.","80000601"},
            {"ผบ.วบก.","80000602"},
            {"บฟ.วบก.", "80000603"},
            {"บค.วบก.","80000604"}

        };
        /*














*/

        private int minimumJobLevel = 10;

        public UsersManage(int minimumJobLevel)
        {
            this.minimumJobLevel = minimumJobLevel;
        }

        public IEnumerable<PersonalInfoDto> GetAll(string searchTerm)
        {
            var pygDummy = IssuesData.GetPYGUDummyRelation();
            if (string.IsNullOrEmpty(searchTerm))
            {
                return this.pisContext.PersonalInfo.Where(d => d.unit.DUMMY_RELATIONSHIP.StartsWith(pygDummy) && new string[] { "A", "I", "B", "J" }.Contains(d.WSTCODE)).ToList().ToDTOs();
            }
            else
                return this.pisContext.PersonalInfo.Where(d => d.unit.DUMMY_RELATIONSHIP.StartsWith(pygDummy) && new string[] { "A", "I", "B", "J" }.Contains(d.WSTCODE) && (d.FULLNAMETH.Contains(searchTerm) || d.CODE.Contains(searchTerm))).ToList().ToDTOs();
        }

        public List<PersonalInfoDto> GetAllVBG(string searchTerm)
        {
            var vbg = IssuesData.GetVBGDummyRelation();
            if (string.IsNullOrEmpty(searchTerm))
            {
                return this.pisContext.PersonalInfo.Where(d => d.unit.DUMMY_RELATIONSHIP.StartsWith(vbg) && new string[] { "A", "I", "B", "J" }.Contains(d.WSTCODE)).ToList().ToDTOs();
            }
            else
                return this.pisContext.PersonalInfo.Where(d => d.unit.DUMMY_RELATIONSHIP.StartsWith(vbg) && (d.FULLNAMETH.Contains(searchTerm) || d.CODE.Contains(searchTerm)) && new string[] { "A", "I", "B", "J" }.Contains(d.WSTCODE)).ToList().ToDTOs();
            
        }

        public List<PersonalInfoDto> GetByWorkGroup(string unitCode)
        {
            var user = this.pisContext.PersonalInfo.Where(d => d.UNITCODE == unitCode && new string[] { "A", "I", "B", "J" }.Contains(d.WSTCODE)).ToList();
            user.ForEach(d => d.POSNAME = this.pisContext.positions.Where(r => r.poscode == d.POSCODE).Select(r => r.posname).FirstOrDefault());
            return user.ToDTOs();
        }

        public List<PersonalInfoDto> GetByWorkGroup(string unitCode, string searchTerm)
        {
            var user = this.pisContext.PersonalInfo.Where(d => d.UNITCODE == unitCode && new string[] { "A", "I", "B", "J" }.Contains(d.WSTCODE));
            if (searchTerm == null)
                searchTerm = string.Empty;
            user = user.Where(d => d.CODE.Contains(searchTerm) || d.FNAME.Contains(searchTerm) || d.LNAME.Contains(searchTerm));
            var res = user.ToList();
            res.ForEach(d => d.POSNAME = this.pisContext.positions.Where(r => r.poscode == d.POSCODE).Select(r => r.posname).FirstOrDefault());
            return res.ToDTOs();
        }

        public PersonalInfoDto GetById(string id)
        {
            throw new NotImplementedException();
        }

        public PersonalInfoDto GetByCode(string code)
        {
            PersonalInfoDto res = null;
            var user = this.pisContext.PersonalInfo.Where(d => d.CODE == code && new string[] { "A", "I", "B", "J" }.Contains(d.WSTCODE)).FirstOrDefault();
            if (user == null)
            {
                var tpsUser = this.tpsContext.tpsusers.Where(r => r.CODE == code).FirstOrDefault();
                if (tpsUser == null)
                    return null;
                res = tpsUser.ToGenericDto();
            }
            else
            {
                res = user.ToDTO();
                res.POSCODE = this.pisContext.positions.Where(d => d.poscode == user.POSCODE).Select(d => d.posname).FirstOrDefault();
            }
            
            return res;
        }

        List<PersonalInfoDto> IUsersManage.GetByCodes(IEnumerable<string> codes)
        {
            var codesAsList = codes.ToList();
            var users = this.pisContext.PersonalInfo
                .Join(this.pisContext.positions, c1 => c1.POSCODE, c2 => c2.poscode, (ob1, ob2) => new { user = ob1, position = ob2 })
                .Where(d => codesAsList.Contains(d.user.CODE) && new string[] { "A", "I", "B", "J" }.Contains(d.user.WSTCODE))
                .Select(d => new
                {
                    user = d.user,
                    positionName = d.position.posname
                }).ToList()
                .Select(c =>
                {
                    var res = c.user.ToDTO();
                    res.POSCODE = c.positionName;
                    return res;
                }).ToList();
            var codesFromPm = users.Select(c => c.CODE).ToList();
            var codesFromTps = codesAsList.Where(d => !codesFromPm.Contains(d)).ToList();
            var usersFromTps = this.tpsContext.tpsusers.Where(r => codesFromTps.Contains(r.CODE)).ToList().Select(c => c.ToGenericDto()).ToList();
            users.AddRange(usersFromTps);
            return users;
        }

        protected UserRights GetUserRights(PersonalInfo user)
        {
            var curRights = UserRights.None;
            if (user == null)
                return curRights;
            WorkUnit userUnit = null;
            if (user.unit != null)
            {
                userUnit = user.unit;
            }
            else
            {
                userUnit = (from unit in pisContext.WorkUnit
                            where unit.personel_info.Count(r => r.CODE == user.CODE) > 0
                            select unit).FirstOrDefault();
                
            }
            if (userUnit == null)
                return UserRights.None;
            var pygDummy = IssuesData.GetPYGUDummyRelation();
            var vbgDummy = IssuesData.GetVBGDummyRelation();
            if (userUnit.DUMMY_RELATIONSHIP.StartsWith(pygDummy))
            {
                curRights |= UserRights.Open | UserRights.MailMeeting;
            }
            if (userUnit.DUMMY_RELATIONSHIP.StartsWith(vbgDummy))
            {
                curRights |= UserRights.Open | UserRights.CreateNewProblem | UserRights.EditProblemCause;
            }
            if (userUnit.unitcode == AbbrCodeDict["ผบ.วบก."])
            {
                curRights |= UserRights.Open | UserRights.CreateNewProblem | UserRights.AssignMeetingResult | UserRights.AssignMeetingReport | UserRights.SystemAdministrator | UserRights.MailMeeting | UserRights.EditProblemCause;
            }
            var reportToUnit = pisContext.Report_To.Where(d => d.CODE == user.CODE).FirstOrDefault();
            if (reportToUnit != null)
            {
                int jobLevel = 0;
                if (int.TryParse(user.JOBGROUP, out jobLevel) && reportToUnit.BAND == "AC1" || jobLevel >=  this.minimumJobLevel)
                    curRights |= UserRights.Open | UserRights.ApprovedProblem;
                if (jobLevel >= this.minimumJobLevel && ((reportToUnit.BAND=="AD" && userUnit.UNIT_LEVEL_ID == "050") || reportToUnit.BAND == "AC1" && userUnit.UNIT_LEVEL_ID == "060"))
                {
                    curRights |= UserRights.MailMeeting;
                }
            }
            
            return curRights;
        }

        public bool IsApproverInUnitCode(string userCode, string unitCode)
        {
            var userInfo = this.GetByCode(userCode);
            var rights = this.GetUserRights(userInfo);
            var dataAccess = new IssuesData();
            if (dataAccess.GetCustomApproverInUnit(unitCode).Count(r => r.CODE == userCode) > 0)
            {
                rights |= UserRights.ApprovedProblem;
            }
            if (!((rights.HasFlag(UserRights.ApprovedProblem) && userInfo.UNITCODE == unitCode) ||
                rights.HasFlag(UserRights.ApprovedProblemAcrossUnit)))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public bool isCanShiftMeeting(PersonalInfoDto userInfo)
        {
            if (userInfo==null)
                return false;
            int joblevel = -1;
            //var reportTo = this.pisContext.Report_To.Where(r=>r.CODE==userInfo.CODE).FirstOrDefault();

            if ((int.TryParse(userInfo.JOBGROUP, out joblevel)) && joblevel >= this.minimumJobLevel) //reportTo != null && reportTo.BAND == "AD")
            {
                return true;
            }
            if (userInfo.TpsType == TpsUserType.SuperAdmin)
                return true;
            return false;
        }
        public UserRights GetUserRights(PersonalInfoDto user)
        {
            if (user == null)
                return UserRights.None;
            if (user.UserType == UserSource.PIS)
            {
                return this.GetUserRights(user.ToEntity());
            }
            else
            {
                if (user.TpsType == TpsUserType.SuperAdmin)
                    return UserRights.SystemAdministrator | UserRights.Administrator | UserRights.CreateNewProblem;
                if (user.TpsType == TpsUserType.Admin)
                {
                    return UserRights.Administrator | UserRights.CreateNewProblem;
                }
                if (user.TpsType == TpsUserType.Staff)
                {
                    return UserRights.Open;
                }
            }
            return UserRights.None;
        }

        public GroupRights GetGroupRights(PersonalInfoDto user, WorkUnitDto group)
        {
            if (user == null || group == null)
                return GroupRights.None;
            var res = GroupRights.None;
            var reportToForThisUser = pisContext.Report_To.Where(d => d.CODE == user.CODE).FirstOrDefault();
            int jobLevel = -1;
            if (reportToForThisUser.BAND == "AC1" && int.TryParse(reportToForThisUser.JOBGROUP, out jobLevel) && jobLevel >= this.minimumJobLevel && user.UNITCODE == group.unitcode)
            {
                res |= GroupRights.Approve;
            }
            if (user.UNITCODE == group.unitcode && user.POSNAME == "วิศวกร" || res.HasFlag(GroupRights.Approve))
            {
                res |= GroupRights.RecieveMail;
            }
            return res;
        }


        public PersonalInfoDto GetByAuthen(string username, string password)
        {
            var user = this.tpsContext.tpsusers.Where(d => d.Username == username).FirstOrDefault();
            if (user == null)
                return null;
            var p = new WebAuthenManager.Password();
            p.HashPassword = user.PASSWORD;
            if (p.VerifyPassword(password))
            {
                var res = user.ToGenericDto();
                if (res.TpsType == (TpsUserType.Staff))
                {
                    res.unit = new WorkUnitDto()
                    {
                        DUMMY_RELATIONSHIP = IssuesData.GetPYGUDummyRelation()
                    };
                }
                else if (res.TpsType == (TpsUserType.Admin) || res.TpsType==TpsUserType.SuperAdmin)
                {
                    var pisAccess = new IssuesData();
                    var masterUnitCode = pisAccess.GetMasterUnitCode();
                    res.unit = this.pisContext.WorkUnit.Where(r => r.unitcode == masterUnitCode).FirstOrDefault().ToDTO();
                    res.UNITCODE = masterUnitCode;
                }
                return res;
            }
            else
            {
                
                return null;
            }
        }

        public PersonalInfoDto GetByUsername(string username)
        {
            var pygDummy = IssuesData.GetPYGUDummyRelation();
            var resEntity = pisContext.PersonalInfo.Where(d => d.CODE == username && d.unit.DUMMY_RELATIONSHIP.StartsWith(pygDummy) && new string[] { "A", "I", "B", "J" }.Contains(d.WSTCODE)).FirstOrDefault();
            if (resEntity == null)
                return null;
            var res = resEntity.ToDTO();
            res.POSNAME = pisContext.positions.Where(d => d.poscode == res.POSCODE).Select(d => d.posname).FirstOrDefault();
            res.WorkgroupName = this.tpsContext.workgroup.Where(d => d.unitcode == res.UNITCODE).Select(d => d.WorkGroupName).FirstOrDefault();
            res.WorkgroupNameTh = this.tpsContext.workgroup.Where(d => d.unitcode == res.UNITCODE).Select(d => d.WorkGroupNameTH).FirstOrDefault();
            return res;
        }

        public string GetPositionByUserId(string usercode)
        {
            throw new NotImplementedException();
        }

        public void LogUserMock(string srcUser, string asUser, string ipAddress)
        {
            var entity = new activitylog()
            {
                AsUsercode = asUser,
                Description = ActivityLogType.MockUser.GetDescription(),
                LogTimeStamp = DateTime.Now,
                LogType = (int)ActivityLogType.MockUser,
                Usercode = srcUser,
                IPAddress = ipAddress
            };
            this.tpsContext.activitylog.Add(entity);
            this.tpsContext.SaveChanges();
        }
        public void LogUserApprove(string srcUser, string asUser, string billNo, string ipAddress)
        {
            var entity = new activitylog()
            {
                AsUsercode = asUser,
                Description = ActivityLogType.ApproveAsUser.GetDescription(),
                LogTimeStamp = DateTime.Now,
                LogType = (int)ActivityLogType.ApproveAsUser,
                Usercode = srcUser,
                BillNo = billNo,
                IPAddress = ipAddress
            };
            this.tpsContext.activitylog.Add(entity);
            this.tpsContext.SaveChanges();
        }
        public void LogUserCancel(string srcUser, string asUser, string billNo, string ipAddress)
        {
            var entity = new activitylog()
            {
                AsUsercode = asUser,
                Description = ActivityLogType.CancelAsUser.GetDescription(),
                LogTimeStamp = DateTime.Now,
                LogType = (int)ActivityLogType.CancelAsUser,
                Usercode = srcUser,
                BillNo = billNo,
                IPAddress = ipAddress
            };
            this.tpsContext.activitylog.Add(entity);
            this.tpsContext.SaveChanges();
        }
    }
}