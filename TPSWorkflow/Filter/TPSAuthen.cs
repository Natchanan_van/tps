﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using TPSWorkflow.DAL;

namespace System.Web.Mvc
{
    public class TPSAuthenAttribute:ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var sHelper = TPSWorkflow.Helpers.SessionHelper.GetInstance();
            if (!sHelper.IsAuthen)
            {
                filterContext.Result = new RedirectResult(WebHelper.Constants.AppPath + "Authen/Index?returnUrl=" + filterContext.HttpContext.Request.Url.ToString().Replace(WebHelper.Constants.AppPath, ""));
            }
        }

        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            //Log("OnActionExecuted", filterContext.RouteData);
        }

        public override void OnResultExecuting(ResultExecutingContext filterContext)
        {
            //Log("OnResultExecuting", filterContext.RouteData);
        }

        public override void OnResultExecuted(ResultExecutedContext filterContext)
        {
            //Log("OnResultExecuted", filterContext.RouteData);
        }


        private void Log(string methodName, RouteData routeData)
        {
            var controllerName = routeData.Values["controller"];
            var actionName = routeData.Values["action"];
            var message = String.Format("{0} controller:{1} action:{2}", methodName, controllerName, actionName);
            Debug.WriteLine(message, "Action Filter Log");
        }
    }

    public class TPSSystemAdminAttribute : TPSAuthenAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);
            var sHelper = TPSWorkflow.Helpers.SessionHelper.GetInstance();
            var userRights = this.resolver.GetUserRights(sHelper.userInfo);
            if (!userRights.HasFlag(UserRights.SystemAdministrator))
            {
                filterContext.Result = new RedirectToRouteResult(
                new RouteValueDictionary { { "controller", "Summary" }, { "action", "Index" } });
            }
        }

        protected IUsersManage resolver;
        public TPSSystemAdminAttribute(IUsersManage Resolver)
        {
            this.resolver = Resolver;
        }
    }
}