﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TPSWorkflow.DAL.DtoS;

namespace TPSWorkflow
{
    public enum MailViewMode
    {
        PDF = 0,
        Html = 1
    }
    public class MemoMailViewModel
    {
        public List<TpsAgenda> Agendas { get; set; }
        public TPSWorkflow.DAL.DtoS.mail_templateDto MailTemplate { get; set; }
        public int LastMeetingId { get; set; }
        public List<TPSWorkflow.DAL.DtoS.PersonalInfoDto> Attendees { get; set; }
        public MailViewMode Mode { get; set; }
        public meetingDto meetingInfo { get; set; }
        public string StartMeetingTimeString { get; set; }
        public string EndMeetingTimeString { get; set; }
        public bool IsUsePdf { get; set; }
        public string pdfLink { get; set; }
        public bool IsPreview { get; set; }
    }
}