﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using TPSWorkflow.DAL.DtoS;

namespace TPSWorkflow
{
    [DataContract]
    public class ApproverViewModel
    {
        [DataMember]
        public List<workgroupDto> WorkGroups { get; set; }
        [DataMember]
        public Dictionary<string, List<PersonalInfoDto>> MainApprover { get; set; }       
        
    }
}