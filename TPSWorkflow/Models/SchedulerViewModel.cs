﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using TPSWorkflow.DAL.DtoS;

namespace TPSWorkflow
{
    [DataContract]
    public class SchedulerViewModel
    {
        [DataMember]
        public List<groupDto> Groups { get; set; }
        [DataMember]
        public List<allowplantDto> AllPlants { get; set; }
        [DataMember]
        public List<DateTime> MeetingDates { get; set; }
    }
}