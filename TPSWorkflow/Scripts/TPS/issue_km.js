﻿/// <reference path="../jquery-2.0.3.js" />
/// <reference path="../jquery-2.0.3.intellisense.js" />


$(document).ready(function () {
    var baseUrl = $('body').attr('data-baseurl');
    (function Prepare() {

        $('.upload').uploadify({
            auto: false,
            buttonClass: 'tps',
            'swf': baseUrl + 'Plugins/uploadify/uploadify.swf',
            'uploader': baseUrl + 'Issues/SubmitIssuesKMFile',
            'onSWFReady': function () {
                if (!isKmAble && !isEditable) {
                    $('#btnAddFile').uploadify('disable', true);
                }
            },
            'onSelect': function (file) {
                $('.forupload').removeClass('hide');
            }
            // Put your options here
        });

        if (!isEditable) {
            $('textarea').attr('disabled', 'disabled');
            $('input[type="radio"]').prop('disabled', true);
            //$('#btnSubmit').attr('disabled', 'disabled');
            if (!isKmAble) {

            }
        }


        if ($('#rdStatusReMeeting').is(':checked')) {
            $('#btnChooseReMtgMonth').prop('disabled', false);
        }
        else {
            $('#btnChooseReMtgMonth').prop('disabled', true);
        }
        
    })();

    (function SetEvent() {
        var isSubmit = false;
        var toDelFileId = new Array();
        var queueError = [];
        var confirmCallback = function () {
            isSubmit = true;
            $('form').submit();
        }
        $('form').submit(function () {
            if (!isSubmit)
                return false;
            
        });

        $('#rdStatusFinal').change(function () {
            $('#btnChooseReMtgMonth').prop('disabled', true);
            $('#txtReMeetingMonth').val('');
            $('#numberSelectorPlaceHolder').addClass('hide');
            
        });
        $('#rdStatusReMeeting').change(function () {
            $('#btnChooseReMtgMonth').prop('disabled', false);
            $('#txtReMeetingMonth').val('1');
        });
        $('#btnChooseReMtgMonth').click(function () {
            var top = $('#txtReMeetingMonthDisplay').offset().top + 40;
            var left = $('#txtReMeetingMonthDisplay').offset().left;
            $('#numberSelectorPlaceHolder').css('top', top).css('left', left);
            $('#numberSelectorPlaceHolder').removeClass('hide');
        });
        $('#numberSelectorPlaceHolder .number').click(function () {
            $('#txtReMeetingMonth').val($(this).text().trim()).change();
            $('#numberSelectorPlaceHolder').addClass('hide');
        });
        $('#txtReMeetingMonth').change(function () {
            var val = $(this).val();
            if (val != '') {
                if (val == '1') {
                    $('#txtReMeetingMonthDisplay').val(val + ' MONTH(S)');
                }
                else {
                    $('#txtReMeetingMonthDisplay').val(val + ' MONTH(S)');
                }
            }
        });

        $('#btnAddFile').uploadify('settings', 'onUploadSuccess', function (file, responseData, isResponse) {
            var responseJson = JSON.parse(responseData);
            if (!responseJson.IsSuccess) {
                queueError.push({
                    file: file,
                    error: responseJson.ErrorMessage
                })
            }
        });

        $('#btnAddFile').uploadify('settings', 'onQueueComplete', function (queueData) {
            if (queueError.length > 0) {
                var errorDetail = "";
                for (i = 0; i < queueError.length; i++) {
                    errorDetail += "Filename: " + queueError[i].file.name + ", Error: " + queueError[i].error + "<br />";
                }
                messageBox.showbig('Upload error ' + queueError.length + 'file(s) <br /> ' + errorDetail + 
                    'Do you want to save change?', 'Techincal Problem System', msgBoxType.YesNo, function () {
                    isSubmit = true;
                    $('form').submit();
                }, function () {
                    return;
                })
            }
            else {
                isSubmit = true;
                $('form').submit();
            }
        });

        $('.btnDelFile').click(function () {
            var prmElemtn = $(this).parents('.fileLabel');
            if (prmElemtn.hasClass('toDelFile')) {
                prmElemtn.removeClass('toDelFile');
            }
            else {
                prmElemtn.addClass('toDelFile');
            }
        });

        $('#btnSubmit').click(function () {
            $('#btnAddFile').uploadify('settings', 'formData', {
                issueId: issueId,
                ASPSESSID: ASPSESSID,
                AUTHID: auth
            });
            
            jsonModel.summary.IssueId = issueId;
            jsonModel.summary.Suggestion = $('#txtSuggestion').val();
            jsonModel.summary.Finalize = $('#txtFinalize').val();
            jsonModel.summary.Status = $('.rdStatus:checked').val();
            var month = $('#txtReMeetingMonth').val();
            if (month == '')
                month = 0;
            if (month != null) {
                jsonModel.summary.ReSumForMonth = month;
            }
            else {
                jsonModel.summary.ReSumForMonth = 0;
            }

            $('#formjson').val(JSON.stringify(jsonModel));

            $('.toDelFile').each(function () {
                toDelFileId.push($(this).attr('data-KmId'));
            });
            $('#delFileIdsJson').val(JSON.stringify(toDelFileId));

            if ($('.uploadify-queue-item').size() > 0) {
                queueError = [];
                $('#btnAddFile').uploadify('upload', '*');
            }
            else {
                var groupId = jsonModel.GroupId;
                var onMonth = parseInt(jsonModel.summary.ReSumForMonth) + (new Date()).getMonth() + 1;
                if ($('#rdStatusReMeeting').is(':checked')) {
                    $.get(baseUrl + 'Issues/GetNextMeetingDate', { groupId: groupId, monthNo: onMonth }, function (res) {
                        if (res.IsSuccess) {
                            confirmSubmit("หมายเหตุ : วันที่ประชุมครั้งถัดไปคือ " + res.Result, confirmCallback);
                        }
                        else {
                            confirmSubmit(confirmCallback);
                        }
                    });
                }
                else {
                    confirmSubmit(confirmCallback);
                }
            }
        });
        $('#txtReMeetingMonth').change();
    })();
   
});