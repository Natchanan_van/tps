﻿/// <reference path="../jquery-2.0.3.js" />
/// <reference path="../jquery-2.0.3.intellisense.js" />
function blurBody() {
    $('#forBlur').removeClass('hide').addClass('show');
}

function blurAndWait() {
    $('#forBlur').removeClass('hide').addClass('show');
    $('#forBlur .loadingImg').removeClass('hide').addClass('show');
    var height = $('#forBlur .loadingImg').height();
    var width = $('#forBlur .loadingImg').width();
    $('#forBlur .loadingImg').css('top', ($(window).height() - height) / 2);
    $('#forBlur .loadingImg').css('left', ($(window).width() - width) / 2);
}

function unblurBody() {
    $('#forBlur').removeClass('show').addClass('hide');
    $('#forBlur .loadingImg').addClass('hide').removeClass('show');
}
function cToJsObjDate(cdate) {
    var date = new Date(parseInt(cdate.substr(6)));
    return date;
}
function cSharpToJsDate(cdate) {
    if (cdate == '/Date(-6847830000000)/') {
        return '';
    }
    var date = new Date(parseInt(cdate.substr(6)));
    var m_names = new Array("January", "February", "March",
        "April", "May", "June", "July", "August", "September",
        "October", "November", "December");
    var day_names = new Array("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday")

    return date.getDate() + ' ' + day_names[date.getDay()] + '<br />' + m_names[date.getMonth()].substr(0, 3) + ' ' + date.getFullYear();
}
function cSharpToJsDateTime(cdate) {
    if (cdate == '/Date(-6847830000000)/') {
        return '';
    }
    var date = new Date(parseInt(cdate.substr(6)));
    var m_names = new Array("January", "February", "March",
        "April", "May", "June", "July", "August", "September",
        "October", "November", "December");
    var day_names = new Array("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday")

    return date.getDate() + ' ' + m_names[date.getMonth()].substr(0, 3) + ' ' + date.getFullYear() + ', ' + date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds();
}
function jsDateToDatePicker(date) {
    return toDigit(date.getDate(), 2) + '-' + (toDigit(date.getMonth() + 1, 2)) + '-' + (date.getFullYear());
}
function toDigit(num, digit) {
    var digitinizer = "";
    for (i = 0; i < digit; i++)
        digitinizer += "0";
    return (digitinizer + num).slice(-digit);
}

function blurClickable(callback) {
    $('#forBlur').removeClass('hide').addClass('show');
    $(document).one('click', '#forBlur', function () {
        unblurBody();
        if (callback != null) {
            callback();
        }
    });
}

function refreshDocHeight() {
    $('.myContainer').height($(document).height());
    $('.midrow').height($('.myContainer').height() - $('.toprow').height() - $('.topMargin').height() - 1);
}

$(document).ready(function () {
    $('#mnuSettings').click(function () {
        var pos = $(this).offset();
        var width = $(this).width();
        
        $('#settingPlaceHolder').css('top', pos.top);
        $('#settingPlaceHolder').css('left', pos.left + width);
        $('#settingPlaceHolder').removeClass('hide');

        blurClickable(function () {
            $('#settingPlaceHolder').addClass('hide');
        });
    });
    if (navigator.appVersion.indexOf("MSIE 8.0") != -1) {
        $('body').addClass('ie8');
        $('.menu').addClass('ie8');
    }
    $('.myContainer').height($(document).height());
    $('.midrow').height($('.myContainer').height() - $('.toprow').height() - $('.topMargin').height() - 1);

    $('#txtLogin').keydown(function (e) {
        if (e.keyCode == 13) {
            e.preventDefault();
            e.stopPropagation();
            var baseUrl = $('body').attr('data-baseurl');
            $.post(baseUrl + 'System/AdminLogin', { mockuser: $(this).val() }, function (res) {
                if (res == 'Success') {
                    window.location.href = baseUrl;
                }
                else {
                    messageBox.show('Invalid User');
                }
            });
        }
    });
    setTimeout(function () {
        $('#txtLogin').removeAttr('disabled').removeProp('disabled');
    }, 1000);
});

if (typeof String.prototype.trim !== 'function') {
    String.prototype.trim = function () {
        return this.replace(/^\s+|\s+$/g, '');
    }
}