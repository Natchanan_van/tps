﻿/// <reference path="../jquery-2.0.3.js" />
/// <reference path="../jquery-2.0.3.intellisense.js" />

$(document).ready(function () {
    var baseUrl = $('body').attr('data-baseurl');
    (function Prepare() {

    })();

    (function SetEvent() {
        $(document).on('click', 'button.btnDelUser', function () {
            var userId = $(this).attr('data-userid');
            var thisDiv = $(this).parents('.userLine');
            messageBox.show('Are you sure you want to delete user "' + $(this).attr('data-userfname') + '"?', "Tps Workflow", 1, function () {
                $.post(baseUrl + 'Settings/DelUser', { userId: userId }, function (res) {
                    if (res.IsSuccess) {
                        messageBox.show('User deleted successfully', 'Techincal Problem System', 0, function () {
                            thisDiv.remove();
                        });
                    }
                    else {
                        messageBox.show('Error while delete user :' + res.ErrorMessage);
                    }
                });
            });
        });
    })();
});