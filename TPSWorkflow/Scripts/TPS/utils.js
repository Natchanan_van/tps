﻿String.prototype.format = function () {
    var formatted = this;
    for (var i = 0; i < arguments.length; i++) {
        var regexp = new RegExp('\\{' + i + '\\}', 'gi');
        formatted = formatted.replace(regexp, arguments[i]);
    }
    return formatted;
};

var confirmSubmit = function (noteString, callback) {
    var strToShow = "คุณต้องการบันทึกข้อมูลหรือไม่";
    if (typeof noteString == 'function') {
        callback = noteString;
        noteString = null;
    }
    if (noteString != null && noteString != '') {
        strToShow += "<br />" + noteString;
    }

    messageBox.show(strToShow, null, msgBoxType.YesNo, callback);
}