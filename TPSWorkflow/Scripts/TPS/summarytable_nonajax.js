﻿/// <reference path="../jquery-2.0.3.js" />
/// <reference path="../jquery-2.0.3.intellisense.js" />
$(document).ready(function () {
    var drawCount = 0;
    var isSearchOpen = false;
    var allCount = 0;
    var oTableObjects = new Array();
    var baseUrl = $('body').attr('data-baseurl');
    (function Prepare() {
        $('input.datePicker').datepicker({
            dateFormat: 'dd-M-yy', onSelect: function () {
                reDrawTablesAndCount();
            }
        });
        if (oldContent == null) {
            $('#txtEndDate').datepicker('setDate', new Date());
            var lastMonth = new Date();
            lastMonth.setMonth(lastMonth.getMonth() - 1);
            $('#txtStartDate').datepicker('setDate', lastMonth);
        }
        else {
            $('#txtSearchProblem').val(oldContent.SearchTerm);
            $('#sltGroupFilter').val(oldContent.groupSearch);
            var startDateSplit = oldContent.startDate.split('-');
            var startDate = new Date(startDateSplit[2], parseInt(startDateSplit[1]) - 1, startDateSplit[0]);
            var endDateSplit = oldContent.endDate.split('-');
            var endDate = new Date(endDateSplit[2], parseInt(endDateSplit[1]) - 1, endDateSplit[0]);
            $('#txtEndDate').datepicker('setDate', endDate);
            $('#txtStartDate').datepicker('setDate', startDate);
            $('#chbIsShowOnlyMe').prop('checked', oldContent.isShowOnlyMe == 'True');
        }

        $('.dt').each(function (i) {
            var status = $(this).attr('data-status');
            var thisDiv = $(this);
            var thisTblObject = $(this).dataTable({
                "aaSorting": [[6, "desc"]],
                sDom: 'rtp',
                "bPaginate": false,
                "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).addClass('issueRow')
                    .attr('data-issueId', aData.IssueId)
                    .attr('data-wgId', aData.WorkGroupId);

                    $(nRow).addClass('editable');

                },
                "fnDrawCallback": function (oSettings) {
                    //i = 0;
                    //while (oTableObjects[i] != this) {
                    //    i++
                    //}
                    //$('#countFor' + i).html(oSettings._iRecordsTotal);
                    //drawCount++;
                    //if (!isSearchOpen) {
                    //    var count = oSettings._iRecordsTotal;
                    //    if ($('#txtSearchProblem').val() != '' && count > 0) {
                    //        thisDiv.parents('.section').children('.sectionTitle').click();
                    //        isSearchOpen = true;
                    //    }
                    //}
                    refreshDocHeight();
                }
            });
            oTableObjects[status] = (thisTblObject);
        });

        // Hide All Section Except New
        $('.btnCollapseSection').addClass('minus');
        $('.sectionContent:not(.headsection)').hide();

        var tblCountSections = $('.tblCount');
        var isShownFirst = false;
        for (var i in tblCountSections) {
            var thisSection = tblCountSections[i];
            if (!isShownFirst && parseInt($(thisSection).text().trim()) > 0) {
                var toShow = '#' + $(thisSection).attr('data-section');
                $(toShow).find('.btnCollapseSection').removeClass('minus').addClass('plus');
                $(toShow).find('.sectionContent').show();
                isShownFirst = true;
            }
        }
        
        //$('#divNew .btnCollapseSection').removeClass('minus').addClass('plus');
        //$('#divNew .sectionContent').show();
        refreshDocHeight();

    })();

    function reDrawTablesAndCount(isShowTable) {
        var aoData = new Array();
        //aoData.push({ "name": "issuestatus", "value": status });
        aoData.push({ "name": "SearchTerm", "value": $('#txtSearchProblem').val() });
        aoData.push({ "name": "groupSearch", "value": $('#sltGroupFilter').val() });
        var x = new Date();
        var startDate = $('#txtStartDate').datepicker('getDate');
        var startDateStr = startDate.getDate() + '-' + (startDate.getMonth() + 1) + '-' + startDate.getFullYear();
        var endDate = $('#txtEndDate').datepicker('getDate');
        var endDateStr = endDate.getDate() + '-' + (endDate.getMonth() + 1) + '-' + endDate.getFullYear();
        aoData.push({ "name": "startDate", "value": startDateStr });
        aoData.push({ "name": "endDate", "value": endDateStr });
        aoData.push({ "name": "isOnlyMe", "value": $('#chbIsShowOnlyMe').is(':checked') });
        var queryString = "";
        $.each(aoData, function (index, val) {
            queryString += val.name + "=" + val.value + "&";
        });
        if (queryString != "")
            queryString = queryString.slice(0, -1);
        //alert(queryString);
        window.location.href = baseUrl + "Summary/Search?" + queryString;
    }

    (function setEvent() {
        $('.sectionTitle').click(function (e) {
            e.stopPropagation();
            var btnCollapse = $(this).find('.btnCollapseSection');
            if (btnCollapse.hasClass('minus')) {
                btnCollapse.parents('.section').find('.sectionContent').show();
                btnCollapse.removeClass('minus').addClass('plus');

                var neightBorSection = btnCollapse.parents('.section').siblings('.section:not(.headsection)');

                neightBorSection.find('.sectionContent').hide();
                neightBorSection.find('.btnCollapseSection').addClass('minus').removeClass('plus');
            }
            else {
                btnCollapse.parents('.section').find('.sectionContent').hide();
                btnCollapse.addClass('minus').removeClass('plus');
            }
            refreshDocHeight();
        });

        $('.btnCollapseSection').click(function () {

        });
        $('#txtSearchProblem').keydown(function (e) {
            if (e.keyCode == 13) {
                reDrawTablesAndCount(true);
            }
        });
        $('#sltGroupFilter').change(function () {
            reDrawTablesAndCount();
        });
        $('#chbIsShowOnlyMe').change(function () {
            reDrawTablesAndCount();
        });

        $(document).on('click', '.issueRow.editable', function () {
            window.open(baseUrl + "issues/editinfo/" + $(this).attr('data-issueId'), '_blank');
        });
        $('#btnScheduler').click(function () {
            blurAndWait();
            $.post(baseUrl + 'Issues/ForceBreakdownScheduler', {}, function (res) {
                if (res.IsSuccess) {
                    messageBox.show('Breakdown loaded successfully <br /> Total Issues Breakdown : ' + res.Result + ' issue(s)', null, null, function () {
                        window.location.reload();
                    });
                }
                else {
                    messageBox.show('Error loading breakdown : ' + res.ErrorMessage + "<br /> Please Contact Administrator", null, null, function () {
                        window.location.reload();
                    });
                }
                unblurBody();

            });
        });
        $('#btnRecover').click(function () {
            window.location.href = baseUrl + 'Summary/Recover';
        });
    })();
});