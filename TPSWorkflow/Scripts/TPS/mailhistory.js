﻿$(document).ready(function () {
    var refreshTable = function () {
        oTableObject.fnDraw();
    }

    var baseUrl = $('body').attr('data-baseurl');
    $('#sltLogTypeSelect option[value="0"]').prop('selected', true);
    $('input.datePicker').datepicker({
        dateFormat: 'dd-M-yy',
        onSelect: function () {
            oTableObject.fnDraw();
        }
    });
    $('#txtEndDate').datepicker('setDate', new Date());
    var lastMonth = new Date();
    lastMonth.setMonth(lastMonth.getMonth() - 1);
    $('#txtStartDate').datepicker('setDate', lastMonth);
    var oTableObject = $('#tblMailLog').dataTable({
        "bProcessing": true,
        "bServerSide": true,
        "sAjaxSource": baseUrl + 'Settings/MailHistoryData',
        "sServerMethod": "POST",

        "fnServerParams": function (aoData) {
            aoData.push({ "name": "mailLogType", "value": $('#sltLogTypeSelect').val() });
            aoData.push({ "name": "searchTerm", "value": $('#txtSearchMailLog').val() });
            var startDate = $('#txtStartDate').datepicker('getDate');
            var startDateStr = startDate.getDate() + '-' + (startDate.getMonth() + 1) + '-' + startDate.getFullYear();
            var endDate = $('#txtEndDate').datepicker('getDate');
            var endDateStr = endDate.getDate() + '-' + (endDate.getMonth() + 1) + '-' + endDate.getFullYear();
            aoData.push({ "name": "startDateTime", "value": startDateStr });
            aoData.push({ "name": "endDateTime", "value": endDateStr });
            aoData.push({ "name": "groupId", "value": $('#sltGroupVal').val() });
        },
        "aoColumns": [
            {
                "mData": "TimeStamp",
                mRender: function (data, type, full) {
                    return cSharpToJsDateTime(data);
                }
            },
            {
                "mData": "IsSent",
                mRender: function (data, type, full) {
                    return data ? "สำเร็จ" : "ล้มเหลว";
                }

            },
            {
                "mData": "MailTypeDescription",
                sClass: ''
            },
            {
                "mData": "SendToAddress"

            },
            { "mData": "SendCCAddress", sClass: '' },
            {
                "mData": "Error", sClass: ''
            },
            {
                "mData": "GroupId"
            }
        ],
        iDisplayLength: 10,
        sDom: 'rtip',
        "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            $(nRow).addClass('mailLogRow')
            .attr('data-mailLogId', aData.MaillogId);
            $(nRow).addClass('editable');
            $('td:eq(3)', nRow).addClass('limitTextshow');
            
            //$('td:eq(0)', nRow).attr('width', 'fit-content');
            //$('td:eq(2)', nRow).attr('width', '20%');
            //$('td:eq(3)', nRow).attr('width', '50%');

            //add class to all td //by Arnonthawajjana//
            //source knowledge
            //http://stackoverflow.com/questions/10983721/datatables-how-to-set-classes-to-table-row-cells-but-not-to-table-header-cells
            /*$('td', nRow).addClass( "damnit" );*/

        },
        "fnDrawCallback": function (oSettings) {
        }
    });
    
    // Events
    $('#sltLogTypeSelect, #sltGroupVal').change(refreshTable);
    $('#txtSearchMailLog').keydown(function (e) {
        if (e.keyCode == 13)
            refreshTable();
    });

    $(document).on('click', '.mailLogRow', function () {
        var data = oTableObject.fnGetData($(this)[0]);
        //prevent problem with minus hypen its try to new line Arnonthawajjana 
        //https://www.experts-exchange.com/questions/27960738/Javascript-jQuery-to-replace-non-breaking-hyphens-with-regular-hyphens.html
        //http://htmlarrows.com/punctuation/non-breaking-hyphen/
        //replace with u+02011 to \u2011
        var SendToAddress = data.SendToAddress;
        SendToAddress = SendToAddress.replace("-","\u2011");
        
        $('#cntTo').addClass('overLimitTextshowInNewLine').html(SendToAddress);
        $('#cntCC').html(data.SendCCAddress);
        // Replace mail image with server image
        data.MailContent = data.MailContent.replace('<img src="cid:Pic1">', '<img src="' + baseUrl + 'Content/Images/ptt_logo_doc.jpg" />');
        $('#cntBody').html(data.MailContent);
        var b = {
            OK: function () {
                $('#contentDlg').addClass('dialog').dialog('close');
            }
        };
        $('#contentDlg').removeClass('hide').dialog({
            modal: true,
            buttons: b,
            title: 'TPS',
            width: 1000,
            height: 800,
            resizable: true,
        });
    });
});