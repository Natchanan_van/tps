﻿$(document).ready(function () {
    $('input.datePicker').datepicker({
        dateFormat: 'dd-M-yy',
    });
    $('#txtEndDate').datepicker('setDate', new Date());
    var lastMonth = new Date();
    lastMonth.setMonth(lastMonth.getMonth() - 1);
    $('#txtStartDate').datepicker('setDate', lastMonth);
    $('#btnExportLog').click(function () {
        window.open(baseUrl + "System/GetActivitiesLog?start=" + $('#txtStartDate').val() + "&end=" + $('#txtEndDate').val());
    });
});