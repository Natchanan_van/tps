﻿/// <reference path="../jquery-2.0.3.js" />
/// <reference path="../jquery-2.0.3.intellisense.js" />

$(document).ready(function () {
    var datesDicts = {};
    var meetings = [];
    $.each(dates, function (key, val) {
        datesDicts[new Date(val)] = new Date(val);
    });
    var baseUrl = $('body').attr('data-baseurl');
    $('#txtSelectDate').datepicker({
        dateFormat: 'dd-M-yy',
        beforeShowDay: function (date) {
            var highlight = datesDicts[date];
            if (highlight) {
                return [true, "highlighted", ''];
            } else {
                return [true, '', ''];
            }
        }
    });



    // *** Events
    $('#btnSelectDate').click(function () {
        $('#txtSelectDate').datepicker('show');
    });
    $('#btnGetMeetings').click(function () {
        $('#meeting-Load').removeClass('hide');
        $.post(baseUrl + 'Memo/GetUnmemoMeetingByDate', { date: $('#txtSelectDate').val() }, function (response) {
            $('#meeting-Load').addClass('hide');
            if (!response.IsSuccess) {
                messageBox.show(response.ErrorMessage);
                return;
            }
            meetings = response.Result;
            if (response.Result.length == 0) {
                messageBox.show("No meeting occured in selected date");
                return;
            }
            $('#meetingSelector').removeClass('hide');
            $('#sltMeetings').empty();
            $.each(meetings, function (key, val) {
                var description = "กลุ่มงาน: " + val.GroupName + ", โรงแยกก๊าซ: " + val.Plants;
                $('<option value="' + val.MeetingId + '">' + description + '</option>').appendTo("#sltMeetings");
            });
        });
    });
    $('#btnCreateMemo').click(function () {
        var thisMeeting = null;
        for (var k in meetings) {
            if (meetings[k].MeetingId == $('#sltMeetings').val())
                thisMeeting = meetings[k];
        }
        if (thisMeeting == null)
            return;
        $('#lblMeetingDate').html($('#txtSelectDate').val());
        $('#lblWorkgroup').html(thisMeeting.GroupName);
        $('#lblPlant').html(thisMeeting.Plants);
        $('#lblAttendees ul').empty();
        $.each(thisMeeting.meeting_attendees, function (key, attendee) {
            $('<li>' + attendee.UserInfo.FULLNAMETH + '</li>').appendTo('#lblAttendees ul');
        });
        $('#detail-Load').removeClass('hide');
        $.post(baseUrl + 'Memo/GetAgendas', { id: thisMeeting.MeetingId }, function (response) {
            $('#detail-Load').addClass('hide');
            if (!response.IsSuccess) {
                messageBox.show(response.ErrorMessage);
                return;
            }
            $('#lblIssues ul').empty();
            //$.each(response.Result, function (index, agendas) {
            for (var index in response.Result) {
                var agendas = response.Result[index];
                var agendaNo = parseInt(index) + 1;
                var text = 'วาระการประชุมที่ ' + agendaNo + ' :' + agendas.Topic;
                var webElement = $('<li>' + text + '</li>');
                if (agendas.SubAgenda != null && agendas.SubAgenda.length > 0) {
                    var list = $('<ul></ul>');
                    //$.each(agendas.SubAgenda, function (subIndex, subAgenda) {
                    for (var subIndex in agendas.SubAgenda) {
                        var subAgenda = agendas.SubAgenda[subIndex];
                        var subAgendaNo = parseInt(subIndex) + 1;
                        var subText = 'วาระการประชุมที่ ' + agendaNo + '.' + subAgendaNo + ' :' + subAgenda.Topic;
                        $('<li>' + subText + '</li>').appendTo(list);
                    }
                    list.appendTo(webElement);
                }
                webElement.appendTo('#lblIssues #agendasUl');
            }
            $('#meetingDlg').removeClass('hide').dialog({
                modal: true,
                buttons: {
                    Yes: function () {
                        $.post(baseUrl + 'Memo/CreateMeeting', { id: $('#sltMeetings').val() }, function (response) {
                            if (!response.IsSuccess) {
                                messageBox.show(response.ErrorMessage);
                                return;
                            }
                            window.location.href = baseUrl + 'Memo/Edit/' + $('#sltMeetings').val();
                        });
                    },
                    No: function () {
                        $('#meetingDlg').addClass('dialog').dialog('close');
                    }
                },
                title: 'TPS',
                width: 1000,
                height: 800,
                resizable: true,
            });
        });
        
    });
});