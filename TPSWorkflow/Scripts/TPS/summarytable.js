﻿/// <reference path="../jquery-2.0.3.js" />
/// <reference path="../jquery-2.0.3.intellisense.js" />
$(document).ready(function () {
    var drawCount = 0;
    var isSearchOpen = false;
    var allCount = 0;
    var oTableObjects = new Array();
    var baseUrl = $('body').attr('data-baseurl');
    (function Prepare() {
        $('input.datePicker').datepicker({
            dateFormat: 'dd-M-yy', onSelect: function () {
                reDrawTablesAndCount();
            }});

        $('#txtEndDate').datepicker('setDate', new Date());
        var lastMonth = new Date();
        lastMonth.setMonth(lastMonth.getMonth() - 1);
        $('#txtStartDate').datepicker('setDate', lastMonth);

        $('.dt').each(function (i) {
            var status = $(this).attr('data-status');
            var thisDiv = $(this);
            var thisTblObject = $(this).dataTable({
                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": baseUrl + 'Summary/TableSource',
                "sServerMethod": "POST",
                "fnServerParams": function (aoData) {
                    aoData.push({ "name": "issuestatus", "value": status });
                    aoData.push({ "name": "issueSearchKey", "value": $('#txtSearchProblem').val() });
                    aoData.push({ "name": "groupSearch", "value": $('#sltGroupFilter').val() });
                    var x = new Date();
                    var startDate = $('#txtStartDate').datepicker('getDate');
                    var startDateStr = startDate.getDate() + '-' + (startDate.getMonth()+1) + '-' + startDate.getFullYear();
                    var endDate = $('#txtEndDate').datepicker('getDate');
                    var endDateStr = endDate.getDate() + '-' + (endDate.getMonth()+1) + '-' + endDate.getFullYear();
                    aoData.push({ "name": "startDate", "value": startDateStr });
                    aoData.push({ "name": "endDate", "value": endDateStr });
                },
                "aoColumns": [
                    {
                        "mData": "GroupName",
                        mRender: function (data, type, full) {
                            if (data != null) {
                                return "<div class='groupHighlight'>" + data + "</div>";
                            }
                            else {
                                return '';
                            }
                        }, sClass:'groupDisplay'
                    },
                    {
                        "mData": "ProblemDate", sClass: 'dateDisplay',
                        mRender: function (data, type, full) {
                            return cSharpToJsDate(data);
                        }

                    },
                    { "mData": "BillNo", sClass: '' },
                    {
                        "mData": "ProblemName",
                        mRender: function (data, type, full) {
                            return "<div class='topicsum' title='" + data + "'>" + data + "</div>";
                        }

                    },
                    { "mData": "PlantDisplayCode", sClass: '' },
                    {
                        "mData": "WorkGroupNameTh", sClass: ''
                    },
                    {
                        "mData": "ModifiedDate", sClass: '', mRender: function (data) {
                            return cSharpToJsDate(data);
                        }, sClass: 'dateDisplay'
                    }

                ],
                "aaSorting": [[6, "desc"]],
                sDom: 'rtp',
                "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).addClass('issueRow')
                    .attr('data-issueId', aData.IssueId)
                    .attr('data-wgId', aData.WorkGroupId);

                    $(nRow).addClass('editable');

                },
                "fnDrawCallback": function (oSettings) {
                    i = 0;
                    while (oTableObjects[i] != this) {
                        i++
                    }
                    $('#countFor' + i).html(oSettings._iRecordsTotal);
                    drawCount++;
                    if (!isSearchOpen) {
                        var count = oSettings._iRecordsTotal;
                        if ($('#txtSearchProblem').val() != '' && count > 0) {
                            thisDiv.parents('.section').children('.sectionTitle').click();
                            isSearchOpen = true;
                        }
                    }
                    refreshDocHeight();
                }
            });
            oTableObjects[status] = (thisTblObject);
        });

        // Hide All Section Except New
        $('.btnCollapseSection').addClass('minus');
        $('.sectionContent:not(.headsection)').hide();

        $('#divNew .btnCollapseSection').removeClass('minus').addClass('plus');
        $('#divNew .sectionContent').show();
        refreshDocHeight();
       
    })();

    function reDrawTablesAndCount(isShowTable) {
        drawCount = 0;
        isSearchOpen = false;
        allCount = 0;
        for (var key in oTableObjects) {
            oTableObjects[key].fnDraw();
            allCount++;
            //$(counters[i]).html(oTableObjects[key].fnSettings()._iRecordsTotal)
        }
        
        
    }
    
    (function setEvent() {
        $('.sectionTitle').click(function (e) {
            e.stopPropagation();
            var btnCollapse = $(this).find('.btnCollapseSection');
            if (btnCollapse.hasClass('minus')) {
                btnCollapse.parents('.section').find('.sectionContent').show();
                btnCollapse.removeClass('minus').addClass('plus');

                var neightBorSection = btnCollapse.parents('.section').siblings('.section:not(.headsection)');

                neightBorSection.find('.sectionContent').hide();
                neightBorSection.find('.btnCollapseSection').addClass('minus').removeClass('plus');
            }
            else {
                btnCollapse.parents('.section').find('.sectionContent').hide();
                btnCollapse.addClass('minus').removeClass('plus');
            }
            refreshDocHeight();
        });
        
        $('.btnCollapseSection').click(function () {
            
        });
        $('#txtSearchProblem').keydown(function (e) {
            if (e.keyCode == 13) {
                reDrawTablesAndCount(true);
            }
        });
        $('#sltGroupFilter').change(function () {
            reDrawTablesAndCount();
        });

        $(document).on('click', '.issueRow.editable', function () {
            window.location.href = baseUrl + "issues/editinfo/" + $(this).attr('data-issueId');
        });
        $('#btnScheduler').click(function () {
            blurAndWait();
            $.post(baseUrl + 'Issues/ForceBreakdownScheduler', {}, function (res) {
                if (res.IsSuccess) {
                    messageBox.show('Breakdown loaded successfully <br /> Total Issues Breakdown : ' + res.Result + ' issue(s)', null, null, function () {
                        window.location.reload();
                    });
                }
                else {
                    messageBox.show('Error loading breakdown : ' + res.ErrorMessage + "<br /> Please Contact Administrator", null, null, function () {
                        window.location.reload();
                    });
                }
                unblurBody();
                
            });
        });

    })();


});