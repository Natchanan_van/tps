﻿/// <reference path="../jquery-2.0.3.js" />
/// <reference path="../jquery-2.0.3.intellisense.js" />

$(document).ready(function () {
    var isSubmit = false;
    var ISSUE_ENUM = {
        New: 0,
        Waiting: 100,
        Approve: 200,
        Ready: 300,
        Pending: 400,
        ClosedWaitKM: 500,
        Closed: 600,
        Cancel: 700
    };
    
    (function prepare() {
        $('.select2').each(function () {
            $(this).select2({ containerCssClass: $(this).attr('class') });
        });
        $('#sltReponser').select2('val', jsonModel.InchargeUserCode);
        $('#sltUserFullName').select2('val', jsonModel.InchargeUserCode);
        $('.datepicker').datepicker({ dateFormat: 'dd-M-yy' });
        if (jsonModel.PendingDate != null) {
            $('.datepicker').datepicker('option', 'dateFormat', 'yy-mm-dd');
            $("#txtPendingDate").datepicker("setDate", jsonModel.PendingDate.substr(0, 10));
            $('.datepicker').datepicker('option', 'dateFormat', 'dd-M-yy');
        }
        //$('#txtPendingDate').spinner({
        //    max: 6,
        //    min: 1
        //});

        //if (jsonModel.Status != ISSUE_ENUM.New && jsonModel.Status != ISSUE_ENUM.Pending && jsonModel.Status != ISSUE_ENUM.Waiting) {
        if (!isEditable) {
            $('input, textarea').attr('disabled', true);
            $('#sltReponser').select2('readonly', true);
            $('#sltUserFullName').select2('readonly', true);
            $('#btnChoosePendingMonth').attr('disabled', true);
            //$('#txtPendingDate').spinner('option', 'disabled', true);
        }
        else {
            $('textarea').attr('disabled', true);
            $('#sltReponser').select2('enable', false);
            $('#sltUserFullName').select2('enable', false);
            //$('#txtPendingDate').spinner('option', 'disabled', true);
            
        }
        
        $('input[type="radio"][name="sltApprove"]:checked').siblings('label').addClass('chosen');
    })();
    
    (function setEvent() {
        $('form').submit(function () {
            if (!isSubmit)
                return false;
        });
        //$('#txtCancelDetail').keydown(function () {
        //    $('#rdCancel').attr('checked', true);
        //});
        //$('#txtPendingDate').change(function () {
        //    $('#rdPending').attr('checked', true);
        //});
        $('#sltUserFullName').change(function () {
           
            $('#sltReponser').select2('val', $(this).select2('val'));
        });
        $('#sltReponser').change(function () {
           
            $('#sltUserFullName').select2('val', $(this).select2('val'));
        });
        $('#rdApprove').change(function () {
            if ($(this).is(':checked')) {
                $('#sltReponser').select2('enable', true);
                $('#sltUserFullName').select2('enable', true);
                $('#btnChoosePendingMonth').prop('disabled', true);
                $('#txtCancelDetail').prop('disabled', true);
            }
        });
        $('#rdPending').change(function () {
            if ($(this).is(':checked')) {
                $('#sltReponser').select2('enable', false);
                $('#sltUserFullName').select2('enable', false);
                $('#sltReponser').select2('val', '');
                $('#sltUserFullName').select2('val', '');
                $('#btnChoosePendingMonth').prop('disabled', false);
                $('#txtCancelDetail').prop('disabled', true);
            }
        });
        $('#rdCancel').change(function () {
            if ($(this).is(':checked')) {
                $('#sltReponser').select2('enable', false);
                $('#sltUserFullName').select2('enable', false);
                $('#sltReponser').select2('val', '');
                $('#sltUserFullName').select2('val', '');
                //$('#txtPendingDate').spinner('disable');
                $('#txtCancelDetail').prop('disabled', false);
                $('#btnChoosePendingMonth').prop('disabled', true);
            }
        });
        if (isEditable) {
            $('input[type="radio"]').change();
        }
        $('#btnSubmit').click(function () {
            if ($('input[type="radio"]:checked').length == 0) {
                messageBox.show('กรุณาเลือกสถานะที่ต้องการ');
                return;
            }

            if ($('#rdApprove').is(':checked')) {
                jsonModel.InchargeUserCode = $('#sltReponser').select2('val');
                if (jsonModel.InchargeUserCode == null || jsonModel.InchargeUserCode == '') {
                    messageBox.show('Please select in-charge employee');
                    return;
                }
                jsonModel.Status = ISSUE_ENUM.Approve;
                var mes = "Mail To Send :";
                mes += $('option[value="' + jsonModel.InchargeUserCode + '"][data-email]').attr('data-email');
                $('#json').val(JSON.stringify(jsonModel));
                confirmSubmit(mes, function () {
                    isSubmit = true;
                    $('form').submit();
                });
                return;
            }
            else if ($('#rdCancel').is(':checked')) {
                jsonModel.Status = ISSUE_ENUM.Cancel;
                jsonModel.CancelDetail = $('#txtCancelDetail').val();
                if (jsonModel.CancelDetail == '') {
                    messageBox.show('กรุณาระบุเหตุผลที่ยกเลิก');
                    return;
                }
            }
            else if ($('#rdPending').is(':checked')) {
                jsonModel.Status = ISSUE_ENUM.Pending;
                //$('.datepicker').datepicker('option', 'dateFormat', 'mm-dd-yy');
                jsonModel.PendingMonth = new Date().getMonth() + parseInt($('#txtPendingDate').val()) + 1;
                //$('.datepicker').datepicker('option', 'dateFormat', 'dd-mm-dd');
                if (jsonModel.PendingMonth == '' || !jsonModel.PendingMonth) {
                    messageBox.show('กรุณาระบุจำนวนเดือนที่เลื่อน');
                    return;
                }
            }

            $('#json').val(JSON.stringify(jsonModel));

            confirmSubmit(function () {
                isSubmit = true;
                $('form').submit();
            });
            
        });
        $('#btnChoosePendingMonth').click(function () {
            var top = $('#txtPendingDateDisplay').offset().top + 40;
            var left = $('#txtPendingDateDisplay').offset().left;
            $('#numberSelectorPlaceHolder').css('top', top).css('left', left).css('width', 200);
            $('#numberSelectorPlaceHolder').removeClass('hide');
        });
        $('#numberSelectorPlaceHolder .number').click(function () {
            $('#txtPendingDate').val($(this).text().trim()).change();
            $('#numberSelectorPlaceHolder').addClass('hide');

        });
        $('#txtPendingDate').change(function () {
            var curMonth = $(this).val();
            if (curMonth != '') {
                if (curMonth == '1') {
                    $('#txtPendingDateDisplay').val(curMonth + ' MONTH(S)');
                }
                else {
                    $('#txtPendingDateDisplay').val(curMonth + ' MONTH(S)');
                }
                
            }
            else {
                $('#txtPendingDateDisplay').val('');
            }
        });
        $('#txtPendingDate').change();
        $('#btnDelete').click(function () {
            var baseUrl = rootUrl();
            inputBox.show('กรุณาป้อนเหตุผลที่ยกเลิก : ', null, function (text) {
                $.post(baseUrl + 'Issues/Delete', { issueId: $('#issueid').val(), reason: text }, function (res) {
                    if (res.IsSuccess) {
                        messageBox.show('บันทึกสำเร็จเรียบร้อยแล้วครับ', null, null, function () {
                            window.location.href = baseUrl + "Summary";
                        });
                    }
                    else {
                        messageBox.show('มีข้อผิดพลาด : ' + res.ErrorMessage);
                    }
                });
            });
        });
    })();

    
});