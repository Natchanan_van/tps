﻿/// <reference path="../jquery-2.0.3.js" />
/// <reference path="../jquery-2.0.3.intellisense.js" />

$(document).ready(function () {
    var baseUrl = $('body').attr('data-baseurl');
    (function Prepare() {
        $('.select2').select2({ width: 200 });
    })();

    (function SetEvent() {
        $('#sltWorkgroup').change(function () {
            var thisUnitCode = $(this).val();
            var thisUsers = jsonModel.MainApprover[thisUnitCode];
            var apiResult = {
                IsSuccess: true,
                Result: thisUsers,
                ErrorMessage: null
            };
            approverList(apiResult, false, $('#mainApproversPlaceholder'));
            $('#mainApproversPlaceholder .isApproverSelect').hide();

            $('#approversPlaceholder').empty().addClass('loading');
            $.post(baseUrl + 'Settings/GetApproverList', { unitCode: thisUnitCode }, function (res) {
                approverList(res, true, '#approversPlaceholder');
                $('#approversPlaceholder .isApproverSelect').hide();
                //$('#approversPlaceholder div.forhightlight').addClass('hightlight');
            });
        });
        $('#sltWorkGroupPresenter').change(function () {
            var thisUnitCode = $(this).val();
            $('#presenterPlaceholder').empty().addClass('loading');
            $.post(baseUrl + 'Settings/GetPresenterList', { unitCode: thisUnitCode }, function (res) {
                approverList(res, true, '#presenterPlaceholder');
                $('#presenterPlaceholder .line.approverline .btnDelApprover').removeClass('btnDelApprover').addClass('btnDelPresenter');
                $('#presenterPlaceholder .isApproverSelect').hide();
                //$('#approversPlaceholder div.forhightlight').addClass('hightlight');
            });
        });
        

        function approverList(res, isShowDel, target) {
            if (isShowDel == null)
                isShowDel = true;
            if (!res.IsSuccess) {
                messageBox.show('Error : ' + res.ErrorMessage);
                return;
            }
            $(target).empty().removeClass('loading');
            for (var key in res.Result) {
                var thisUser = res.Result[key];
                var thisDiv = $('.line.approverline.prototype').clone().attr('data-usercode', thisUser.CODE);
                thisDiv.removeClass('prototype');
                thisDiv.find('.usercode').text(thisUser.CODE);
                thisDiv.find('.userfullname').text(thisUser.INAME + thisUser.FNAME + ' ' + thisUser.LNAME);
                thisDiv.find('.userunit').width(75).html(thisUser.unit.unitabbr);
                if (!isShowDel) {
                    thisDiv.find('.btnDelApprover').hide();
                    
                }
                else {
                    thisDiv.find('.btnDelApprover').attr('data-usercode', thisUser.CODE);
                }
                thisDiv.find('.isApproverSelect').attr('checked', false).attr('data-usercode', thisUser.CODE);
                thisDiv.appendTo(target);
                
            }
        }
        
        

        $('#btnAdd').click(function () {
            blurBody();
            $('#addType').val('approver');
            var thisUnitCode = $('#sltWorkgroup').val();
            $('#addApproverDiv').appendTo('#mainContainer');
            $('#addApproverPlaceHolder').addClass('loading');
            $('#txtSearchUser').val('');
            $.post(baseUrl + 'Settings/GetAllUsersListPresenter', { unitCode: thisUnitCode }, function (res) {
                var newRes = new Array();
                var nowCode = new Array();
                $('#approversPlaceholder .approverline').each(function() {
                    nowCode.push($(this).attr('data-usercode'));
                });
                
                if (res.Result != null) {
                    for (var key in res.Result) {
                        if (($.inArray(res.Result[key].CODE, nowCode, 0) < 0)) {
                            newRes.push(res.Result[key]);
                        }
                    }
                    res.Result = newRes;
                }
                approverList(res, false, '#addApproverPlaceHolder');
                $('#addApproverPlaceHolder div.forhightlight').addClass('hightlight');
            });
        });

        $('#btnAddPresenter').click(function () {
            blurBody();
            $('#addType').val('presenter');
            var thisUnitCode = $('#sltWorkGroupPresenter').val();
            $('#addApproverDiv').appendTo('#mainContainer');
            $('#addApproverPlaceHolder').empty().addClass('loading');
            $('#txtSearchUser').val('');
            $.post(baseUrl + 'Settings/GetAllUsersListPresenter', { unitCode: thisUnitCode }, function (res) {
                var newRes = new Array();
                var nowCode = new Array();
                $('#presenterPlaceholder .approverline').each(function () {
                    nowCode.push($(this).attr('data-usercode'));
                });

                if (res.Result != null) {
                    for (var key in res.Result) {
                        if (($.inArray(res.Result[key].CODE, nowCode, 0) < 0)) {
                            newRes.push(res.Result[key]);
                        }
                    }
                    res.Result = newRes;
                }
                approverList(res, false, '#addApproverPlaceHolder');
                $('#addApproverPlaceHolder div.forhightlight').addClass('hightlight');
            });
        });

        $('#txtSearchUser').keydown(function (e) {
            if (e.keyCode == 13) {
                if ($('#addType').val() == 'approver') {
                    var thisUnitCode = $('#sltWorkgroup').val();
                    $.post(baseUrl + 'Settings/GetAllUsersListApprover', { unitCode: thisUnitCode, searchTerm: $(this).val() }, function (res) {
                        approverList(res, false, '#addApproverPlaceHolder');
                    });
                }
                if ($('#addType').val() == 'presenter') {
                    var thisUnitCode = $('#sltWorkGroupPresenter').val();
                    $.post(baseUrl + 'Settings/GetAllUsersListPresenter', { unitCode: thisUnitCode, searchTerm: $(this).val() }, function (res) {
                        approverList(res, false, '#addApproverPlaceHolder');
                    });
                }
            }
        });

        $('#btnAddConfirm').click(function () {
            var codeArray = new Array();
            $('.isApproverSelect:checked').each(function () {
                codeArray.push($(this).attr('data-usercode'));
            });
            if ($('#addType').val() == 'approver') {
                $.post(baseUrl + 'Settings/AddApprovers', { userCodes: JSON.stringify(codeArray), unitcode: $('#sltWorkgroup').val() }, function (res) {
                    if (!res.IsSuccess) {
                        messageBox.show('Error : ' + res.ErrorMessage);
                        return; a
                    }
                    $('#addApproverDiv').appendTo('#dlgPlaceHolder');
                    unblurBody();
                    $('#sltWorkgroup').change();
                });
            }
            else {
                $.post(baseUrl + 'Settings/AddPresenter', { userCodes: JSON.stringify(codeArray), unitcode: $('#sltWorkGroupPresenter').val() }, function (res) {
                    if (!res.IsSuccess) {
                        messageBox.show('Error : ' + res.ErrorMessage);
                        return; a
                    }
                    $('#addApproverDiv').appendTo('#dlgPlaceHolder');
                    unblurBody();
                    $('#sltWorkGroupPresenter').change();
                });
            }
            
        });

        $('#btnCloseAdd').click(function () {
            $('#addApproverDiv').appendTo('#dlgPlaceHolder');
            unblurBody();
        });

        $(document).on('click','.btnDelApprover',function() {
            var userCode = $(this).attr('data-usercode');
            var unitcode = $('#sltWorkgroup').val();
            messageBox.show('Are you sure to delete user "' + userCode + '" ?', 'Techincal Problem System', 1, function () {
                $.post(baseUrl + 'Settings/DelApprover', { usercode: userCode, unitcode: unitcode }, function (res) {
                    $('#sltWorkgroup').change();
                });
            });
        });
        $(document).on('click', '.btnDelPresenter', function () {
            var userCode = $(this).attr('data-usercode');
            var unitcode = $('#sltWorkGroupPresenter').val();
            messageBox.show('Are you sure to delete user "' + userCode + '" ?', 'Techincal Problem System', 1, function () {
                $.post(baseUrl + 'Settings/DelPresenter', { usercode: userCode, unitcode: unitcode }, function (res) {
                    $('#sltWorkGroupPresenter').change();
                });
            });
        });
    })();

    $('#sltWorkgroup').change();
    $('#sltWorkGroupPresenter').change();
});