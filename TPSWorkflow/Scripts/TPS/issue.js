﻿/// <reference path="../jquery-2.0.3.js" />
/// <reference path="../jquery-2.0.3.intellisense.js" />
$(document).ready(function () {
    var baseUrl = $('body').attr('data-baseurl');
    var billNo = '';
    var isSubmit = false;
    function updateMachineName(machineNo) {
        $('#isLoadPlant').addClass('loading');
        $.post(baseUrl + 'Issues/GetMachineFromId', { machineNo: machineNo }, function (res) {
            $('#isLoadPlant').removeClass('loading');
            $('#txtMachineFullName').val(res);
        });
    }
    (function prePare() {
        
        $('.datepicker').datepicker({ dateFormat: 'dd-M-yy' });
        if (jsonModel != null) {
            $('.datepicker').datepicker('option', 'dateFormat', 'yy-mm-dd');
            if (jsonModel.ProblemDate != null && jsonModel.ProblemDate.substr(0, 10) != '1753-01-01')
                $("#problemDate").datepicker("setDate", jsonModel.ProblemDate.substr(0, 10));
            if (jsonModel.RepairDate != null) 
                $("#fixDate").datepicker("setDate", jsonModel.RepairDate.substr(0, 10));
            if (jsonModel.RepairDueDate != null)
                $("#dueDate").datepicker("setDate", jsonModel.RepairDueDate.substr(0, 10));
            $('.datepicker').datepicker('option', 'dateFormat', 'dd-M-yy');
        }
        else {
            $('input:not(.billNoDigit), select').attr('disabled', true);
        }
        
        

        
    })();

    //Events Handler
    (function setEvent() {
        //$('#sltMachineId').change(function () {
        //    $('#txtMachineFullName').val($(this).find('option:selected').attr('data-machinename'));
        //});
        $('#btnSubmitBillNo').click(function () {
            billNo = '';
            $('.billNoDigit').each(function () {
                if ($(this).val().trim() != '') {
                    billNo += $(this).val();
                }
            });
            
            while (billNo.length < 10) {
                billNo = '0' + billNo;
            }
            //console.log(billNo);
            $('.billNoDigit').each(function (i) {
                $(this).val(billNo[i]);
            });
            $.post(baseUrl + 'issues/getBillDataFromSap', { billNo: billNo }, function (res) {

                if (!res.IsSuccess) {
                    messageBox.show('Error : ' + res.ErrorMessage);
                    return;
                }
                $('input, select').attr('disabled', false);
                $('.disabled').attr('disabled', true);
                var billData = res.Result;
                $('#txtRepairNo').val(billData.RepairBillNo);
                $('#sltPlantId').val(billData.plantshortcode);
                $('#txtPlantCode').val(billData.plantcode);
                
                //if ($('#sltMachineId option[value="' + billData.MachineNo + '"]').length > 0) {
                //    $('#sltMachineId').val(billData.MachineNo).change();
                //}
                //else if (billData.MachineNo != null) {
                //    var newOption = $('<option data-machinename="{0}" value="{0}">{0}</option>'.format(billData.MachineNo));
                //    newOption.appendTo($('#sltMachineId'));
                //    $('#sltMachineId').val(billData.MachineNo).change();
                //}
                    
                if (billData.MachineNo == null)
                    billData.MachineNo = '';
                $('#txtMachineNo').val(billData.MachineNo);
                
                $('#txtMachineFullName').val(billData.MachineFullName);
                
                $('#sltWorkGroupId').val(billData.unitcode);
                $("#problemDate").datepicker("setDate", cToJsObjDate(billData.ProblemDate));
                $('#txtProblemName').val(billData.ProblemName);
                $('#txtProblemDetail').val(billData.ProblemDetail);
                $("#fixDate").datepicker("setDate", billData.RepairDate);
                $("#dueDate").datepicker("setDate", billData.RepairDueDate);

            });
            //$.post(baseUrl + '/issue/getSapIssue', { billNo: billNo }, function (res) {

            //});

        });
        $('.billNoDigit').keydown(function (e) {
            //e.preventDefault();
            if (e.keyCode == 8) {
                if ($(this).val() != '')
                    $(this).val('');
                else
                    $(this).prev('input').focus().val('');
                return;
            }
        });
        $('.billNoDigit').keypress(function (e) {
            e.preventDefault();
            $(this).val('');
            var charCode = e.which || e.charCode || e.keyCode || 0;
            var charStr = String.fromCharCode(charCode);
            if (/(\w|\d)/.test(charStr)) {
                $(this).val(charStr);
                $(this).next('input').focus();
            }
        });
        $('.billNoDigit').on('paste', function (jqEvent) {
            jqEvent.preventDefault();
            var e = jqEvent.originalEvent;
            var pastedText = undefined;
            if (window.clipboardData && window.clipboardData.getData) { // IE
                pastedText = window.clipboardData.getData('Text');
            } else if (e.clipboardData && e.clipboardData.getData) {
                pastedText = e.clipboardData.getData('text/plain');
            }
            if (pastedText.length > 10)
                return;
            $('.billNoDigit').each(function (index) {
                $(this).val(pastedText[index]);
            });
        });
        $('form').submit(function () {
            if (!isSubmit)
                return false;
        });
        $('#btnSubmit').click(function () {
            //Validate
            var isDigitComplete = true;
            $('.billNoDigit').each(function () {
                if ($(this).val() == null || $(this).val() == '')
                    isDigitComplete = false;
            });
            if (!isDigitComplete) {
                messageBox.show('กรุณาระบุเลขที่ใบแจ้งซ่อมให้ครบถ้วน')
                return;
            }
            if ($('#txtProblemName').val() == '') {
                messageBox.show('กรุณาระบุชื่อปัญหา')
                return;
            }
            if ($('#txtProblemDetail').val() == '') {
                messageBox.show('กรุณาระบุรายละเอียดปัญหา')
                return;
            }
            if ($('#fixDate').val() == '') {
                messageBox.show('กรุณาระบุวันที่เริ่มแก้ไข')
                return;
            }
            if ($('#dueDate').val() == '') {
                messageBox.show('กรุณาระบุวันที่ดำเนินการเสร็จ')
                return;
            }
            if ($('#sltWorkGroupId').val() == null) {
                messageBox.show('กรุณาระบุกลุ่มงานรับผิดชอบ')
                return;
            }
            if ($('#txtMachineFullName').val() == '') {
                messageBox.show('กรุณาระบุเครื่องจักร')
                return;
            }
            //if (!parseInt($('#txtMachineNo').val())) {
            //    messageBox.show('กรุณาระบุหมายเลขเครื่องจักรเป็นตัวเลข')
            //    return;
            //}
            
            if ($('#sltPlantId').val() == null) {
                messageBox.show('กรุณาระบุโรงแยกก๊าซ')
                return;
            }

            var baseUrl = $('body').attr('data-baseurl');
            var unitcode = $('#sltWorkGroupId').val();
            var issueId = $('#issueid').val();
            if (issueId == -1) {
                $.when($.ajax(baseUrl + "Issues/ApproveWantedMail", { data: { unitcode: unitcode } }), $.ajax(baseUrl + "Issues/CCMail", { data: { unitcode: unitcode } })).done(function (res1, res2) {
                    var mes = "หมายเหตุ: จะมีการแจ้งเตือนอีเมล์ไปที่ <br/>";
                    mes += "Mail : " + res1[0].join(';');
                    mes += " <br />";
                    mes += "CC : " + res2[0].join(';');
                    confirmSubmit(mes, function () {
                        isSubmit = true;
                        $('input').attr('disabled', false);
                        $('form').submit();
                    });
                });
            }
            else {
                confirmSubmit(function () {
                    isSubmit = true;
                    $('input').attr('disabled', false);
                    $('form').submit();
                });
            }
            
           
        });
        $('#calFixDate').click(function () {
            if (!$('#fixDate').is(':disabled')) {
                $('#fixDate').datepicker('show');
            }
        });
        $('#calDueDate').click(function () {
            if (!$('#dueDate').is(':disabled')) {
                $('#dueDate').datepicker('show');
            }
        })
        $('#calProblemDate').click(function () {
            if (!$('#problemDate').is(':disabled')) {
                $('#problemDate').datepicker('show');
            }
        });
        $('#btnReApprove').click(function () {
            var issueId = $('#issueid').val();
            var unitcode = $('#sltWorkGroupId').val();
            $.post(baseUrl + 'Issues/ReApprove', { issueid: issueId }, function (res) {
                if (res.IsSuccess) {
                    $.when($.ajax(baseUrl + "Issues/ApproveWantedMail", { data: { unitcode: unitcode } }), $.ajax(baseUrl + "Issues/CCMail", { data: { unitcode: unitcode } })).done(function (res1, res2) {

                        var mes = "Mail : " + res1[0].join(';');
                        mes += " <br />";
                        mes += "CC : " + res2[0].join(';');

                        messageBox.showbig(mes, null, 0, function () {
                            isSubmit = true;
                            $('input').attr('disabled', false);
                            $('form').submit();
                        });

                    });
                    //messageBox.show('ทำการเปลี่ยนสถานะเรียบร้อยแล้วครับ', null, null, function () {
                    //    window.location.href = baseUrl + 'Issues/Approve/' + issueId;
                    //});
                }
                else {
                    messageBox.show('Error :' + res.ErrorMessage);
                }
            });
        });
        $('#txtMachineNo').change(function () {
            var no = $(this).val();
            updateMachineName(no);
        });
        $('#btnUnbreakDown').click(function () {
            inputBox.show('กรุณาป้อนเหตุผลที่ยกเลิก : ', null, function (text) {
                $.post(baseUrl + 'Issues/Unbreakdown', { issueId: $('#issueid').val(), reason: text }, function (res) {
                    if (res.IsSuccess) {
                        messageBox.show('บันทึกสำเร็จเรียบร้อยแล้วครับ', null, null, function () {
                            window.location.href = baseUrl + "Summary";
                        });
                    }
                    else {
                        messageBox.show('มีข้อผิดพลาด : ' + res.ErrorMessage);
                    }
                });
            });
        });
    })();
});