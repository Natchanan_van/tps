﻿/// <reference path="../jquery-2.0.3.js" />
/// <reference path="../jquery-2.0.3.intellisense.js" />

$(document).ready(function () {
    var isSubmit = false;
    var baseUrl = $('body').attr('data-baseurl');
    var attendeeList = new Array();
    var toAddList = new Array();
    var isDirty = false;
    var isEventPropagate = false;
    function renderSubMemo(subMemoModel) {
        var thisDiv = $('#containerPrototype').clone().attr('id','');
        thisDiv.find('.mainNo').text(subMemoModel.MainNumber);
        thisDiv.find('.subNo').text(subMemoModel.SubNumber);
        thisDiv.find('.txtMemo').val(subMemoModel.Text);
        if (subMemoModel.MainNumber == 1) {
            thisDiv.appendTo('#subContent1');
        }
        else if (subMemoModel.MainNumber == 2) {
            thisDiv.appendTo('#subContent2');
        }
        else if (subMemoModel.MainNumber >= 4) {
            thisDiv.appendTo('#subContent3');
        }
        
    }
    function drawMailerAsync() {
        $('#addMailDiv').addClass('loading');
        var groupId = jsonModel.GroupId;
        var searchTerm = $('#txtSearchUser').val();
        $.post(baseUrl + 'Settings/GetAllUsers', { searchTerm: searchTerm }, function (res) {
            $('#addMailDiv').removeClass('loading');
            if (!res.IsSuccess) {
                messageBox.show('Error loading mail list : ' + res.ErrorMessage);
                return;
            }
            $('#addMailPlaceHolder').empty();
            for (i = 0; i < res.Result.length; i++) {
                var thisUser = res.Result[i];
                var thisDiv = userByPrototype(thisUser, toAddList);
                thisDiv.appendTo('#addMailPlaceHolder');
            }
        });

    }
    function renderMemo(memoModel) {
        if (memoModel.SubMemo != null) {
            var memoFor1 = [];
            var memoFor2 = [];
            var memoForOther = [];
            $.each(memoModel.SubMemo, function (index, val) {
                renderSubMemo(val);
            });
        }
    }
    function gatherMemo(div) {
        var res = new Array();
        $(div).find('.memocontainer').each(function () {

            res.push({
                MainNumber: parseInt($(this).find('.mainNo').text()),
                SubNumber: parseInt($(this).find('.subNo').text()),
                Text: $(this).find('.txtMemo').val()
            });
        });
        return res;
    }
    function updateAttd() {
        var attdCount = $('#subContent0 .mailline').length;
        $('#attdNo').text(attdCount);
        refreshDocHeight();
    }
    function renderUsers(users) {
        attendeeList = new Array();
        $.each(users, function (index, val) {
            if (val.UserInfo != null) {
                var thisMail = $('.mailline.prototype').clone().removeClass('prototype');
                thisMail.find('.usercode').text(val.UserInfo.CODE);
                thisMail.find('.userfullname').text(val.UserInfo.FULLNAMETH);
                thisMail.attr('data-usercode', val.UserInfo.CODE);
                thisMail.find('input.chbIsAttended').prop('checked', val.Attended == 0);
                thisMail.find('input.chbIsAttended').prop('disabled', !isEditable);
                if (!isEditable) {
                    thisMail.find('.btnDelMail').remove();
                }
                thisMail.appendTo('#subContent0');
                

                attendeeList.push(val.UserInfo);
            }
            
        });
        updateAttd();
        
    }
    function userByPrototype(val,selected) {
        var thisMail = $('.mailline.prototype').clone().removeClass('prototype');
        thisMail.find('.chbIsAdd').attr('data-usercode', val.CODE);
        thisMail.find('.usercode').text(val.CODE);
        thisMail.find('.userfullname').text(val.FULLNAMETH);
        thisMail.find('.btnTps').addClass('hide');
        thisMail.attr('data-usercode', val.CODE);
        thisMail.find('.userUnit').text(val.unit.unitabbr);
        if (selected != null) {
            if ($.inArray(val.CODE, selected) >= 0) {
                thisMail.find('.chbIsAdd').prop('checked', true);
            }
        }
        //thisMail.appendTo('#subContent0')
        return thisMail;
    }
    function saveMemo(isPreview, callback) {
        jsonModel.memo.SubMemo = new Array();
        submemo1 = gatherMemo('#subContent1');
        submemo2 = gatherMemo('#subContent2');
        submemo3 = gatherMemo('#subContent3');
        jsonModel.memo.SubMemo = jsonModel.memo.SubMemo.concat(submemo1);
        //jsonModel.memo.SubMemo = jsonModel.memo.SubMemo.concat(submemo2);
        jsonModel.memo.SubMemo = jsonModel.memo.SubMemo.concat(submemo3);
        jsonModel.memo.Memo2 = $('#txtMemo2').val();
        jsonModel.meeting_attendees = new Array();
        $('#subContent0 .mailline').each(function (index, val) {
            var thisAttd = {
                AttendeeCode: $(val).find('.usercode').text(),
                Attended: $(val).find('.chbIsAttended').is(':checked') ? 0 : -100
            };
            jsonModel.meeting_attendees.push(thisAttd);
        });
        if (jsonModel.meeting_attendees.length == 0) {
            messageBox.show('You must have at least one attendee');
            return;
        }
        console.log(JSON.stringify(jsonModel.memo));

        $.post(baseUrl + 'Memo/SubmitMemo', { json: JSON.stringify(jsonModel), Preview: isPreview ? "1" : "0" }, function (res) {
            callback(res);
        });
    }
    
    (function Prepare() {
        renderMemo(jsonModel.memo);
        renderUsers(jsonModel.meeting_attendees);
        if (!isEditable) {
            $('.btnAddSub').hide();
            $('input, textarea').prop('disabled', true);
        }
    })();

    (function setEvent() {
        $('#btnSetIssues').click(function () {
            window.location.href = $(this).find('a').attr('href');
        });
        $('.memoTitle').click(function () {
            $('.memoTitle').removeClass('active');
            $(this).addClass('active');
            $('.memoTitle').each(function () {
                $(this).siblings('.memoSubContent').addClass('hidden');
            });
            $(this).siblings('.memoSubContent').removeClass('hidden');
            
        });
        $('form').submit(function () {
            return isSubmit;
        });
        
        
        $('#btnSaveMemo').click(function () {
            isSubmit = false;
            //jsonModel.memo.Memo1 = $('#txtMemo1').val();
            //jsonModel.memo.Memo2 = $('#txtMemo2').val();
            //jsonModel.memo.Memo3 = $('#txtMemo3').val();
            //$('#json').val(JSON.stringify(jsonModel));
            $('#saveLoading').removeClass('hide');
            saveMemo(false, function (res) {
                $('#saveLoading').addClass('hide');
                if (res.IsSuccess) {
                    isDirty = false;
                    messageBox.show('Saved Successfully', 'Technical Problem System', msgBoxType.OK, function () {
                        window.location.reload();
                    });
                }
                else {
                    messageBox.show('Error : ' + res.ErrorMessage);
                }
            });
          
                
        });
        $('#btnSendMemo').click(function () {
            if (!isMeetingReadyForSent) {
                $('#btnStatus').click();
                return;
            }
            saveMemo(true, function () {
                window.location.href = baseUrl + "Memo/SendMemo?meetingId=" + jsonModel.MeetingId;
            });
        });
        $('#btnExportMemo').click(function () {
            //window.location.href = baseUrl + "Memo/ExportMemo?meetingId=" + jsonModel.MeetingId;
            saveMemo(false, function () {
                window.open(baseUrl + "Memo/ExportMemo?meetingId=" + jsonModel.MeetingId);
            });
        });
        $('#btnPreviewMemo').click(function () {
            //window.location.href = baseUrl + "Memo/ExportMemo?meetingId=" + jsonModel.MeetingId;
            saveMemo(false, function () {
                window.open(baseUrl + "Memo/ExportMemo?IsPreview=1&meetingId=" + jsonModel.MeetingId);
            });
        });
        $('#btnAddSub').click(function () {
            
        });
        $(document).on('click', '.btnDelMemo', function () {
            var container = $(this).parents('.memocontainer');
            container.nextAll().each(function (i) {
                var oldSubNo = parseInt($(this).find('.subNo').text());
                $(this).find('.subNo').text(oldSubNo - 1);
            });
            container.remove();
            isDirty = true;
        });
        $('#btnAddAttd').click(function (e) {
            e.stopPropagation();
            toAddList = new Array();
            $('#addMailDiv').prependTo($('#mainContainer'));
            drawMailerAsync();
            
        });
        $('#btnCloseAdd2').click(function () {
            $('#addMailDiv').appendTo('#memoDlg');
        });
        $('#txtSearchUser').keydown(function (e) {

            if (e.keyCode == 13) {
                isEventPropagate = true;
                e.stopPropagation();
                e.stopImmediatePropagation();
                drawMailerAsync();
            }
        });
        $(document).on('change', '.chbIsAdd', function (e) {
            if ($(e.target).is(':checked')) {
                toAddList.push($(e.target).attr('data-usercode'));
            }
            else {
                var index = $.inArray($(e.target).attr('data-usercode'), toAddList);
                toAddList.splice(index, 1);
            }
        });
        $('#btnAddConfirm').click(function () {
            if (isEventPropagate) {
                isEventPropagate = false;
                return;
            }
            $('#addMailDiv .chbIsAdd:checked').each(function (index, val) {
                var userCode = $(this).attr('data-usercode');
                if ($('#subContent0 .mailline[data-usercode="' + userCode + '"]').length == 0) {
                    var lineDiv = $(this).parents('.mailline');
                    $(val).addClass('hidden');
                    lineDiv.find('.btnDelSmall').removeClass('hide');
                    lineDiv.find('.userUnit').remove();
                    lineDiv.appendTo('#subContent0');
                }
                
            });
            $('#addMailDiv').appendTo('#memoDlg');
            updateAttd();
            isDirty = true;
        });
        $(document).on('click', '.btnDelMail', function () {
            $(this).parents('.mailline').remove();
            updateAttd();
            isDirty = true;
        });
        $(document).on('click', '.btnAddSub', function (e) {
            e.stopPropagation();
            //Find Active Agenda
            var activeTitle = $(e.target).parents('.memoTitle');
            if (activeTitle.length == 0) {
                return;
            }
            //Get Max Sub No
            var maxSubNo = parseInt(activeTitle.siblings('.memoSubContent').find('.memocontainer').last().find('.subNo').text());
            if (!maxSubNo)
                maxSubNo = 1;
            else
                maxSubNo++;
            var mainNo = parseInt(activeTitle.attr('data-agId'));
            var agType = activeTitle.attr('data-agtype');
            var agDisplayId = parseInt(activeTitle.attr('data-agDisplayId'));
            var newSubMemo = {
                MainNumber: mainNo,
                SubNumber: maxSubNo,
                Text: '',
                DisplayMainNumber: agDisplayId
            };
            renderSubMemo(newSubMemo);
            isDirty = true;
        });
        $('#btnDelMeeting').click(function () {
            var id = jsonModel.MeetingId;
            messageBox.show('Are you sure you want to Delete this Meeting? <br />'
                               + 'Waring : All data related to this meeting will be removed',
                              null, msgBoxType.YesNo, function () {
                                  $.post(baseUrl + 'Memo/RemoveMeeting/' + id, {}, function (res) {
                                      if (res.IsSuccess) {
                                          messageBox.show('Meeting Removed Successfully', null, null, function () {
                                              window.location.href = baseUrl + "Memo";
                                          });
                                      }
                                      else {
                                          messageBox.show('Error while delete meeting : ' + res.ErrorMessage);
                                      }
                                  });
                              });
        });
        $('#btnStatus').click(function () {
            var html = $('#divListStatus').html();
            messageBox.show(html);
        });
        window.onbeforeunload = function () {
            if (isDirty) {
                return 'There are some modification on memo. Are you sure you want to navigate to other windows?';
            }
        }
        
    })();
    $('#subContent0').siblings('.memoTitle').click();
    $('textArea, input').change(function () {
        isDirty = true;
    });
});