﻿$(document).ready(function () {
    var baseUrl = $('body').attr('data-baseurl');
    function reDrawTablesAndCount(isShowTable) {
        var aoData = new Array();
        //aoData.push({ "name": "issuestatus", "value": status });
        aoData.push({ "name": "SearchTerm", "value": $('#txtSearchProblem').val() });
        //aoData.push({ "name": "groupSearch", "value": $('#sltGroupFilter').val() });
        var x = new Date();
        var startDate = $('#txtStartDate').datepicker('getDate');
        var startDateStr = startDate.getDate() + '-' + (startDate.getMonth() + 1) + '-' + startDate.getFullYear();
        var endDate = $('#txtEndDate').datepicker('getDate');
        var endDateStr = endDate.getDate() + '-' + (endDate.getMonth() + 1) + '-' + endDate.getFullYear();
        aoData.push({ "name": "startDate", "value": startDateStr });
        aoData.push({ "name": "endDate", "value": endDateStr });
        var queryString = "";
        $.each(aoData, function (index, val) {
            queryString += val.name + "=" + val.value + "&";
        });
        if (queryString != "")
            queryString = queryString.slice(0, -1);
        //alert(queryString);
        window.location.href = baseUrl + "Summary/RecoverSearch?" + queryString;
    }

    (function Prepare() {
        $('input.datePicker').datepicker({
            dateFormat: 'dd-M-yy', onSelect: function () {
                reDrawTablesAndCount();
            }
        });
        if (oldContent == null) {
            $('#txtEndDate').datepicker('setDate', new Date());
            var lastMonth = new Date();
            lastMonth.setMonth(lastMonth.getMonth() - 1);
            $('#txtStartDate').datepicker('setDate', lastMonth);
        }
        else {
            var startDateSplit = oldContent.startDate.split('-');
            var startDate = new Date(startDateSplit[2], parseInt(startDateSplit[1]) - 1, startDateSplit[0]);
            var endDateSplit = oldContent.endDate.split('-');
            var endDate = new Date(endDateSplit[2], parseInt(endDateSplit[1]) - 1, endDateSplit[0]);
            $('#txtEndDate').datepicker('setDate', endDate);
            $('#txtStartDate').datepicker('setDate', startDate);
            $('#txtSearchProblem').val(oldContent.SearchTerm);
        }
        $('.dt').each(function (i) {
            var thisDiv = $(this);
            var thisTblObject = $(this).dataTable({
                "aaSorting": [[6, "desc"]],
                sDom: 'rtp',
                "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).addClass('')
                    .attr('data-issueId', aData.IssueId)
                    .attr('data-wgId', aData.WorkGroupId)
                    .attr('data-billno', aData[2]);

                    //$(nRow).addClass('editable');

                },
                "fnDrawCallback": function (oSettings) {
                    //i = 0;
                    //while (oTableObjects[i] != this) {
                    //    i++
                    //}
                    //$('#countFor' + i).html(oSettings._iRecordsTotal);
                    //drawCount++;
                    //if (!isSearchOpen) {
                    //    var count = oSettings._iRecordsTotal;
                    //    if ($('#txtSearchProblem').val() != '' && count > 0) {
                    //        thisDiv.parents('.section').children('.sectionTitle').click();
                    //        isSearchOpen = true;
                    //    }
                    //}
                    refreshDocHeight();
                }
            });
        });
    })();

    $(document).on('click', '.recoverRow', function () {
        $('.recoverRow').removeClass('active');
        $(this).addClass('active');
    });
    $('#txtSearchProblem').keydown(function (e) {
        if (e.keyCode == 13) {
            e.stopPropagation();
            e.preventDefault();
            reDrawTablesAndCount();
        }
    });
    $('#btnSearch').click(function () {
        reDrawTablesAndCount();
    });
    $('#btnRecover').click(function () {
        var activeRow = $('.recoverRow.active');
        if (activeRow.length == 0) {
            return;
        }
        messageBox.show('ต้องการกู้ใบแจ้งซ่อม ' + activeRow.attr('data-billno') + ' หรือไม่?', null, 1, function () {
            var issueId = activeRow.attr('data-issueId');
            $.post(rootUrl() + 'Issues/RecoverIssue', { issueId: issueId }, function (res) {
                if (res.IsSuccess) {
                    window.location.reload();
                }
                else {
                    messageBox.show('Error :' + res.ErrorMessage);
                }
            });
        });
    });
    $('#btnDestroy').click(function () {
        var activeRow = $('.recoverRow.active');
        if (activeRow.length == 0) {
            return;
        }
        messageBox.show('ต้องการทำลายใบแจ้งซ่อม ' + activeRow.attr('data-billno') + ' ออกจากระบบหรือไม่?', null, 1, function () {
            var issueId = activeRow.attr('data-issueId');
            $.post(rootUrl() + 'Issues/DestroyIssue', { issueId: issueId }, function (res) {
                if (res.IsSuccess) {
                    window.location.reload();
                }
                else {
                    messageBox.show('Error :' + res.ErrorMessage);
                }
            });
        });
    });

});