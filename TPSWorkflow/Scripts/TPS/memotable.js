﻿/// <reference path="../jquery-2.0.3.js" />
/// <reference path="../jquery-2.0.3.intellisense.js" />
/// 

$(document).ready(function () {

    var oTableObject = {};
    var baseUrl = $('body').attr('data-baseurl');
    (function Prepare() {
        $('#txtStartDate, #txtEndDate').datepicker({
            dateFormat: 'dd-M-yy',
            onSelect: function (dateStr) {
                oTableObject.fnDraw();
            }
        });
        var today = new Date();
        var lastMonth = new Date();
        lastMonth.setFullYear(lastMonth.getFullYear() - 1);
        $('#txtStartDate').datepicker('setDate', lastMonth);
        $('#txtEndDate').datepicker('setDate', today);


        oTableObject = $('#memoTable').dataTable({
            "bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": baseUrl + 'Memo/TableSource',
            "sServerMethod": "POST",
            "fnServerParams": function (aoData) {
                aoData.push({ "name": "groupSearch", "value": $('#groupVal').val() });
                aoData.push({ "name": "datesearch", "value": $('#txtStartDate').val() });
                aoData.push({ "name": "dateend", "value": $('#txtEndDate').val() });
                aoData.push({ "name": "statusSearch", "value": $('#statusVal').val() });
            },
            "bPaginate": true,
            aoColumns: [
                {
                    "mData": null, sClass: 'center'
                },
                {
                    mData: "MeetingDate", mRender : function ( data, type, full ) {
                        return cSharpToJsDate(data);
                    },
                    sClass: 'center'
                },
                {
                    mData: function (aData) {
                        return aData.memo.ModifiedDate;
                    },
                    sClass:'center',
                    mRender:function(data, type, full ) {
                        return cSharpToJsDate(data);
                    }
                },
                {
                    mData: function (aData) {
                        return aData.memo.UpdateByFullName;
                    },
                    sClass: 'center'
                },
                { mData: "GroupName", sClass: 'center' },
                //,{
                //    mData: null, mRender: function () {
                //        return "<button class='btnDelSmall btnTps'>X</button>";
                //    },
                //    sClass: 'noBorder'
                //}
                {
                    mData: 'IsReady', sClass: 'center', mRender: function (data, type, full) {
                        if (data == true) {
                            return 'พร้อม';
                        }
                        else {
                            return 'มีวาระรอการบันทึก';
                        }
                    }
                },
                {
                    mData: function (aData) {
                        if (!aData.IsMemo) {
                            return 'Draft';
                        }
                        else if (aData.isSentMail){
                            return 'Sent';
                        }
                        else {
                            return 'Editted';
                        }
                    }
                }
            ],
            sDom: 'rtp',
            "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                $(nRow).addClass('memoRow')
                .attr('data-memoid', aData.MeetingId)
                .attr('data-wgId', aData.WorkGroupId);
                //if (userWorkGroupId == 'all' || parseInt(userWorkGroupId) == aData.WorkGroupId) {
                //    $(nRow).addClass('editable');
                $(nRow).addClass('editable');
            },
            "fnDrawCallback": function (oSettings) {
                
                for (var i = oSettings._iDisplayStart, iLen = i + oSettings.aiDisplay.length ; i < iLen ; i++) {
                    
                    $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[(i % oSettings.aiDisplay.length)]].nTr).html(i + 1);
                }
                
            },

     

            });







        //$('.datepicker').datepicker({ dateFormat: 'dd-M-yy' });
    })();

    (function setEvent() {
        $('#btnFilterDate').click(function () {
            var pos = $(this).offset();
            var height = $(this).outerHeight();
            blurBody();
            var myDate = new Date();
            var prettyDate = (myDate.getMonth() + 1) + '/' + myDate.getDate() + '/' +
                    myDate.getFullYear();
            $(this).datepicker("dialog", prettyDate, function onSelect(dateStr) {
                $('#dateVal').val(dateStr);
                unblurBody();
                oTableObject.fnDraw();
            },
            {
                dateFormat: 'dd-mm-yy',
                onClose: function () {
                    unblurBody();
                }
            },
            [pos.left, pos.top + height]);
        });
        
        $('#btnFilterGroup').click(function () {
            var pos = $(this).offset();
            var height = $(this).outerHeight();
            $('.groupSelector').css('top', pos.top + height);
            $('.groupSelector').css('left', pos.left);
            blurBody();
            $('.groupSelector').addClass('show');
        });
        $('#btnFilterStatus').click(function () {
            var pos = $(this).offset();
            var height = $(this).outerHeight();
            $('.statusSelector').css('top', pos.top + height);
            $('.statusSelector').css('left', pos.left);
            blurBody();
            $('.statusSelector').addClass('show');
        });

        $('.search-select').change(function () {

            oTableObject.fnDraw();
        });
        

        $('#btnReset').click(function () {
            $('#groupVal').val('-1');
            $('#dateVal').val('');
            $('#statusVal').val('0');
            $('.groupselect').removeClass('btn-primary').addClass('btn-default');
            $('.groupselect.all').removeClass('btn-default').addClass('btn-primary');
            $('.statusselect').removeClass('btn-primary').addClass('btn-default');
            $('.statusselect.all').removeClass('btn-default').addClass('btn-primary');
            oTableObject.fnDraw();
        });

        $('#btnScheduler').click(function () {
            blurAndWait();
            $.post(baseUrl + 'Issues/ForceMemoScheduler', {}, function (res) {
                if (res.IsSuccess) {
                    messageBox.show('Memo created successfully', null, null, function () {
                        window.location.reload();
                    });
                }
                else {
                    messageBox.show('Error while creating memo : ' + res.ErrorMessage + "<br /> Please Contact Administrator", null, null, function () {
                        window.location.reload();
                    });
                }

                unblurBody();

            });
        });

        $(document).on('click', '.memoRow.editable', function () {
            window.location.href = baseUrl + "memo/edit/" + $(this).attr('data-memoid');
        });
    })();
});