﻿/// <reference path="../jquery-2.0.3.js" />
/// <reference path="../jquery-2.0.3.intellisense.js" />

$(document).ready(function () {
    var baseUrl = $('body').attr('data-baseurl');
    var tblConfig, oTablPending, oTblMain, svConfig, tblAddConfig, oTblAdd;
    (function initializeTables() {
        tblConfig = {
            "aaSorting": [[6, "desc"]],
            sDom: 'rtp',
            "bPaginate": false,
            "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                $(nRow).addClass('meetingIssues')
                .attr('data-issueId', aData.IssueId)
                .attr('data-wgId', aData.WorkGroupId);



            },
            "fnDrawCallback": function (oSettings) {

                refreshDocHeight();
            }
        };
        oTblPending = $('#tblMemoIssuesPending').dataTable(tblConfig);
        oTblMain = $('#tblMemoIssuesMain').dataTable(tblConfig);
        svConfig = {
            "bProcessing": true,
            "bServerSide": false,
            "sAjaxSource": baseUrl + 'Memo/GetIssuesToAddToMeetingDt',
            "sServerMethod": "POST",
            "fnServerParams": function (aoData) {
                aoData.push({ name: "meetingId", "value": jsonModel.MeetingId });
            },
            oLanguage:{
                "sProcessing": '<img src="' + baseUrl + '/Content/Images/ajax-loader.gif" /> Procesing...',
            },
            
            "aoColumns": [
                        {
                            "mData": function (aData) {
                                return aData.IssueId;
                            },
                            mRender: function (data, type, full) {
                                return '<input type="checkbox" class="chbAddIssues" data-id="' + data + '" />';
                            }
                        },
                        {
                            "mData": "GroupName",
                            mRender: function (data, type, full) {
                                if (data != null) {
                                    return "<div class='groupHighlight'>" + data + "</div>";
                                }
                                else {
                                    return '';
                                }
                            }, sClass: 'groupDisplay'
                        },
                        {
                            "mData": "ProblemDate", sClass: 'dateDisplay',
                            mRender: function (data, type, full) {
                                return cSharpToJsDate(data);
                            }

                        },
                        { "mData": "BillNo", sClass: '' },
                        {
                            "mData": "ProblemName",
                            mRender: function (data, type, full) {
                                return "<div class='topicsum fixwidth' title='" + data + "'>" + data + "</div>";
                            }

                        },
                        { "mData": "PlantDisplayCode", sClass: '' },
                        {
                            "mData": "WorkGroupNameTh", sClass: ''
                        },
                        {
                            "mData": "StatusText"
                        }, {
                            "mData": "IsHadMeeting",
                            mRender: function (data, type, full) {
                                return data ? "เคย" : "ไม่เคย";
                            }
                        }

            ],
            "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                $(nRow).addClass('meetingIssuesAdd').addClass('cursor')
                .attr('data-issueId', aData.IssueId)
                .attr('data-wgId', aData.WorkGroupId);



            },
        };
        tblAddConfig = $.extend(false, tblConfig, svConfig); // Clone
        oTblAdd = $('#tblMemoIssuesAdd').dataTable(tblAddConfig);
    })();
    (function setEvents() {
       
        $('#btnAddIssues').click(function () {
            $('#AddIssuesDialog').removeClass('loading').prependTo('#mainContainer');
            $('#AddIssuesDialog .placeholder').removeClass('hidden');
            oTblAdd.fnReloadAjax();
        });
        $('#btnCloseAdd').click(function () {
            $('#AddIssuesDialog').appendTo('#memoDlg');
        });
        $(document).on('click', '.meetingIssuesAdd', function (e) {
            
            var checkbox = $(this).find('input[type="checkbox"]');
            checkbox.prop("checked", !checkbox.prop("checked"));
        });
        $(document).on('click', '.chbAddIssues', function (e) {
            e.stopPropagation();
        });
        $('#btnAddConfirm').click(function () {
            var issuesToAdd = [];
            $('.chbAddIssues:checked').each(function () {
                issuesToAdd.push($(this).attr('data-id'));
            });
            $('#AddIssuesDialog').addClass('loading').find('.placeholder').addClass('hidden');;
            $.post(baseUrl + 'Memo/AddIssuesToMeeting', { issueIds: JSON.stringify(issuesToAdd), meetingId: jsonModel.MeetingId }, function (res) {
                $('#AddIssuesDialog').appendTo('#memoDlg');
                if (res.IsSuccess) {
                    window.location.reload();
                }
                else {
                    messageBox.show(res.ErrorMessage);
                }
            });
        });
        $(document).on('click', '.btnDelIssues', function () {
            var issueId = $(this).attr('data-issueId');
            var billNo=$(this).attr('data-billno');
            messageBox.show('Are you sure to delete issue No.' + billNo + ' from this memo?', null, msgBoxType.YesNo, function () {
                $.post(baseUrl + 'Memo/DeleteIssuesFromMeeting', { meetingId: jsonModel.MeetingId, issueId: issueId }, function (res) {
                    if (res.IsSuccess) {
                        window.location.reload();
                    }
                    else {
                        messageBox.show(res.ErrorMessage);
                    }
                });
            });
            
        });
    })();
});
