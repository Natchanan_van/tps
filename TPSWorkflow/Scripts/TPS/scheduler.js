﻿/// <reference path="../jquery-2.0.3.js" />
/// <reference path="../jquery-2.0.3.intellisense.js" />
/// 
$(document).ready(function () {
    var baseUrl = $('body').attr('data-baseurl');
    var newPlantCodes = new Array();
    var isChanged = false;
    var addUserCodes = new Array();
    var delUserCodes = new Array();
    var currentGroupIndex = 0;
    function drawPlant(data) {
        $('#plantPlaceHolder').empty();
        if (newPlantCodes.length == 0) {
            
        }
        for (i = 0; i < data.length; i++) {
            var thisDiv = $('.plant.prototype').clone().removeClass('prototype');
            if (typeof (data[i]) == 'object') {
                thisDiv.find('.plantname').html(data[i].plantdisplaycode).appendTo('#plantPlaceHolder');
            }
            else if (typeof (data[i]) == 'string') {
                thisDiv.find('.plantname').html(data[i]).appendTo('#plantPlaceHolder');
            }
            
        }
    }
    function getMailerByPrototype(thisUser) {
        var thisDiv = $('.mailingline.prototype').clone().removeClass('prototype');
        thisDiv.find('.usercode').html(thisUser.CODE);
        thisDiv.find('.userfullname').html(thisUser.INAME + thisUser.FNAME + ' ' + thisUser.LNAME);
        thisDiv.find('.chbAddMailer').attr('data-usercode', thisUser.CODE);
        thisDiv.data('usermodel', thisUser);
        return thisDiv;
    }
    function drawMailerAsync(data) {
        $('#mailPlaceHolder').addClass('loading');
        var groupId = $('#sltGrp').val();
        $.post(baseUrl + 'Settings/GetMailer', { groupid: groupId }, function (res) {
            $('#mailPlaceHolder').removeClass('loading');
            if (!res.IsSuccess) {
                messageBox.show('Error loading mail list : ' + res.ErrorMessage);
                return;
            }
            $('#mailPlaceHolder').empty();
            for (i = 0; i < res.Result.length; i++) {
                var thisUser = res.Result[i];
                var thisDiv = getMailerByPrototype(thisUser);
                thisDiv.find('.chbAddMailer').hide();
                thisDiv.appendTo('#mailPlaceHolder');
            }
        });
    }
    function drawTime(timestart, timeend) {
        var timeStartJS = cToJsObjDate(timestart);
        var timeEndJS = cToJsObjDate(timeend);
        var d = '2014-01-02T00:00:00';
        var hrStart = timestart.substr(11, 2);
    
        $('#timeStartDigit1').val(hrStart[0]);
        $('#timeStartDigit2').val(hrStart[1]);
        
        var minStart = timestart.substr(14, 2);
        if (minStart.length == 1) {
            minStart = '0' + minStart;
        }
        if (minStart.length == 0) {
            minStart = '00';
        }

        $('#timeStartDigit3').val(minStart[0]);
        $('#timeStartDigit4').val(minStart[1]);

        var hrEnd = timeend.substr(11, 2);
        if (hrEnd.length == 1) {
            hrEnd = '0' + hrEnd;
        }
        if (hrEnd.length == 0) {
            hrEnd = '00';
        }

        $('#timeEndDigit1').val(hrEnd[0]);
        $('#timeEndDigit2').val(hrEnd[1]);

        var minEnd = timeend.substr(14, 2);
        if (minEnd.length == 1) {
            minEnd = '0' + minEnd;
        }
        if (minEnd.length == 0) {
            minEnd = '00';
        }

        $('#timeEndDigit3').val(minEnd[0]);
        $('#timeEndDigit4').val(minEnd[1]);
    }
    function setDay(dayofWeek) {
        $('#chbday' + dayofWeek).prop('checked', true);
    }
    function setWeek(week) {
        $('#week' + week).prop('checked', true);
        
    }
    function getCurrentGroupData() {
        for (i = 0; i < jsonModel.length; i++) {
            if ($('#sltGrp').val() == jsonModel[i].GroupId) {
                currentGroupIndex = i;
                return jsonModel[i];
            }
                
        }
    }
    function drawData() {
        var thisGroup = getCurrentGroupData();
        drawPlant(thisGroup.allowplant);
        drawMailerAsync(thisGroup.group_mailing);
        drawTime(thisGroup.TimeStart, thisGroup.TimeEnd);
        setDay(thisGroup.DayOfWeek);
        setWeek(thisGroup.WeekOfMonth);
        $('#chbIsSuspended').prop('checked', thisGroup.IsSuspended);
        $('#chbIsShift').prop('checked', thisGroup.IsShiftThisMonth);
        $('#chbIsChangeLocation').prop('checked', thisGroup.Location != "");
        $('#txtLocationChanged').val(thisGroup.Location)
        onSusPendedChange(thisGroup.SuspendedEnd);
        onShiftChange(thisGroup.ShiftDate);
        onChbForTextChange('#chbIsChangeLocation', '#locationChanged');
        $('.mtgDate').addClass('hide');
        $('#mtgDate' + currentGroupIndex).removeClass('hide');
    }
    function renewData() {
        newPlantCodes = new Array();
        isChanged = false;
        addUserCodes = new Array();
        delUserCodes = new Array();
    }
    function onSusPendedChange(date){
        if ($('#chbIsSuspended').is(':checked')) {
            $('#dateSuspended').removeClass('hide');
            if (date != null) {
                $('.datepicker').datepicker('option', 'dateFormat', 'yy-mm-dd');
                $('#txtSuspendDate').datepicker("setDate", date.substr(0, 10));
                $('.datepicker').datepicker('option', 'dateFormat', 'dd-M-yy');
            }
            
        }
        else {
            $('#dateSuspended').addClass('hide');
        }
    }
    function onShiftChange(date) {
        if ($('#chbIsShift').is(':checked')) {
            $('#dateShift').removeClass('hide');
            if (date != null) {
                $('.datepicker').datepicker('option', 'dateFormat', 'yy-mm-dd');
                $('#txtDateShift').datepicker("setDate", date.substr(0, 10));
                $('.datepicker').datepicker('option', 'dateFormat', 'dd-M-yy');
            }

        }
        else {
            $('#dateShift').addClass('hide');
        }
    }
    function onChbForTextChange(chbId, txtId) {
        if ($(chbId).is(':checked')) {
            $(txtId).removeClass('hide');
        }
        else {
            $(txtId).addClass('hide');
        }
    }

    (function Prepare() {
        $('.datepicker').datepicker({ dateFormat: 'dd-M-yy', minDate: new Date() });
        if (!isCanShift) {
            $('#chbIsSuspended').prop('disabled', true);
            $('#txtSuspendDate').datepicker("option", "disabled", true);
        }
        //var thisGroup = getCurrentGroupData();
        //drawPlant(thisGroup.allowplant);
        //drawMailerAsync(thisGroup.group_mailing);
        //drawTime(thisGroup.TimeStart, thisGroup.TimeEnd);
        //setDay(thisGroup.DayOfWeek);
        //setWeek(thisGroup.WeekOfMonth);
        if (currentGroup != null) {
            $('#sltGrp').val(currentGroup);
        }
        $('.select2').select2({ width: 400 });
        $('#mtgDate' + currentGroupIndex).removeClass('hide');
        drawData();
    })();

    (function SetEvent() {
        
        $(document).on('change','#sltGrp',function () {
            
        });
        $('#chbIsSuspended').change(function () {
            onSusPendedChange();
        });
        $('#chbIsShift').change(function () {
            onShiftChange();
        });
        $('#chbIsChangeLocation').change(function () {
            onChbForTextChange('#chbIsChangeLocation', '#locationChanged');
        });
        $(document).on('select2-selecting', '#sltGrp', function (e) {
            e.preventDefault();
            $('#sltGrp').select2('close');
            var newVal = e.val;
            if (newVal == $('#sltGrp').val())
                return;
            if (isChanged) {
                messageBox.show('Save changed made in this group?', 'Techincal Problem System', 1, function () {
                    $('#currentGroup').val(newVal);
                    $('#btnSave').click();
                }
                , function () {
                    $('#sltGrp').select2('val', newVal);
                    renewData();
                    drawData();

                });
            }
            else {
                $('#sltGrp').select2('val', newVal);
                drawData();
            }
            
        });

        $('.digit').keydown(function (e) {
            //e.preventDefault();
            if (e.keyCode == 8) {
                if ($(this).val() != '')
                    $(this).val('');
                else
                    $(this).prev('input').focus().val('');
                return;
            }
        });
        $('.digit').keypress(function (e) {
            e.preventDefault();
           
            
            var charStr = String.fromCharCode(e.keyCode);
            if (/(\d)/.test(charStr)) {
                $(this).val('');
                $(this).val(charStr);
                $(this).next('input').focus();
                isChanged = true;
            }

        });
        $('#btnAddPlant').click(function () {
            blurBody();
            $('#plantDialog').appendTo('#maincontainer');
            var top = $('#maincontainer').offset().top + 40;
            var left = $('#maincontainer').offset().left + 20;
            var width = $('#maincontainer').width() - 20;
            $('#plantDialog').css('top', top).css('left', left).css('width', width);
            var thisGroup = getCurrentGroupData();
            var thisGroupPlantsCode = new Array();
            if (newPlantCodes.length > 0)
                thisGroupPlantsCode = newPlantCodes;
            else {
                for (var key in thisGroup.allowplant) {
                    thisGroupPlantsCode.push(thisGroup.allowplant[key].plantshortcode);
                }
            }
            $('.isGroupSelect').attr('checked', false);
            $('.isGroupSelect').each(function (i) {
                if ($.inArray($(this).attr('data-plantid'), thisGroupPlantsCode, 0) >= 0) {
                    $(this).prop('checked', true);
                }
            });
        });
        $('#btnCloseGrp').click(function () {
            unblurBody();
            $('#plantDialog').appendTo('.dlgPlaceholder');
        });
        $('#btnAddPlantConfirm').click(function () {
            newPlantCodes = new Array();
            displayPlants = new Array();
            $('#groupSelectPlaceHolder .isGroupSelect:checked').each(function () {
                newPlantCodes.push($(this).attr('data-plantid'));
                displayPlants.push($(this).attr('data-plantdisplay'));
            });
            isChanged = true;
            unblurBody();
            drawPlant(displayPlants);
            $('#plantDialog').appendTo('.dlgPlaceholder');
        });
        $('.chbday,.weekselect,.digit').change(function () {
            isChanged = true;
        });
        function drawMailableFromSv(unitcode, searchterm) {
            $('#addMailerPlaceholder').empty();
            $('#addMailerPlaceholder').addClass('loading');
            
            $.post(baseUrl + 'Settings/GetMailableUserList', { unitcode: unitcode, searchterm: searchterm }, function (res) {
                $('#addMailerPlaceholder').removeClass('loading');
                if (!res.IsSuccess) {
                    messageBox.show('Error : ' + res.ErrorMessage);
                    return;
                }
                for (i = 0; i < res.Result.length; i++) {
                    var thisUser = res.Result[i];
                    var thisDiv = getMailerByPrototype(thisUser);
                    thisDiv.find('.btnDelMailer').hide();
                    var thisDivUserCode = thisDiv.find('.chbAddMailer').attr('data-usercode');
                    if ($.inArray(thisDivUserCode, addUserCodes, 0) >= 0)
                        thisDiv.find('.chbAddMailer').prop("checked", true);
                    thisDiv.appendTo('#addMailerPlaceholder');
                }
            });
        }
        $('#txtSearchUser').keydown(function (e) {
            if (e.keyCode == 13) {
                drawMailableFromSv(getCurrentGroupData().unitcode, $(this).val());
            }
        });
        $('#btnAddMailer').click(function () {
            blurBody();
            $('#mailerDialog').appendTo('#maincontainer');
            var top = $('#maincontainer').offset().top + 40;
            var left = $('#maincontainer').offset().left + 20;
            var width = $('#maincontainer').width() - 20;
            $('#mailerDialog').css('top', top).css('left', left).css('width', width);
            var thisGroup = getCurrentGroupData();
            drawMailableFromSv(thisGroup.unitcode, '');
        });
        $('#btnCloseMail').click(function () {
            unblurBody();
            $('#mailerDialog').appendTo('.dlgPlaceholder');
            
        });
        $('#btnAddMailerConfirm').click(function () {
            isChanged = true;
            addUserCodes = new Array();
            $('#mailPlaceHolder .new').remove();
            $('#addMailerPlaceholder .chbAddMailer:checked').each(function (i) {
                addUserCodes.push($(this).attr('data-usercode'));
                var thisUser = $(this).parents('.mailingline').data('usermodel');
                var thisDiv = getMailerByPrototype(thisUser);
                thisDiv.addClass('new').find('.chbAddMailer').hide();
                thisDiv.appendTo('#mailPlaceHolder');
            });
            unblurBody();
            $('#mailerDialog').appendTo('.dlgPlaceholder');
        });
        $(document).on('click', '.btnDelMailer', function () {
            if ($(this).parents('.mailingline').hasClass('new')) {
                var thisUserCode = $(this).parents('.mailingline').data('usermodel').CODE;
                var index = $.inArray(thisUserCode, addUserCodes, 0);
                addUserCodes.splice(index, 1);
                $(this).parents('.mailingline').remove();
            }
            else if ($(this).parents('.mailingline').hasClass('toDel')) {
                $(this).parents('.mailingline').removeClass('toDel');
            }
            else {
                $(this).parents('.mailingline').addClass('toDel');
            }
        });
        $('#btnSave').click(function () {
            $('#newGroups').val(JSON.stringify(newPlantCodes));
            $('#addUsers').val(JSON.stringify(addUserCodes));
            var toDelUsers = new Array();
            $('.mailingline.toDel').each(function () {
                toDelUsers.push($(this).data('usermodel').CODE);
            });
            $('#delUsers').val(JSON.stringify(toDelUsers));
            $('#groupId').val($('#sltGrp').val());
            $('#dayInWeek').val($('.chbday:checked').val());
            $('#weekmonth').val($('.weekselect:checked').val());
            var hr = $('#timeStartDigit1').val() + $('#timeStartDigit2').val();
            $('#starttimehr').val(hr);
            hr = $('#timeStartDigit3').val() + $('#timeStartDigit4').val();
            $('#starttimemin').val(hr);
            hr = $('#timeEndDigit1').val() + $('#timeEndDigit2').val();
            $('#endtimehr').val(hr);
            hr = $('#timeEndDigit3').val() + $('#timeEndDigit4').val();
            $('#endtimemin').val(hr);
            if ($('#chbIsSuspended').is(':checked')) {
                $('#isSuspended').val(1);
                $('#susDate').val($('#txtSuspendDate').val());
            }
            else {
                $('#isSuspended').val(0);
            }
            if ($('#chbIsShift').is(':checked')) {
                $('#isShift').val(1);
                $('#shiftDate').val($('#txtDateShift').val());
            }
            else {
                $('#isShift').val(0);
            }
            if ($('#chbIsChangeLocation').is(':checked')) {
                $('#meetingLocation').val($('#txtLocationChanged').val());
            }
            else {
                $('#meetingLocation').val('');
            }
            $('form').submit();
        });
    })();
});