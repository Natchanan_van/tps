﻿/// <reference path="../jquery-2.0.3.js" />
/// <reference path="../jquery-2.0.3.intellisense.js" />

$(document).ready(function () {
    (function Prepare() {
        var baseUrl = $('body').attr('data-baseurl');

        //Uploadify
        $('.upload').uploadify({
            auto: false,
            'swf': baseUrl + 'Plugins/uploadify/uploadify.swf',
            'uploader': baseUrl + 'Issues/SubmitIssuesDetailFile',
            'onSWFReady': function () {
                if (!isEditable) {
                    $('#btnAddFile').uploadify('disable', true);
                }
            }
            // Put your options here
        });

        if (!isEditable) {
            $('textarea').attr('disabled', 'disabled');
            $('#btnSubmit').attr('disabled', 'disabled');
            //$('#btnAddFile').uploadify('disable', true);
        }

        
    })();

    (function SetEvent() {
        var isSubmit = false;
        var toDelFileId = new Array();
        var queueError = [];
        $('form').submit(function () {
            if (!isSubmit)
                return false;
        });
        
        

        $('#btnAddFile').uploadify('settings', 'onQueueComplete', function (queueData) {
            if (queueError.length > 0) {
                var errorDetail = "";
                for (i = 0; i < queueError.length; i++) {
                    errorDetail += "Filename: " + queueError[i].file.name + ", Error: " + queueError[i].error + "<br />";
                }
                messageBox.showbig('Upload error ' + queueError.length + 'file(s) <br /> ' + errorDetail +
                    'Do you want to save change?', 'Techincal Problem System', msgBoxType.YesNo, function () {
                        isSubmit = true;
                        $('form').submit();
                    }, function () {
                        return;
                    })
            }
            else {
                isSubmit = true;
                $('form').submit();
            }
        });
        $('#btnAddFile').uploadify('settings', 'onUploadSuccess', function (file, responseData, isResponse) {
            var responseJson = JSON.parse(responseData);
            if (!responseJson.IsSuccess) {
                queueError.push({
                    file: file,
                    error: responseJson.ErrorMessage
                })
            }
        });
        $('#btnAddFile').uploadify('settings', 'onSelect', function (file) {
            $('.forupload').removeClass('hide');
        });

        $('.btnDelFile').click(function () {
            var prmElemtn = $(this).parents('.fileLabel');
            if (prmElemtn.hasClass('toDelFile')) {
                prmElemtn.removeClass('toDelFile');
            }
            else {
                prmElemtn.addClass('toDelFile');
            }
        });

        $('#btnSubmit').click(function () {
            $('#btnAddFile').uploadify('settings', 'formData', {
                issueId: issueId,
                ASPSESSID: ASPSESSID,
                AUTHID: auth
            });
            queueError = [];
            var isValid = true;
            var invalidMessage = '';
            $('textarea').each(function () {
                if ($(this).val().trim() == '') {
                    isValid = false;
                    invalidMessage = 'กรุณากรอกช่อง "' + $(this).attr('data-validatetxt') + '"';
                }
            });
            if (!isValid) {
                messageBox.show(invalidMessage);
                return;
            }

            jsonModel.IssueId = issueId;
            jsonModel.BaseProblem = $('#txtBaseProblem').val();
            jsonModel.FixDetail = $('#txtFixDetail').val();
            jsonModel.PossibleCauses = $('#txtPossibleCause').val();
            jsonModel.ProblemCauses = $('#txtProblemCause').val();
            jsonModel.SolveDirection = $('#txtSolve').val();
            $('#formjson').val(JSON.stringify(jsonModel));

            $('.toDelFile').each(function () {
                toDelFileId.push($(this).attr('data-fileId'));
            });
            $('#delFileIdsJson').val(JSON.stringify(toDelFileId));

            var fileCount = $('.uploadify-queue-item').size() + $('.fileLabel:not(.toDelFile):not(.title)').length;
            if (fileCount <= 0) {
                messageBox.show('Please Upload Some File');
                return;
            }

            confirmSubmit(function () {
                if ($('.uploadify-queue-item').size() > 0) {
                    $('#btnAddFile').uploadify('upload', '*');
                }
                else {
                    isSubmit = true;
                    $('form').submit()
                }
            });
            
        });
    })();
});