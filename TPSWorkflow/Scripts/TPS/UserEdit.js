﻿/// <reference path="../jquery-2.0.3.js" />
/// <reference path="../jquery-2.0.3.intellisense.js" />

$(document).ready(function () {
    var baseUrl = $('body').attr('data-baseurl');
    var isSetPassword = false;
    (function Prepare() {
        //$('.select2').select2({ width: 400 });
        
        $('.select2short').select2({ width: 100 });
        $('#txtIname').select2('val', jsonModel.INAME);
        $('#btnUpPic').uploadify({
            auto: true,
            buttonClass:'btnTps short inline',
            'swf': baseUrl + 'Plugins/uploadify/uploadify.swf',
            'uploader': baseUrl + 'Settings/UploadTempPic',
            multi: false,
            'fileTypeDesc': 'Image Files',
            'fileTypeExts': '*.gbtif; *.jpg; *.png'
            // Put your options here
        });

      
        if (jsonModel.TpsType == 1) {
            $('#btnStaff').addClass('active');
        }
        else if (jsonModel.TpsType == 2) {
            $('#btnAdmin').addClass('active');
        }
        else if (jsonModel.TpsType == 3) {
            $('#btnSuperAdmin').addClass('active');
        }
        

        if (jsonModel.TpsUserId < 0) {
            $('#btnChangePass').remove();
            isSetPassword = true;
        }
        else {
            $('#txtPassword,#txtPasswordConf').prop('disabled', true);
        }
    })();

    (function setEvent() {
        $('#btnUpPic').uploadify('settings', 'onUploadSuccess', function (file, data, response) {
            var resObj = JSON.parse(data);
            if (resObj.IsSuccess) {
                $('#imgAvatar').attr('src', resObj.Result.Url).attr('data-filetempid', resObj.Result.FileTempId);
            }
        });

        $('.usertab.toggle').click(function () {
            $(this).addClass('active').siblings().removeClass('active');
        });
        
        $('#btnAdd').click(function () {
            jsonModel.Username = $('#txtUsername').val();
            jsonModel.INAME = $('#txtIname').val();
            jsonModel.FNAME = $('#txtFname').val();
            jsonModel.LNAME = $('#txtLname').val();
            jsonModel.HOMETEL = $('#txtTelNo').val();
            jsonModel.UNITCODE = "";
            jsonModel.Department = $('#txtWg').val();

            jsonModel.TpsType = -1;
            if ($('#btnStaff').hasClass('active')) {
                jsonModel.TpsType = 1; //Staff
            }
            if ($('#btnAdmin').hasClass('active')) {
                jsonModel.TpsType = 2; //Admin
            }
            if ($('#btnSuperAdmin').hasClass('active')) {
                jsonModel.TpsType = 3; //SuperAdmin
            }

            jsonModel.EmailAddr = $('#txtEmail').val();

            if (!jsonModel.Username) {
                messageBox.show('Please Enter Username');
                return;
            }

            if (!jsonModel.EmailAddr) {
                messageBox.show('Please Enter Email Address');
                return;
            }
            //if (!jsonModel.UNITCODE) {
            //    messageBox.show('Please Select Workgroup');
            //    return;
            //}


            if (jsonModel.TpsType == -1) {
                messageBox.show('Please Select User Type');
                return;
            }

            if (isSetPassword && (!($('#txtPassword').val()) || $('#txtPassword').val() != $('#txtPasswordConf').val())) {
                messageBox.show('Incorrect Password');
                return;
            }
            if ($('#imgAvatar').attr('data-filetempid')) {
                $('#imgtempname').val($('#imgAvatar').attr('data-filetempid'));
            }
            //if ($('#imgtempname').val() == '') {
            //    messageBox.show('Please Upload Avatar');
            //    return;
            //}

            $('#json').val(JSON.stringify(jsonModel));
            $('#password').val($('#txtPassword').val());
            
            $('form').submit();
        });
        $('#btnChangePass').click(function () {
            $('#txtPassword,#txtPasswordConf').prop('disabled', false);
            isSetPassword = true;
        });
    })();
});