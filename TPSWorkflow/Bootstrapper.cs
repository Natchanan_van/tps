using System.Web.Mvc;
using Microsoft.Practices.Unity;
using TPSWorkflow.DAL;
using Unity.Mvc5;

namespace TPSWorkflow
{
    public static class Bootstrapper
    {
        public static void Initialise()
        {
            var container = BuildUnityContainer();

            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
        }

        private static IUnityContainer BuildUnityContainer()
        {
            var container = new UnityContainer();

            // register all your components with the container here
            // it is NOT necessary to register your controllers

            
            // e.g. container.RegisterType<ITestService, TestService>();

            container.RegisterType<IUsersManage, UsersManage>(new InjectionConstructor(Properties.Settings.Default.MinimumJobLevel));
            container.RegisterType<IIssuesData, IssuesData>();
            container.RegisterType<IMailLogger, MailLogger>(new InjectionConstructor(string.Empty));

            return container;
        }
    }
}