﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TPSWorkflow.DAL.DtoS;
using System.IO;

namespace TPSWorkflow
{
    public class TPSPathHelper
    {
        public static string ResolvePathFromProblem(problem_fileDto file)
        {
            var fileName = string.Format("{0}_{1}_{2}.{3}", file.FileId, Path.GetFileNameWithoutExtension(file.FileName), Guid.NewGuid().ToString(), Path.GetExtension(file.FileName).Trim('.'));
            var folder = Path.Combine(Properties.Settings.Default.StoragePath, "Problem");
            if (!Directory.Exists(folder))
                Directory.CreateDirectory(folder);
            return Path.Combine(folder, fileName);
        }

        public static string ResolvePathFromKM(summary_kmDto file)
        {
            var fileName = string.Format("{0}_{1}_{2}.{3}", file.KmId, Path.GetFileNameWithoutExtension(file.FileName), Guid.NewGuid().ToString(), Path.GetExtension(file.FileName).Trim('.'));
            var folder = Path.Combine(Properties.Settings.Default.StoragePath, "KM");
            if (!Directory.Exists(folder))
                Directory.CreateDirectory(folder);
            return Path.Combine(folder, fileName);
        }

        public static string GetPhotoPathUrl(string userCode)
        {
            return string.Format("http://hq-web-s13.pttplc.com/directory/photo/{0}.jpg", userCode);
        }

        public static string GetTempLocationForFileName(string fileName)
        {
 
            var tempFolder = Path.Combine(Properties.Settings.Default.StoragePath, "Temp");
            if (!Directory.Exists(tempFolder))
                Directory.CreateDirectory(tempFolder);
            return Path.Combine(tempFolder, fileName);
        }

        public static string GetAvatarLocationForFileName(string fileName)
        {
            var tempFolder = Path.Combine(Properties.Settings.Default.StoragePath, "Avatars");
            if (!Directory.Exists(tempFolder))
                Directory.CreateDirectory(tempFolder);
            return Path.Combine(tempFolder, fileName);
        }

        public static string GetLogFilePath()
        {
            var dataAccess = new TPSWorkflow.DAL.IssuesData();
            return dataAccess.GetLogConfigPath();
        }

        public static string LogDefaultPath()
        {
            var tempFolder = Path.Combine(Properties.Settings.Default.StoragePath, "Log");
            if (!Directory.Exists(tempFolder))
                Directory.CreateDirectory(tempFolder);
            var filePath = Path.Combine(tempFolder, "Log.txt");
            if (!File.Exists(filePath))
            {
                File.CreateText(filePath).Close();
            }

            return filePath;
        }
    }
}