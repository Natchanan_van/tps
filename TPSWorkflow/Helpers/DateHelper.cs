﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace TPSWorkflow
{
    public static class DateHelper
    {
        public static string TPSDateTime(this DateTime src)
        {
            var c = new System.Globalization.CultureInfo("en-US");
            if (src == System.Data.SqlTypes.SqlDateTime.MinValue)
            {
                return string.Empty;
            }
            return string.Format("{0} {1} <br /> {2} {3}", src.Day, src.ToString("dddd", c), src.ToString("MMM", c), src.ToString("yyyy", c));
        }
        public static string TpsMailDateTime(this DateTime src)
        {
            var c = new System.Globalization.CultureInfo("en-US");
            return string.Format("{0}-{1}-{2}", src.Day, src.ToString("MMM", c), src.ToString("yyyy", c));
        }

        public static string TpsReportDateTime(this DateTime src)
        {
            var c = new System.Globalization.CultureInfo("th-TH");
            if (src == DateTime.MinValue)
                return string.Empty;
            return string.Format("วันที่ {0} เดือน {1} พ.ศ. {2}", src.Day, src.ToString("MMMM", c), src.ToString("yyyy", c));
        }

        public static DateTime ParseDatePickerString(this string src)
        {
            DateTime result;
            DateTime.TryParseExact(src, "d-MMM-yyyy", new CultureInfo("en-US"), DateTimeStyles.AssumeLocal, out result);
            return result;
        }
    }
}