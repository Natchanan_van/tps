﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;
using TPSWorkflow.DAL.DtoS;

namespace TPSWorkflow.Helpers
{
    public class SessionHelper
    {
        #region Singleton
        protected static SessionHelper instance;

        public static SessionHelper GetInstance()
        {
            return GetInstance(null);
        }

        public static SessionHelper GetInstance(HttpSessionStateBase session)
        {
            //if (instance == null)
                instance = new SessionHelper(session);
            return instance;
        }
        #endregion

        protected HttpSessionStateBase session;

        private SessionHelper(HttpSessionStateBase session)
        {
            this.session = session;
            if (session == null)
                this.session = new HttpSessionStateWrapper(HttpContext.Current.Session);
        }

        public PersonalInfoDto userInfo
        {
            get
          {
                return session["UserInfo"] as PersonalInfoDto;
            }
            set
            {
                session["UserInfo"] = value;
            }
        }

        public bool IsAuthen
        {
            get
            {
                return userInfo != null;
            }
        }

        public TPSWorkflow.DAL.UserRights Rights {
            get
            {
                if (session["Rights"] != null)
                    return (TPSWorkflow.DAL.UserRights)session["Rights"];
                else
                    return TPSWorkflow.DAL.UserRights.None;
            }
            set
            {
                session["Rights"] = value;
            }
        }

        public bool IsMaster
        {
            get
            {
                if (session["IsMaster"] != null)
                    return (bool)session["IsMaster"];
                else
                    return false;
            }
            set
            {
                session["IsMaster"] = value;
            }
        }

        public string MockFromUser
        {
            get
            {
                if (session["MockFrom"] != null)
                    return session["MockFrom"].ToString();
                else
                    return null;
            }
            set
            {
                session["MockFrom"] = value;
            }
        }

        public void Destroy()
        {
            this.session.Clear();
            this.session.Abandon();
        }

        public void ClearData()
        {
            session["MockFrom"] = null;
        }

    }
}