﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>
<% var appPath = WebHelper.Constants.AppPath; %>

<style>
    .warning {
        background-image: url(<%=appPath %>Content/Images/PTT_64.png);
        -moz-background-size: contain;
        -o-background-size: contain;
        -webkit-background-size: contain;
        background-size: contain;
        background-position:center;
        background-repeat:no-repeat;
    }

    #msgBox {
        text-align: left;
    }
</style>

<script type="text/javascript">
    var msgBoxType = {
        YesNo: 1,
        OK: 0
    };
    var messageBox = {
        show: function (message, title, type, callback, callback2) {
            //TODO: Real msg box
            $('#msgBoxContent').html(message);
            if (title == null) title = 'Technical Problem System';
            if (type == null) type = msgBoxType.OK;
            $('#msgBox').siblings('.ui-dialog-titlebar').show();

            var b = new Object();
            $('#msgBoxIcon').addClass('warning');
            if (type == msgBoxType.YesNo) {
                b = {
                    Yes: function () {
                        $('#msgBox').addClass('dialog').dialog('close');
                        callback();
                    },
                    No: function () {
                        $('#msgBox').addClass('dialog').dialog('close');
                        if (callback2 != null) {
                            callback2();
                        }
                    }
                };
            }
            else if (type == msgBoxType.OK) {
                b = {
                    OK: function () {
                        $('#msgBox').addClass('dialog').dialog('close');
                        if ((callback) != null)
                            callback();
                    }
                };
            }
            $('#msgBox').show().addClass('dialog').dialog({
                modal: true,
                buttons: b,
                title: title,
                width: 550,
                height: 300,
                resizable: false,
            });
            //callback();
        },
        showbig: function (message, title, type, callback, callback2) {
            messageBox.show(message, title, type, callback, callback2);
            $('#msgBox').dialog('option', 'height', 600);
            $('#msgBox').dialog('option', 'width', 1000);
        }
    };
    var inputBox = {
        show: function (message, title, callback, callback2) {
            if (title == null) title = 'Technical Problem System';
            $('#inputBoxContent').html(message);
            $('#inputBox').siblings('.ui-dialog-titlebar').show();
            $('#inputBoxOutput').val('');
            $('#inputBox').show().addClass('dialog').dialog({
                modal: true,
                buttons: {
                    Yes: function () {
                        $('#inputBox').addClass('dialog').dialog('close');
                        if (callback != null) {
                            callback($('#inputBoxOutput').val());
                        };
                    },
                    No: function () {
                        $('#inputBox').addClass('dialog').dialog('close');
                        if (callback2 != null) {
                            callback2();
                        }
                    }
                },
                title: title,
                width: 500,
                height: 300,
                resizable: false,

            });
        }
    }
</script>
<div id="msgBox" style="display: none;">
    <%--<div id="msgBoxIcon" class="warning" style="display: inline-block; width: 64px; height: 80px; vertical-align: top;"></div>--%>
    <img id="msgBoxIcon" src="<%=Url.Content("~/Content/Images/PTT_64.png") %>" class="warning" style="display: inline-block; width: 64px; height: 80px; vertical-align: top;" />
    <div style="margin-left: 10px; display: inline-block; width: 80%;">
        <div id="msgBoxContent" style="word-wrap:break-word;"></div>
    </div>
</div>

<div id="inputBox" style="display: none;">
    <%--<div id="msgBoxIcon" class="warning" style="display: inline-block; width: 64px; height: 80px; vertical-align: top;"></div>--%>
    <img id="Img1" src="<%=Url.Content("~/Content/Images/PTT_64.png") %>" class="warning" style="display: inline-block; width: 64px; height: 80px; vertical-align: top;" />
    <div style="margin-left: 10px; display: inline-block; width: 80%;">
        <div id="inputBoxContent" style="word-wrap:break-word;"></div>
        <input type="text" id="inputBoxOutput" style="margin-top:20px;" />
    </div>
</div>