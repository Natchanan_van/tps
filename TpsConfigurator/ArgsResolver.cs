﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Data.SqlClient;
using System.Net.Mail;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;
using TPSWorkflow.DAL;


namespace TpsConfigurator
{
    public enum ResolveMode {
        All = 0,
        Meeting = 1,
        Breakdown = 2
    }
    public class ArgsResolver
    {
        private static DateTime nextMeetingDate(int dayofWeek, int weekofMonth)
        {
            var day = dayofWeek;
            var week = weekofMonth;
            var p = DateTime.Now.AddDays(1);

            while (!(p.DayOfWeek == (DayOfWeek)day && (((p.Day - 1) / 7) + 1) == week))
            {
                p = p.AddDays(1);
            }
            return p;
        }

        private static string getLogFilePath(SqlConnection tpsCon)
        {
            var sql = "SELECT ConfigValue FROM TpsConfig WHERE ConfigKey='LogPath'";
            var sqlCommand = new SqlCommand(sql, tpsCon);
            var res = sqlCommand.ExecuteScalar();
            if (res != null)
                return res.ToString();
            else
                return string.Empty;
        }

        private static void LogMail(MailMessage msg, bool isSuccess, string logPath)
        {
            //var logger = new TPSWorkflow.TextMailLogger(logPath);
            //logger.AddLog(msg.To.Select(c => c.Address).ToList(),
            //        msg.CC.Select(c => c.Address).ToList(),
            //        msg.Subject,
            //        isSuccess);
        }

        public static SchedulerResult ResolveCommandLineArgs(bool isForced, string workingDirectory, ResolveMode resolveMode)
        {
            try
            {
                int breakdownCount = 0;
                Logger l = null;
                if (!isForced)
                {
                    l = Logger.OpenLog();
                }
                var args = Environment.GetCommandLineArgs();
                if (args.Length == 1 || isForced)
                {
                    try
                    {
                        {
                            var webConfigPath = Path.Combine(Path.GetDirectoryName(args[0]), "Web.Config");
                            if (!File.Exists(webConfigPath) && !string.IsNullOrEmpty(workingDirectory))
                            {
                                webConfigPath = Path.Combine(workingDirectory, "Web.Config");
                            }
                            if (File.Exists(webConfigPath))
                            {
                                string MailServer = string.Empty;
                                var MailUser = string.Empty;
                                var mailPassword = string.Empty;
                                var reader = XmlReader.Create(new StreamReader(webConfigPath));
                                reader.ReadToFollowing("connectionStrings");
                                while (!(reader.Name == "add" && reader.GetAttribute("name") == "SAPEntities") || reader.EOF)
                                {
                                    reader.Read();
                                }
                                var pmConnectionStringEf = reader.GetAttribute("connectionString");
                                var searcherPm = "connection string=\"";
                                var pmConnectionString = pmConnectionStringEf.Substring(pmConnectionStringEf.IndexOf(searcherPm) + searcherPm.Length).Trim('\"');
                                while (!(reader.Name == "add" && reader.GetAttribute("name") == "TPSEntities") || reader.EOF)
                                {
                                    reader.Read();
                                }
                                var config = TPSWorkflow.TpsConfig.LoadFromWebConfig(webConfigPath);
                                //var smtp = new SmtpClient
                                //{
                                //    Host = config.MailServer,
                                //    Port = 25,
                                //    EnableSsl = false,
                                //    DeliveryMethod = SmtpDeliveryMethod.Network,
                                //    //UseDefaultCredentials = false,
                                //    Timeout = 180000,
                                //    //Credentials = null
                                //};
#if (GGMAIL)
                                var smtp = new SmtpClient
                                {
                                    Host = config.MailServer,
                                    Port = 587,
                                    EnableSsl = true,
                                    DeliveryMethod = SmtpDeliveryMethod.Network,
                                    UseDefaultCredentials = false,
                                    Timeout = 180000,
                                    Credentials = null
                                };
#else
                            var smtp = new SmtpClient
                            {
                                Host = config.MailServer,
                                Port = 25,
                                EnableSsl = false,
                                DeliveryMethod = SmtpDeliveryMethod.Network,
                                //UseDefaultCredentials = false,
                                Timeout = 180000,
                                //Credentials = null
                            };
#endif
                                if (string.IsNullOrEmpty(config.MailPassword))
                                {
                                    smtp.UseDefaultCredentials = false;
                                    smtp.Credentials = null;
                                }
                                else
                                {
                                    smtp.Credentials = new NetworkCredential(config.MailUser, config.MailPassword);
                                }
                                var tpsConnectionStringEf = reader.GetAttribute("connectionString");
                                var searcher = "connection string=\"";
                                var tpsConnectionString = tpsConnectionStringEf.Substring(tpsConnectionStringEf.IndexOf(searcher) + searcher.Length).Trim('\"');
                                var sqlConn = new SqlConnection(tpsConnectionString);
                                sqlConn.Open();

                                if (resolveMode == ResolveMode.All || resolveMode == ResolveMode.Meeting)
                                {
                                    var command1 = new SqlCommand("EXEC sp_tps_resolvemeeting", sqlConn);
                                    command1.ExecuteNonQuery();
                                    var mailContentCreator = new MailScheduleCreator(config.TpsConnectionString, config.PisConnectionString);
                                    var mailList = mailContentCreator.CreateScheduleMail();
                                    foreach(var mailDetail in mailList)
                                    {
                                        var to = string.Join(";", mailDetail.To);
                                        var subject = mailDetail.Subject;
                                        var content = mailDetail.Body;
                                        var mailMessage = new MailMessage();
                                        mailMessage.From = new MailAddress(config.MailUser, "TPS System");
                                        var toSplit = to.Split(';');

                                        if (!config.IsTestMail)
                                        {
                                            foreach (var mail in toSplit)
                                            {
                                                try
                                                {
                                                    mailMessage.To.Add(new MailAddress(mail.Trim()));
                                                }
                                                catch
                                                {
                                                    
                                                }
                                            }
                                        }
                                        else
                                        {
                                            content += "<br /> TO : " + to;
                                            var testSplit = config.TestMailTo.Split(';');
                                            foreach (var mail in testSplit)
                                            {
                                                try
                                                {
                                                    if (!string.IsNullOrEmpty(mail.Trim()))
                                                    {
                                                        mailMessage.To.Add(new MailAddress(mail));
                                                    }
                                                }
                                                catch
                                                {
                                                    throw;
                                                }
                                            }
                                        }
                                        mailMessage.Subject = subject.ToString();
                                        mailMessage.Body = content.ToString();
                                        mailMessage.IsBodyHtml = true;
                                        AlternateView htmlView = AlternateView.CreateAlternateViewFromString(content.ToString());
                                        htmlView.ContentType = new System.Net.Mime.ContentType("text/html");
                                        mailMessage.AlternateViews.Add(htmlView);
                                        if (mailDetail.CalendarView != null)
                                            mailMessage.AlternateViews.Add(mailDetail.CalendarView);

                                        ServicePointManager.ServerCertificateValidationCallback = delegate(object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
                                        {
                                            return true;
                                        };
                                        var toList = string.Join(",", mailMessage.To.Select(c => c.Address).ToArray());
                                        var ccList = string.Join(",", mailMessage.CC.Select(c => c.Address).ToArray());
                                        var logger = new TPSWorkflow.DAL.MailLogger(tpsConnectionStringEf);
                                        int? groupId = null;
                                        if (mailDetail.GroupId > 0)
                                            groupId = mailDetail.GroupId;
                                        try
                                        {
                                            smtp.Send(mailMessage);
                                            if (mailDetail.GroupId > 0)
                                                mailContentCreator.UpdateCalendarId(mailDetail.GroupId);
                                            logger.LogMail(toList, ccList, mailMessage.Body, mailDetail.MailType, true, "", groupId);
                                        }
                                        catch (Exception ex)
                                        {
                                            logger.LogMail(toList, ccList, mailMessage.Body,mailDetail.MailType, false, ex.Message, groupId);
                                        }



                                        if (!isForced)
                                        {
                                            l.AppendLog(string.Format("Mail Send Successfully to : {0}", to));
                                        }
                                    }
                                }



                                if (resolveMode == ResolveMode.All || resolveMode == ResolveMode.Breakdown)
                                {
                                    var sqlConnPm = new SqlConnection(pmConnectionString);

                                    #region Scraping Data

                                    sqlConnPm.Open();

                                    #region Get All Plants

                                    var plantsQuery = new SqlCommand("SELECT * FROM allowplant", sqlConn);
                                    var plantsReader = plantsQuery.ExecuteReader();
                                    var queryCond = string.Empty;
                                    if (plantsReader.HasRows)
                                    {
                                        var queryCondList = new List<string>();
                                        while (plantsReader.Read())
                                        {
                                            queryCondList.Add(string.Format("TPLNR LIKE '{0}%'", plantsReader["plantsearch"]));
                                        }
                                        queryCond = string.Join(" OR ", queryCondList.ToArray());
                                    }
                                    else
                                    {
                                        queryCond = "1 = 2";
                                    }

                                    #endregion

                                    var queryForScrap = new SqlCommand(string.Format("SELECT * FROM PM_NOTI WHERE ({0}) AND lower(MSAUS) = 'x'", queryCond), sqlConnPm);
                                    var scrapReader = queryForScrap.ExecuteReader();

                                    while (scrapReader.Read())
                                    {
                                        #region Check if already Scrap

                                        var queryForTps = new SqlCommand("SELECT 1 FROM Issues WHERE BillNo = '" + scrapReader["QMNUM"].ToString().Substring(2) + "'", sqlConn);
                                        if (queryForTps.ExecuteScalar() != null)
                                            continue;

                                        #endregion


                                        var bcCulture = new System.Globalization.CultureInfo("en-US");
                                        var thisIssues = new Dictionary<string, string>();
                                        thisIssues["CreatedDate"] = DateTime.Now.ToString(bcCulture);
                                        thisIssues["ModifiedDate"] = DateTime.Now.ToString(bcCulture);
                                        var groupName = string.Empty;
                                        if (string.IsNullOrEmpty(groupName))
                                        {
                                            switch (scrapReader["INGRP"].ToString())
                                            {
                                                case "G01":
                                                    groupName = "บค.วบก.";
                                                    break;
                                                case "G02":
                                                    groupName = "บง.วบก.";
                                                    break;
                                                case "G03":
                                                    groupName = "บฟ.วบก.";
                                                    break;
                                                case "G04":
                                                    groupName = "บง.วบก.";
                                                    break;
                                                case "G05":
                                                    groupName = "ผบ.วบก.";
                                                    break;
                                                case "G08":
                                                    groupName = "วก.วบก.";
                                                    break;
                                                case "G10":
                                                    groupName = "ซญ.วบก.";
                                                    break;
                                                case "G11":
                                                    groupName = "ตร.วบก.";
                                                    break;
                                            }
                                        }
                                        var unitCodeQuery = new SqlCommand(string.Format("SELECT unitcode FROM workgroup WHERE WorkgroupNameTH = '{0}' OR WorkGroupName = '{0}'", groupName), sqlConn);
                                        var unitCodeRes = unitCodeQuery.ExecuteScalar();
                                        string unitCode;
                                        if (unitCodeRes != null)
                                        {
                                            unitCode = unitCodeRes.ToString();
                                        }
                                        else
                                        {
                                            unitCode = string.Empty;
                                        }

                                        thisIssues["BillNo"] = scrapReader["QMNUM"].ToString().Substring(2);
                                        thisIssues["RepairBillNo"] = scrapReader["AUFNR"].ToString().TrimStart('0');
                                        thisIssues["plantcode"] = scrapReader["TPLNR"].ToString();
                                        var plantEntityQuery = new SqlCommand(string.Format("SELECT plantshortcode, plantsearch FROM allowplant WHERE '{0}' LIKE plantsearch + '%'", thisIssues["plantcode"]), sqlConn);
                                        var plantEntity = plantEntityQuery.ExecuteReader();
                                        if (plantEntity.HasRows)
                                        {
                                            var currentPlant = new Dictionary<string, string>();
                                            currentPlant["plantshortcode"] = string.Empty;
                                            currentPlant["plantsearch"] = string.Empty;
                                            while (plantEntity.Read())
                                            {
                                                if (currentPlant["plantsearch"] == string.Empty || currentPlant["plantsearch"].Length < plantEntity["plantsearch"].ToString().Length)
                                                {
                                                    currentPlant["plantshortcode"] = plantEntity["plantshortcode"].ToString();
                                                    currentPlant["plantsearch"] = plantEntity["plantsearch"].ToString();
                                                }
                                            }
                                            thisIssues["plantshortcode"] = currentPlant["plantshortcode"];
                                        }
                                        else
                                        {
                                            continue;
                                        }
                                        //thisIssues["plantshortcode"] = scrapReader["TPLNR"].ToString().Substring(0, 4);

                                        var groupIdQuery = new SqlCommand(string.Format("SELECT GroupId FROM allowplant WHERE plantshortcode = '{0}'", thisIssues["plantshortcode"]), sqlConn);
                                        var groupIdObj = groupIdQuery.ExecuteScalar();
                                        var groupId = groupIdObj == null || groupIdObj.Equals(DBNull.Value) ? -1 : (int)groupIdObj;

                                        int machineNo = 0;
                                        if (int.TryParse(scrapReader["EQUNR"].ToString(), out machineNo))
                                        {
                                            thisIssues["MachineNo"] = machineNo.ToString();
                                            var machineQuery = new SqlCommand(string.Format("SELECT SHTXT FROM PM_EQ WHERE EQUNR LIKE '%{0}'", machineNo), sqlConnPm);
                                            var machineFullNameObj = machineQuery.ExecuteScalar();
                                            thisIssues["MachineFullName"] = machineFullNameObj == null ? string.Empty : machineFullNameObj.ToString();
                                        }
                                        else
                                        {
                                            thisIssues["MachineNo"] = null;
                                            thisIssues["MachineFullName"] = string.Empty;
                                        }

                                        var x = scrapReader["QMDAT"].GetType();
                                        try
                                        {
                                            thisIssues["ProblemDate"] = ((DateTime)scrapReader["QMDAT"]).ToString(bcCulture);
                                        }
                                        catch (Exception)
                                        {
                                            thisIssues["ProblemDate"] = System.Data.SqlTypes.SqlDateTime.MinValue.Value.ToString(bcCulture);
                                        }
                                        thisIssues["ProblemName"] = scrapReader["QMTXT"].ToString();
                                        thisIssues["unitcode"] = unitCode;

                                        var strField = string.Join(",", thisIssues.Select(r => r.Key).ToArray());
                                        if (thisIssues["MachineNo"] == null)
                                            thisIssues["MachineNo"] = "NULL";

                                        #region New Field

                                        var detailQuery = new SqlCommand(string.Format("SELECT * FROM PM_ORDER WHERE QMNUM Like '%{0}%'", scrapReader["QMNUM"]), sqlConnPm);
                                        var detailRes = detailQuery.ExecuteReader();
                                        if (detailRes.Read())
                                        {
                                            thisIssues["ProblemDetail"] = detailRes["KTEXT"].ToString();
                                            var date = (DateTime?)detailRes["GSTRP"];
                                            if (date != null)
                                            {
                                                thisIssues["RepairDate"] = "'" + date.GetValueOrDefault(DateTime.Now).ToString(bcCulture) + "'";
                                            }
                                            else
                                            {
                                                thisIssues["RepairDate"] = "NULL";
                                            }
                                            date = (DateTime?)detailRes["GLTRP"];
                                            if (date != null)
                                            {
                                                thisIssues["RepairDueDate"] = "'" + date.GetValueOrDefault(DateTime.Now).ToString(bcCulture) + "'";
                                            }
                                            else
                                            {
                                                thisIssues["RepairDueDate"] = "NULL";
                                            }

                                        }
                                        else
                                        {
                                            thisIssues["ProblemDetail"] = "NULL";
                                            thisIssues["RepairDate"] = "NULL";
                                            thisIssues["RepairDueDate"] = "NULL";
                                        }


                                        #endregion
                                        string sql = string.Format("INSERT INTO Issues (CreatedDate,ModifiedDate,BillNo,RepairBillNo,plantshortcode,plantcode,MachineNo,MachineFullName,ProblemDate,ProblemName,unitcode, Status, CreatedType, ApproverCode, CancelDetail, ProblemDetail, RepairDate, RepairDueDate) " +
                                            " VALUES ('{0}','{1}','{2}','{3}','{4}','{5}',{6},'{7}','{8}','{9}','{10}', 0, 5, '', '', '{11}', {12}, {13})",
                                            thisIssues["CreatedDate"], thisIssues["ModifiedDate"], thisIssues["BillNo"], thisIssues["RepairBillNo"], thisIssues["plantshortcode"], thisIssues["plantcode"]
                                            , thisIssues["MachineNo"], thisIssues["MachineFullName"], thisIssues["ProblemDate"], thisIssues["ProblemName"], thisIssues["unitcode"],
                                            thisIssues["ProblemDetail"], thisIssues["RepairDate"], thisIssues["RepairDueDate"]);
                                        //var t = sqlConn.BeginTransaction();
                                        try
                                        {
                                            var insertQuery = new SqlCommand(sql, sqlConn);
                                            if (insertQuery.ExecuteNonQuery() > 0)
                                            {
                                                breakdownCount++;
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                        }
                                        //t.Rollback();
                                    }
                                    #endregion

                                    sqlConnPm.Close();
                                }
                                sqlConn.Close();
                            }
                            else
                            {
                                if (!isForced)
                                {
                                    l.AppendLog("Cannot Find Web.Config at : " + webConfigPath);
                                }
                            }
                        }


                    }
                    catch (Exception ex)
                    {
                        if (!isForced)
                        {
                            l.AppendLog("Exception as :" + ex.Message);
                        }
                        else
                        {
                            throw;
                        }

                    }
                    finally
                    {
                        if (!isForced)
                        {
                            l.AppendLog("Execution Complete At :" + DateTime.Now.ToString());
                            l.Close();
                            try
                            {
                                System.Windows.Application.Current.Shutdown(0);
                            }
                            catch
                            {
                            }
                        }


                    }

                }

                return new SchedulerResult()
                {
                    BreakDownCount = breakdownCount
                };
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message + "<br />" + ex.StackTrace);
            }
            
        }

        public static SchedulerResult ResolveCommandLineArgs()
        {
            return ResolveCommandLineArgs(false, string.Empty, ResolveMode.All);
        }
    }
}
