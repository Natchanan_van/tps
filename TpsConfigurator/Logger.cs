﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;

namespace TpsConfigurator
{
    public class Logger
    {
        protected System.IO.StreamWriter writer;
        public static Logger OpenLog()
        {
            return Logger.OpenLog(Path.Combine(Environment.CurrentDirectory, "Log.txt"));
            //return Logger.OpenLog("C:\\Log.txt");
        }
        public static Logger OpenLog(string path)
        {
            var res = new Logger();
            res.writer = new StreamWriter(path, true);
            return res;
        }

        public void AppendLog(string message)
        {
            var mes = string.Format("{0} : {1}", DateTime.Now.ToString(), message);
            writer.WriteLine(mes);
        }

        public void Close()
        {
            writer.Flush();
            writer.Close();

        }
    }
}

