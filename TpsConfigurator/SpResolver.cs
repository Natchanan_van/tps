﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TpsConfigurator.Stored;


namespace TpsConfigurator
{
    public class SpResolver
    {
        public static string ResolveSp(string PisLinkName, string PisDbName, string TpsDbName)
        {
            var sb = new StringBuilder();
            sb.Append(TpsConfigurator.Stored.SpResouces.fn_getmailcontent);
            sb.Append(SpResouces.Sp_tps_mail);
            sb.Append(SpResouces.Sp_ResolveMeeting);
            sb.Append(SpResouces.Sp_everyday);
            var spString = sb.ToString();
            if (!string.IsNullOrEmpty(PisLinkName))
            {
                spString = spString.Replace("[HQ-DB-V07]", string.Format("[{0}]", PisLinkName));
            }
            else
            {
                spString = spString.Replace("[HQ-DB-V07].", string.Empty);
            }
            if (!string.IsNullOrEmpty(PisDbName))
            {
                spString = spString.Replace("PIS.dbo.", string.Format("{0}.dbo.", PisDbName));
            }
            if (!string.IsNullOrEmpty(TpsDbName))
            {
                spString = spString.Replace("[gastpsmeeting_test]", string.Format("[{0}]", TpsDbName));
            }
            return spString;
        }
    }
}
