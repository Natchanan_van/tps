﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TpsConfigurator
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        private string currentConfigPath = null;
        private TPSWorkflow.TpsConfig currentConfig = null;

        public MainWindow()
        {
            InitializeComponent();
            TpsConfigurator.ArgsResolver.ResolveCommandLineArgs();
        }

        private void LoadConfig(string webConfigPath)
        {
            this.currentConfigPath = webConfigPath;
            var c = TPSWorkflow.TpsConfig.LoadFromWebConfig(webConfigPath);

            this.txtTpsDbAddress.Text = c.TpsDatabase.DbAddress;
            this.txtTpsDbName.Text = c.TpsDatabase.DbName;
            this.txtTpsDbUser.Text = c.TpsDatabase.DbUser;
            this.txtTpsDbPass.Password = c.TpsDatabase.DbPassword;

            this.txtPisDbAddress.Text = c.PisDatabase.DbAddress;
            this.txtPisDbName.Text = c.PisDatabase.DbName;
            this.txtPisDbUser.Text = c.PisDatabase.DbUser;
            this.txtPisDbPass.Password = c.PisDatabase.DbPassword;

            this.txtPmDbAddress.Text = c.PmDatabase.DbAddress;
            this.txtPmDbName.Text = c.PmDatabase.DbName;
            this.txtPmDbUser.Text = c.PmDatabase.DbUser;
            this.txtPmDbPassword.Password = c.PmDatabase.DbPassword;

            this.txtStoragePath.Text = c.TpsStoragePath;
            this.txtTempSize.Text = c.TpsTempSize.ToString();
            this.txtMailServer.Text = c.MailServer;
            this.txtMailUser.Text = c.MailUser;
            this.txtMailPassword.Password = c.MailPassword;
            this.txtMailTo.Text = c.TestMailTo;
            this.txtMailCc.Text = c.TestMailCc;
            this.chbIsTest.IsChecked = c.IsTestMail;

            this.currentConfig = c;
        }

        private void Window_Loaded_1(object sender, RoutedEventArgs e)
        {
            var args = Environment.GetCommandLineArgs();
            var webConfigPath = System.IO.Path.Combine(System.IO.Path.GetDirectoryName(args[0]), "Web.Config");
            if (!System.IO.File.Exists(webConfigPath))
            {
                MessageBox.Show("Cannot Load Web.Config File", "Tps Configuration", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                this.Close();
                return;
            }
            this.LoadConfig(webConfigPath);
        }

        private void txtStoragePath_GotFocus(object sender, RoutedEventArgs e)
        {
            var folderDialog = new System.Windows.Forms.FolderBrowserDialog();
            if (folderDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                txtStoragePath.Text = folderDialog.SelectedPath;
            }
            btnSaveConfig.Focus();
        }

        private void btnSaveConfig_Click(object sender, RoutedEventArgs e)
        {
            this.currentConfig.TpsDatabase.DbAddress = this.txtTpsDbAddress.Text;
            this.currentConfig.TpsDatabase.DbName = this.txtTpsDbName.Text;
            this.currentConfig.TpsDatabase.DbUser = this.txtTpsDbUser.Text;
            this.currentConfig.TpsDatabase.DbPassword = this.txtTpsDbPass.Password;

            this.currentConfig.PisDatabase.DbAddress = this.txtPisDbAddress.Text;
            this.currentConfig.PisDatabase.DbName = this.txtPisDbName.Text;
            this.currentConfig.PisDatabase.DbUser = this.txtPisDbUser.Text;
            this.currentConfig.PisDatabase.DbPassword = this.txtPisDbPass.Password;

            this.currentConfig.PmDatabase.DbAddress = this.txtPmDbAddress.Text;
            this.currentConfig.PmDatabase.DbName = this.txtPmDbName.Text;
            this.currentConfig.PmDatabase.DbUser = this.txtPmDbUser.Text;
            this.currentConfig.PmDatabase.DbPassword = this.txtPmDbPassword.Password;

            this.currentConfig.TpsStoragePath = this.txtStoragePath.Text;
            int tempSize = 0;
            int.TryParse(this.txtTempSize.Text, out tempSize);
            this.currentConfig.TpsTempSize = tempSize;
            this.currentConfig.MailServer = this.txtMailServer.Text;
            this.currentConfig.MailUser = this.txtMailUser.Text;
            this.currentConfig.MailPassword = this.txtMailPassword.Password;
            this.currentConfig.TestMailTo = this.txtMailTo.Text;
            this.currentConfig.TestMailCc = this.txtMailCc.Text;
            this.currentConfig.IsTestMail = this.chbIsTest.IsChecked.GetValueOrDefault(false);

            this.currentConfig.WriteToWebConfig(this.currentConfigPath);
            MessageBox.Show("Saved Successfully to : " + this.currentConfigPath);
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btnLoadConfig_Click(object sender, RoutedEventArgs e)
        {
            var openDialog = new System.Windows.Forms.OpenFileDialog();
            if (openDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                this.LoadConfig(openDialog.FileName);
            }
        }

        private void btnSp_Click(object sender, RoutedEventArgs e)
        {
            var spWindow = new SpWindows(txtPisDbName.Text, txtTpsDbName.Text);
            spWindow.ShowDialog();
        }
    }
}
