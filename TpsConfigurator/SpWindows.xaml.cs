﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace TpsConfigurator
{
    /// <summary>
    /// Interaction logic for SpWindows.xaml
    /// </summary>
    public partial class SpWindows : Window
    {
        private string pisDbName = null;
        private string tpsDbName = null;

        public SpWindows(string pisDbName, string tpsDbName)
        {
            InitializeComponent();
            this.pisDbName = pisDbName;
            this.tpsDbName = tpsDbName;
        }

        private void btnGenSp_Click(object sender, RoutedEventArgs e)
        {
            txtSp.Text = SpResolver.ResolveSp(txtPisLink.Text, pisDbName, tpsDbName);
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Window_Loaded_1(object sender, RoutedEventArgs e)
        {
            btnGenSp_Click(sender, e);
        }
    }
}
