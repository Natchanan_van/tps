﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.Xml;
using System.Text.RegularExpressions;
using Ecobz.Utilities;

namespace TPSWorkflow
{
    [Serializable, DataContract]
    public class TpsConfig
    {
        #region Properties
        [DataMember]
        public DbInfo TpsDatabase { get; set; }
        [DataMember]
        public DbInfo PmDatabase { get; set; }
        [DataMember]
        public DbInfo PisDatabase { get; set; }
        [DataMember]
        public string TpsStoragePath { get; set; }
        [DataMember]
        public int TpsTempSize { get; set; }
        [DataMember]
        public string MailServer { get; set; }
        [DataMember]
        public string MailUser { get; set; }
        [DataMember]
        public string MailPassword { get; set; }
        [DataMember]
        public bool IsTestMail { get; set; }
        [DataMember]
        public string TestMailTo { get; set; }
        [DataMember]
        public string TestMailCc { get; set; }
        [DataMember]
        public string TpsConnectionString { get; set; }
        [DataMember]
        public string PisConnectionString { get; set; }
        [DataMember]
        public string PmConnectionString { get; set; }
        #endregion


        #region Methods

        private string getConnectionString(string entityName, string config)
        {
            var dbResolver = new Regex(@"<add name=""" + entityName + @""" connectionString=""(?<chris>[^""]+)"".*\/>");
            var match = dbResolver.Match(config);
            if (match.Success)
            {
                return match.Groups["chris"].Value;
            }
            return string.Empty;
        }
        private string ResolveConString(string entityName, DbInfo dbConfig, string src)
        {
            // {ip} {db} {user} {pass} Replace
            var newConstring = @"metadata=res://*/DAL.{modelname}.csdl|res://*/DAL.{modelname}.ssdl|res://*/DAL.{modelname}.msl;provider=System.Data.SqlClient;provider connection string=&quot;data source={ip};initial catalog={db};persist security info=True;user id={user};password={pass};multipleactiveresultsets=True;application name=EntityFramework&quot;"" providerName=""System.Data.EntityClient";
            var modelName = string.Empty;
            switch (entityName)
            {
                case "PISEntities":
                    modelName = "UsersModel";
                    break;
                case "SAPEntities":
                    modelName = "SAPDataModel";
                    break;
                case "TPSEntities":
                    modelName = "TPSModel";
                    break;
            }
            newConstring = newConstring.Replace("{ip}", dbConfig.DbAddress);
            newConstring = newConstring.Replace("{db}", dbConfig.DbName);
            newConstring = newConstring.Replace("{user}", dbConfig.DbUser);
            newConstring = newConstring.Replace("{pass}", dbConfig.DbPassword);
            newConstring = newConstring.Replace("{modelname}", modelName);
            var dbResolver = new Regex(@"<add name=""" + entityName + @""" connectionString=""(?<chris>.*)""\s* />");
            var match = dbResolver.Match(src);
            var resultString = match.ReplaceByGroupName("chris", src, newConstring);
            return resultString;
        }
        private string ResolvePropertyValue(string PropertyName, string newValue, string src)
        {
            var resolver = new Regex(@"\<setting name=""" + PropertyName + @""" serializeAs=""\w*""\>\s*\<value\>(?<val>.*)\</value\>");
            var match = resolver.Match(src);
            return match.ReplaceByGroupName("val", src, newValue);
            //return string.Empty;
        }


        #region Load Methods
        public static TpsConfig LoadFromFile(string path)
        {
            var xml = new StreamReader(path).ReadToEnd();
            return TpsConfig.LoadFromXml(xml);
        }
        public static TpsConfig LoadFromXml(string xml)
        {
            var ser = new XmlSerializer(typeof(TpsConfig));
            var reader = new StringReader(xml);
            return (TpsConfig)ser.Deserialize(reader);
        }
        public static TpsConfig LoadFromWebConfig(string path)
        {
            var res = new TpsConfig();
            res.TpsDatabase = new DbInfo();
            res.PisDatabase = new DbInfo();
            res.PmDatabase = new DbInfo();

            var allConfig = string.Empty;
            using (var readerStr = new StreamReader(path))
            {
                allConfig = readerStr.ReadToEnd();
                readerStr.Close();
            }
            var tpsConstring = res.getConnectionString("TPSEntities", allConfig);
            res.TpsDatabase.SetFromConnectionString(tpsConstring);
            var pisConstring = res.getConnectionString("PISEntities", allConfig);
            res.PisDatabase.SetFromConnectionString(pisConstring);
            var pmConstring = res.getConnectionString("SAPEntities", allConfig);
            res.PmDatabase.SetFromConnectionString(pmConstring);
            res.TpsConnectionString = System.Net.WebUtility.HtmlDecode(tpsConstring);
            res.PmConnectionString = System.Net.WebUtility.HtmlDecode(pmConstring);
            res.PisConnectionString = System.Net.WebUtility.HtmlDecode(pisConstring);

            try
            {
                using (var strReader = new StreamReader(path))
                {
                    
                    using (var reader = XmlReader.Create(strReader))
                    {
                        var doc = new XmlDocument();
                        doc.Load(reader);
                        var nodeList = doc.SelectNodes("/configuration/applicationSettings/TPSWorkflow.Properties.Settings/setting");
                        foreach (var node in nodeList.Cast<XmlNode>())
                        {
                            switch (node.Attributes["name"].Value)
                            {
                                case "StoragePath":
                                    res.TpsStoragePath = node.ChildNodes[0].InnerXml;
                                    break;
                                case "TempStroageSizeBytes":
                                    res.TpsTempSize = int.Parse(node.ChildNodes[0].InnerXml);
                                    break;
                                case "MailServer":
                                    res.MailServer = node.ChildNodes[0].InnerXml;
                                    break;
                                case "MailUser":
                                    res.MailUser = node.ChildNodes[0].InnerXml;
                                    break;
                                case "MailPassword":
                                    res.MailPassword = node.ChildNodes[0].InnerXml;
                                    break;
                                case "IsTestMail":
                                    res.IsTestMail = node.ChildNodes[0].InnerXml != "n";
                                    break;
                                case "TestMailTo":
                                    res.TestMailTo = node.ChildNodes[0].InnerXml;
                                    break;
                                case "TestMailCc":
                                    res.TestMailCc = node.ChildNodes[0].InnerXml;
                                    break;
                            }
                        }
                    }
                    strReader.Close();
                }
            }
            catch (Exception ex )
            {
                throw;
            }

            return res;
        } 
        #endregion

        #region Save Methods
        public void SaveAs(string path)
        {
            using (var writer = new StreamWriter(path, false))
            {
                var ser = new XmlSerializer(typeof(TpsConfig));
                var textWriter = new StringWriter();
                ser.Serialize(writer, this);
                writer.Flush();
                writer.Close();
            }
        }
        public void WriteToWebConfig(string path)
        {
            var reader = new StreamReader(path);
            var webConfig = reader.ReadToEnd();
            reader.Close();

            webConfig = this.ResolveConString("PISEntities", this.PisDatabase, webConfig);
            webConfig = this.ResolveConString("SAPEntities", this.PmDatabase, webConfig);
            webConfig = this.ResolveConString("TPSEntities", this.TpsDatabase, webConfig);

            webConfig = this.ResolvePropertyValue("StoragePath", this.TpsStoragePath, webConfig);
            webConfig = this.ResolvePropertyValue("TempStroageSizeBytes", this.TpsTempSize.ToString(), webConfig);
            webConfig = this.ResolvePropertyValue("MailServer", this.MailServer, webConfig);
            webConfig = this.ResolvePropertyValue("MailUser", this.MailUser, webConfig);
            webConfig = this.ResolvePropertyValue("MaillPassword", this.MailPassword, webConfig);
            webConfig = this.ResolvePropertyValue("IsTestMail", this.IsTestMail ? "y" : "n", webConfig);
            webConfig = this.ResolvePropertyValue("TestMailTo", this.TestMailTo, webConfig);
            webConfig = this.ResolvePropertyValue("TestMailCc", this.TestMailCc, webConfig);

            var writer = new StreamWriter(path, false);
            writer.Write(webConfig);
            writer.Close();
            //throw new NotImplementedException();
        }  
        #endregion

        #endregion
    }

    [Serializable, DataContract]
    public class DbInfo
    {
        [DataMember]
        public string DbName { get; set; }
        [DataMember]
        public string DbAddress { get; set; }
        [DataMember]
        public string DbUser { get; set; }
        [DataMember]
        public string DbPassword { get; set; }

        public bool SetFromConnectionString(string conString)
        {
            var conStringResolver = new Regex(@"(.*)data source=(?<dbAddr>[^;]*)(.*)initial catalog=(?<dbName>[^;]*)(.*)user id=(?<dbUser>[^;]*)(.*)password=(?<dbPass>[^;]*)(.*)");
            var match = conStringResolver.Match(conString);
            if (match.Success)
            {
                this.DbAddress = match.Groups["dbAddr"].Value;
                this.DbName = match.Groups["dbName"].Value;
                this.DbUser = match.Groups["dbUser"].Value;
                this.DbPassword = match.Groups["dbPass"].Value;
                return true;
            }
            return false;

        }
    }
}
