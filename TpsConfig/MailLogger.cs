﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;

namespace TPSWorkflow
{
    public class TextMailLogger
    {
        private string logFilePath = null;
        public TextMailLogger(string logPath)
        {
            this.logFilePath = logPath;
        }

        public void AddLog(List<string> toList, List<string> ccList, string header, bool isSuccess, DateTime time)
        {
            try
            {
                using (var fs = new StreamWriter(logFilePath, true))
                {
                    var toWrite = new List<string>(){
                    string.Join(";",toList.ToArray()),
                    string.Join(";",toList.ToArray()),
                    header,
                    isSuccess?"Success":"Failed",
                    time.ToString()
                };
                    fs.WriteLine(string.Join(",", toWrite));
                    fs.Close();
                }
            }
            catch { }
        }

        public void AddLog(List<string> toList, List<string> ccList, string header, bool isSuccess)
        {
            this.AddLog(toList, ccList, header, isSuccess, DateTime.Now);
        }
    }
}