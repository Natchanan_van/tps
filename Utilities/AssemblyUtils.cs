﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;

namespace Ecobz.Utilities
{
    public class AssemblyUtils
    {
        public static Guid GetAssemblyGuid(Assembly asm)
        {
            //The following line (part of the original answer) is misleading.
            //**Do not** use it unless you want to return the System.Reflection.Assembly type's GUID.
            Console.WriteLine(asm.GetType().GUID.ToString());


            // The following is the correct code.
            var attribute = (GuidAttribute)asm.GetCustomAttributes(typeof(GuidAttribute), true)[0];
            var id = attribute.Value;
            var guid = Guid.Parse(id);
            return guid;
        }
    }
}
