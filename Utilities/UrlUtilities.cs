﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ecobz.Utilities
{
    public static class UrlUtilities
    {
        public static string ToFullUrl(this Uri uri)
        {
            return string.Format("{0}://{1}{2}{3}",
                                    uri.Scheme,
                                    uri.Host,
                                    uri.Port == 80
                                        ? string.Empty
                                        : ":" + uri.Port, "/");
        }
    }
}
