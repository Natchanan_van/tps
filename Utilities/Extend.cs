﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.Linq.Expressions;
using System.ComponentModel;
using System.Reflection;
using System.Text.RegularExpressions;

namespace Ecobz.Utilities
{
    public static class EnumExtension
    {

        public static IEnumerable<Enum> GetFlags(this Enum value)
        {
            return GetFlags(value, Enum.GetValues(value.GetType()).Cast<Enum>().ToArray());
        }

        public static IEnumerable<Enum> GetIndividualFlags(this Enum value)
        {
            return GetFlags(value, GetFlagValues(value.GetType()).ToArray());
        }

        private static IEnumerable<Enum> GetFlags(Enum value, Enum[] values)
        {
            ulong bits = Convert.ToUInt64(value);
            List<Enum> results = new List<Enum>();
            for (int i = values.Length - 1; i >= 0; i--)
            {
                ulong mask = Convert.ToUInt64(values[i]);
                if (i == 0 && mask == 0L)
                    break;
                if ((bits & mask) == mask)
                {
                    results.Add(values[i]);
                    bits -= mask;
                }
            }
            if (bits != 0L)
                return Enumerable.Empty<Enum>();
            if (Convert.ToUInt64(value) != 0L)
                return results.Reverse<Enum>();
            if (bits == Convert.ToUInt64(value) && values.Length > 0 && Convert.ToUInt64(values[0]) == 0L)
                return values.Take(1);
            return Enumerable.Empty<Enum>();
        }

        private static IEnumerable<Enum> GetFlagValues(Type enumType)
        {
            ulong flag = 0x1;
            foreach (var value in Enum.GetValues(enumType).Cast<Enum>())
            {
                ulong bits = Convert.ToUInt64(value);
                if (bits == 0L)
                    //yield return value;
                    continue; // skip the zero value
                while (flag < bits) flag <<= 1;
                if (flag == bits)
                    yield return value;
            }
        }

        public static String GetDescription(this Enum theEnum)
        {
            //Enumerations.GetEnumDescription;
            FieldInfo fi = theEnum.GetType().GetField(theEnum.ToString());
            DescriptionAttribute[] attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);
            if (attributes.Length > 0)
            {
                return attributes[0].Description;
            }
            else
            {
                return theEnum.ToString();
            }
        }

        public static Enum FromString(this Enum myEnum, string value)
        {
            return (Enum)Enum.Parse(myEnum.GetType(), value);
        }

        public static string ToNullableString(this object obj)
        {
            if (obj == null) return string.Empty;
            else return obj.ToString();
        }

        public static bool ContainsNonCaseSensitive(this string conString, string comparer)
        {
            return conString.ToNullableString().ToLower().Contains(comparer.ToNullableString().ToLower());
        }
        public static bool ContainsNonCaseSensitive(this string conString, object comparer)
        {
            return conString.ToNullableString().ToLower().Contains(comparer.ToNullableString().ToLower());
        }

        public static string ReplaceByGroupName(this System.Text.RegularExpressions.Match match, string groupName, string src, string toReplace)
        {
            
            if (match.Groups[groupName] != null)
            {
                var thisMatch = match.Groups[groupName];
                var res = string.Format("{0}{1}{2}", src.Substring(0, thisMatch.Index), toReplace, src.Substring(thisMatch.Index + thisMatch.Length));
                return res;
            }
            else
            {
                return src;
            }
        }
    }

    public class ClassUtils
    {
        public static Guid GetClassGuid(Type classType)
        {
            var guidAttribute = (GuidAttribute)System.Attribute.GetCustomAttribute(classType, typeof(GuidAttribute));
            return new Guid(guidAttribute.Value);
        }
    }

    public class FileUtils
    {
        private static readonly Dictionary<string, string> MIMETypesDictionary = new Dictionary<string, string>
        {
            {"ai", "application/postscript"},
            {"aif", "audio/x-aiff"},
            {"aifc", "audio/x-aiff"},
            {"aiff", "audio/x-aiff"},
            {"asc", "text/plain"},
            {"atom", "application/atom+xml"},
            {"au", "audio/basic"},
            {"avi", "video/x-msvideo"},
            {"bcpio", "application/x-bcpio"},
            {"bin", "application/octet-stream"},
            {"bmp", "image/bmp"},
            {"cdf", "application/x-netcdf"},
            {"cgm", "image/cgm"},
            {"class", "application/octet-stream"},
            {"cpio", "application/x-cpio"},
            {"cpt", "application/mac-compactpro"},
            {"csh", "application/x-csh"},
            {"css", "text/css"},
            {"dcr", "application/x-director"},
            {"dif", "video/x-dv"},
            {"dir", "application/x-director"},
            {"djv", "image/vnd.djvu"},
            {"djvu", "image/vnd.djvu"},
            {"dll", "application/octet-stream"},
            {"dmg", "application/octet-stream"},
            {"dms", "application/octet-stream"},
            {"doc", "application/msword"},
            {"docx","application/vnd.openxmlformats-officedocument.wordprocessingml.document"},
            {"dotx", "application/vnd.openxmlformats-officedocument.wordprocessingml.template"},
            {"docm","application/vnd.ms-word.document.macroEnabled.12"},
            {"dotm","application/vnd.ms-word.template.macroEnabled.12"},
            {"dtd", "application/xml-dtd"},
            {"dv", "video/x-dv"},
            {"dvi", "application/x-dvi"},
            {"dxr", "application/x-director"},
            {"eps", "application/postscript"},
            {"etx", "text/x-setext"},
            {"exe", "application/octet-stream"},
            {"ez", "application/andrew-inset"},
            {"gif", "image/gif"},
            {"gram", "application/srgs"},
            {"grxml", "application/srgs+xml"},
            {"gtar", "application/x-gtar"},
            {"hdf", "application/x-hdf"},
            {"hqx", "application/mac-binhex40"},
            {"htm", "text/html"},
            {"html", "text/html"},
            {"ice", "x-conference/x-cooltalk"},
            {"ico", "image/x-icon"},
            {"ics", "text/calendar"},
            {"ief", "image/ief"},
            {"ifb", "text/calendar"},
            {"iges", "model/iges"},
            {"igs", "model/iges"},
            {"jnlp", "application/x-java-jnlp-file"},
            {"jp2", "image/jp2"},
            {"jpe", "image/jpeg"},
            {"jpeg", "image/jpeg"},
            {"jpg", "image/jpeg"},
            {"js", "application/x-javascript"},
            {"kar", "audio/midi"},
            {"latex", "application/x-latex"},
            {"lha", "application/octet-stream"},
            {"lzh", "application/octet-stream"},
            {"m3u", "audio/x-mpegurl"},
            {"m4a", "audio/mp4a-latm"},
            {"m4b", "audio/mp4a-latm"},
            {"m4p", "audio/mp4a-latm"},
            {"m4u", "video/vnd.mpegurl"},
            {"m4v", "video/x-m4v"},
            {"mac", "image/x-macpaint"},
            {"man", "application/x-troff-man"},
            {"mathml", "application/mathml+xml"},
            {"me", "application/x-troff-me"},
            {"mesh", "model/mesh"},
            {"mid", "audio/midi"},
            {"midi", "audio/midi"},
            {"mif", "application/vnd.mif"},
            {"mov", "video/quicktime"},
            {"movie", "video/x-sgi-movie"},
            {"mp2", "audio/mpeg"},
            {"mp3", "audio/mpeg"},
            {"mp4", "video/mp4"},
            {"mpe", "video/mpeg"},
            {"mpeg", "video/mpeg"},
            {"mpg", "video/mpeg"},
            {"mpga", "audio/mpeg"},
            {"ms", "application/x-troff-ms"},
            {"msh", "model/mesh"},
            {"mxu", "video/vnd.mpegurl"},
            {"nc", "application/x-netcdf"},
            {"oda", "application/oda"},
            {"ogg", "application/ogg"},
            {"pbm", "image/x-portable-bitmap"},
            {"pct", "image/pict"},
            {"pdb", "chemical/x-pdb"},
            {"pdf", "application/pdf"},
            {"pgm", "image/x-portable-graymap"},
            {"pgn", "application/x-chess-pgn"},
            {"pic", "image/pict"},
            {"pict", "image/pict"},
            {"png", "image/png"}, 
            {"pnm", "image/x-portable-anymap"},
            {"pnt", "image/x-macpaint"},
            {"pntg", "image/x-macpaint"},
            {"ppm", "image/x-portable-pixmap"},
            {"ppt", "application/vnd.ms-powerpoint"},
            {"pptx","application/vnd.openxmlformats-officedocument.presentationml.presentation"},
            {"potx","application/vnd.openxmlformats-officedocument.presentationml.template"},
            {"ppsx","application/vnd.openxmlformats-officedocument.presentationml.slideshow"},
            {"ppam","application/vnd.ms-powerpoint.addin.macroEnabled.12"},
            {"pptm","application/vnd.ms-powerpoint.presentation.macroEnabled.12"},
            {"potm","application/vnd.ms-powerpoint.template.macroEnabled.12"},
            {"ppsm","application/vnd.ms-powerpoint.slideshow.macroEnabled.12"},
            {"ps", "application/postscript"},
            {"qt", "video/quicktime"},
            {"qti", "image/x-quicktime"},
            {"qtif", "image/x-quicktime"},
            {"ra", "audio/x-pn-realaudio"},
            {"ram", "audio/x-pn-realaudio"},
            {"ras", "image/x-cmu-raster"},
            {"rdf", "application/rdf+xml"},
            {"rgb", "image/x-rgb"},
            {"rm", "application/vnd.rn-realmedia"},
            {"roff", "application/x-troff"},
            {"rtf", "text/rtf"},
            {"rtx", "text/richtext"},
            {"sgm", "text/sgml"},
            {"sgml", "text/sgml"},
            {"sh", "application/x-sh"},
            {"shar", "application/x-shar"},
            {"silo", "model/mesh"},
            {"sit", "application/x-stuffit"},
            {"skd", "application/x-koan"},
            {"skm", "application/x-koan"},
            {"skp", "application/x-koan"},
            {"skt", "application/x-koan"},
            {"smi", "application/smil"},
            {"smil", "application/smil"},
            {"snd", "audio/basic"},
            {"so", "application/octet-stream"},
            {"spl", "application/x-futuresplash"},
            {"src", "application/x-wais-source"},
            {"sv4cpio", "application/x-sv4cpio"},
            {"sv4crc", "application/x-sv4crc"},
            {"svg", "image/svg+xml"},
            {"swf", "application/x-shockwave-flash"},
            {"t", "application/x-troff"},
            {"tar", "application/x-tar"},
            {"tcl", "application/x-tcl"},
            {"tex", "application/x-tex"},
            {"texi", "application/x-texinfo"},
            {"texinfo", "application/x-texinfo"},
            {"tif", "image/tiff"},
            {"tiff", "image/tiff"},
            {"tr", "application/x-troff"},
            {"tsv", "text/tab-separated-values"},
            {"txt", "text/plain"},
            {"ustar", "application/x-ustar"},
            {"vcd", "application/x-cdlink"},
            {"vrml", "model/vrml"},
            {"vxml", "application/voicexml+xml"},
            {"wav", "audio/x-wav"},
            {"wbmp", "image/vnd.wap.wbmp"},
            {"wbmxl", "application/vnd.wap.wbxml"},
            {"wml", "text/vnd.wap.wml"},
            {"wmlc", "application/vnd.wap.wmlc"},
            {"wmls", "text/vnd.wap.wmlscript"},
            {"wmlsc", "application/vnd.wap.wmlscriptc"},
            {"wrl", "model/vrml"},
            {"xbm", "image/x-xbitmap"},
            {"xht", "application/xhtml+xml"},
            {"xhtml", "application/xhtml+xml"},
            {"xls", "application/vnd.ms-excel"},                        
            {"xml", "application/xml"},
            {"xpm", "image/x-xpixmap"},
            {"xsl", "application/xml"},
            {"xlsx","application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"},
            {"xltx","application/vnd.openxmlformats-officedocument.spreadsheetml.template"},
            {"xlsm","application/vnd.ms-excel.sheet.macroEnabled.12"},
            {"xltm","application/vnd.ms-excel.template.macroEnabled.12"},
            {"xlam","application/vnd.ms-excel.addin.macroEnabled.12"},
            {"xlsb","application/vnd.ms-excel.sheet.binary.macroEnabled.12"},
            {"xslt", "application/xslt+xml"},
            {"xul", "application/vnd.mozilla.xul+xml"},
            {"xwd", "image/x-xwindowdump"},
            {"xyz", "chemical/x-xyz"},
            {"zip", "application/zip"}
        };

        public static string GetMIMEType(string fileName)
        {
            if (MIMETypesDictionary.ContainsKey(Path.GetExtension(fileName).Remove(0, 1)))
            {
                return MIMETypesDictionary[Path.GetExtension(fileName).Remove(0, 1)];
            }
            return "unknown/unknown";
        }

        public static string GetFileExtension(string path)
        {
            var folder = Path.GetDirectoryName(path);
            var files = Directory.EnumerateFiles(folder);
            return files.Where(c => Path.GetFileName(path) == Path.GetFileNameWithoutExtension(c)).Select(d => Path.GetExtension(d)).FirstOrDefault();
        }
    }

    public class ReflectionUtils
    {
        public static void CopyBasicProperty(object src, object dest, params string[] exclude)
        {
            var srcType = src.GetType();
            var entityType = dest.GetType();
            var destMembers = entityType.GetProperties();
            var srcMembers = srcType.GetProperties();
            foreach (var classProperty in srcMembers.Where(c => !exclude.Any(d => d.Equals(c.Name, StringComparison.CurrentCultureIgnoreCase))))
            {
                if (!classProperty.GetGetMethod().IsVirtual)
                {
                    var matchedMember = destMembers.Where(c => c.Name == classProperty.Name).FirstOrDefault();
                    if (matchedMember != null)
                    {
                        classProperty.SetValue(dest, matchedMember.GetValue(src, null), null);
                    }
                }
            }
        }

        /// <summary>
        /// Check memberwise public-accessor field equality
        /// </summary>
        /// <param name="obj1"></param>
        /// <param name="obj2"></param>
        /// <returns></returns>
        public static bool IsMemberWiseEqual(object obj1, object obj2)
        {
            if (obj1 == null || obj2 == null)
                return obj1 == obj2;
            var type = obj1.GetType();
            if (!object.ReferenceEquals(type, obj2.GetType()))
                return false;
            var props = type.GetProperties();
            return !(props.Select(c => (c.GetValue(obj1, null)).Equals(c.GetValue(obj2, null))).Contains(false));
        }

        public static T1 NameValueCollectionToObject<T1>(System.Collections.Specialized.NameValueCollection nameValueCollection)
        {
            var t = typeof(T1);
            var res = Activator.CreateInstance(t);
            foreach (var key in nameValueCollection.AllKeys)
            {
                var propertyInfo = t.GetProperty(key);
                if (propertyInfo != null)
                {
                    propertyInfo.SetValue(res, nameValueCollection[key], null);
                }
            }
            return (T1)res;

        }

       
    }

    public static class IEnumerableExtension
    {
        public static IOrderedEnumerable<TSource> OrderedByMultiField<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector, bool isDescending = false)
        {
            if (!isDescending)
            {
                if (source is IOrderedEnumerable<TSource>)
                {
                    var ord = (IOrderedEnumerable<TSource>)source;
                    return ord.ThenBy(keySelector);
                }
                else
                {
                    var ord = source.OrderBy(keySelector);
                    return ord;
                }
            }
            else
            {
                if (source is IOrderedEnumerable<TSource>)
                {
                    var ord = (IOrderedEnumerable<TSource>)source;
                    return ord.ThenByDescending(keySelector);
                }
                else
                {
                    return source.OrderByDescending(keySelector);
                }
            }
        }

        public static IOrderedQueryable<TSource> OrderedByMultiField<TSource, TKey>(this IQueryable<TSource> source, Expression<Func<TSource, TKey>> keySelector, bool isDescending = false)
        {
            if (!isDescending)
            {
                if (source is IOrderedQueryable<TSource>)
                {
                    try
                    {
                        var ord = (IOrderedQueryable<TSource>)source;
                        return (IOrderedQueryable<TSource>)ord.ThenBy(keySelector);
                    }
                    catch (Exception)
                    {
                        var ord = source.OrderBy(keySelector);
                        return (IOrderedQueryable<TSource>)ord;
                    }
                }
                else
                {
                    var ord = source.OrderBy(keySelector);
                    return (IOrderedQueryable<TSource>)ord;
                }
            }
            else
            {
                if (source is IOrderedQueryable<TSource>)
                {
                    try
                    {
                        var ord = (IOrderedQueryable<TSource>)source;
                        return (IOrderedQueryable<TSource>)ord.ThenByDescending(keySelector);
                    }
                    catch
                    {
                        return (IOrderedQueryable<TSource>)source.OrderByDescending(keySelector);
                    }
                }
                else
                {
                    return (IOrderedQueryable<TSource>)source.OrderByDescending(keySelector);
                }
            }
        }

        /// <summary>
        /// Take only some of query result
        /// </summary>
        /// <param name="start">Starting Index (0 or Less means from starts)</param>
        /// <param name="count">Number of column take (Less than 0 for all)</param>
        /// <returns></returns>
        public static IQueryable<TSource> TakeSome<TSource>(this IQueryable<TSource> source, int start, int count)
        {
            IQueryable<TSource> queryResult;
            queryResult = source;
            if (start < 0)
                queryResult = queryResult.Skip(start);
            if (count >= 0)
                queryResult = queryResult.Take(count);
            return queryResult;
        }

        public static IOrderedQueryable<TSource> OrderByExt<TSource, TKey>(this IQueryable<TSource> source, Expression<Func<TSource, TKey>> keySelector, bool isDescending = false)
        {
            if (isDescending)
            {
                return source.OrderByDescending(keySelector);
            }
            else
            {
                return source.OrderBy(keySelector);
            }
        }
    }

    public static class StringExtension
    {
        public static string EmptyIfNull(this string src)
        {
            if (string.IsNullOrEmpty(src))
                return string.Empty;
            else
                return src;
        }
    }
}
