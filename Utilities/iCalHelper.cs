﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Mail;
using System.Globalization;

namespace Ecobz.Utilities
{
    public class iCalHelper
    {
        public static AlternateView CreateICalContent(DateTime dateTimeEnd, DateTime dateTimeStart, string location, string title, string description, string uuid)
        {
            
            var dateFormatIcal = "yyyyMMddTHHmmssZ";
            var now = DateTime.Now.ToString(dateFormatIcal, new CultureInfo("en-US"));
            var endString = dateTimeEnd.ToUniversalTime().ToString(dateFormatIcal, new CultureInfo("en-US"));
            var startString = dateTimeStart.ToUniversalTime().ToString(dateFormatIcal, new CultureInfo("en-US"));
            var iCal = string.Format(
@"BEGIN:VCALENDAR
PRODID:-//Microsoft Corporation//Outlook 14.0 MIMEDIR//EN
VERSION:2.0
METHOD:REQUEST
CATEGORIES:APPOINTMENT
X-MS-OLK-FORCEINSPECTOROPEN:TRUE
BEGIN:VEVENT
CLASS:PUBLIC
CREATED:{0}
DTEND:{1}
DTSTAMP:{0}
DTSTART:{2}
LAST-MODIFIED:{0}
LOCATION:{3}
PRIORITY:5
SEQUENCE:0
SUMMARY;LANGUAGE=en-us:{4}
TRANSP:OPAQUE
UID:{6}
X-MICROSOFT-CDO-BUSYSTATUS:BUSY
X-MICROSOFT-CDO-IMPORTANCE:1
X-MICROSOFT-DISALLOW-COUNTER:FALSE
X-MS-OLK-AUTOFILLLOCATION:FALSE
X-MS-OLK-CONFTYPE:0
BEGIN:VALARM
TRIGGER:-PT60M
ACTION:DISPLAY
DESCRIPTION:Reminder
END:VALARM
END:VEVENT
END:VCALENDAR"
                        , now, endString, startString, location, title, description, uuid);
//            iCal =
//@"BEGIN:VCALENDAR
//PRODID:-//Microsoft Corporation//Outlook 14.0 MIMEDIR//EN
//VERSION:2.0
//METHOD:PUBLISH
//X-MS-OLK-FORCEINSPECTOROPEN:TRUE
//BEGIN:VEVENT
//CLASS:PUBLIC
//CREATED:20141015T212806Z
//DESCRIPTION:desc
//DTEND:20140928T024000Z
//DTSTAMP:20141015T212806Z
//DTSTART:20140928T010000Z
//LAST-MODIFIED:20141015T212806Z
//LOCATION:test noti
//PRIORITY:5
//SEQUENCE:0
//SUMMARY;LANGUAGE=en-us:test noti
//TRANSP:OPAQUE
//UID:D8BFD357-88A7-455C-86BC-C2CECA9AC5C6
//X-MICROSOFT-CDO-BUSYSTATUS:BUSY
//X-MICROSOFT-CDO-IMPORTANCE:1
//X-MICROSOFT-DISALLOW-COUNTER:FALSE
//X-MS-OLK-AUTOFILLLOCATION:FALSE
//X-MS-OLK-CONFTYPE:0
//BEGIN:VALARM
//TRIGGER:-PT60M
//ACTION:DISPLAY
//DESCRIPTION:Reminder
//END:VALARM
//END:VEVENT
//END:VCALENDAR";
            var calType=new System.Net.Mime.ContentType("text/calendar");
            calType.Parameters.Add("METHOD", "REQUEST");
            var iCalView = AlternateView.CreateAlternateViewFromString(iCal, calType);
            
            return iCalView;
        }

        public static AlternateView CreateICalContent(DateTime dateTimeEnd, DateTime dateTimeStart, string location, string title, string description)
        {
            return CreateICalContent(dateTimeEnd, dateTimeStart, location, title, description, Guid.NewGuid().ToString());
        }
    }
}
