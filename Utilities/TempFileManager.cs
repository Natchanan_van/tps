﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Ecobz.Utilities
{
    public class TempFileManager
    {
        private string tempPath = string.Empty;

        public TempFileManager(string appName)
        {
            this.tempPath = Path.Combine(Path.GetTempPath(), appName);
            if (!Directory.Exists(tempPath))
                Directory.CreateDirectory(tempPath);
        }

        public string GetTempPathForFileName(string fileName)
        {
            return Path.Combine(this.tempPath, fileName);
        }

        /// <summary>
        /// Clear Temporary File which older than span
        /// </summary>
        /// <param name="span">Time to Set</param>
        public void ClearTemp(TimeSpan span)
        {
            foreach (var filePath in Directory.EnumerateFiles(this.tempPath))
            {
                try
                {
                    var fInfo = new FileInfo(filePath);
                    if ((DateTime.Now - fInfo.CreationTime) > span)
                        fInfo.Delete();
                }
                catch (Exception) { }
            }
        }

        public void ClearTemp()
        {
            this.ClearTemp(new TimeSpan(1, 0, 0, 0));
        }
    }
}
